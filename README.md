# Among Z

![](img/title.png)

## Membres

>   [**Matthieu JACQUEMET p1810569**<br>](https://forge.univ-lyon1.fr/p1810569)
>   [**Nicolas LERAY p1809852**<br>](https://forge.univ-lyon1.fr/p1809852)
>   [**Riyad ENNAOURA p1702710**<br>](https://forge.univ-lyon1.fr/p1702710)


## Description

Among Z est un jeu de survie dans lequel vous devrez faire face à des hordes de
zombies. Arriverez vous à restez envie le plus longtemps ?

## Prérequis

-   g++ avec standard C++ 11 minimum
-   Carte graphique compatible OpenGL 4.3 minimum
-   libglvnd
-   libglvnd-dev (pour les distribution basés sur Debian)
-   cmake


### Platformes

-    **Linux** : fonctionnel (Ubuntu >= 20.04 lts)
-    **Windows** : non testé


## Compilation

    mkdir build
    cd build
    cmake .. [-DCMAKE_BUILD_TYPE=<config>]
    cmake --build . [--target <target>] [-- -j <proc>]

 - config : configuration (Debug | Release | MinSizeRel | RelWithDebiInfo)
 - target : cible a compiler (all | amongz | asset_formater | gdk | dist)
 - proc   : nombre d'unitée de compilation en parallèle (utiliser $(nproc))

### Notes

Le jeu doit être lancé depuis le même répertoire où se trouve le dossier `data`,
dans ce cas:

    build/amongz


Si à l'éxecution certaines librairies ne sont pas trouvés, executez ces 
commande dans le terminal où le jeu doit être lancé:

    export LD_LIBRARY_PATH="extern/linux64/misc/lib:extern/linux64/bullet/lib:$LD_LIBRARY_PATH"

## Cibles

### amonz

Executable principale pour lancer le jeu

### gdk (Game Development Kit)

Code générique pouvant être réutiliser d'un jeu à l'autre.
Utiliser -DSHARED_GDK=YES pour compiler gdk en bibliotéque partagée

### asset_formater

Outile servant à convertir le assets exportés de logitiels d'édition de model 3D
(i.e. Blender) pour les serialiser en donnés utilisés par les classes du jeu.

### dist

Genere la distribution finale sous forme d'archive compréssé (tar.gz).
A utiliser avec l'option -DCMAKE_BUILD_TYPE=Release


## Contrôles par défaut

| Action            | AZERTY        | QWERTY        | Contrôleur de jeu |
| -------           |:------------: | :-----------: | :---------------: |
| avancer           | Z             | W             | ![](artifacts/img/l_stick.svg) |
| reculer           | S             | S             | ![](artifacts/img/l_stick.svg) |
| gauche            | Q             | A             | ![](artifacts/img/l_stick.svg) |
| droite            | D             | D             | ![](artifacts/img/l_stick.svg) |
| sauter            | espace        | espace        | ![](artifacts/img/a.svg)    |
| courire           | shift         | shift         | ![](artifacts/img/l.svg)    |
| basculer arme<br> primaire/secondaire | molette souris | molette souris | ![](artifacts/img/y.svg) |
| s'accroupir       | ctrl          | ctrl          | ![](artifacts/img/b.svg)    |
| ramasser item/intéragire | E      | E             | ![](artifacts/img/x.svg) (maintenir) |
| viser             | clic droit    | clic droit    | ![](artifacts/img/lt.svg)   |
| tirer             | clic gauche   | clic gauche   | ![](artifacts/img/rt.svg)   |
| recharger arme    | R             | R             | ![](artifacts/img/x.svg)    |
| arme primaire     | 1             | 1             | |                 |
| arme secondaire   | 2             | 2             | |                 |


## Hiérarchie des fichers

```
amongz
|   CMakeLists.txt              script de compilation pour cmake
|   README.md
|   COPYING
|
|___utils/                      scripts utilitaires
|
|___src/                        code source
|   |___amongz                  code du jeu principale
|   |___gdk                     code générique pouvant être réutilisé dans d'autres jeux
|   |___tools                   code pour les outils
|
|___extern                      librairies externes
|___doc                         documentation
|___data                        données du jeu
|___assets                      assets sources
|___artifacts                   documents du projet
```