// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/displayRegionCullCallbackData.h>
#include <iterator>

#include "config_material.h"
#include "renderPipeline.h"
#include "render_utils.h"
#include "utils.h"

#include "opaqueStage.h"


using namespace std;



DEFINE_TYPEHANDLE(OpaqueStage)
DEFINE_TYPEHANDLE(OpaqueStage::RenderPass)


OpaqueStage::OpaqueStage(RenderPipeline* pipeline): 
    RenderStage("opaque", pipeline),
    _near_far(1, 0.0f),
    _view_angle(1, 0.0f),
    _resolution(1, 0)
{
    if (pipeline != nullptr)
        init();
}


void OpaqueStage::draw(DisplayRegionDrawCallbackData* data) {


    Thread* current_thread = Thread::get_current_thread();
    // nout << "draw thread" << current_thread << endl;

    SceneSetup* setup = data->get_scene_setup();
    DisplayRegion* dr = setup->get_display_region();
    GraphicsOutput* buffer = dr->get_window();

    // this display region is no longer in the gbuffer, unhook it
    if (buffer != _gbuffer) {
        dr->clear_draw_callback();
        data->upcall();
        return;
    }
    
    GraphicsStateGuardian* gsg = buffer->get_gsg();
    CullResult* result = dr->get_cull_result(current_thread);
    CullBinManager* bin_manager = CullBinManager::get_global_ptr();
    bool force = !gsg->get_effective_incomplete_render();


    int num_bins = bin_manager->get_num_bins();

    gsg->clear_state_and_transform();


    if (is_main_region(dr))
        scene_data->update_data(setup);


    if (!gsg->set_scene(setup)) {
        display_cat.error()
            << gsg->get_type() << " cannot render scene.\n";

    } else if (gsg->begin_scene()) {

        for (int i = 0; i < num_bins; ++i) {
            int bin_index = bin_manager->get_bin(i);
            CullBin* bin = result->get_bin(bin_index);
            
            if (culled_bins->add_bin_to_groups(setup, bin) == 0) {

                gsg->push_group_marker(bin->get_name());
                bin->draw(force, current_thread);
                gsg->pop_group_marker();
            }
        }
        gsg->end_scene();
    }
}



void OpaqueStage::render(RenderCallbackData* data) {
    
    if (data->get_callback_type() == CallbackGraphicsWindow::RCT_begin_frame) {

        data->set_render_flag(false);
        culled_bins->clear();
    }
    else
        data->upcall();
}


void OpaqueStage::cull(DisplayRegionCullCallbackData* data) {

    SceneSetup* setup = data->get_scene_setup();
    const RenderState* state = setup->get_initial_state();

    if (state != nullptr)
        setup->set_initial_state(state->compose(_state));
    else
        setup->set_initial_state(_state);

    data->upcall();
}


void OpaqueStage::setup() {

    hook_display_regions();
}


void OpaqueStage::update() {

    hook_display_regions();
}


void OpaqueStage::init() {

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(true);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(8,8,8,8);
    fb_prop.set_depth_bits(24);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);
    fb_prop.set_aux_hrgba(2);
    fb_prop.set_aux_rgba(2);


    add_callback(-1); // for hooking new display region

    _gbuffer = add_buffer(fb_prop, "gbuffer");
    _pipeline->set_buffer(_gbuffer);

    LVector2i size = _gbuffer->get_size();
    _resolution[0] = size;


    culled_bins = new CulledBins;
    scene_data = new SceneData;


    _state.set_shader(Shader::load(SHADER_LANG,
                        GDK_SHADER_PATH VERT_SHADER("basic"),
                        GDK_SHADER_PATH FRAG_SHADER("opaque")));

    _state.set_shader_input("u_resolution", _resolution);
    _state.set_shader_input("u_near_far", _near_far);
    _state.set_shader_input("u_view_angle", _view_angle);


    setup_target(depth_and_stencil, _gbuffer, DrawableRegion::RTP_depth_stencil);
    setup_target(diffuse_and_matID, _gbuffer, DrawableRegion::RTP_color);
    setup_target(specular_and_roughness, _gbuffer, DrawableRegion::RTP_aux_rgba_0);
    setup_target(normal, _gbuffer, DrawableRegion::RTP_aux_rgba_1);

    setup_target(accumulator);
    setup_target(subsurface);
    setup_target(emissive);
    setup_target(velocity);


    accumulator->setup_2d_texture(size.get_x(), size.get_y(), 
                        Texture::T_float, Texture::F_rgb16);

    _gbuffer->set_clear_color_active(false);
    _gbuffer->set_clear_depth_active(true);
    _gbuffer->set_clear_stencil_active(true);

    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_rgba_0, false);
    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_rgba_1, false);

    setup_passes();
    hook_display_regions();
}


void OpaqueStage::dimensions_changed(LVector2i size) {
    
    RenderStage::dimensions_changed(size);

    hook_display_regions();
    _resolution[0] = size;
}


TypedWritable* OpaqueStage::make_from_bam(const FactoryParams& params) {

    OpaqueStage* me = new OpaqueStage;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


bool OpaqueStage::is_main_region(DisplayRegion* region) const {

    nassertr(_gbuffer->get_num_active_display_regions() > 0, false)
    return _gbuffer->get_active_display_region(0) == region;
}


void OpaqueStage::hook_display_regions() {

    int num_region = _gbuffer->get_num_active_display_regions();

    for (int i=0; i<num_region; ++i) {
        DisplayRegion* region = _gbuffer->get_active_display_region(i);

        if (region->get_cull_callback() != _cull_callback)
            region->set_cull_callback(_cull_callback);

        if (region->get_draw_callback() != _draw_callback)
            region->set_draw_callback(_draw_callback);        
    }
}


void OpaqueStage::setup_passes() {

    static const RenderPass::Component components[] = {
        {"dynamic", velocity, DrawableRegion::RTP_aux_hrgba_0},
        {"emissive", emissive, DrawableRegion::RTP_aux_hrgba_1},
        {"subsurface", subsurface, DrawableRegion::RTP_aux_rgba_2},
    };
    static const int num_passes = 3;


    for (int bits = 1; bits < (0x1 << num_passes); ++bits) {
        RenderPass* pass = new RenderPass(this);

        for (int i=0; i<num_passes; ++i) {
            if ((bits >> i) & 0x1)
                pass->add_component(components[i]);
        }
        pass->finalize();
        _passes.push_back(pass);
    }
}


void OpaqueStage::setup_target(OutputPort<Texture>& port) {

    port = new Texture(port.get_name());

    port->set_wrap_u(SamplerState::WM_clamp);
    port->set_wrap_v(SamplerState::WM_clamp);
    port->set_minfilter(SamplerState::FT_nearest);
    port->set_magfilter(SamplerState::FT_nearest);
    port->set_keep_ram_image(false);
}



void OpaqueStage::setup_target(OutputPort<Texture>& port, 
            GraphicsOutput* buffer, RenderTexturePlane plane)
{
    setup_target(port);

    string name = shader_input_id_prefix.get_value() + port.get_name();
    _state.set_shader_input(name, port);

    buffer->add_render_texture(port, GraphicsOutput::RTM_bind_or_copy, plane);
}




OpaqueStage::RenderPass::RenderPass(OpaqueStage* stage) {

    FrameBufferProperties props = stage->_gbuffer->get_fb_properties();

    _buffer = stage->add_buffer(props, "gbuffer");
    _buffer->disable_clears();
    
    DisplayRegion* dr = _buffer->make_mono_display_region();
    dr->set_draw_callback(this);

    string group_name = "pass#" + stage->_passes.size();
    _group = stage->culled_bins->add_group(group_name);

    attach_default_targets(stage->_gbuffer);
}



void OpaqueStage::RenderPass::add_component(const Component& component) {
    
    _buffer->add_render_texture(component._texture, 
        GraphicsOutput::RTM_bind_or_copy, component._plane);

    string& name = const_cast<string&>(_buffer->get_name());
    name += "-" + component._name;
}


void OpaqueStage::RenderPass::finalize() {

    string name = _buffer->get_name();
    _group->add_bin_name(name);

    CullBinManager* mgr = CullBinManager::get_global_ptr();
    mgr->add_bin(name, CullBin::BT_state_sorted, 0);
}


void OpaqueStage::RenderPass::do_callback(CallbackData*) {

    _group->draw();
}


void OpaqueStage::RenderPass::attach_default_targets(GraphicsOutput* buffer) {
    
    static const auto mode = GraphicsOutput::RTM_bind_or_copy;
    int num_targets = buffer->count_textures();

    for (int i=0; i<num_targets; ++i) {

        Texture* texture = buffer->get_texture(i);
        RenderTexturePlane plane = buffer->get_texture_plane(i);

        _buffer->add_render_texture(texture, mode, plane);
    }
}
















// bool OpaqueStage::is_first_region(DisplayRegion* region) const {

//     int num_region = _gbuffer->get_num_active_display_regions();
//     nassertr(num_region > 0, false)

//     DisplayRegion* dr = _gbuffer->get_active_display_region(0);
//     int min_sort = dr->get_sort();

//     for (int i=1; i<num_region; ++i) {
//         dr = _gbuffer->get_active_display_region(i);
//         int sort = dr->get_sort();
//         if (sort < min_sort)
//             min_sort = sort;
//     }
//     return region->get_sort() == min_sort;
// }