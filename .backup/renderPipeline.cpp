// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "renderPipeline.h"

#include <panda3d/displayRegionCullCallbackData.h>
#include <panda3d/cullResult.h>
#include <panda3d/sceneSetup.h>
#include <panda3d/graphicsStateGuardian.h>
#include <panda3d/displayRegion.h>
#include <panda3d/cullBinManager.h>
#include <panda3d/thread.h>
#include <panda3d/graphicsPipeSelection.h>
#include <panda3d/dataGraphTraverser.h>

#include "cullHandlerHook.h"
#include "renderStage.h"


using namespace std;

DEFINE_TYPEHANDLE(RenderPipeline)


RenderPipeline::RenderPipeline( const string& name, GraphicsOutput* host, 
                                LVecBase2i dimension): 
    PipelineNode(name), 
    _host(host),
    _dimensions(dimension)
{

    if (_dimensions == LVecBase2i(0.0f)) {
        if (host != nullptr && host->has_size())
            _dimensions = host->get_size();
    }

    do_add_task(method(update), "update-render-pipeline", 40);
}


void RenderPipeline::add_stage(RenderStage* stage) {
    _stages.push_back(stage);
}


void RenderPipeline::add_debug_stage(const string& name, Texture* texture) {
    
    nassertv(get_debug_stage(name) != nullptr)

    auto found = _debug_stages.find(name);

    if (found == _debug_stages.end())
        _debug_stages[name] = texture;
}


Texture* RenderPipeline::get_debug_stage(const string& name) {
    
    auto found = _debug_stages.find(name);

    if (found == _debug_stages.end())
        return found->second;

    return nullptr;
}


GraphicsOutput* RenderPipeline::get_buffer() const {
    return _buffer;
}


void RenderPipeline::set_buffer(GraphicsOutput* buffer) {
    _buffer = buffer;
}


void RenderPipeline::set_host(GraphicsOutput* host) {
    _host = host;
}


GraphicsOutput* RenderPipeline::get_host() const {
    return _host;
}


LVecBase2i RenderPipeline::get_dimensions() const {
    return _dimensions;
}


void RenderPipeline::set_dimensions(LVecBase2i size) {
    
    if (size != _dimensions) {
        for (RenderStage* stage: _stages)
            stage->dimensions_changed(size);
    }
    _dimensions = size;
}


void RenderPipeline::set_dimensions(int width, int height) {

    set_dimensions(LVecBase2i(width, height));    
}


void RenderPipeline::remove_stage(size_t n) {
    
    nassertv(n < _stages.size())
    _stages.erase(_stages.begin() + n);
    init();
}


void RenderPipeline::remove_stage(RenderStage* stage) {
    
    Stages::iterator found = find(_stages.begin(), _stages.end(), stage);
    nassertv(found != _stages.end())
    _stages.erase(found);
    init();
}


RenderStage* RenderPipeline::get_stage(const string& name) const {
    
    for (PT(RenderStage) stage: _stages) {
        if (stage->get_name() == name)
            return stage;
    }
    return nullptr;
}


RenderStage* RenderPipeline::get_stage(size_t n) const {
    
    nassertr(n < _stages.size(), nullptr)
    return _stages[n];
}


size_t RenderPipeline::get_num_stage() const {

    return _stages.size();
}


int RenderPipeline::get_stage_index(const RenderStage* stage) const {
    
    Stages::const_iterator it = find(_stages.begin(), _stages.end(), stage);

    if (it != _stages.end())
        return it - _stages.begin();
    
    return -1;
}


Texture* RenderPipeline::init() {

    collect_inputs();
    sort_stages_buffers();

    return final_render;
}


bool RenderPipeline::record_object(CullableObject* object, const CullTraverser* trav) {
    
    const NodeInfoAttrib* attrib;

    if (object->_state->get_attrib(attrib) && attrib->get_node()) {
        PandaNode* node = attrib->get_node();
        TypeHandle type = node->get_type();

        NodeHandlers::iterator it = _node_handlers.find(type);

        if (LIKELY(it != _node_handlers.end())) {
            it->second->record_node(node, object, trav);
            return false;
        }
    }
    return true;
}


AsyncTask::DoneStatus RenderPipeline::update(AsyncTask* task) {

    for (RenderStage* stage : _stages)
        stage->update();

    return AsyncTask::DS_again;
}


void RenderPipeline::sort_stages_buffers() {

    nassertv(_host != nullptr)

    // we start from the host buffer sort
    int current_sort = _host->get_child_sort();

    Stages::reverse_iterator it;

    for (it=_stages.rbegin(); it != _stages.rend(); ++it) {

        RenderStage* stage = *it;
        stage->_visited = false;

        // first fist find the highest sort of the stage's buffers
        int sort_offset = 0;

        for (GraphicsOutput* buffer: stage->_buffers)
            sort_offset = max(sort_offset, buffer->get_sort());

        int base_sort = current_sort -= sort_offset + 1;

        for (GraphicsOutput* buffer: stage->_buffers) {
            int sort = base_sort + buffer->get_sort();
            current_sort = min(current_sort, sort);
            buffer->set_sort(sort);
        }
    }
}

// RenderStage* RenderPipeline::add_stage(const string& name) {
    
//     auto result = _stage_constructs.find(name);
//     nassertr(result != _stage_constructs.end(), nullptr)

//     return result->second(this);
// }


// RenderPipeline::StageConstructs RenderPipeline::_stage_constructs;
