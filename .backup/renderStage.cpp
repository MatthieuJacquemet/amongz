// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsEngine.h>
#include <panda3d/displayRegionCullCallbackData.h>
#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/displayRegion.h>

#include "renderPipeline.h"
#include "renderStage.h"
#include "callbacks.h"

using namespace std;
using StageCallback = RenderStage::StageCallback;
using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;


DEFINE_TYPEHANDLE(RenderStage)
DEFINE_TYPEHANDLE(RenderStage::StageCallback)


// #define make_callback(method) \
//     _make_callback(this, as_function(&RenderStage::method))


// RenderStage::StageCallback* _make_callback(RenderStage* stage,
//                         RenderStage::StageCallback::Func* func)
// {
//     RenderStage::StageCallback* cb = new RenderStage::StageCallback(stage);
//     cb->_func = std::bind(func, placeholders::_1, cb);

//     return cb;
// }

// template<class T, void (RenderStage::*func)(T*)>
// class Callback<void (RenderStage::*)(T*), func>  {

// public:
//     static void call(CallbackData* data, RenderStage::StageCallback* callback) {

//         T* cdata = DCAST(T, data);

//         if (PT(RenderStage) stage = callback->_stage.lock())
//             (stage->*func)(cdata);
//         else
//             callback_garbage_collect(cdata);
//     }
// };




void RenderStage::setup() {
    
}

void RenderStage::init() {
    
}


RenderStage::RenderStage(const RenderStage& copy):
    _pipeline(copy._pipeline)
{
    
}



void RenderStage::dimensions_changed(LVector2i size) {

    for (GraphicsOutput* output: _buffers) {

        if (output->is_of_type(GraphicsBuffer::get_class_type())) {
            GraphicsBuffer* buffer = DCAST(GraphicsBuffer, output);
            buffer->set_size(size.get_x(), size.get_y());
        } else
            output->set_size_and_recalc(size.get_x(), size.get_y());
    }    
}


RenderPipeline* RenderStage::get_pipeline() const {

    return _pipeline;    
}


GraphicsOutput* RenderStage::add_buffer(FrameBufferProperties fb_prop,
    const string& name, int sort, LVecBase2i size, GraphicsOutput* host)
{
    if (host == nullptr)
        host = _pipeline->get_host();

    if (size == LVecBase2i(0))
        size = _pipeline->get_dimensions();

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    
    auto* buffer =  engine->make_output( host->get_pipe(), name, sort, fb_prop, 
                                WindowProperties::size(size),
                                GraphicsPipe::BF_refuse_window | 
                                GraphicsPipe::BF_refuse_parasite |
                                GraphicsPipe::BF_resizeable,
                                host->get_gsg(), host);

    _buffers.push_back(buffer);

    return buffer;
}


void RenderStage::add_buffer(GraphicsOutput* buffer) {

    _buffers.push_back(buffer);    
}


GraphicsOutput* RenderStage::get_buffer() const {
    
    if (_buffers.size() > 0)
        return _buffers[0];
    
    return nullptr;
}


GraphicsOutput* RenderStage::get_buffer(size_t n) const {
    
    nassertr(n < _buffers.size(), nullptr)
    return _buffers[n];
}


GraphicsOutput* RenderStage::get_buffer(const string& name) const {

    for (GraphicsOutput* buffer: _buffers) {
        if (buffer->get_name() == name)
            return buffer;
    }
    return nullptr;
}


size_t RenderStage::get_num_buffers() const {
    
    return _buffers.size();
}


void RenderStage::remove_buffer(GraphicsOutput* buffer) {
    
    Buffers::iterator found = find(_buffers.begin(), _buffers.end(), buffer);
    nassertv(found != _buffers.end())
    _buffers.erase(found);
}


void RenderStage::remove_buffer(size_t n) {

    nassertv(n < _buffers.size())
    _buffers.erase(next(_buffers.begin(), n));
}


int RenderStage::get_index() const {
    
    return _pipeline->get_stage_index(this);
}


void RenderStage::update() {
    
}

void RenderStage::record_node(PandaNode* node, CullableObject* object, 
                                const CullTraverser* trav)
{
    
}



void RenderStage::write_datagram(BamWriter* manager, Datagram& me) {

    manager->write_pointer(me, _pipeline);    
}


void RenderStage::fillin(DatagramIterator& scan, BamReader *manager) {
    
    manager->read_pointer(scan);
}


int RenderStage::complete_pointers(TypedWritable** p_list, BamReader* manager) {
    
    int n = PipelineNode::complete_pointers(p_list, manager);
    _pipeline = DCAST(RenderPipeline, p_list[n++]);
    init();

    return n;
}


GraphicsOutput* RenderStage::add_callback(int sort) {

    
    GraphicsOutput* host = _pipeline->get_host();
    
    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    
    FrameBufferProperties fb_props;
    fb_props.clear();

    WindowProperties win_props;
    win_props.clear_size();

    auto buffer = engine->make_output(host->get_pipe(), get_name(), 
                                    sort, fb_props, win_props, 
                                    GraphicsPipe::BF_require_callback_window,
                                    host->get_gsg(), host);

    add_buffer(buffer);

    if (buffer->is_of_type(CallbackGraphicsWindow::get_class_type())) {
        CallbackGraphicsWindow* window = DCAST(CallbackGraphicsWindow, buffer);
        window->set_render_callback(_render_callback);
    }

    return buffer;
}

