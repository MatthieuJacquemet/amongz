// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/shaderAttrib.h>

#include "shaderAttribHelper.h"

using namespace std;


ShaderAttribHelper::ShaderAttribHelper(Shader* shader):
    _shader(shader)
{
    
}


CPT(ShaderAttribHelper) ShaderAttribHelper::make(Shader* shader) {
    
    ShaderAttribHelper* attrib = new ShaderAttribHelper(shader);
    return return_new(attrib);
}


void ShaderAttribHelper::register_with_read_factory() {

    TypeHandle type = ShaderAttrib::get_class_type();
    BamReader::get_factory()->register_factory(type, make_from_bam);
}


TypeHandle ShaderAttribHelper::get_type() const {
    return ShaderAttrib::get_class_type();
}


int ShaderAttribHelper::get_slot() const {
    return ShaderAttrib::get_class_slot();
}


void ShaderAttribHelper::write_datagram(BamWriter *manager, Datagram &dg) {
    
    Shader::ShaderFile file;
    file._vertex = _shader->get_filename(Shader::ST_vertex);
    file._fragment = _shader->get_filename(Shader::ST_fragment);
    file._geometry = _shader->get_filename(Shader::ST_geometry);
    file._tess_control = _shader->get_filename(Shader::ST_tess_control);
    file._tess_evaluation = _shader->get_filename(Shader::ST_tess_evaluation);
    
    dg.add_uint8(_shader->get_language());
    file.write_datagram(dg);
}


TypedWritable* ShaderAttribHelper::make_from_bam(const FactoryParams& params) {

    BamReader* manager;
    DatagramIterator scan;

    parse_params(params, scan, manager);

    Shader::ShaderLanguage language = (Shader::ShaderLanguage)scan.get_uint8();
    
    Shader::ShaderFile file;
    file.read_datagram(scan);

    PT(Shader) shader = Shader::load(language, file._vertex,
                                file._fragment, file._geometry,
                                file._tess_control,
                                file._tess_evaluation);

    return (TypedWritable*)ShaderAttrib::make(shader).p();
}

