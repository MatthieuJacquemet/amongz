// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <panda3d/typedWritableReferenceCount.h>

#include "utils.h"


class Material: public Namable, public TypedWritableReferenceCount {

    REGISTER_TYPE_POST_INIT("Material", Namable, TypedWritableReferenceCount)

public:

    enum Type {
        T_texture,
        T_texture_image,
        T_texture_sampler,
        T_buffer,
        T_array,
        T_vector,
        T_matrix
    };

    Material();
    virtual ~Material() = default;

    static void register_with_read_factory();

protected:
    virtual void fillin(DatagramIterator& scan, BamReader* manager) override;
    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    static TypedWritable* make_from_bam(const FactoryParams& params);

private:
    CPT(ShaderAttrib) _attrib;

    typedef ShaderInput (*ReadHandler)(CPT_InternalName, DatagramIterator&, BamReader*);
    typedef void (*WriteHandler)(const ShaderInput&, BamWriter*, Datagram&);

    struct ReadWriteHandlers {
        ReadHandler _read;
        WriteHandler _write;
    };

    typedef std::map<std::string, ReadWriteHandlers> ReadWriteHandlers;
    typedef std::map<std::string, ShaderInput> Inputs;

    static ReadWriteHandlers _handlers;
    Inputs _inputs;

    static ShaderInput get_input(CPT_InternalName id, DatagramIterator& scan, BamReader* manager);

    static void post_init(TypeHandle);
};


#endif // __MATERIAL_H__