#version 450

#extension GL_ARB_shader_image_load_store : enable
#extension GL_ARB_fragment_shader_interlock: enable


flat in uint v_light_id;
in vec3 v_depth;

coherent volatile layout(r32ui) uniform uimage2D u_light_counter;
coherent volatile layout(rgba32f) uniform image2DArray u_lights_tiles;
layout(rgba8) uniform image2D u_output;


coherent layout(rgba32f) uniform image2DArray u_lightVolume;
uniform vec2 u_nearFar;

#pragma include "transforms.inc.glsl"


void main() {

    ivec2 coord = ivec2(gl_FragCoord.xy);

    // beginInvocationInterlockARB();

    // uint old_id = imageAtomicExchange(u_light_mask, coord, u_id + 1).r;
    // uint counter;

    // memoryBarrier();


    // if (old_id != v_light_id + 1) {
    //     counter = imageAtomicAdd(u_light_counter, coord, 1);
    //     memoryBarrier();
    // }
    // else {
    //     memoryBarrier();
    //     counter = imageLoad(u_light_counter, coord).r;
    // }
    // imageAtomicAdd(u_light_counter, coord, 1);
    // float z = v_depth.x / v_depth.y;
    float z = gl_FragCoord.z;
    float halfDepthRange = abs(fwidth(z) * 0.5);
    float depth = z + halfDepthRange;
    float linear_depth = get_linear_z_from_z(depth, u_near_far.x, u_near_far.y);

    beginInvocationInterlockARB();

    uint counter = imageLoad(u_light_counter, coord).r;
    uint tile_id = imageLoad(u_lights_ids, ivec3(coord, counter-1)).x;

    if (tile_id != v_light_id + 1)
        imageStore(u_light_counter, coord, uvec4(counter+1, 0,0,0));
    else 
        counter--;

    ivec3 array_coord = ivec3(coord, counter);

    imageStore(u_lights_ids, array_coord, uvec4(v_light_id + 1, 0,0,0));
    
    endInvocationInterlockARB();


    if (gl_FrontFacing)
        imageStore(u_lights_min, array_coord, vec4(linear_depth, 0,0,0));
    else
        imageStore(u_lights_max, array_coord, vec4(linear_depth, 0,0,0));
}






    // memoryBarrier();
    
    // imageStore(u_light_counter, coord, uvec4(v_light_id + 1, 0,0,0));
    // imageStore(u_output, coord, vec4(1,0,0,0));




    // if (tile_id != u_id + 1) {
    //     imageStore(u_lights_ids, ivec3(coord, counter), uvec4(u_id + 1, 0,0,0));
    //     imageStore(u_light_counter, coord, uvec4(counter+1, 0,0,0));
    // }
    // else 
    //     counter--;

    // endInvocationInterlockARB();

    //     counter = (uint)imageAtomicIncWrap(u_light_counter, coord, 16);
    // else
    //     counter = imageLoad(u_light_counter, coord).r;










    // vec3 ray = normalize(v_pos.xyw + dFdx(v_pos.xyw)*v_delta.x + dFdy(v_pos.xyw)*v_delta.y);
    // float t = -v_plane.w/dot(v_plane.xyz,ray);
    
    // float depth = 1.0;

    // if (t > 0) {
    // 	float isectWc = ray.z*t;
    // 	depth = min(1.0, 0.5 + 0.5*(-2000.0/(999.0*isectWc) + 1001.0/999.0));
    // }

    // float depth = v_pos.z;

    // imageStore(u_lights_min, array_coord, ivec4(1,0,0,0));


    // float linear_z = get_linear_z_from_z(depth, u_near_far.x, u_near_far.y);
    // int z = floatBitsToInt(1.0);
    // if (gl_FrontFacing)

    // float id = uintBitsToFloat(1);
    // imageStore(u_lights_tiles, ivec3(coord, counter), vec4(id,0,0,0));

    // ivec2 coo = coord*16;
    // for (int i=0; i<16; ++i) {
    //     for (int j=0; j<16; ++j)
    //         imageStore(u_output, coo + ivec2(i,j), vec4(float(v_light_id + 1) * 0.5,0,0,0));
    // }



    // else
    //     imageStore(u_lights_max, ivec3(coord, 0), vec4(v_delta.x,0,0,0));

    // imageAtomicMax(u_lights_min, array_coord, z);
    // imageAtomicMax(u_lights_max, array_coord, z);

    // imageStore(u_lights_ids, array_coord, uvec4(u_id,0,0,0));