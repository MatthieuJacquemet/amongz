// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsBuffer.h>

#include "renderPipeline.h"
#include "decalStage.h"

using namespace std;


void DecalStage::setup() {
    
    _pipeline->set_state("decal-opaque", _opaque_state);
    _pipeline->set_state("decal-transparent", _transparent_state);

    
}


DecalStage::DecalStage() {

    FrameBufferProperties fb_props;

    fb_props.set_force_hardware(true);
    fb_props.set_rgb_color(true);
    fb_props.set_stereo(false);
    fb_props.set_accum_bits(0);
    fb_props.set_rgba_bits(8,8,8,8);
    fb_props.set_depth_bits(24);
    fb_props.set_stencil_bits(8);
    fb_props.set_multisamples(0);
    fb_props.set_back_buffers(0);
    fb_props.set_aux_rgba(2);

    setup_buffer(fb_props, "decal-opque");
}
