// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bamReader.h>
#include <panda3d/shaderAttrib.h>
#include <panda3d/shaderInput.h>
#include <panda3d/paramTexture.h>
#include <panda3d/texturePool.h>
#include <panda3d/samplerState.h>
#include <panda3d/bamReaderParam.h>

#include "material.h"

using namespace std;


DEFINE_TYPEHANDLE(Material)


Material::Material() {
    
}


void Material::register_with_read_factory() {
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

template<class T>
ShaderInput _read_input(CPT_InternalName id, DatagramIterator& scan, BamReader* manager) {
    
    T value;
    int32_t priority = scan.get_int32();
    generic_read_datagram(value, scan);

    return ShaderInput(id, value, priority);
}


size_t get_buffer_size(const Shader::ShaderPtrData& data) {

    size_t size;

    switch (data._type) {
        case Shader::SPT_uint:
        case Shader::SPT_int: size = sizeof(int); break;
        case Shader::SPT_float: size = sizeof(float); break;
        case Shader::SPT_double: size = sizeof(double); break;
        case Shader::SPT_unknown: size = 1;
    }
    return data._size * size;
}



void write_input(const ShaderInput& input, BamWriter* manager, Datagram& me) {
    
    const Shader::ShaderPtrData& data = input.get_ptr();
    size_t size = get_buffer_size(data);

    me.add_int32(input.get_priority());

    switch (input.get_value_type()) {
    
        case T_texture:

            Texture* texture = DCAST(Texture, input.get_value());
            ma.add_string(texture->get_fullpath());
            break;
        
        case ShaderInput::M_texture_image:

            ParamTextureImage* param = DCAST(ParamTextureImage, input.get_value());
            me.add_string(param->get_texture()->get_fullpath());
            me.add_bool(param->has_read_access());
            me.add_bool(param->has_write_access());
            me.add_int32(param->get_bind_layer());
            me.add_int32(param->get_bind_level());


        case ShaderInput::M_texture_sampler:

            ParamTextureSampler* param = DCAST(ParamTextureSampler, input.get_value());
            me.add_string(param->get_texture()->get_fullpath());
            SamplerState& sampler = param->get_sampler();
            sampler.write_datagram(me);


        case ShaderInput::M_buffer:

            TypedWritableReferenceCount* object = input.get_value();
            object->write_datagram(manager, me);
            



        case ShaderInput::M_numeric:
            me.add_uint32(size);
        case ShaderInput::M_vector:
            me.append_data(data._ptr, size);

        default:    
            return;
    }
}

template<>
ShaderInput _read_input<Texture>(CPT_InternalName id, DatagramIterator& scan, BamReader* manager) {

    using ShaderInputType = ShaderInput::ShaderInputType;

    int32_t priority = scan.get_int32();

    string name = scan.get_string();
    ShaderInputType mode = (ShaderInputType) scan.get_uint8();

    Texture* texture = TexturePool::load_texture(name);
    nassertr(texture != nullptr, ShaderInput())
    
    switch (mode) {
    case ShaderInputType::M_texture:
        return ShaderInput(id, texture, priority);
    
    case ShaderInputType::M_texture_image:
        return ShaderInput(id, texture, scan.get_bool(), scan.get_bool(), 
                        scan.get_int32(), scan.get_int32(), priority);
    
    case ShaderInputType::M_texture_sampler:
        SamplerState sampler;
        sampler.read_datagram(scan, manager);
        return ShaderInput(id, texture, sampler, priority);
    
    default:
        nout << "error : unknwon texture input mode" << endl;
    }
    return ShaderInput();
}


template<>
ShaderInput _read_input<ShaderBuffer>(CPT_InternalName id, DatagramIterator& scan, BamReader* manager) {

    int32_t priority = scan.get_int32();
    
    FactoryParams params;
    params.add_param(new BamReaderParam(scan, manager));
 
    TypeHandle type = ShaderBuffer::get_class_type();
    WritableFactory* factory = BamReader::get_factory();

    TypedWritable* object = factory->make_instance(type, params);
    nassertr(object != nullptr, ShaderInput())

    return ShaderInput(id, DCAST(ShaderBuffer, object), priority);
}


#define REGISTER_HANDLERS(Type) \
    _handlers[#Type] = {&_read_input<Type>, &_write_input<Type>};

void Material::post_init(TypeHandle) {

    REGISTER_HANDLERS(Texture)
    REGISTER_HANDLERS(ShaderBuffer)
    REGISTER_HANDLERS(PTA_float)
    REGISTER_HANDLERS(PTA_double)
    REGISTER_HANDLERS(PTA_int)
    REGISTER_HANDLERS(PTA_LVecBase4f)
    REGISTER_HANDLERS(PTA_LVecBase3f)
    REGISTER_HANDLERS(PTA_LVecBase2f)
    REGISTER_HANDLERS(PTA_LVecBase4d)
    REGISTER_HANDLERS(PTA_LVecBase3d)
    REGISTER_HANDLERS(PTA_LVecBase2d)
    REGISTER_HANDLERS(PTA_LVecBase4i)
    REGISTER_HANDLERS(PTA_LVecBase3i)
    REGISTER_HANDLERS(PTA_LVecBase2i)
    REGISTER_HANDLERS(PTA_LMatrix4f)
    REGISTER_HANDLERS(PTA_LMatrix3f)
    REGISTER_HANDLERS(PTA_LMatrix4d)
    REGISTER_HANDLERS(PTA_LMatrix3d)
    REGISTER_HANDLERS(LVecBase4f)
    REGISTER_HANDLERS(LVecBase3f)
    REGISTER_HANDLERS(LVecBase2f)
    REGISTER_HANDLERS(LVecBase4d)
    REGISTER_HANDLERS(LVecBase3d)
    REGISTER_HANDLERS(LVecBase2d)
    REGISTER_HANDLERS(LVecBase4i)
    REGISTER_HANDLERS(LVecBase3i)
    REGISTER_HANDLERS(LVecBase2i)
    REGISTER_HANDLERS(LMatrix4f)
    REGISTER_HANDLERS(LMatrix3f)
    REGISTER_HANDLERS(LMatrix4d)
    REGISTER_HANDLERS(LMatrix3d)

}




ShaderInput Material::get_input(CPT_InternalName id, DatagramIterator& scan, BamReader* manager) {

    string type_name = scan.get_string();

    ReadWriteHandlers::iterator it = _handlers.find(type_name);
    nassertr(it != _handlers.end(), ShaderInput())

    return it->second._read(id, scan, manager);
}


void Material::fillin(DatagramIterator& scan, BamReader* manager) {

    Shader::ShaderLanguage lang = (Shader::ShaderLanguage)scan.get_uint8();

    if (UNLIKELY(lang == Shader::SL_none))
        return;

    PT(Shader) shader = Shader::load(lang, scan.get_string(),
                            scan.get_string(), scan.get_string(),
                            scan.get_string(), scan.get_string());

    _attrib = ShaderAttrib::make(shader);

    uint8_t num_inputs = scan.get_uint8();

    for (uint8_t i=0; i<num_inputs; ++i) {

        string name = scan.get_string();

        ShaderInput input = get_input(name, scan, manager);
        _attrib = _attrib->set_shader_input(input);
    }
}


void Material::write_datagram(BamWriter* manager, Datagram& me) {

    if (UNLIKELY(_attrib == nullptr)) {
        me.add_uint8(Shader::SL_none);
        return;
    }

    const Shader* shader = _attrib->get_shader();
    me.add_uint8(shader->get_language());

    me.add_string(shader->get_filename(Shader::ST_vertex));
    me.add_string(shader->get_filename(Shader::ST_fragment));
    me.add_string(shader->get_filename(Shader::ST_geometry));
    me.add_string(shader->get_filename(Shader::ST_tess_control));
    me.add_string(shader->get_filename(Shader::ST_tess_evaluation));

    me.add_uint8(_input_names.size());

    for (const string& name: _input_names) {

        const ShaderInput& input = _attrib->get_shader_input(name);

        
    }
}



TypedWritable* Material::make_from_bam(const FactoryParams& params) {
    
    Material* me = new Material;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}
