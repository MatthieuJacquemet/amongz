// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.



// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/cullFaceAttrib.h>
#include <panda3d/cullTraverser.h>
#include <panda3d/cullHandler.h>
#include <panda3d/cullBinAttrib.h>
#include <panda3d/cullBinUnsorted.h>
#include <panda3d/geomDrawCallbackData.h>
#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/glgsg.h>


#include "renderPipeline.h"
#include "render_utils.h"
#include "pointLightNode.h"
#include "spotLightNode.h"
#include "config_render_pipeline.h"
#include "lightStage.h"

using namespace std;

DEFINE_TYPEHANDLE(LightStage)

LightStage::Stages LightStage::_stages;
PT(LightStage) LightStage::_current = nullptr;



LightStage::LightStage(RenderPipeline* pipeline):
    RenderStage("light", pipeline),
    _hw_conservative_raster(0),
    _fragment_shader_interlock(false),
    _half_tile_size(1, 0.0f),
    _depth_trans(1, 0.0f),
    _screen_size(1, 0.0f),
    _view_scale(1, 0.0f)
{
    
    if (pipeline != nullptr)
        init();
}



LightStage* LightStage::get_current_stage(CullTraverser* trav) {
    
    SceneSetup* setup = trav->get_scene();
    GraphicsOutput* buffer = setup->get_display_region()->get_window();

    if (_current == nullptr || _current->_pipeline->get_buffer() != buffer) {
        // update the cached renderer
        Stages::iterator found = _stages.find(buffer);
        _current = found->second;
    }

    return _current;
}


const RenderState* LightStage::get_binning_state() const {

    return _binning;    
}


LightStage::~LightStage() {

    _stages.erase(_pipeline->get_buffer());
}


void LightStage::init() {

    _stages[_pipeline->get_buffer()] = this;

    setup_textures();
    setup_buffers();
    setup_shaders();
}


void LightStage::dimensions_changed(LVector2i size) {

    _num_tile_x = get_ceil_size(size.get_x(), SST_16);
    _num_tile_y = get_ceil_size(size.get_y(), SST_16);

    if (!_hw_conservative_raster) {

        static size_t tile_size = as_numeric(SST_16);

        _view_scale[0].set_x((float)size.get_x() / (_num_tile_x * tile_size));
        _view_scale[0].set_y((float)size.get_y() / (_num_tile_y * tile_size));

        _half_tile_size[0].set_x(1.0f / _num_tile_x);
        _half_tile_size[0].set_y(1.0f / _num_tile_y);
    }

    _screen_size[0] = size;
    _buffer->set_size_and_recalc(size.get_x(), size.get_y());
}


void LightStage::setup_textures() {

    light_volume = new Texture("light-volume");

    light_volume->setup_2d_texture_array(_num_tile_x, _num_tile_y, 
                                        max_lights_per_tile,
                                        Texture::T_float,
                                        Texture::F_rgb32);
}


void LightStage::setup_shaders() {
    
    GraphicsStateGuardian* gsg = _buffer->get_gsg();

    // before loading shaders we need to check the GPU capabilities

    if (gsg->has_extension("GL_NV_conservative_raster"))
        _hw_conservative_raster = GL_CONSERVATIVE_RASTERIZATION_NV;
    else if (gsg->has_extension("GL_INTEL_conservative_rasterization"))
        _hw_conservative_raster = GL_CONSERVATIVE_RASTERIZATION_INTEL;

    if (gsg->has_extension("GL_ARB_fragment_shader_interlock"))
        _fragment_shader_interlock = true;
    else {
        rp_error("fragment shader interlock is not supported by the GPU," 
                "light stage is disabled.");
        return;
    }

    if (_hw_conservative_raster)
        _binning.set_shader(Shader::load(SHADER_LANG, 
                GDK_SHADER_PATH VERT_SHADER("light"),
                GDK_SHADER_PATH FRAG_SHADER("lightBinning")));
    else {
        _binning.set_shader(Shader::load(SHADER_LANG, 
                GDK_SHADER_PATH VERT_SHADER("lightConservative"),
                GDK_SHADER_PATH FRAG_SHADER("lightBinningConservative"),
                GDK_SHADER_PATH GEOM_SHADER("conservative")));

        _binning.set_shader_input("u_HalfPixelSize", _half_tile_size);
        _binning.set_shader_input("u_viewScale", _view_scale);
    }

    _blit_depth.set_shader(Shader::load(SHADER_LANG,
                        GDK_SHADER_PATH VERT_SHADER("quad"),
                        GDK_SHADER_PATH FRAG_SHADER("blitDepth")));

    _accumulate.set_shader(Shader::load_compute(SHADER_LANG, 
                        GDK_SHADER_PATH COMP_SHADER("accumulateLighting")));
    



    _blit_depth.set_shader_input("u_trans", _depth_trans);
    _blit_depth.set_depth_test(false);
    _blit_depth.set_depth_write(true);

    _accumulate.set_shader_input("u_lightVolume", light_volume);
    _accumulate.set_shader_input("u_screenSize", _screen_size);
    _accumulate.set_shader_input("u_accumulator", accumulator);

    _binning.set_bin("lights", CullBin::BT_unsorted);
    _binning.set_shader_input("u_lightVolume", light_volume);
    _binning.set_depth_write(false);
    _binning.set_depth_test(true);
    _binning.set_two_sided(true);
}


void LightStage::draw(DisplayRegionDrawCallbackData* data) {

    GraphicsStateGuardian* gsg = _buffer->get_gsg();


    if (scene_data) {
        const Lens* lens = scene_data->get_lens();
        LMatrix4f proj = lens->get_projection_mat();

        _depth_trans[0].set_x(proj.get_col(2).get_z());
        _depth_trans[0].set_y(proj.get_col(3).get_z());
    }

    // blit the max depth to tiles
    draw_fullscreen_quad(gsg, _blit_depth);

    // bin the lights
    if (_bins != nullptr) {
        if (_hw_conservative_raster != 0) {
            glEnable(_hw_conservative_raster);
            _bins->draw();
            glDisable(_hw_conservative_raster);
        } else
            _bins->draw();
    }

    // accumulate lighting    
    dispatch_compute(gsg, _accumulate, _num_tile_x, _num_tile_y);
}


void LightStage::setup() {

    if (hi_depth) {
        _accumulate.set_shader_input("u_depth", hi_depth);
        _blit_depth.set_shader_input("u_depth", hi_depth);
    }

    if (accumulator) {
        _accumulate.set_shader_input("u_accumulator", accumulator);
        output = accumulator;
    }

    if (culled_bins) {
        _bins = culled_bins.get_data()->add_group("lights");
        _bins->add_bin_name("lights");
    }

    if (scene_data)
        _binning.set_shader_input("u_nearFar", scene_data->_near_far);

    if (normal)
        _accumulate.set_shader_input("u_normal", normal);
}


void LightStage::setup_buffers() {

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(false);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(0,0,0,0);
    fb_prop.set_depth_bits(24);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);

    _buffer = add_buffer(fb_prop, "lights", 1);
    _buffer->disable_clears();
    
    DisplayRegion* dr = _buffer->make_mono_display_region();
    dr->disable_clears();

    dr->set_draw_callback(_draw_callback);
}