// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "config_tiled_lighting.h"
#include "lightBufferManager.h"
#include <panda3d/dconfig.h>



ConfigVariableInt light_buffer_size
("light-buffer-size", 2048,
 PRC_DESC(  "Sets the maximum number of light on the GPU storage buffer;"
            "Using high value cause result in higher memory usage"));


ConfigureDef(config_tiled_lighting);

ConfigureFn(config_tiled_lighting) {
    init_tiled_lighting();
}


void init_tiled_lighting() {
    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;

    LightBufferManager::init_type();
}
