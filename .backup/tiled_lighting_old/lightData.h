// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIGHTDATA_H__
#define __LIGHTDATA_H__


#include <panda3d/aa_luse.h>


struct LightData {

    static const size_t size;
    static const size_t block;

    enum Type {
        T_point = 0,
        T_spot,
        T_area,
        T_tube,
        T_directional,
        T_none
    };

    uint16_t type;
    uint16_t shadow_map;
    LVecBase3f color;
    LVecBase4f data;
};


#endif // __LIGHTDATA_H__