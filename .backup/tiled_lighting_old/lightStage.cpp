// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/cullFaceAttrib.h>
#include <panda3d/cullTraverser.h>
#include <panda3d/cullHandler.h>
#include <panda3d/cullBinAttrib.h>
#include <panda3d/cullBinUnsorted.h>
#include <panda3d/geomDrawCallbackData.h>
#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/glgsg.h>
#include <algorithm>

#include "lightStage.h"
#include "renderPipeline.h"
#include "lightBufferManager.h"
#include "render_utils.h"
#include "pointLightNode.h"
#include "spotLightNode.h"


using namespace std;
using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;



bool LightStage::_light_buffer_init = false;



class WireRenderCallback: public CallbackObject {

public:
    ALLOC_DELETED_CHAIN(WireRenderCallback);

    WireRenderCallback(LightStage* stage): _stage(stage) {}
    ~WireRenderCallback() = default;

    virtual void do_callback(CallbackData *cbdata) {

        DisplayRegionDrawCallbackData* data = 
            DCAST(DisplayRegionDrawCallbackData, cbdata);
        
        GraphicsStateGuardian* gsg = _stage->_buffer->get_gsg();
        bool force = !gsg->get_effective_incomplete_render();

        // draw_fullscreen_quad(gsg, _stage->_blit.get_state(), force);

        if (_stage->_bins != nullptr) {
            _stage->_bins->draw();
        }
    }

private:
    PT(LightStage) _stage;
};




void LightStage::draw(DisplayRegionDrawCallbackData* data) {

    GraphicsStateGuardian* gsg = _buffer->get_gsg();

    LightBufferManager* mgr = DCAST(LightBufferManager,
                                    LightManager::get_global_ptr());
    mgr->update_light_buffer(gsg);


    LVecBase2i size = _pipeline->get_dimensions();
    _view_size[0] = size;

    size_t tile_size = as_numeric(_light_render.get_tile_size());

    size_t num_tile_x = _light_render.get_num_tile_x();
    size_t num_tile_y = _light_render.get_num_tile_y();
    
    _view_scale[0].set_x((float)size.get_x() / (num_tile_x * tile_size));
    _view_scale[0].set_y((float)size.get_y() / (num_tile_y * tile_size));

    _half_tile_size[0].set_x(1.0 / num_tile_x);
    _half_tile_size[0].set_y(1.0 / num_tile_y);

    // combine the light tiles
    dispatch_compute(gsg, _combine,
                    get_ceil_size(num_tile_x, SST_16),
                    get_ceil_size(num_tile_y, SST_16));


    _light_render.draw_lights(data);


    // blit the output
    // dispatch_compute(gsg, get_shader_attrib(_debug), num_tile_x, num_tile_y);


    dispatch_compute(gsg, get_shader_attrib(_debug),num_tile_x,num_tile_y);


    // GLGraphicsStateGuardian* glgsg = DCAST(GLGraphicsStateGuardian, gsg);

    // cout << "ccccccccccccccc" << endl;
    // gsg->push_group_marker("ccccccccccccc");
    // glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS);

    // gsg->set_state_and_transform(_debug.get_state(), TransformState::make_identity());

    // gsg->dispatch_compute(num_tile_x, num_tile_y, 1);

    // glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS);

    // gsg->pop_group_marker();
    // cout << "ddddddddddddddddddd" << endl;




    // // accumulate lighting    
    // dispatch_compute(gsg, get_shader_attrib(_accumulate), 
    //                 _num_tile_x, _num_tile_y);
}




void LightStage::setup() {

    if (hi_depth) {
        int tile_size = max(_light_render.get_tile_size() - 1, 0);
        _combine.set_shader_input("u_depth_lod", hi_depth, true, false, -1, tile_size);
        _accumulate.set_shader_input("u_depth", hi_depth);
    }
    
    if (accumulator) {
        _accumulate.set_shader_input("u_accumulator", accumulator);
        // output = accumulator;
    }

    if (culled_bins) {
        _bins = culled_bins.get_data()->add_group("lights");
        _bins->add_bin_name("lights");
    }

    if (normal)
        _accumulate.set_shader_input("u_normal", normal);

    light_debug = _debug_tex2;
}


LightStage::LightStage(RenderPipeline* pipeline): 
    RenderStage("light", pipeline),
    _view_size(1, LVecBase2i(0,0)),
    _view_scale(1,0),
    _half_tile_size(1,0),
    _light_render(this)
{
    
    if (pipeline != nullptr)
        init();

    LightBufferManager* mgr = DCAST(LightBufferManager,
                                    LightManager::get_global_ptr());

    mgr->map_light_type(PointLightNode::get_class_type(), LightData::T_point);
    mgr->map_light_type(SpotLightNode::get_class_type(), LightData::T_spot);


    setup_debug_buffer();
    setup_textures();

    GraphicsOutput* buffer = _light_render.get_buffer();
    DisplayRegion* dr = buffer->get_active_display_region(0);

    dr->set_draw_callback(_draw_callback);
    
    setup_shaders();
}


void LightStage::setup_debug_buffer() {

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(true);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(8,8,8,8);
    fb_prop.set_depth_bits(24);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);

    _buffer = add_buffer(fb_prop, "lights wireframe", 1);

    _buffer->disable_clears();
    
    DisplayRegion* dr = _buffer->make_mono_display_region();

    WireRenderCallback* cb = new WireRenderCallback(this);


    dr->set_draw_callback(cb);
    dr->disable_clears();

    // _wire_region = _buffer->make_mono_display_region();

    // NodePath wire_root("wire root");
    // NodePath cam_np;
    // NodePath quad_np;

    // create_fullscreen_quad(wire_root, cam_np, quad_np, "debug_texture");

    // quad_np.set_shader_input("u_counter", _light_render->get_counter_texture());
    // quad_np.set_shader_input("u_lights_tiles", _light_render->get_tiles_buffer());

    // _wire_region->set_camera(cam_np);
    // _wire_region->disable_clears();
    
    // _wire_region = _buffer->make_mono_display_region();
    // _wire_region->set_sort(1);

    // WireRenderCallback* cb = new WireRenderCallback(this);
    // _wire_region->set_draw_callback(cb);
    // _wire_region->disable_clears();

    // _debug_bin = CullBinUnsorted::make_bin("wire bin", pipeline->get_host()->get_gsg(),
    //                                 _wire_region->get_draw_region_pcollector());    
}


void LightStage::setup_textures() {


    _debug_tex2 = new Texture("debug 2");

    _buffer->add_render_texture(_debug_tex2, 
                                GraphicsOutput::RTM_bind_or_copy, 
                                DrawableRegion::RTP_color);

    // _debug_tex              = new Texture("debug");
    // _debug_tex->setup_2d_texture( _num_tile_x, _num_tile_y, 
    //                             Texture::T_byte,
    //                             Texture::F_rgba8);

    // _debug_tex->set_minfilter(SamplerState::FT_nearest);
    // _debug_tex->set_magfilter(SamplerState::FT_nearest);

    // _buffer->add_render_texture(_debug_tex, GraphicsOutput::RTM_bind_or_copy, 
    //                             DrawableRegion::RTP_color);

    // _buffer->set_clear_color(LColor(0,0,0,0));


    
    // _wire_buffer->disable_clears();

}


void LightStage::setup_shaders() {

    LightBufferManager* mgr = DCAST(LightBufferManager,
                                    LightManager::get_global_ptr());


    _combine_lights = Shader::load_compute(SHADER_LANG,
                        GDK_SHADER_PATH COMP_SHADER("combine_light_tiles"));

    _accumulate_light = Shader::load_compute(SHADER_LANG, 
                        GDK_SHADER_PATH COMP_SHADER("accumulate_lighting"));
    
    _debug_shader = Shader::load_compute(SHADER_LANG, 
                        GDK_SHADER_PATH COMP_SHADER("debug_light_buffer"));

    // _debug_shader = Shader::load(SHADER_LANG, 
    //                             VERT_SHADER("quad2"),
    //                             FRAG_SHADER("debug_light_buffer"));

    // _debug_shader = Shader::load(SHADER_LANG, 
    //                             VERT_SHADER("light"),
    //                             FRAG_SHADER("light_id"),
    //                             GEOM_SHADER("conservative"));

    Texture* counter = _light_render.get_counter_texture();
    Texture* tiles = _light_render.get_tiles_buffer();
    Texture* tiles_ids = _light_render.get_tiles_ids_buffer();
    Texture* tiles_min_depth = _light_render.get_tiles_min_depth_buffer();
    Texture* tiles_max_depth = _light_render.get_tiles_max_depth_buffer();

    _combine.set_shader(_combine_lights);
    _combine.set_shader_input("lightBuffer", mgr->get_buffer());
    _combine.set_shader_input("u_lights_tiles", tiles);
    _combine.set_shader_input("u_light_counter", counter);
    _combine.set_shader_input("u_debug_tex", _debug_tex2);
    _combine.set_shader_input("u_lights_ids", tiles_ids);
    _combine.set_shader_input("u_lights_min", tiles_min_depth);
    _combine.set_shader_input("u_lights_max", tiles_max_depth);


    _accumulate.set_shader(_accumulate_light);
    _accumulate.set_shader_input("lightBuffer", mgr->get_buffer());
    _accumulate.set_shader_input("u_lights_tiles", tiles);
    _accumulate.set_shader_input("u_light_counter", counter);
    _accumulate.set_shader_input("u_screen_size", _view_size);

    // _debug.set_shader(_debug_shader);
    // _debug.set_shader_input("u_lights_tiles", tiles);
    // _debug.set_shader_input("u_light_counter", counter, true, false, -1, 0);
    // _debug.set_shader_input("u_output", _debug_tex2);

    // NodePath debug = root.attach_new_node(make_fullscreen_quad());

    _debug.set_shader(_debug_shader);
    _debug.set_shader_input("u_lights_tiles", tiles);
    _debug.set_shader_input("u_lights_ids", tiles_ids);
    _debug.set_shader_input("u_lights_min", tiles_min_depth);
    _debug.set_shader_input("u_lights_max", tiles_max_depth);

    _debug.set_shader_input("u_light_counter", counter, true, false, -1, 0);
    _debug.set_shader_input("u_output", _debug_tex2);

//     debug.set_depth_write(false);
//     debug.set_depth_test(false);
//     debug.set_material_off(true);
//     debug.set_two_sided(true);
}



// void LightStage::LightGroup::draw() const {
    
    // CulledBins::Group::draw();
    // Thread* current_thread = Thread::get_current_thread();

    // Setups::const_iterator it;
    // LightStage::LightTypes::const_iterator lt;
    // const LightStage::LightTypes& types = _stage->_light_types;

    // for (lt = types.begin(); lt != types.end(); ++lt) {
        
    //     for (it = _setups.begin(); it != _setups.end(); ++it) {

    //         SceneSetup* setup = it->first;
    //         DisplayRegion* dr = setup->get_display_region();
    //         GraphicsStateGuardian* gsg = dr->get_window()->get_gsg();
    //         bool force = !gsg->get_effective_incomplete_render();
    //         gsg->clear_state_and_transform();

            
    //         // setup the light assignment rendering
    //         // gsg->set_state_and_transform(_stage->_lighting_state, 
    //         //                             TransformState::make_identity());

    //         if (!gsg->set_scene(setup))
    //             display_cat.error() << gsg->get_type() 
    //                 << "cannot set scene setup" << endl;

    //         else if (gsg->begin_scene()) {
    //             for (CullBin* bin: it->second) {
    //                 if (bin->get_name() == lt->_type_handle.get_name()) {
    //                     gsg->push_group_marker(bin->get_name());
    //                     bin->draw(force, current_thread);
    //                     gsg->pop_group_marker();
    //                 }
    //             }
    //             gsg->end_scene();
    //         }
    //     }
    // }
// }







// PT(LightDrawCallback) LightStage::_light_draw_cb;


// class LightDrawCallback: public CallbackObject {

// public:
//     ALLOC_DELETED_CHAIN(LightDrawCallback);

//     LightDrawCallback() {}

//     ~LightDrawCallback() {}

//     virtual void do_callback(CallbackData *cbdata) {

//         GeomDrawCallbackData* data = DCAST(GeomDrawCallbackData, cbdata);
//         GraphicsStateGuardianBase* gsg = data->get_gsg();


//         if (gsg->is_of_type(GLGraphicsStateGuardian::get_class_type())) {
//             PT(GLGraphicsStateGuardian) glgsg = DCAST(GLGraphicsStateGuardian, gsg);
//             glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS);
//         }
//         data->upcall();
//     }
// };
