// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIGHTLODS_H__
#define __LIGHTLODS_H__

#include <panda3d/geomNode.h>
#include <panda3d/pta_LMatrix4.h>
#include <panda3d/pta_int.h>
#include <panda3d/callbackObject.h>

class Geom;
class CullTraverser;
class CullTraverserData;


class LightLods: public GeomNode {

public:

    LightLods();
    ~LightLods() = default;

    void add_for_draw(CullTraverser* trav, CullTraverserData& data);
    void collect(int32_t* ids, LMatrix4* trans, size_t& offset);

    // void set_geom(const Geom* geom);
    // CPT(Geom) get_geom() const;

    virtual void add_light_for_draw(int lod, int id, LMatrix4 transform);
    virtual bool safe_to_flatten() const;
    virtual bool safe_to_combine() const;

    void clear_lights();

    void set_max_num_lights(size_t n);
    size_t get_max_num_lights() const;
    size_t get_num_lights() const;    

    void add_lod(Geom* geom);
    size_t get_num_lods() const;

private:

    using GeomNode::add_geom;
    using GeomNode::get_num_geoms;

    size_t _max_lights;
    
    struct LodData {
        PTA_int _offset;
        PTA_int _ids;
        PTA_LMatrix4 _trans;
    };

    typedef pvector<LodData> Lods;

    Lods _lods;

    class DrawCallback: public CallbackObject {
    public:
        ALLOC_DELETED_CHAIN(DrawCallback);

        DrawCallback() = default;
        ~DrawCallback() = default;

        virtual void do_callback(CallbackData *cbdata);
    };

    PT(DrawCallback) _draw_cb;
};
#endif // __LIGHTLODS_H__