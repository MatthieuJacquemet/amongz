// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIGHTMESHRENDER_H__
#define __LIGHTMESHRENDER_H__


#include <panda3d/nodePath.h>

#include "lightData.h"
#include "lightLods.h"
#include "utils.h"


typedef ShortSizePoT TileSize;


class LightStage;
class CullBin;
class DisplayRegionDrawCallbackData;


class LightMeshRender {

public:
    LightMeshRender(LightStage* light_stage);
    ~LightMeshRender();

    void draw_lights(DisplayRegionDrawCallbackData* data);

    bool add_light_for_draw(int id, LightData::Type type, int lod,
                            CPT(TransformState) transform);


    void set_lod_geom(LightData::Type type, size_t lod, Geom* geom);
    const Geom* get_lod_geom(LightData::Type type, size_t lod) const;
    size_t get_num_lods(LightData::Type type) const;

    TileSize get_tile_size() const;
    size_t get_num_tile_x() const;
    size_t get_num_tile_y() const;

    GraphicsOutput* get_buffer() const;
    GraphicsOutput* get_debug_buffer() const;

    Texture* get_counter_texture() const;
    Texture* get_tiles_buffer() const;
    Texture* get_tiles_ids_buffer() const;
    Texture* get_tiles_min_depth_buffer() const;
    Texture* get_tiles_max_depth_buffer() const;

    static LightMeshRender* get_current_renderer(CullTraverser* trav);

    LightStage* get_stage() const;

    NodePath _assignment;

private:


    void setup_shaders();
    void setup_buffers();
    void setup_geom_lods();


    LightStage* _light_stage;

    PTA_int _instance_offset;

    size_t _num_tile_x;
    size_t _num_tile_y;
    size_t _num_light_per_tile;
    size_t _num_rendered_lights;
    size_t _max_rendered_lights;
    TileSize _tile_size;

    size_t _geom_lod;
    
    PT(GraphicsOutput) _buffer;
    PT(GraphicsOutput) _debug_buffer;

    NodePath _transforms;

    
    CPT(RenderState) _assignment_state;
    
    PT(Shader) _light_assignment;
    PT(Shader) _update_transforms;

    PT(ShaderBuffer) _light_types_ids;
    PT(Texture) _light_transforms;
    PT(Texture) _light_ids;


    PT(Texture) _light_counter;
    PT(Texture) _tile_buffer;
    PT(Texture) _tiles_ids;
    PT(Texture) _tiles_min_depth;
    PT(Texture) _tiles_max_depth;

    PT(CullBin) _debug_bin;


    typedef std::array<LightLods, LightData::T_none> LightPools;
    LightPools _lights_pools;

    int _hw_conservative_raster;
    
    typedef pmap<PT(GraphicsOutput), LightMeshRender*> Renderers;
    static Renderers _renderers;
    static LightMeshRender* _current_renderer;
};
#endif // __LIGHTMESHRENDER_H__