// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIGHTBUFFERMANAGER_H__
#define __LIGHTBUFFERMANAGER_H__


#include <panda3d/nodePath.h>
#include "lightManager.h"
#include "lightData.h"
#include "slotPool.h"
#include "utils.h"

class ShadowBufferManager;


class LightBufferManager: public LightManager {

    REGISTER_TYPE_WO_INIT

public:
    ALLOC_DELETED_CHAIN(LightBufferManager)

    virtual ~LightBufferManager() = default;

    ShaderBuffer* get_buffer();

    virtual void add_light(LightBase* light);
    virtual void remove_light(LightBase* light);
    virtual void add_light_for_draw(LightBase* light, CullTraverser* trav, CullTraverserData& data);

    ShadowBufferManager* get_shadow_manager() const;

    virtual size_t get_num_lights() const;

    size_t get_buffer_size() const;
    void set_buffer_size(size_t size);

    LightData::Type get_light_type(const TypeHandle type) const;
    LightData::Type get_light_type(LightBase* light) const;
    void map_light_type(const TypeHandle handle, LightData::Type type);
    bool has_light_type(LightData::Type type) const;

private:
    LightBufferManager();

    void setup_buffer();

    int _collect_frame;

    typedef SlotPool<PT(LightBase)> Lights;

    ShadowBufferManager* _shadow_manager;

    void init_shadow_maps();
    void update_light_buffer(GraphicsStateGuardian* gsg);
    void collect_light_data(LightBase* light, LightData& data) const;

    class CData: public CycleData {
    public:
        CData();
        CData(const CData& copy);
    
        virtual CycleData* make_copy() const override;
        virtual void write_datagram(BamWriter *manager, Datagram &dg) const;
        virtual void fillin(DatagramIterator &scan, BamReader *manager);
        virtual TypeHandle get_parent_type() const {
            return LightBufferManager::get_class_type();
        }

        Lights _lights;
        NodePath _update;
        size_t _update_buffer_size;
        size_t _light_buffer_size;
    };

    PipelineCycler<CData> _cycler;
    typedef CycleDataReader<CData> CDReader;
    typedef CycleDataWriter<CData> CDWriter;


    PT(ShaderBuffer) _light_buffer;
    PT(Texture) _update_buffer;
    PT(Shader) _update_lights;
    // LightManager* create();

    NodePath _wireframe;
    
    static LightManager* create();

    typedef pmap<TypeHandle, LightData::Type> LightTypes;
    LightTypes _light_types;

public:
    static void init_type() {
        LightManager::init_type();
        register_type(_type_handle, "LightBufferManager",
                        LightManager::get_class_type());
        
        LightManager::_instancer = create;
    }

    friend class LightStage;
};
#endif // __LIGHTBUFFERMANAGER_H__