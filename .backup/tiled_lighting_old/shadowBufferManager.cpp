// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsEngine.h>
#include "shadowBufferManager.h"

using namespace std;



ShadowBufferManager::ShadowBufferManager(GraphicsOutput* host) {

    _shadow_map = new Texture("shadow-maps");
    _shadow_map->setup_2d_texture_array(8, 1024, 1024, 
                                Texture::T_unsigned_byte, 
                                Texture::F_depth_component);

    _shadow_cubemap = new Texture("shadow-cubemaps");
    _shadow_cubemap->setup_cube_map_array(1024, 8,
                                Texture::T_unsigned_byte, 
                                Texture::F_depth_component);

    _shadow_map_buffer = create_buffer(_shadow_map, host);
    _shadow_cubemap_buffer = create_buffer(_shadow_cubemap, host);
}



void ShadowBufferManager::set_max_shadow_map(size_t n) {

    nassertv(n > 0)
    _num_shadow_map = n;
    _shadow_map->set_z_size(n);
}


void ShadowBufferManager::set_max_shadow_cubemap(size_t n) {
    
    nassertv(n > 0)
    _num_shadow_cubemap = n;
    _shadow_cubemap->set_z_size(n);
}


size_t ShadowBufferManager::get_max_shadow_map() const {
    
    return _shadow_map->get_z_size();
}


size_t ShadowBufferManager::get_max_shadow_cubemap() const {
    
    return _shadow_cubemap->get_z_size();
}


void ShadowBufferManager::set_shadow_map_size(ShadowSize size) {
    
    nassertv(size >= SST_128 && size <= SST_4096)

    size_t res = as_numeric(size);
    _shadow_map_size = size;
    _shadow_map->set_x_size(res);
    _shadow_map->set_y_size(res);
    _shadow_map_buffer->set_size_and_recalc(res, res);
}


void ShadowBufferManager::set_shadow_cubemap_size(ShadowSize size) {
    
    
    nassertv(size >= SST_128 && size <= SST_4096)

    size_t res = as_numeric(size);
    _shadow_cubemap_size = size;
    _shadow_cubemap->set_x_size(res);
    _shadow_cubemap->set_y_size(res);
    _shadow_cubemap_buffer->set_size_and_recalc(res, res);
}


GraphicsOutput* ShadowBufferManager::create_buffer(Texture* texture, GraphicsOutput* host) {

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    GraphicsPipe* pipe = nullptr;
    GraphicsStateGuardian* gsg = nullptr;

    if (host != nullptr) {
        pipe = host->get_pipe();
        gsg = host->get_gsg();
    }

    FrameBufferProperties fb_props;
    fb_props.set_multisamples(0);
    fb_props.set_color_bits(0);
    fb_props.set_accum_bits(0);
    fb_props.set_back_buffers(0);
    fb_props.set_depth_bits(24);

    WindowProperties win_props = WindowProperties::size(texture->get_x_size(), 
                                                        texture->get_y_size());


    GraphicsOutput* buffer = engine->make_output(pipe, texture->get_name(), 
                                0, fb_props, win_props,
                                GraphicsPipe::BF_refuse_window |
                                GraphicsPipe::BF_refuse_parasite |
                                GraphicsPipe::BF_resizeable,
                                gsg, host);


    texture->set_clear_color(LColor(1));
    texture->set_border_color(LColor(1));
    texture->set_wrap_u(SamplerState::WM_border_color);
    texture->set_wrap_v(SamplerState::WM_border_color);
    texture->set_minfilter(SamplerState::FT_shadow);
    texture->set_magfilter(SamplerState::FT_shadow);

    buffer->add_render_texture(texture, GraphicsOutput::RTM_bind_layered, 
                                        DrawableRegion::RTP_depth);

    return buffer;
}


void ShadowBufferManager::update() {
    

}

// ShadowSize ShadowBufferManager::get_size_from_resolution(size_t size) {

//     switch (size) {
//         case 64:    return SST_64;
//         case 128:   return SST_128;
//         case 256:   return SST_256;
//         case 512:   return SST_512;
//         case 1024:  return SST_1024;
//         case 2048:  return SST_2048;
//         case 4096:  return SST_4096;
//         default:    return SST_invalid;
//     }   
// }


// void ShadowBufferManager::set_max_shadow_map_size(ShadowMapSize level) {
    
//     nassertv(level >= 0 && level <= 5)
    
//     size_t resolution = get_max_shadow_map_resolution();
//     _max_shadow_map_size = level;

//     while (level > _shadow_maps.size()) {
//         ShadowMapBuffer shadow_map = 
//             ShadowMapBuffer::create_shadow_map( resolution, 
//                                                 _num_shadow_map_layer, 
//                                                 _host);
//         _shadow_maps.push_back(shadow_map);
//         resolution = resolution << 1;
//     }

//     while (level < _shadow_maps.size()) {
//         ShadowMapBuffer shadow_map = _shadow_maps.back();
//         DisplayRegion* region = shadow_map.get_buffer()->make_mono_display_region();
//         region->set_target_tex_page(0);
//     }
// }



// void ShadowBufferManager::set_max_shadow_cubemap_size(ShadowMapSize level) {

//     nassertv(level >= 0 && level <= 5)

//     size_t resolution = get_max_shadow_cubemap_resolution();
//     _max_shadow_cubemap_size = level;

//     while (level > _shadow_cubemaps.size()) {
//         _shadow_cubemaps.push_back(ShadowMapBuffer::create_shadow_cubemap(resolution));
//         resolution = resolution << 1;
//     }

//     while (level < _shadow_cubemaps.size())
//         _shadow_cubemaps.pop_back();
// }



// ShadowBufferManager::ShadowMapSize ShadowBufferManager::get_max_shadow_map_size() const {

//     return _max_shadow_map_size;    
// }


// Texture* ShadowMapBuffer::get_texture() const {
    
//     return _texture;
// }


// GraphicsOutput* ShadowMapBuffer::get_buffer() const {
    
//     return _buffer;
// }


// ShadowBufferManager::ShadowMapSize ShadowBufferManager::get_max_shadow_cubemap_size() const {
    
//     return _max_shadow_cubemap_size;
// }


// size_t ShadowBufferManager::get_max_shadow_map_resolution() const {
    
//     return get_resolution_from_size(_max_shadow_map_size);
// }


// size_t ShadowBufferManager::get_max_shadow_cubemap_resolution() const {
    
//     return get_resolution_from_size(_max_shadow_cubemap_size);
// }


// size_t ShadowBufferManager::get_resolution_from_size(ShadowMapSize size) {
    
//     return 0x1 << (size + 7);
// }




// void ShadowBufferManager::update_buffers() {
    
//     int required = max( _max_shadow_map_size,
//                         _max_shadow_cubemap_size);

//     size_t resolution = get_resolution_from_size(
//                             static_cast<ShadowMapSize>(_buffers.size()));

//     while (required > _buffer->get_num_display_regions()) {
//         GraphicsOutput* buffer = create_buffer(resolution);
//         buffer->add_render_texture()
//         _buffers.push_back(buffer);
//         resolution = resolution << 1;
//     }

//     while (required > _buffers.size())
//         _buffers.pop_back();
// }


// ShadowMapBuffer::~ShadowMapBuffer() {
    
// }


// ShadowMapBuffer::ShadowMapBuffer(const ShadowMapBuffer& copy):
//     _texture(copy._texture), _buffer(copy._buffer) 
// {
    
// }


// ShadowMapBuffer::ShadowMapBuffer() {
    
// }


// GraphicsOutput* ShadowMapBuffer::create_buffer( size_t resolution,
//                                                 GraphicsOutput* host)
// {
    
//     GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

//     GraphicsPipe* pipe = nullptr;
//     GraphicsStateGuardian* gsg = nullptr;

//     if (host != nullptr) {
//         pipe = host->get_pipe();
//         gsg = host->get_gsg();
//     }


//     FrameBufferProperties fb_props;
//     fb_props.set_multisamples(0);
//     fb_props.set_color_bits(0);
//     fb_props.set_accum_bits(0);
//     fb_props.set_back_buffers(0);
//     fb_props.set_depth_bits(24);

//     WindowProperties win_props = WindowProperties::size(resolution, resolution);
//     string name("shadow-buffer");

//     return engine->make_output( pipe, name + to_string(resolution),
//                                 0, fb_props, win_props,
//                                 GraphicsPipe::BF_refuse_window |
//                                 GraphicsPipe::BF_refuse_parasite |
//                                 GraphicsPipe::BF_resizeable,
//                                 gsg, host);
// }


// ShadowMapBuffer ShadowMapBuffer::create_shadow_map( size_t resolution,
//                                                     size_t num_layer,
//                                                     GraphicsOutput* host)
// {    
    
//     string name("shadow_map_");
//     Texture* texture = new Texture(name + to_string(resolution));

//     texture->setup_2d_texture_array(num_layer,
//                                 resolution, resolution, 
//                                 Texture::T_unsigned_byte, 
//                                 Texture::F_depth_component);

//     texture->set_clear_color(LColor(1));
//     texture->set_wrap_u(SamplerState::WM_border_color);
//     texture->set_wrap_v(SamplerState::WM_border_color);
//     texture->set_border_color(LColor(1));
//     texture->set_minfilter(SamplerState::FT_shadow);
//     texture->set_magfilter(SamplerState::FT_shadow);

//     ShadowMapBuffer object;
//     object._texture = texture;
//     object._buffer = create_buffer(resolution);
    
// }


// ShadowMapBuffer ShadowMapBuffer::create_shadow_cubemap( size_t resolution,
//                                                         size_t num_layer,
//                                                         GraphicsOutput* host)
// {
    
//     string name("shadow_cubemap_");
//     Texture* texture = new Texture(name + to_string(resolution));

//     texture->setup_cube_map_array(resolution, num_layer,
//                                 Texture::T_unsigned_byte, 
//                                 Texture::F_depth_component);

//     texture->set_clear_color(LColor(1));
//     texture->set_wrap_u(SamplerState::WM_border_color);
//     texture->set_wrap_v(SamplerState::WM_border_color);
//     texture->set_border_color(LColor(1));
//     texture->set_minfilter(SamplerState::FT_shadow);
//     texture->set_magfilter(SamplerState::FT_shadow);

//     ShadowMapBuffer instance;
//     instance._buffer = create_buffer(resolution, host);
//     instance._texture = texture;

//     return instance;
// }


// ShadowBufferManager::ShadowBufferManager(GraphicsOutput* host): _host(host),
//     _max_shadow_map_size(SM_1024), _max_shadow_cubemap_size(SM_1024)
// {

//     _buffer = create_buffer(get_resolution_from_size(SM_1024));

//     DisplayRegion* dr = _buffer->make_display_region();

//     Camera* cam = new Camera("cam");
//     NodePath cam_np(cam);

//     dr->set_camera(cam_np);

    

//     _buffer->add_render_texture(tex,GraphicsOutput::RTM_bind_layered, GraphicsOutput::RTP_depth);

//     set_max_shadow_cubemap_size(11)
// }


// ShadowBufferManager::~ShadowBufferManager() {
    
// }
