// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsOutput.h>
#include <panda3d/displayRegion.h>
#include "culledBins.h"

using namespace std;



void CulledBins::Group::clear() {
    _setups.clear();
}


void CulledBins::Group::draw() const {
    
    Thread* current_thread = Thread::get_current_thread();

    Setups::const_iterator it;

    for (it = _setups.begin(); it != _setups.end(); ++it) {

        SceneSetup* setup = it->first;
        DisplayRegion* dr = setup->get_display_region();
        GraphicsStateGuardian* gsg = dr->get_window()->get_gsg();

        bool force = !gsg->get_effective_incomplete_render();
        gsg->clear_state_and_transform();

        if (!gsg->set_scene(setup))
            display_cat.error() << gsg->get_type() 
                << "cannot set scene setup" << endl;

        else if (gsg->begin_scene()) {
            for (CullBin* bin: it->second) {
                gsg->push_group_marker(bin->get_name());
                bin->draw(force, current_thread);
                gsg->pop_group_marker();
            }
            gsg->end_scene();
        }
    }
}


void CulledBins::Group::add_bin(SceneSetup* setup, CullBin* bin) {
    
    _setups[setup].push_back(bin);
}


void CulledBins::Group::add_bin_name(const string& name) {
    
    _bins.insert(name);
}


void CulledBins::Group::remove_bin_name(const string& name) {
    
    _bins.erase(name);
}


bool CulledBins::Group::has_bin_name(const string& name) const {

    return _bins.find(name) != _bins.end();
}


CulledBins::Group::Setups CulledBins::Group::get_setups() {
    return _setups;
}


size_t CulledBins::add_bin_to_groups(SceneSetup* setup, CullBin* bin) {

    size_t count = 0;

    for (Groups::value_type it: _groups) {
        Group* group = it.second;
        if (group->has_bin_name(bin->get_name())) {
            group->add_bin(setup, bin);
            ++count;
        }
    }
    return count;
}


CulledBins::Group* CulledBins::add_group(const string& name) {
    
    Group* group = new Group;
    _groups[name] = group;
    
    return group;
}


void CulledBins::remove_group(const string& name) {
    
    Groups::iterator it = _groups.find(name);

    if (it != _groups.end())
        _groups.erase(it);
}


CulledBins::Group* CulledBins::get_group(const string& name) const{
    
    Groups::const_iterator found = _groups.find(name);
    if (found != _groups.end())
        return found->second;
    
    return nullptr;
}


void CulledBins::clear() {
    
    for (Groups::value_type it : _groups) 
        it.second->clear();
}
