#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices = 6) out;

uniform usamplerBuffer u_ids;

// uniform mat4 p3d_ModelViewProjectionMatrix;

uniform vec2 u_half_pixel_size;
uniform vec2 u_view_scale;
uniform vec3 u_normal_color;
uniform vec3 u_conservative_color;
uniform vec2 u_near_far;

flat out vec4 v_aabb;
flat in uint v_id[];
flat out uint v_light_id;

out vec4 v_pos;
out vec4 v_plane;
out vec2 v_delta;

#define THRESHOLD 0.0000125
#define FLT_MAX 9999999999999.0


#pragma include "light.inc.glsl"


layout(std430) buffer lightBuffer {

    LightData light_datas[];
};



// void main(void) {

//     vec4 vertices[3];
//     vec2 pos[3];
//     vec4 aabb = vec4(1.0, 1.0, -1.0, -1.0);

//     int vert_len = gl_in.length();

//     for (int i = 0; i < vert_len; ++i) {

// 		vec4 vert = p3d_ModelViewProjectionMatrix * gl_in[i].gl_Position;
//         vert.xy *= u_view_scale;

//         vertices[i] = vert; // / vert.w;
        
//         if (vert.w <= 0) {
//             aabb.xy = min(aabb.xy, vert.xy);
//             aabb.zw = max(aabb.zw, vert.xy);
//         }
//         else {
//             aabb.xy = min(aabb.xy, vert.xy / vert.w);
//             aabb.zw = max(aabb.zw, vert.xy / vert.w);
//         }
//     }

//     vec4 aabb_conservative = aabb + vec4(-u_half_pixel_size, u_half_pixel_size);
// 	vec3 planes[3];


//     v_plane.xyz = normalize(cross(  vertices[1].xyw - vertices[0].xyw, 
//                                     vertices[2].xyw - vertices[0].xyw));

//     vec4 trianglePlane;
        
//     trianglePlane.xyz = normalize(cross(vertices[1].xyz - vertices[0].xyz, vertices[2].xyz - vertices[0].xyz));
//     trianglePlane.w = -dot(vertices[0].xyz, trianglePlane.xyz);
    
//     if (trianglePlane.z == 0.0)
//     {
//         return;
//     }
    


//     for (int i = 0; i < vert_len; ++i) {

//         vec3 plane = cross(vertices[i].xyw, vertices[(i + 2) % 3].xyw);
//         plane.z -= dot(u_half_pixel_size, abs(plane.xy));
//         planes[i] = plane;
//     }

//     for (int i = 0; i < vert_len; ++i) {

//         if (dot(vertices[i].xyw, v_plane.xyz) < THRESHOLD)

//             gl_Position = vertices[i];

//         else {
//             vec3 intersect = cross(planes[i], planes[(i+1) % 3]);
//             intersect.xy /= intersect.z;

//             gl_Position.xy = intersect.xy;
//             gl_Position.w = sign(intersect.z);
//             gl_Position.z = 1.0; //-(trianglePlane.x * intersect.x + trianglePlane.y * intersect.y + trianglePlane.w) / trianglePlane.z;
//         }

//         v_plane.w = -dot(v_plane.xyz,vertices[(i+1) % 3].xyw);
//         v_delta = 0.5*sign(v_plane.xy);
//         v_aabb = aabb_conservative;
//         v_pos = gl_Position;

//         EmitVertex();
//     }
    
//     EndPrimitive();
// }

vec4 isect(vec4 posA,vec4 posB) {
	float t = (posB.w-u_near_far.x)/(posB.w-posA.w);
	return mix(posB, posA, t);
}

void extend_aabb(inout vec4 aabb, vec4 vert) {
    vert.xy /= vert.w;
    aabb.xy = min(aabb.xy, vert.xy);
    aabb.zw = max(aabb.zw, vert.xy);
}

void main(void)
{    
    vec4 vertex[3];
    vec2 texCoord[3];
    vec4 original_vert[3];

    mat4 mvp = light_datas[v_id[0]].transform;

    for (int i = 0; i < gl_in.length(); ++i) {
        
        vertex[i] = mvp * gl_in[i].gl_Position;
        original_vert[i] = vertex[i];
        vertex[i] /= vertex[i].w;
    }
    // if (original_vert[0].w < near && original_vert[1].w < near && original_vert[2].w < near)
    //     return;

    // Triangle plane to later calculate the new z coordinate.

    vec4 trianglePlane;
        
    trianglePlane.xyz = normalize(cross(vertex[1].xyz - vertex[0].xyz, vertex[2].xyz - vertex[0].xyz));
        
    trianglePlane.w = -dot(vertex[0].xyz, trianglePlane.xyz);
    
    // if (trianglePlane.z == 0.0)
    //     return;
    

    // AABB initialized with maximum/minimum NDC values.
    vec4 aabb = vec4(FLT_MAX, FLT_MAX, -FLT_MAX, -FLT_MAX);
    int n_clipped = 0;

    // Cull the vertices and find the bounding box of the triangle

    for (int i = 0; i < gl_in.length(); ++i) {

        vec4 current = original_vert[i];
        
        if (current.w < u_near_far.x) {

            vec4 next = original_vert[(i+1) % 3];
            vec4 next_next = original_vert[(i+2) % 3];

            if (next.w > u_near_far.x)
                extend_aabb(aabb, isect(next, current));

            if (next_next.w > u_near_far.x)
                extend_aabb(aabb, isect(next_next, current));

        } else
            extend_aabb(aabb, current);
    }

    // aabb = clamp(aabb, vec4(-1), vec4(1));
    // Add offset of half pixel size to AABB.
    v_aabb = aabb + vec4(-u_half_pixel_size, u_half_pixel_size);
        
    // Calculate triangle for conservative rasterization.
    
	vec3 plane[3];
	       
    for (int i = 0; i < gl_in.length(); ++i)
    {

		plane[i] = cross(vertex[i].xyw, vertex[(i + 2) % 3].xyw);
		
		plane[i].z -= dot(u_half_pixel_size, abs(plane[i].xy));
    }
        
    // Create conservative rasterized triangle.
    
    vec3 intersect[3];

    for (int i = 0; i < gl_in.length(); ++i)
    {
        intersect[i] = cross(plane[i], plane[(i+1) % 3]);
        
        // if (intersect[i].z == 0.0)
        //     return;

        intersect[i].xy /= intersect[i].z; 
    }

    v_plane.xyz = normalize(cross(  vertex[1].xyw - vertex[0].xyw, 
                                    vertex[2].xyw - vertex[0].xyw));

    for (int i = 0; i < gl_in.length(); ++i) {

        gl_Position.xyw = intersect[i];
        gl_Position.w = sign(original_vert[i].w);
        gl_Position.z = -(trianglePlane.x * intersect[i].x + trianglePlane.y * intersect[i].y + trianglePlane.w) / trianglePlane.z;

        // v_plane.w = -dot(v_plane.xyz,vertices[(i+1) % 3].xyw);
        v_delta = vec2(n_clipped, 0);
        v_pos.xy = intersect[i].xy / intersect[i].z;

        EmitVertex();
    }


    // for (int i = 0; i < gl_in.length(); ++i) {

    //     gl_Position = original_vert[i];
    //     EmitVertex();
    // }


    // for (i = 0; i < gl_in.length(); i++)
    // {
    //     gl_Position.xyw = intersect[i];
        
    //     // Calculate the new z-Coordinate derived from a point on a plane.
    //     gl_Position.z = -(trianglePlane.x * intersect[i].x + trianglePlane.y * intersect[i].y + trianglePlane.w) / trianglePlane.z;   


    //     EmitVertex();
    // }
    
    EndPrimitive();
}

// void main(void)
// {
//     int i;
    
//     vec4 vertex[3];
    
//     vec2 pos[3];

//     // Axis aligned bounding box (AABB) initialized with maximum/minimum NDC values.
//     vec4 aabb = vec4(1.0, 1.0, -1.0, -1.0);

//     for (i = 0; i < gl_in.length(); i++)
//     {
// 		vertex[i] = p3d_ModelViewProjectionMatrix * gl_in[i].gl_Position;
		
// 		// Later NDC position if each fragment. 
// 		pos[i] = vertex[i].xy / vertex[i].w;  

// 		// Get AABB of the triangle.
// 		aabb.xy = min(aabb.xy, pos[i].xy);

// 		aabb.zw = max(aabb.zw, pos[i].xy);
//     }
        
//     //
//     // Calculate triangle for conservative rasterization.
//     //
    
//     for (int i = 0; i < gl_in.length(); ++i) {

//         vec4 current = vertex[i];
        
//         if (current.w < u_near_far.x) {
//             vec4 next = vertex[(i+1) % 3];
//             vec4 next_next = vertex[(i+2) % 3];

//             if (next.w > u_near_far.x) {
//                 vec4 vert = isect(next, current);
//                 vert.xy /= vert.w;
//                 aabb.xy = min(aabb.xy, vert.xy);
//                 aabb.zw = max(aabb.zw, vert.xy);
//             }
//             if (next_next.w > u_near_far.x) {
//                 vec4 vert = isect(next_next, current);
//                 vert.xy /= vert.w;
//                 aabb.xy = min(aabb.xy, vert.xy);
//                 aabb.zw = max(aabb.zw, vert.xy);
//             }
//         } else {
//             current.xy /= current.w;
//             aabb.xy = min(aabb.xy, current.xy);
//             aabb.zw = max(aabb.zw, current.xy);
//         }
//     }

//     // Add offset of half pixel size to AABB.
//     vec4 aabbConservative = aabb + vec4(-u_half_pixel_size, u_half_pixel_size);

// 	//
    
// 	vec3 plane[3];

//     for (i = 0; i < gl_in.length(); i++)
//     {
//     	// Calculate the plane through each edge of the triangle.
//         // The plane equation is A*x + B*y + C*w = 0. Note, that D component from the plane is zero, as it goes throught the origin. Also, w is used as the third dimension.
//         //
//     	// Note: The plane goes through the origin, as (vertex[i].x, vertex[i].y, vertex[i].w) are vectors from the origin.
//     	// Note: A*x + B*y + C*w = 0 <=> A*x/w + B*y/w + C = 0 [w != 0] <=> A*xClip + B*yClip + C
//     	//       A*xClip + B*yClip + C is a vector in 2D space. In this case, it is the normal of the edge in 2D space.
//     	// 
//     	// By calculating the cross product of the two vectors (the end points of the edge), we gather the normal of the plane.
// 		plane[i] = normalize(cross(vertex[i].xyw, vertex[(i + 2) % 3].xyw));
		
// 		// Move plane, by adjusting C.
// 		//
// 		// Note: A*(x+dx) + B*(y+dy) + C*w = 0 [Another plane with a specific distance d given by dx and dy] <=>
// 		//       A*x + A*dx + B*y + B*dy + C*w = 0 <=>
// 		//       A*xClip + B*yClip + C + A*dx + B*dy = 0
// 		//
// 		// Half pixel size is d. Absolute of plane's xy, as the sign is already in the normal of the plane. 
// 		plane[i].z -= dot(u_half_pixel_size, abs(plane[i].xy));
//     }
    
//     // Create conservative, red rasterized triangle ... 
    
//     vec3 intersect;
        
//     for (i = 0; i < gl_in.length(); i++)
//     {
//     	// As both planes go through the origin, the intersecting line goes also through the origin. This simplifies the intersection calculation.
//     	// The intersecting line is perpendicular to both planes (see Wolfram MathWorld),
//     	// so the intersection line is just the cross product of both normals. 
//     	intersect = cross(plane[i], plane[(i+1) % 3]);
    
//     	// The line is a direction (x, y, w) but projects to the same point in window space.
//     	//
//     	// Compare: (x, y, w) <=> (x/w, y/w, 1) => (xClip, yClip)
//     	//
//     	gl_Position.xy = intersect.xy / intersect.z;
//         gl_Position.w = sign(intersect.z);
//         gl_Position.z = 1.0 ;
// 		// Later NDC position if each fragment.
// 		v_pos.xy = intersect.xy / intersect.z;
// 		v_aabb = aabbConservative;
		

//         EmitVertex();
//     }
    
//     EndPrimitive();
    
//     // ... and create "normal", blue rasterized triangle for comparison.
    
//     // for (i = 0; i < gl_in.length(); i++)
//     // {
//     // 	gl_Position = vertex[i];
		
// 	// 	v_pos = pos[i];
// 	// 	v_aabb = aabb;
		
// 	// 	v_color = vec4(0.0, 0.0, 1.0, 1.0);

//     //     EmitVertex();
//     // }
    
//     // EndPrimitive();            
// }