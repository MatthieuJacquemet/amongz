// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIGHTSTAGE_H__
#define __LIGHTSTAGE_H__

#include "renderStage.h"
#include "shaderState.h"
#include "culledBins.h"
#include "sceneData.h"
#include "utils.h"


class LightStage: public RenderStage {

    REGISTER_TYPE("LightStage", RenderStage)

public:
    using TileSize = ShortSizePoT;

    LightStage(RenderPipeline* pipeline=nullptr);
    virtual ~LightStage();

    virtual void draw(DisplayRegionDrawCallbackData* data) override;
    static LightStage* get_current_stage(CullTraverser* trav);


    INPUT_PORT(Texture, hi_depth)
    INPUT_PORT(Texture, normal)
    INPUT_PORT(Texture, accumulator)
    INPUT_PORT(Texture, specular_and_roughness)
    INPUT_PORT(CulledBins, culled_bins)
    INPUT_PORT(SceneData, scene_data)

    OUTPUT_PORT(Texture, light_volume)
    OUTPUT_PORT(Texture, output)

    const RenderState* get_binning_state() const;

protected:
    virtual void setup() override;
    virtual void init() override;

    virtual void dimensions_changed(LVector2i size) override;

private:
    int _hw_conservative_raster;
    bool _fragment_shader_interlock;

    void setup_buffers();
    void setup_textures();
    void setup_shaders();

    GraphicsOutput* _buffer;

    ShaderState _blit_depth;
    ShaderState _accumulate;
    ShaderState _binning;

    PTA_LVecBase2f _view_scale;
    PTA_LVecBase2i _screen_size;
    PTA_LVecBase2f _half_tile_size;
    PTA_LVecBase2f _depth_trans;

    size_t _num_tile_x;
    size_t _num_tile_y;


    PT(CulledBins::Group) _bins;

    PT(Texture) _debug_tex;
    PT(Texture) _debug_tex2;

    typedef std::map<GraphicsOutput*, PT(LightStage)> Stages;
    static Stages _stages;
    static PT(LightStage) _current;
};

#endif // __LIGHTSTAGE_H__