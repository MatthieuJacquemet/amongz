// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PBRPIPELINE_H__
#define __PBRPIPELINE_H__

#include "render_utils.h"
#include "shaderMaterial.h"
#include "shaderState.h"
#include "renderPipeline.h"


// class RenderCallbackData;



//  class RenderPass: public RenderScene {
    
//     REGISTER_TYPE("RenderPass", RenderScene)

// public:
//     ALLOC_DELETED_CHAIN(RenderPass);
    
//     using RenderTexturePlane = DrawableRegion::RenderTexturePlane;

//     struct Component {
//         std::string _name;
//         Texture* _texture;
//         RenderTexturePlane _plane;
//     };

//     RenderPass() = default;
//     ~RenderPass() = default;

//     void add_component(const Component& component);
//     void attach_targets(GraphicsBuffer* buffer);

// protected:
//     virtual void setup() override;

// private:
//     typedef std::vector<Component> Components;
//     Components _components;
// };



// class PbrPipeline: public RenderPipeline {

//     REGISTER_TYPE("PbrPipeline", RenderPipeline)

// public:
//     PbrPipeline(GraphicsOutput* host, LVector2i size);
//     virtual ~PbrPipeline() = default;

// private:
//     void setup_passes();
//     void setup_targets();
//     void setup_shader();

// protected:

//     PT(Texture) _depth_and_stencil;
//     PT(Texture) _diffuse_and_matID;
//     PT(Texture) _specular_and_roughness;
//     PT(Texture) _normal;

//     // extra targets
//     PT(Texture) _emissive;
//     PT(Texture) _velocity;
//     PT(Texture) _subsurface;

//     ShaderState _state;

// private:
//     typedef std::vector<PT(RenderPass)> Passes;

//     Passes _passes;
// };



MATERIAL("PBR standard opaque") {

    SHADING_LANGUAGE(GLSL)
    VERTEX_SHADER(GDK_SHADER_PATH "PBR.vert")
    FRAGMENT_SHADER(GDK_SHADER_PATH "PBR.frag")

    SHADER_PROP(Texture, "albedo")
    SHADER_PROP(Texture, "roughness")    
    SHADER_PROP(Texture, "metalness")
    SHADER_PROP(Texture, "normal map")  
};

#endif // __PBRPIPELINE_H__










// void make_pbr_pipeline(RenderPipeline* pipeline);
