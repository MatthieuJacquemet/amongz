// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "multiSoundManager.h"

using namespace std;



bool MultiAudioManager::is_valid() {
    return true;
}

PT(AudioSound) MultiAudioManager::get_sound(const Filename &file_name, bool positional, int mode) 
{
    
}


PT(AudioSound) MultiAudioManager::get_sound(const Filename &file_name, 
                bool positional, int mode)
{
    
}

PT(AudioSound) MultiAudioManager::get_sound(MovieAudio *source, 
                bool positional, int mode)
{
    
}

PT(AudioSound) MultiAudioManager::get_sound(MovieAudio *source, bool positional, int mode) 
{
    
}

void MultiAudioManager::uncache_sound(const Filename &file_name) 
{
    
}

void MultiAudioManager::clear_cache() 
{
    
}

void MultiAudioManager::set_cache_limit(unsigned int count) 
{
    
}

unsigned int MultiAudioManager::get_cache_limit() const 
{
    
}

void MultiAudioManager::set_volume(PN_stdfloat volume) 
{
    
}

PN_stdfloat MultiAudioManager::get_volume() const 
{
    
}

void MultiAudioManager::set_active(bool flag) 
{
    
}

bool MultiAudioManager::get_active() const 
{
    
}

void MultiAudioManager::set_concurrent_sound_limit(unsigned int limit override) 
{
    
}

unsigned int MultiAudioManager::get_concurrent_sound_limit() const 
{
    
}

void MultiAudioManager::reduce_sounds_playing_to(unsigned int count) 
{
    
}

void MultiAudioManager::stop_all_sounds() {
    
    int num_player = _map->get_num_player();
    for (int i=0; i<num_player; ++i) {
        Player* 
    }
}


MultiAudioManager::MultiAudioManager(Map* map): _map(map) {

}

MultiAudioSound::MultiAudioSound() 
{
    
}

void MultiAudioSound::play() 
{
    
}

void MultiAudioSound::stop() 
{
    
}

void MultiAudioSound::set_loop(bool loop) 
{
    
}

bool MultiAudioSound::get_loop() const 
{
    
}

void MultiAudioSound::set_loop_count(unsigned long loop_count) 
{
    
}

unsigned long MultiAudioSound::get_loop_count() const 
{
    
}

void MultiAudioSound::set_time(PN_stdfloat start_time) 
{
    
}

PN_stdfloat MultiAudioSound::get_time() const 
{
    
}

void MultiAudioSound::set_volume(PN_stdfloat volume) 
{
    
}

PN_stdfloat MultiAudioSound::get_volume() const 
{
    
}

void MultiAudioSound::set_balance(PN_stdfloat balance_right) 
{
    
}

PN_stdfloat MultiAudioSound::get_balance() const 
{
    
}

void MultiAudioSound::set_play_rate(PN_stdfloat play_rate) 
{
    
}

PN_stdfloat MultiAudioSound::get_play_rate() const 
{
    
}

void MultiAudioSound::set_active(bool flag) 
{
    
}

bool MultiAudioSound::get_active() const 
{
    
}

void MultiAudioSound::set_finished_event(const std::string& event) 
{
    
}

const std::string& MultiAudioSound::get_finished_event() const 
{
    
}

const std::string& MultiAudioSound::get_name() const 
{
    
}

PN_stdfloat MultiAudioSound::length() const 
{
    
}

void MultiAudioSound::set_3d_attributes(PN_stdfloat px, PN_stdfloat py, PN_stdfloat pz,
                                    PN_stdfloat vx, PN_stdfloat vy, PN_stdfloat vz) 
{
    
}

void MultiAudioSound::get_3d_attributes(PN_stdfloat *px, PN_stdfloat *py, PN_stdfloat *pz,
                                    PN_stdfloat *vx, PN_stdfloat *vy, PN_stdfloat *vz) 
{
    
}

bool MultiAudioSound::configure_filters(FilterProperties *config) {
    
}

SoundStatus MultiAudioSound::status() const {
    
}
