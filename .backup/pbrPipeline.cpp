// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsBuffer.h>
// #include "stages/opaque/PbrPipeline.h"
// #include "stages/forward/forwardStage.h"
// #include "stages/hiZ/downscaleDepth.h"
// #include "stages/overlay/overlayStage.h"
// #include "stages/lighting/lighting.h"

#include "pbrPipeline.h"


using namespace std;
using RenderTexturePlane = DrawableRegion::RenderTexturePlane;

// DEFINE_TYPEHANDLE(PbrPipeline)
// DEFINE_TYPEHANDLE(RenderPass)




// PbrPipeline::PbrPipeline(GraphicsOutput* host, LVector2i size):
//     RenderPipeline(host, size)
// {

//     FrameBufferProperties fb_prop;

//     fb_prop.set_force_hardware(true);
//     fb_prop.set_rgb_color(true);
//     fb_prop.set_stereo(false);
//     fb_prop.set_accum_bits(0);
//     fb_prop.set_rgba_bits(8,8,8,8);
//     fb_prop.set_depth_bits(24);
//     fb_prop.set_stencil_bits(8);
//     fb_prop.set_multisamples(0);
//     fb_prop.set_back_buffers(0);
//     fb_prop.set_aux_rgba(1);
//     fb_prop.set_aux_hrgba(1);

//     setup_buffer(fb_prop);
//     setup_targets();
//     setup_passes();
//     setup_shader();
// }


// void PbrPipeline::setup_passes() {

//     static const RenderPass::Component components[] = {
//         {"dynamic", _velocity, RenderTexturePlane::RTP_aux_hrgba_1},
//         {"emissive", _emissive, RenderTexturePlane::RTP_aux_hrgba_2},
//         {"subsurface", _subsurface, RenderTexturePlane::RTP_aux_rgba_1},
//     };
//     static const int num_passes = 3;


//     for (int bits = 1; bits < (0x1 << num_passes); ++bits) {
//         RenderPass* pass = new RenderPass;
//         add_stage(pass);

//         for (int i=0; i<num_passes; ++i) {
//             if ((bits >> i) & 0x1)
//                 pass->add_component(components[i]);
//         }
//     }
// }


// void PbrPipeline::setup_targets() {

//     _buffer->set_clear_color_active(false);
//     _buffer->set_clear_depth_active(true);
//     _buffer->set_clear_stencil_active(true);

//     _buffer->set_clear_active(DrawableRegion::RTP_aux_rgba_0, false);
//     _buffer->set_clear_active(DrawableRegion::RTP_aux_rgba_1, false);


//     _depth_and_stencil = add_target("depth and stencil", 
//         RenderTexturePlane::RTP_depth_stencil);

//     _diffuse_and_matID = add_target("diffuse and matID",
//         RenderTexturePlane::RTP_color);

//     _specular_and_roughness = add_target("specular and roughness",
//         RenderTexturePlane::RTP_aux_rgba_0);

//     _normal = add_target("normal",
//         RenderTexturePlane::RTP_aux_hrgba_0);

//     _emissive = make_target("esmissive");
//     _velocity = make_target("velocity");
//     _subsurface = make_target("subsurface");
// }


// void PbrPipeline::setup_shader() {
    

//     _state.set_shader(Shader::load(SHADER_LANG,
//                         GDK_SHADER_PATH VERT_SHADER("basic"),
//                         GDK_SHADER_PATH FRAG_SHADER("opaque")));

//     // _state.set_shader_input("u_resolution", _resolution);
//     // _state.set_shader_input("u_near_far", _near_far);
//     // _state.set_shader_input("u_view_angle", _view_angle);
// }


// void RenderPass::add_component(const Component& component) {

//     _components.push_back(component);
// }


// void RenderPass::attach_targets(GraphicsBuffer* buffer) {
    
//     static const auto mode = GraphicsOutput::RTM_bind_or_copy;
//     int num_targets = buffer->count_textures();

//     for (int i=0; i<num_targets; ++i) {
//         Texture* texture = buffer->get_texture(i);

//         DrawableRegion::RenderTexturePlane plane = 
//             buffer->get_texture_plane(i);

//         _buffer->add_render_texture(texture, mode, plane);
//     }

//     for (Component& component: _components)
//         _buffer->add_render_texture(component._texture, 
//             GraphicsOutput::RTM_bind_or_copy, component._plane);
// }


// void inc_fb_props(FrameBufferProperties& fb_props, RenderTexturePlane plane) {

//     switch (plane) {

//     case RenderTexturePlane::RTP_aux_rgba_0:
//     case RenderTexturePlane::RTP_aux_rgba_1:
//     case RenderTexturePlane::RTP_aux_rgba_2:
//     case RenderTexturePlane::RTP_aux_rgba_3:
//         fb_props.set_aux_rgba(fb_props.get_aux_rgba() + 1);
//         break;
//     case RenderTexturePlane::RTP_aux_hrgba_0:
//     case RenderTexturePlane::RTP_aux_hrgba_1:
//     case RenderTexturePlane::RTP_aux_hrgba_2:
//     case RenderTexturePlane::RTP_aux_hrgba_3:
//         fb_props.set_aux_hrgba(fb_props.get_aux_hrgba() + 1);
//         break;
//     case RenderTexturePlane::RTP_aux_float_0:
//     case RenderTexturePlane::RTP_aux_float_1:
//     case RenderTexturePlane::RTP_aux_float_2:
//     case RenderTexturePlane::RTP_aux_float_3:
//         fb_props.set_aux_float(fb_props.get_aux_float() + 1);
//         break;
//     default:
//         break;
//     }
// }


// void RenderPass::setup() {
    
//     GraphicsBuffer* buffer = _pipeline->get_buffer();
//     FrameBufferProperties fb_props = buffer->get_fb_properties();
//     string name;
    
//     for (Component& component: _components) {
//         name += "-" + component._name;
//         inc_fb_props(fb_props, component._plane);
//     }

//     add_bin_name(name);

//     CullBinManager* mgr = CullBinManager::get_global_ptr();
//     mgr->add_bin(name, CullBin::BT_state_sorted, 0);

//     if (setup_buffer(fb_props, name))
//         attach_targets(buffer);
// }





















// void make_pbr_pipeline(RenderPipeline* pipeline) {

    // ForwardStage* forward   = new ForwardStage(pipeline);
    // PbrPipeline* opaque     = new PbrPipeline(pipeline);
    // DownscaleDepth* hi_z         = new DownscaleDepth(pipeline);
    // OverlayStage* overlay   = new OverlayStage(pipeline);
    // LightStage* light       = new LightStage(pipeline);


    // opaque->culled_bins.connect(overlay->culled_bins);
    // opaque->culled_bins.connect(light->culled_bins);
    // opaque->culled_bins.connect(forward->culled_bins);

    // opaque->depth_and_stencil.connect(hi_z->depth);
    // opaque->depth_and_stencil.connect(forward->depth);

    // opaque->scene_data.connect(hi_z->scene_data);
    // opaque->scene_data.connect(light->scene_data);

    // opaque->normal.connect(light->normal);
    // opaque->accumulator.connect(light->accumulator);
    // opaque->specular_and_roughness.connect(light->specular_and_roughness);

    // hi_z->output.connect(light->hi_depth);

    // light->output.connect(forward->accumulator);

    // forward->output.connect(overlay->color);
    // overlay->output.connect(pipeline->final_render);
// }
