// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OPAQUESTAGE_H__
#define __OPAQUESTAGE_H__

#include <panda3d/drawableRegion.h>

#include "culledBins.h"
#include "sceneData.h"
#include "renderStage.h"
#include "shaderState.h"


class OpaqueStage: public RenderStage {

    REGISTER_TYPE("OpaqueStage", RenderStage)

public:
    OpaqueStage(RenderPipeline* pipeline=nullptr);
    OpaqueStage(const OpaqueStage& copy) = delete;
    virtual ~OpaqueStage() = default;

    virtual void draw(DisplayRegionDrawCallbackData* data) override;
    virtual void render(RenderCallbackData* data) override;
    virtual void cull(DisplayRegionCullCallbackData* data) override;

    OUTPUT_PORT(Texture, depth_and_stencil)
    OUTPUT_PORT(Texture, diffuse_and_matID)
    OUTPUT_PORT(Texture, specular_and_roughness)
    OUTPUT_PORT(Texture, normal)
    OUTPUT_PORT(Texture, emissive)
    OUTPUT_PORT(Texture, velocity)
    OUTPUT_PORT(Texture, subsurface)

    OUTPUT_PORT(Texture, accumulator)

    OUTPUT_PORT(SceneData, scene_data)
    OUTPUT_PORT(CulledBins, culled_bins)


protected:
    virtual void setup() override;
    virtual void update() override;
    virtual void init() override;

    virtual void dimensions_changed(LVector2i size) override;

private:
    using RenderTexturePlane = DrawableRegion::RenderTexturePlane;

    class RenderPass: public CallbackObject {
        
        REGISTER_TYPE("OpaqueStage::RenderPass", CallbackObject)

    public:
        ALLOC_DELETED_CHAIN(RenderPass);
        
        struct Component {
            std::string _name;
            Texture* _texture;
            RenderTexturePlane _plane;
        };

        RenderPass(OpaqueStage* stage);
        ~RenderPass() = default;

        void add_component(const Component& component);
        void finalize();

        virtual void do_callback(CallbackData*);

        void attach_default_targets(GraphicsOutput* buffer);

    private:
        GraphicsOutput* _buffer;
        PT(CulledBins::Group) _group;
    };


    typedef std::vector<PT(RenderPass)> RenderPasses;
    RenderPasses _passes;

    static TypedWritable* make_from_bam(const FactoryParams& params);

    ShaderState _state;

    bool is_main_region(DisplayRegion* region) const;

    void hook_display_regions();

    PT(GraphicsOutput) _gbuffer;

    PTA_LVecBase2f _near_far;
    PTA_LVecBase2f _view_angle;
    PTA_LVecBase2i _resolution;

    void setup_passes();

    void setup_target(OutputPort<Texture>& port);

    void setup_target(OutputPort<Texture>& port, GraphicsOutput* buffer,
                        RenderTexturePlane plane);
};



#endif // __OPAQUESTAGE_H__