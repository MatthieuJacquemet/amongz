// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CULLEDBINSHANDLER_H__
#define __CULLEDBINSHANDLER_H__


#include <panda3d/referenceCount.h>

class CullBin;
class SceneSetup;

class CulledBinsHandler: public ReferenceCount {

public:
    CulledBinsHandler() = default;
    ~CulledBinsHandler() = default;

    void clear();
    void add_bin(SceneSetup* setup, CullBin* bin);
    
    void draw(GraphicsOutput* buffer, Thread* current_thread) const;

protected:
    class Setup: public PointerTo<SceneSetup> {
    
    public:
        Setup(SceneSetup* setup);

        bool operator <(const Setup& other) const;

        void draw(GraphicsOutput* buffer, Thread* current_thread) const;
        void add_bin(CullBin* bin) const;

    private:
        typedef std::vector<PT(CullBin)> CullBins;
        mutable CullBins _bins;
    };

    typedef std::set<Setup> Setups;
    Setups _setups;
};

#endif // __CULLEDBINSHANDLER_H__