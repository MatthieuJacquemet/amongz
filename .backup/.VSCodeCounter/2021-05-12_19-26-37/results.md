# Summary

Date : 2021-05-12 19:26:37

Directory /home/matthieu/Projects/amongz

Total : 266 files,  12397 codes, 5473 comments, 8091 blanks, all 25961 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 265 | 12,315 | 5,473 | 8,055 | 25,843 |
| Markdown | 1 | 82 | 0 | 36 | 118 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 266 | 12,397 | 5,473 | 8,091 | 25,961 |
| src | 265 | 12,315 | 5,473 | 8,055 | 25,843 |
| src/amongz | 120 | 5,811 | 2,444 | 3,756 | 12,011 |
| src/amongz/player | 39 | 2,344 | 1,082 | 1,597 | 5,023 |
| src/amongz/player/controller | 21 | 175 | 350 | 164 | 689 |
| src/amongz/player/controller/states | 16 | 117 | 270 | 114 | 501 |
| src/amongz/player/fps | 8 | 1,028 | 231 | 596 | 1,855 |
| src/amongz/player/fps/player_input | 6 | 433 | 182 | 275 | 890 |
| src/amongz/player/view | 4 | 440 | 203 | 322 | 965 |
| src/amongz/weapon | 38 | 1,872 | 664 | 1,166 | 3,702 |
| src/amongz/weapon/ammo | 8 | 360 | 128 | 217 | 705 |
| src/amongz/weapon/attachment | 22 | 1,087 | 388 | 725 | 2,200 |
| src/amongz/zombie | 25 | 668 | 372 | 443 | 1,483 |
| src/amongz/zombie/controller | 19 | 322 | 282 | 238 | 842 |
| src/amongz/zombie/controller/states | 14 | 98 | 202 | 79 | 379 |
| src/amongz/zombie/view | 2 | 80 | 33 | 56 | 169 |
| src/gdk | 109 | 5,648 | 2,420 | 3,732 | 11,800 |
| src/gdk/anim | 12 | 469 | 175 | 288 | 932 |
| src/gdk/csv | 2 | 182 | 92 | 119 | 393 |
| src/gdk/fsm | 4 | 161 | 112 | 123 | 396 |
| src/gdk/input | 4 | 222 | 137 | 181 | 540 |
| src/gdk/light | 18 | 453 | 131 | 312 | 896 |
| src/gdk/pipeline | 9 | 324 | 152 | 232 | 708 |
| src/gdk/primitive | 4 | 176 | 116 | 121 | 413 |
| src/gdk/render_pipeline | 36 | 2,452 | 996 | 1,648 | 5,096 |
| src/gdk/render_pipeline/stages | 22 | 1,630 | 726 | 1,120 | 3,476 |
| src/gdk/render_pipeline/stages/forward | 2 | 100 | 33 | 54 | 187 |
| src/gdk/render_pipeline/stages/hiZ | 2 | 108 | 32 | 61 | 201 |
| src/gdk/render_pipeline/stages/opaque | 2 | 177 | 34 | 85 | 296 |
| src/gdk/render_pipeline/stages/ssr | 2 | 73 | 32 | 43 | 148 |
| src/gdk/render_pipeline/stages/tiled_lighting | 14 | 1,172 | 595 | 877 | 2,644 |
| src/gdk/viewport | 4 | 238 | 168 | 168 | 574 |
| src/tools | 35 | 841 | 593 | 559 | 1,993 |
| src/tools/asset_formater | 35 | 841 | 593 | 559 | 1,993 |

[details](details.md)