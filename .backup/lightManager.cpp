#include "lightManager.h"
#include "lightBase.h"
#include "lightCallbackData.h"


using namespace std;


Singleton<LightManager> LightManager::_instance;
LightManager::ManagerInstancer LightManager::_instancer;

DEFINE_TYPEHANDLE(LightManager)


LightManager* LightManager::create() {
    
    return new LightManager;
}


void LightManager::add_light(LightBase* light) {
    
}


void LightManager::remove_light(LightBase* light) {
    
}


void LightManager::add_light_for_draw(LightBase* light, CullTraverser* trav, CullTraverserData& data) {
    
}


size_t LightManager::get_num_lights() const {
    
    return 0;
}


LightManager* LightManager::get_global_ptr() {

    if (UNLIKELY(!_instance))
        _instance = _instancer();

    return _instance;
}
