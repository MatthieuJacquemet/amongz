#version 450


flat in vec4 v_aabb;
in vec4 v_pos;
in vec4 v_plane;
in vec2 v_delta;
flat in uint v_light_id;

uniform vec2 u_near_far;

coherent layout(r32ui) uniform uimage2D u_light_counter;
coherent layout(rgba32f) uniform image2DArray u_lights_tiles;
uniform usamplerBuffer u_ids;

#pragma include "transforms.inc.glsl"


void main() {

    ivec2 tex_size = imageSize(u_light_counter);

    vec2 pos = (gl_FragCoord.xy / vec2(tex_size.xy)) * 2.0 - 1.0;

    // discard fragment that are outside conservative triangle
	if (pos.x < v_aabb.x || pos.y < v_aabb.y || 
            pos.x > v_aabb.z || pos.y > v_aabb.w)
        discard;
    
    ivec2 coord = ivec2(gl_FragCoord.xy);


    uint counter = imageLoad(u_light_counter, coord).r;
    
    uint tile_id = floatBitsToUint(imageLoad(u_lights_tiles, ivec3(coord, counter-1)).r);
    // memoryBarrier();

    if (tile_id != v_light_id + 1)
        imageStore(u_light_counter, coord, uvec4(counter+1, 0,0,0));
    else 
        counter--;

    ivec3 array_coord = ivec3(coord, counter-1);


    float halfDepthRange = abs(fwidth(gl_FragCoord.z) * 0.5);
    float depth =  gl_FragCoord.z + halfDepthRange;

    float id = uintBitsToFloat(1);
    imageStore(u_lights_tiles, ivec3(coord, counter), vec4(id,0,0,0));

}