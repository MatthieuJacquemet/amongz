// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/texturePool.h>
#include <panda3d/globPattern.h>
#include <panda3d/virtualFileSystem.h>
#include <panda3d/colorBlendAttrib.h>
#include <panda3d/cullBin.h>
#include <panda3d/cullTraverserData.h>

#include "render_utils.h"
#include "defines.h"
#include "redDotSight.h"


using namespace std;

DEFINE_TYPEHANDLE(RedDotSight)


bool RedDotSight::_texture_loaded = false;
TextureCollection RedDotSight::_crosshairs;



void RedDotSight::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* RedDotSight::make_copy() const {
    return new RedDotSight(*this);
}

size_t RedDotSight::get_num_crosshair_texture() {
    return _crosshairs.size();
}

Texture* RedDotSight::get_crosshair_texture(size_t id) {
    
    nassertr(id < _crosshairs.size(), nullptr)
    return _crosshairs.get_texture(id);
}

Texture* RedDotSight::get_crosshair_texture(const std::string& name) {
    return _crosshairs.find_texture(name);
}

Color RedDotSight::get_default_color() const {
    return _data->_default_color;
}

Texture* RedDotSight::get_default_texture() const {
    return _data->_default_texture;
}


void RedDotSight::load_textures() {
    
    nassertv(_texture_loaded == false)
    _texture_loaded = true;

    Filename path(CROSSHAIR_PATH);

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFileList) files = vfs->scan_directory(path);

    if (files == nullptr)
        return;

    for (size_t i=0; i<files->get_num_files(); ++i) {
        
        VirtualFile* file = files->get_file(i);
        Filename filename = file->get_filename();
        Texture* tex = TexturePool::load_texture(filename);
        
        if (tex != nullptr) {
            tex->set_name(filename.get_basename_wo_extension());
            tex->set_wrap_u(SamplerState::WM_clamp);
            tex->set_wrap_v(SamplerState::WM_clamp);
            _crosshairs.add_texture(tex);
        }
    }
}

bool RedDotSight::cull_callback(CullTraverser* trav, CullTraverserData& data) {
    
    CPT(TransformState) modelview = data.get_modelview_transform(trav);
    float z = _lens_np.get_z();

    LVector4 pos = LVector4(0,0.8,z,1) * modelview->get_mat();
    _offset[0] = pos.get_xyz().normalized().get_xz();

    return true;
}


void RedDotSight::write_datagram(BamWriter* manager, Datagram& me) {
    
    Sight::write_datagram(manager, me);

    _data->_default_color.write_datagram(me);
    
    string texture_name = "";

    if (_data->_default_texture != nullptr)
        texture_name = _data->_default_texture->get_name();
    
    me.add_string(texture_name);
    _lens_np.write_datagram(manager, me);
}


void RedDotSight::fillin(DatagramIterator& scan, BamReader *manager) {

    Sight::fillin(scan, manager);

    if (!_texture_loaded)
        load_textures();

    _data->_default_color.read_datagram(scan);
    _data->_default_texture = _crosshairs.find_texture(scan.get_string());
    
    _crosshair_color[0] = _data->_default_color;
    _crosshair_texture = _data->_default_texture;

    _lens_np.fillin(scan, manager);

    for (int i=0; i<_crosshairs.size(); ++i) {
        if (_crosshairs.get_texture(i) == _crosshair_texture)
            _crosshair_id = i;
    }
}


int RedDotSight::complete_pointers(TypedWritable **plist, BamReader *manager) {
    
    return _lens_np.complete_pointers(plist, manager);
}


void RedDotSight::finalize(BamReader* reader) {
    
    Sight::finalize(reader);

    if (_lens_np.is_empty())
        return;

    _lens_np.set_shader(Shader::load(SHADER_LANG, 
                    SHADER_PATH VERT_SHADER("redDot"),
                    SHADER_PATH FRAG_SHADER("redDot")), 20);

    _lens_np.set_shader_input("u_crosshair", _crosshair_texture);

    _lens_np.set_bin("transparent", CullBin::BT_back_to_front);
    _lens_np.set_attrib(ColorBlendAttrib::make(ColorBlendAttrib::M_add));
}


void RedDotSight::r_copy_children(const PandaNode* from, InstanceMap& inst_map,
                               Thread* current_thread)
{
    Sight::r_copy_children(from, inst_map, current_thread);

    const RedDotSight* sight = DCAST(RedDotSight, from);
    nassertv(!sight->_lens_np.is_empty())

    InstanceMap::iterator it = inst_map.find(sight->_lens_np.node());

    if (it != inst_map.end()) {
        _lens_np = NodePath(it->second);
        _lens_np.set_shader_input("u_color", _crosshair_color);
        _lens_np.set_shader_input("u_offset", _offset);
    }
}


TypedWritable* RedDotSight::make_from_bam(const FactoryParams& params) {

    RedDotSight* me = new RedDotSight;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);
    manager->register_finalize(me);

    return me;
}


RedDotSight::RedDotSight(const string& name): 
    Sight(name),
    _data(new Data),
    _crosshair_id(0),
    _crosshair_color(1, 0.0f),
    _offset(1, 0.0f)
{
    set_cull_callback();
}


void RedDotSight::set_color(Color color) {

    _crosshair_color[0] = color;
}


Color RedDotSight::get_color() const {

    return LVecBase4f(_crosshair_color[0]);    
}


void RedDotSight::set_crosshair(size_t id) {

    nassertv(!_lens_np.is_empty());

    _crosshair_id = id;
    _crosshair_texture = get_crosshair_texture(id);
    _lens_np.set_shader_input("u_crosshair", _crosshair_texture);
}


size_t RedDotSight::get_crosshair() const {

    return _crosshair_id;
}


Texture* RedDotSight::get_texture() const {

    return _crosshair_texture;    
}

RedDotSight::RedDotSight(const RedDotSight& copy):
    Sight(copy),
    _data(copy._data),
    _crosshair_color(copy._crosshair_color),
    _crosshair_texture(copy._crosshair_texture),
    _offset(1, 0.0f),
    _crosshair_id(0)
{

}
