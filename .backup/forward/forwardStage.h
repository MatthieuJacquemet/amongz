// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FORWARDSTAGE_H__
#define __FORWARDSTAGE_H__


#include "renderStage.h"
#include "culledBins.h"


class DisplayRegionDrawCallbackData;


class ForwardStage: public RenderStage {

    REGISTER_TYPE("ForwardStage", RenderStage)

public:
    ForwardStage(RenderPipeline* pipeline=nullptr);
    ~ForwardStage();

    void draw(DisplayRegionDrawCallbackData* data) override;

    void add_bin_name(const std::string& name);
    void remove_bin_name(const std::string& name);

    INPUT_PORT(Texture, accumulator)
    INPUT_PORT(Texture, depth)
    INPUT_PORT(CulledBins, culled_bins)

    OUTPUT_PORT(Texture, output)

protected:
    ForwardStage();

    void setup() override;
    void init() override;

private:
    PT(GraphicsOutput) _buffer;
    PT(CulledBins::Group) _forward_bins;

    typedef std::unordered_set<std::string> BinNames;
    BinNames _bin_names;
};
#endif // __FORWARDSTAGE_H__