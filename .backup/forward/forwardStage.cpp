// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/displayRegionDrawCallbackData.h>

#include "forwardStage.h"
#include "renderPipeline.h"


using namespace std;

DEFINE_TYPEHANDLE(ForwardStage)


ForwardStage::ForwardStage(RenderPipeline* pipeline): 
    RenderStage("forward", pipeline)
{
    if (pipeline != nullptr)
        init();
}


ForwardStage::~ForwardStage() {
    
    if (culled_bins) {
        for (const string& name: _bin_names)
            culled_bins->remove_group(name);
    }
}


void ForwardStage::draw(DisplayRegionDrawCallbackData* data) {
    
    SceneSetup* setup = data->get_scene_setup();
    Thread* current_thread = Thread::get_current_thread();

    if (setup != nullptr)
        data->upcall();

    // now, draw forward pass, e.g : transparent objects

    if (_forward_bins != nullptr)
        _forward_bins->draw();
}


void ForwardStage::setup() {

    if (depth)
        _buffer->add_render_texture(depth,
                                GraphicsOutput::RTM_bind_or_copy,
                                DrawableRegion::RTP_depth_stencil);

    if (accumulator) {
        _buffer->add_render_texture(accumulator,
                                GraphicsOutput::RTM_bind_or_copy,
                                DrawableRegion::RTP_aux_hrgba_0);

        output = accumulator;
    }


    if (culled_bins) {
        _forward_bins = culled_bins->add_group("forward");

        for (const string& name: _bin_names)
            _forward_bins->add_bin_name(name);
    }
}


void ForwardStage::init() {
    
    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(false);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_depth_bits(24);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);
    fb_prop.set_aux_hrgba(1);


    _buffer = add_buffer(fb_prop, "fbuffer");
    _buffer->disable_clears();

    _buffer->make_mono_display_region()->set_draw_callback(_draw_callback);
    
    add_bin_name("forward");
    add_bin_name("transparent");
}


void ForwardStage::add_bin_name(const string& name) {
    
    _bin_names.insert(name);
}


void ForwardStage::remove_bin_name(const string& name) {
    
    _bin_names.erase(name);
}


ForwardStage::ForwardStage():
    RenderStage("forward")
{
    
}
