#version 450

#extension GL_ARB_shader_image_load_store : enable
#extension GL_ARB_fragment_shader_interlock: enable

// layout(early_fragment_tests) in;
layout(pixel_interlock_ordered) in;

flat in uint v_instanceID;

coherent volatile layout(rgba32ui) uniform uimage2DArray u_lightVolume;
uniform sampler2D u_linearDepth;

uniform vec2 u_nearFar;
uniform uint u_lightType;
uniform uint u_instanceOffset;

#pragma include "transforms.inc.glsl"

// light cell structure:
// |         r         |         g         |         b         |         a         |
// 0___________________4___________________8__________________12___13___14________16
// |     minDepth      |     maxDepth      |      lightID      |type|flag|         |
// |___________________|___________________|___________________|____|____|_________|
// 
// flags:                               F: FrontFace written
// 11______________________________12   B: BackFace written
//  | F | B | I |   |   |   |   |   |   I: Intersect
//  |___|___|__ |__ |__ |__ |_ _|__ |


#define LIGHT_TYPE_MASK     0xFF000000u
#define FRONT_WRITE_FLAG    0x00800000u
#define BACK_WRITE_FLAG     0x00400000u
#define INSERSECT_FLAG      0x00200000u


vec3 computeDepth(ivec2 coord) {

    vec3 depth;
    float z = gl_FragCoord.z; // + fwidth(gl_FragCoord.z) * 0.5;

    depth.x = get_linear_z_from_z(z, u_nearFar.x, u_nearFar.y); // linear depth
    depth.yz = texelFetch(u_linearDepth, coord, 4).xy; // max depth

    return depth;
}


void processCell(inout uvec4 cellData, in vec3 depth) {

    if (gl_FrontFacing)
        cellData.x = floatBitsToUint(depth.x);
    else {
        cellData.y = floatBitsToUint(min(depth.x, depth.z));
        if (depth.x > depth.y)
            cellData.w |= INSERSECT_FLAG;
    }
    cellData.w |= BACK_WRITE_FLAG << int(gl_FrontFacing);
}



void initCell(ivec3 cellCoord) {

    vec3 depth = computeDepth(cellCoord.xy);

    uvec4 cellData = uvec4(0);
    cellData.z = v_instanceID + u_instanceOffset;
    cellData.w = u_lightType << 24;

    processCell(cellData, depth);

    ++cellCoord.z;
    imageStore(u_lightVolume, cellCoord, cellData);
    imageStore(u_lightVolume, ivec3(cellCoord.xy, 0), uvec4(cellCoord.z));
}


void updateCell(ivec3 cellCoord, uvec4 cellData) {

    vec3 depth = computeDepth(cellCoord.xy);

    if (gl_FrontFacing)
        cellData.x = floatBitsToUint(depth.x);

    if (uintBitsToFloat(cellData.x) > depth.z) {
        imageStore(u_lightVolume, ivec3(cellCoord.xy, 0), uvec4(cellCoord.z - 1));
        return;
    }

    processCell(cellData, depth);
    imageStore(u_lightVolume, cellCoord, cellData);
}



void main() {

    ivec2 coord = ivec2(gl_FragCoord.xy);


    beginInvocationInterlockARB();


    uvec4 tileData = imageLoad(u_lightVolume, ivec3(coord, 0));
    ivec3 cellCoord = ivec3(coord, tileData.x);


    if (cellCoord.z == 0)
        initCell(cellCoord);
    else {
        uvec4 cellData = imageLoad(u_lightVolume, cellCoord);

        if (cellData.z == v_instanceID + u_instanceOffset) {
            // check if we have not already written the face
            // if ((cellData.w & (BACK_WRITE_FLAG << int(gl_FrontFacing))) == 0)
            if (gl_FrontFacing) {
                if ((cellData.w & FRONT_WRITE_FLAG) == 0)
                    updateCell(cellCoord, cellData);
            } else {
                if ((cellData.w & BACK_WRITE_FLAG) == 0)
                    updateCell(cellCoord, cellData);
            }
        } else
            initCell(cellCoord);
    }

    endInvocationInterlockARB();
}