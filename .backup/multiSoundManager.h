// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MULTIAUDIOMANAGER_H__
#define __MULTIAUDIOMANAGER_H__

#include <panda3d/audioManager.h>


class MultiAudioSound: public AudioSound {

public:
    MultiAudioSound();
    ~MultiAudioSound();

    virtual void play() override;
    virtual void stop() override;

    // loop: false = play once; true = play forever.  inits to false.
    virtual void set_loop(bool loop=true) override;
    virtual bool get_loop() const override;

    // loop_count: 0 = forever; 1 = play once; n = play n times.  inits to 1.
    virtual void set_loop_count(unsigned long loop_count=1) override;
    virtual unsigned long get_loop_count() const override;

    virtual void set_time(PN_stdfloat start_time=0.0) override;
    virtual PN_stdfloat get_time() const override;

    // 0 = minimum; 1.0 = maximum.  inits to 1.0.
    virtual void set_volume(PN_stdfloat volume=1.0) override;
    virtual PN_stdfloat get_volume() const override;

    // -1.0 is hard left 0.0 is centered 1.0 is hard right inits to 0.0.
    virtual void set_balance(PN_stdfloat balance_right=0.0) override;
    virtual PN_stdfloat get_balance() const override;

    // play_rate is any positive PN_stdfloat value.  inits to 1.0.
    virtual void set_play_rate(PN_stdfloat play_rate=1.0f) override;
    virtual PN_stdfloat get_play_rate() const override;

    // inits to manager's state.
    virtual void set_active(bool flag=true) override;
    virtual bool get_active() const override;

    virtual void set_finished_event(const std::string& event) override;
    virtual const std::string& get_finished_event() const override;


    virtual const std::string& get_name() const override;

    virtual PN_stdfloat length() const override;

    virtual void set_3d_attributes(PN_stdfloat px, PN_stdfloat py, PN_stdfloat pz,
                                    PN_stdfloat vx, PN_stdfloat vy, PN_stdfloat vz) override;

    virtual void get_3d_attributes(PN_stdfloat *px, PN_stdfloat *py, PN_stdfloat *pz,
                                    PN_stdfloat *vx, PN_stdfloat *vy, PN_stdfloat *vz) override;



    virtual bool configure_filters(FilterProperties *config);

    virtual SoundStatus status() const override;

private:
    MultiAudioManager* _manager;
}


class MultiAudioManager: public AudioManager {

public:
    MultiAudioManager(Map* map);
    virtual ~MultiAudioManager() = default;


    virtual bool is_valid() override;

    // Get a sound:
    virtual PT(AudioSound) get_sound(const Filename &file_name, bool positional = false, int mode=SM_heuristic) override;
    virtual PT(AudioSound) get_sound(MovieAudio *source, bool positional = false, int mode=SM_heuristic) override;

    PT(AudioSound) get_null_sound();

    virtual void uncache_sound(const Filename &file_name) override;
    virtual void clear_cache() override;
    virtual void set_cache_limit(unsigned int count) override;
    virtual unsigned int get_cache_limit() const override;

    virtual void set_volume(PN_stdfloat volume) override;
    virtual PN_stdfloat get_volume() const override;

    virtual void set_active(bool flag) override;
    virtual bool get_active() const override;

    virtual void set_concurrent_sound_limit(unsigned int limit override) override;
    virtual unsigned int get_concurrent_sound_limit() const override;

    virtual void reduce_sounds_playing_to(unsigned int count) override;

    virtual void stop_all_sounds() override;

                                                                                        
private:
    Map* _map;
};

#endif // __MULTIAUDIOMANAGER_H__