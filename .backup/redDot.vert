#version 450

uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform vec2 u_offset;

in vec4 p3d_Vertex;
in vec2 p3d_MultiTexCoord0;
in vec3 p3d_Normal;

out vec2 v_texcoord;
out vec2 v_crosshairCoord;
out vec3 normal;

void main() {

    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    v_texcoord = p3d_MultiTexCoord0;

    // vec4 coord = p3d_ModelViewMatrix * p3d_Vertex;
    // vec3 dir = normalize(coord.xyz) * mat3(p3d_ModelViewMatrix);


    vec4 coord = p3d_ModelViewMatrix * p3d_Vertex;
    // vec4 point = p3d_ModelViewMatrix * vec4(0,0.4,0,1);

    v_crosshairCoord = normalize(coord.xyz).xy - u_offset;
}