// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/frameBufferProperties.h>

#include "overlayStage.h"


using namespace std;


DEFINE_TYPEHANDLE(OverlayStage)


OverlayStage::OverlayStage(RenderPipeline* pipeline):
    RenderStage("overlay", pipeline)
{
    if (pipeline != nullptr)
        init();
}


void OverlayStage::init() {
    
    FrameBufferProperties fb_prop;

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(true);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(8,8,8,8);
    fb_prop.set_depth_bits(24);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);

    _buffer = add_buffer(fb_prop, "overlay");
    _buffer->disable_clears();
    
    DisplayRegion* dr = _buffer->make_mono_display_region();
    dr->set_draw_callback(_draw_callback);
    dr->disable_clears();

    CullBinManager* mgr = CullBinManager::get_global_ptr();
    mgr->add_bin("overlay", CullBinEnums::BT_unsorted, 0);
}


void OverlayStage::setup() {

    if (depth)
        _buffer->add_render_texture(depth, 
                                GraphicsOutput::RTM_bind_or_copy, 
                                DrawableRegion::RTP_depth_stencil);
    
    if (color) {
        _buffer->add_render_texture(color,
                                GraphicsOutput::RTM_bind_or_copy, 
                                DrawableRegion::RTP_color);
        output = color;
    }    

    if (culled_bins) {
        _group = culled_bins->add_group("overlay");
        _group->add_bin_name("overlay");
    }
}


void OverlayStage::draw(DisplayRegionDrawCallbackData* cbdata) {

    if (_group != nullptr)
        _group->draw();
}




