// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OVERLAYSTAGE_H__
#define __OVERLAYSTAGE_H__


#include "renderStage.h"
#include "culledBins.h"

class OverlayStage: public RenderStage {

    REGISTER_TYPE("OverlayStage", RenderStage)

public:
    OverlayStage(RenderPipeline* pipeline=nullptr);
    virtual ~OverlayStage() = default;

    INPUT_PORT(Texture, depth)
    INPUT_PORT(Texture, color)
    INPUT_PORT(CulledBins, culled_bins)

    OUTPUT_PORT(Texture, output)

protected:
    virtual void init() override;
    virtual void setup() override;

    virtual void draw(DisplayRegionDrawCallbackData* cbdata) override;

private:
    PT(GraphicsOutput) _buffer;
    PT(CulledBins::Group) _group;
};


#endif // __OVERLAYSTAGE_H__