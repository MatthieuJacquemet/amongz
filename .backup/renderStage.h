// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDERSTAGE_H__
#define __RENDERSTAGE_H__


#include <panda3d/callbackObject.h>
#include <panda3d/callbackGraphicsWindow.h>
#include <panda3d/nameUniquifier.h>
#include <string>

#include "pipelineNode.h"

class DisplayRegion;
class RenderPipeline;
class DisplayRegionDrawCallbackData;
class DisplayRegionCullCallbackData;



class RenderStage: public PipelineNode {

    REGISTER_TYPE("RenderStage", PipelineNode)

public:
    using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;

    RenderStage(const std::string& name="", RenderPipeline* pipeline=nullptr);
    RenderStage(const RenderStage& copy);
    virtual ~RenderStage() = default;

    virtual void cull(DisplayRegionCullCallbackData* data);
    virtual void draw(DisplayRegionDrawCallbackData* data);
    virtual void render(RenderCallbackData* data);

    virtual void dimensions_changed(LVector2i size);

    RenderPipeline* get_pipeline() const;

    size_t get_num_buffers() const;

    GraphicsOutput* get_buffer() const;
    GraphicsOutput* get_buffer(size_t n) const;
    GraphicsOutput* get_buffer(const std::string& name) const;

    void remove_buffer(GraphicsOutput* buffer);
    void remove_buffer(size_t n);

    int get_index() const;

    virtual void update();

    virtual void record_node(PandaNode* node, CullableObject* object, 
                            const CullTraverser* trav);
    class StageCallback: public CallbackObject {
    
        REGISTER_TYPE("RenderStage::StageCallback", StageCallback)

    public:
        ALLOC_DELETED_CHAIN(StageCallback);

        typedef void Func(RenderStage*, CallbackData*);

        StageCallback(RenderStage* stage, std::function<Func> func);
        ~StageCallback() = default;

    private:
        void do_callback(CallbackData* data) override;

        WPT(RenderStage) _stage;
        std::function<Func> _func;
    };

protected:

    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;
    virtual int complete_pointers(TypedWritable** p_list, BamReader* manager) override;

    void add_buffer(GraphicsOutput* buffer);

    GraphicsOutput* add_callback(int sort=0);

    GraphicsOutput* add_buffer(FrameBufferProperties fb_prop,
        const std::string& name, int sort=0, LVecBase2i size=LVecBase2i(0), 
        GraphicsOutput* host=nullptr);


    PT(StageCallback) _render_callback;
    PT(StageCallback) _cull_callback;
    PT(StageCallback) _draw_callback;

    RenderPipeline* _pipeline;

    typedef std::vector<PT(GraphicsOutput)> Buffers;
    Buffers _buffers;
    
    void do_traverse() final;
    virtual void setup();
    virtual void init();

    void set_name(const std::string& name);

private:
    friend class RenderPipeline;
};

#endif // __RENDERSTAGE_H__