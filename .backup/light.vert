#version 450

#pragma include "light.inc.glsl"

in vec4 p3d_Vertex;
flat out uint v_instanceID;
out vec3 v_depth;

uniform mat4x3 p3d_InstanceMatrix;
uniform mat4 p3d_ProjectionMatrix;

uniform usamplerBuffer u_ids;
uniform uint u_instance_offset;



void main() {

    uint light_id = texelFetch(u_ids, gl_InstanceID + int(u_instance_offset)).r;

    mat4 mvp = light_datas[light_id].transform;
    v_light_id = light_id;

    vec4 vert = p3d_Vertex;

    // if (u_instance_offset == 0)
    //     vert.xy = vert.xy*0.3 + vec2(0.5,0);
    // else
    //     vert.xy = vert.xy*0.3 - vec2(0.5,0);

    // int offset =  int(gl_InstanceID + u_instance_offset) * 4;

    // vec4 data0 = texelFetch(u_transforms, offset + 0);
    // vec4 data1 = texelFetch(u_transforms, offset + 1);
    // vec4 data2 = texelFetch(u_transforms, offset + 2);
    // vec4 data3 = texelFetch(u_transforms, offset + 3);


    // mvp = mat4(data0, data1, data2, data3);

    // gl_Position = p3d_ModelViewProjectionMatrix * vert;
    gl_Position = p3d_ProjectionMatrix * mvp * vert;
    v_depth.x = gl_Position.z;
    v_depth.y = gl_Position.w;
    v_depth.z = 1.0;
}
