// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SHADERATTRIBHELPER_H__
#define __SHADERATTRIBHELPER_H__


#include <panda3d/renderAttrib.h>

class Shader;
class FactoryParams;


class ShaderAttribHelper: public RenderAttrib {

private:
    ShaderAttribHelper(Shader* shader);

public:
    static CPT(ShaderAttribHelper) make(Shader* shader);
    ~ShaderAttribHelper() = default;

    static void register_with_read_factory();

    virtual TypeHandle get_type() const override;
    virtual int get_slot() const override;

private:
    virtual void write_datagram(BamWriter *manager, Datagram &dg) override;

    static TypedWritable* make_from_bam(const FactoryParams& params);

    PT(Shader) _shader;
};


#endif // __SHADERATTRIBHELPER_H__