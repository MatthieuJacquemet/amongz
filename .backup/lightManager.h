// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __LIGHTMANAGER_H__
#define __LIGHTMANAGER_H__


#include <panda3d/shaderBuffer.h>

#include "utils.h"


class CullTraverser;
class CullTraverserData;
class LightBase;

class LightManager: public TypedWritable {

    REGISTER_TYPE_WO_INIT

public:
    virtual ~LightManager() = default;

    virtual void add_light(LightBase* light);
    virtual void remove_light(LightBase* light);
    virtual void add_light_for_draw(LightBase* light, CullTraverser* trav, CullTraverserData& data);
    virtual size_t get_num_lights() const;

    static LightManager* get_global_ptr();

protected:
    LightManager() = default;

    typedef LightManager* (*ManagerInstancer)();
    static ManagerInstancer _instancer;

public:
    static void init_type() {
        TypedWritable::init_type();
        register_type(_type_handle, "LightManager",
                    TypedWritable::get_class_type());

        if (_instancer == nullptr)
            _instancer = create;
    }

private:
    static LightManager* create();
    static Singleton<LightManager> _instance;
};
#endif // __LIGHTMANAGER_H__