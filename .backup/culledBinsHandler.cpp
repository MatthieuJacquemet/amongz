// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsOutput.h>
#include <panda3d/displayRegion.h>
#include <panda3d/pStatGPUTimer.h>
#include "culledBinsHandler.h"

using namespace std;



void CulledBinsHandler::clear() {

    _setups.clear();
}


void CulledBinsHandler::draw(GraphicsOutput* buffer, Thread* current_thread) const {

    for (const Setup& setup: _setups)
        setup.draw(buffer, current_thread);
}


CulledBinsHandler::Setup::Setup(SceneSetup* setup):
    PointerTo<SceneSetup>(setup)
{
    
}


bool CulledBinsHandler::Setup::operator <(const Setup& other) const {

    DisplayRegion* dr0 = other->get_display_region();
    DisplayRegion* dr1 = (*this)->get_display_region();

    return dr0->get_sort() < dr1->get_sort();
}


void CulledBinsHandler::Setup::draw(GraphicsOutput* buffer, Thread* current_thread) const {

    SceneSetup* setup = p();

    DisplayRegion* dr = setup->get_display_region();
    GraphicsStateGuardian* gsg = buffer->get_gsg();

    bool force = !gsg->get_effective_incomplete_render();
    gsg->clear_state_and_transform();

    DisplayRegionPipelineReader dr_reader(dr, current_thread);
    buffer->change_scenes(&dr_reader);

    gsg->prepare_display_region(&dr_reader);

    if (dr_reader.is_any_clear_active()) {
        PStatGPUTimer timer(gsg, buffer->get_clear_window_pcollector(), 
                                current_thread);
        gsg->clear(dr_reader.get_object());
    }

    if (!gsg->set_scene(setup))
        display_cat.error() << gsg->get_type() 
            << "cannot set scene setup" << endl;

    else if (gsg->begin_scene()) {

        for (CullBin* bin: _bins) {
            gsg->push_group_marker(bin->get_name());
            bin->draw(force, current_thread);
            gsg->pop_group_marker();
        }
        gsg->end_scene();
    }
}


void CulledBinsHandler::Setup::add_bin(CullBin* bin) const {
    _bins.push_back(bin);
}


void CulledBinsHandler::add_bin(SceneSetup* setup, CullBin* bin) {

    auto it = _setups.insert(setup);
    it.first->add_bin(bin);
}












// void CulledBinsHandler::add_bin(SceneSetup* setup, CullBin* bin) {

//     // we search in reverse order because the requested setup is likely
//     // to be inserted recently

//     CulledPools::reverse_iterator it;

//     for (it = _pools.rbegin(); it != _pools.rend(); ++it) {
//         if (it->get_setup() == setup)
//             break;
//     }
//     if (it == _pools.rend()) {
//         _pools.push_back(setup);
//         it = _pools.rbegin();
//     }

//     it->add_bin(bin);
// }
