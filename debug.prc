# show-frame-rate-meter true
# show-buffers true
# bullet-filter-algorithm callback
# bullet-enable-contact-events true
# notify-level-Interval spam
# notify-level-dgraph spam
# notify-level-pgraph spam
# notify-level-glgsg spam
# notify-level-chan debug
# default-directnotify-level spam
# notify-level-event spam
# notify-level-device spam
# notify-level-gobj info
notify-level warning
notify-level-pstats error
garbage-collect-states-rate 0.01
garbage-collect-states true
shadow-update-rate 0.5

notify-level-shader info


gl-check-errors true
sync-video true

# notify-output debug.log

want-pstats false
pstats-gpu-timing 1
want-directtools true
framebuffer-multisample false
multisamples 0
load-file-type p3assimp
framebuffer-stencil true

hardware-animated-vertices true
bullet-filter-algorithm groups-mask
textures-power-2 none
win-fixed-size false
max-bullet-holes 20

texture-anisotropic-degree 16

model-cache-compiled-shaders false
model-cache-textures true
model-cache-compressed-textures true

texture-quality-level fastest
compressed-textures true

allow-incomplete-render true
preload-simple-textures 1
simple-image-size 16 16
# async-load-delay 0.1
allow-async-bind 1
loader-num-threads 2
loader-thread-priority normal

assert-abort true

gl-debug true
gl-debug-synchronous true
gl-enable-memory-barriers true
# gl-debug-abort-level error

gl-version 4 6

clip-plane-cull false
preload-textures false
texture-minfilter mipmap


lock-to-one-cpu false
support-threads true

max-lights-per-tile 256

load-display pandagl
audio-library-name p3openal_audio

framebuffer-hardware true
framebuffer-software false
basic-shaders-only false

# threading-model Cull/
threaded-physic false

hardware-skinning true
max-light-lods 4
render-pipeline pbr

model-cache-dir $XDG_CACHE_HOME/panda3d
model-cache-textures false

vfs-mount $MAIN_DIR/data /data
vfs-mount $MAIN_DIR/data/gdk /gdk

model-path /data
model-path /data/model
model-path /gdk/shader/include

window-title AmongZ
win-size 1280 720


watch-shader-path /data/shader
watch-shader-path /gdk/shader
watch-shader-path /gdk/shader/shadow
watch-shader-path /gdk/shader/include

shader-watch-poll-rate 1

point-shadow-slots 8
shadow-depth-bits 32
shadow-host-name main
shadow-main-cam-tag main
shadow-host-sort 49

gl-coordinate-system default
gl-force-fbo-color false

notify-level-gdk info
notify-level-express warning