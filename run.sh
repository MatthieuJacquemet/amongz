#!/usr/bin/env bash

INCLUDE_PATH="$(find src -type d -exec echo -n \ -I{} \;)"
INCLUDE_PATH="$INCLUDE_PATH -I/usr/local/include/bullet -I/usr/local/include/panda3d"

# echo $ROOT_INCLUDE_PATH
cling -std=c++11 -fno-rtti -g $INCLUDE_PATH src/amongz/main.cpp
#  -L/usr/local/lib