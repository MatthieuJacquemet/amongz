#include <panda3d/pandaSystem.h>
#include <panda3d/load_prc_file.h>
#include <panda3d/texturePool.h>
#include <panda3d/clockObject.h>

using namespace std;


int main() {

    ClockObject* clock = ClockObject::get_global_clock();


    Texture* texture = TexturePool::load_texture("data/model/m4a1_c.png");

    if (!texture->compress_ram_image())
        nout << "  couldn't compress " << texture->get_name() << "\n";

    texture->set_compression(Texture::CM_dxt1);


    Filename filename = texture->get_filename().get_filename_index(0);


    filename.set_extension("txo");

    if (!texture->write(filename)) {
        nout << "failed to write texture in " << filename << endl;
        return 1;
    }

    nout << "  Writing " << filename;
    
    if (texture->get_ram_image_compression() != Texture::CM_off)
        nout << " (compressed " << texture->get_ram_image_compression() << ")";

    nout << "\n";


    nout << "loading txo" << endl;
    double start = clock->get_real_time();
    Texture* loaded_tex = TexturePool::load_texture("data/model/m4a1_c.txo");

    nout << "loaded in " << clock->get_real_time() - start << endl;

    vector<unsigned char> buffer;
    size_t num_bytes = 8388835;

    nout << endl;

    start = clock->get_real_time();

    

    size_t bytes_read = 0;
    while (bytes_read < num_bytes) {
        size_t bytes_left = num_bytes - bytes_read;


        bytes_left = std::min(bytes_left, (size_t)4*1024*1024);
        double s = clock->get_real_time();
        buffer.resize(buffer.size() + bytes_left);
        nout << "buffer.resize " << bytes_left << " in " << clock->get_real_time() - s << endl;

        unsigned char *ptr = &buffer[bytes_read];
        memset(ptr, 0, bytes_left);


        bytes_read += bytes_left;
    }
    nout << "buffer resize " << clock->get_real_time() - start << endl;

    // vector<unsigned char, std::allocator<unsigned char>> b_buffer;
    // sizeof(std::allocator<long>)
    // double start = clock->get_real_time();
    // b_buffer.resize(8388835);
    // nout << "vector<unsigned char> " << clock->get_real_time() - start << endl;

    // start = clock->get_real_time();
    // PTA_uchar a_buffer;
    // a_buffer.v().assign(b_buffer.begin(), b_buffer.end() - 1);
    // nout << "PTA_uchar " << clock->get_real_time() - start << endl;
}