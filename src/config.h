// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CONFIG_H__
#define __CONFIG_H__

#ifdef __CLING__


#pragma cling load("librt")
#pragma cling load("libssl")
#pragma cling load("libcrypto")
// #pragma cling load("libld")
#pragma cling load("libfftw3")
#pragma cling load("libvorbisfile")
#pragma cling load("libvorbis")
#pragma cling load("libogg")
#pragma cling load("libjpeg")
#pragma cling load("libtiff")
#pragma cling load("libpng")
#pragma cling load("libz")
#pragma cling load("libOpenEXR")
#pragma cling load("libOpenEXRUtil")
#pragma cling load("libIlmThread")
#pragma cling load("libIex")
// #pragma cling load("libIexMath")
// #pragma cling load("libImath")
// #pragma cling load("libHalf")
#pragma cling load("libharfbuzz")
#pragma cling load("libfreetype")
#pragma cling load("libSM")
#pragma cling load("libICE")
#pragma cling load("libX11")
#pragma cling load("libXext")
#pragma cling load("libOpenGL")
#pragma cling load("libGLX")

#pragma cling add_include_path("/usr/local/include/panda3d")
#pragma cling add_library_path("/usr/local/lib")

#pragma cling load("/usr/local/lib/libpanda.so.1.11.0")
// #pragma cling load("libpandaexpress")
// #pragma cling load("/usr/local/lib/libpandaegg.so")
// #pragma cling load("/usr/local/lib/libpandafx.so")
// #pragma cling load("/usr/local/lib/libpandagl.so")
// #pragma cling load("/usr/local/lib/libp3dtool.so")
// #pragma cling load("libp3dtoolconfig")
// #pragma cling load("libp3interrogatedb")
// #pragma cling load("/usr/local/lib/libp3direct.so")
// #pragma cling load("/usr/local/lib/libp3ffmpeg.so")
// #pragma cling load("/usr/local/lib/libp3openal_audio.so")
// #pragma cling load("/usr/local/lib/libp3ptloader.so")
// #pragma cling load("/usr/local/lib/libp3bullet.so")
// #pragma cling load("/usr/local/lib/libp3ai.so")
// #pragma cling load("libp3prc")

// #pragma cling load("/usr/local/lib/libBulletDynamics.so")
// #pragma cling load("/usr/local/lib/libBulletCollision.so")
// #pragma cling load("libLinearMath")
// #pragma cling load("/usr/local/lib/libBulletSoftBody.so.2.89")

#endif



#define DATA_PATH "/data/"

#define PRC_PATH DATA_PATH
#ifdef NDEBUG
    #define COMMON_PRC PRC_PATH "release.prc"
#else
    #define COMMON_PRC PRC_PATH "debug.prc"
#endif


#endif // __CONFIG_H__