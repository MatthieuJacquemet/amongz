// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/eventHandler.h>
#include <panda3d/dataNode.h>
#include <panda3d/virtualFileSystem.h>
#include <panda3d/shaderAttrib.h>
#include <panda3d/inputDeviceManager.h>
#include <panda3d/shadeModelAttrib.h>
#include <panda3d/throw_event.h>
#include <panda3d/dataGraphTraverser.h>
#include <panda3d/bulletWorld.h>
#include <panda3d/bulletTriangleMesh.h>
#include <panda3d/bamFile.h>
#include <panda3d/bulletTriangleMeshShape.h>
#include <panda3d/asyncTask.h>
#include <panda3d/cIntervalManager.h>
#include <panda3d/modelPool.h>
#include <panda3d/texturePool.h>
#include <panda3d/fontPool.h>
#include <panda3d/texturePoolFilter.h>

#include "asyncTextureLoader.h"
#include "config_game.h"
#include "color.h"
#include "materialsPool.h"
#include "collisionHandler.h"
#include "viewportManager.h"
#include "config.h"
#include "testMap.h"
#include "game.h"

using namespace std;


DEFINE_TYPEHANDLE(Game)
Singleton<Game> Game::_global_ptr;



Game* Game::get_global_ptr() {

    if (UNLIKELY(!_global_ptr))
        _global_ptr = new Game;

    return _global_ptr;
}


void Game::cleanup() {

    _global_ptr.release();
}


GraphicsWindow* Game::get_window() const {
    
    return _window;
}



Game::Game():
    _event_handler(EventHandler::get_global_event_handler()),
    _data_root(new DataNode("root"))
{   
    nassertv(_global_ptr == nullptr)

    TexturePool* pool = TexturePool::get_global_ptr();
    pool->register_texture_type(AsyncTextureLoader::make, "txo png jpg");

    register_hook(callback(process_window_event), "window-event");
    
    if (garbage_collect_states)
        do_add_task(named_method(process_garbage_collect), 46);

    do_add_task(named_method(process_input),   -30);
    do_add_task(named_method(process_data),    -20);
    do_add_task(named_method(process_event),   -10);
    do_add_task(named_method(process_interval), 0);
    do_add_task(named_method(process_render),   50);
}


AsyncTask::DoneStatus Game::process_garbage_collect(AsyncTask *task) {

    TransformState::garbage_collect();
    RenderState::garbage_collect();

    return AsyncTask::DS_cont;
}


Game::~Game() {

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    engine->remove_all_windows();
    _event_handler->remove_all_hooks();

    delete _event_handler;

    if (_viewport_mgr != nullptr)
        delete _viewport_mgr;
}


void Game::close_window() {

    if (_window != nullptr) {

        GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
        engine->remove_window(_window);
    }
}


DataNode* Game::get_data_root() const {
    return _data_root;
}


ViewportManager* Game::get_viewport_manager() const {
    return _viewport_mgr;
}


AudioManager* Game::get_audio_manager(const string& name) const {

    PT(AudioManager)& mgr = _audio_mgrs[name];

    if (mgr.is_null())
        mgr = AudioManager::create_AudioManager();

    return mgr;
}


void Game::process_window_event(const Event* event) {

    if (event->get_num_parameters() == 1) {

        EventParameter param = event->get_parameter(0);
        const GraphicsOutput *window;
        DCAST_INTO_V(window, param.get_ptr());

        if (window != _window)
            return;
        
        if (!window->is_valid()) {
            close_window();
            set_exit_status(ES_success);
            AsyncTaskManager::get_global_ptr()->stop_threads();
        } else
            _viewport_mgr->update_layout();
    }
}


Map* Game::get_current_map() {
    return _current_map;
}


void Game::load_map(const std::string& name) {
    
    _current_map = Map::load(name);

    if (_current_map != nullptr) {
        _current_map->add_player(_current_map->make_player("player"));
        // _current_map->prepare_scene(_window->get_gsg());

        ModelPool::garbage_collect();
        TexturePool::garbage_collect();
        MaterialsPool::garbage_collect();
        FontPool::garbage_collect();
    }
}


void Game::start() {

    ApplicationFramework::start();

    _window = DCAST(GraphicsWindow, open_window("main"));

    if (_window == nullptr) {
        game_error("Unable to create window.");
        set_exit_status(ES_failure);
        return;
    }

    _window->set_clear_depth_active(false);
    _window->set_clear_stencil_active(false);
    _window->set_clear_color_active(true);
    _window->set_clear_color(Color::black());

    _viewport_mgr = new ViewportManager(_window, ViewportManager::LP_column);
    _viewport_mgr->set_margin(8);

    load_map("dead-end");
}


AsyncTask::DoneStatus Game::process_event(AsyncTask *task) {

    throw_event("NewFrame");
    _event_handler->process_events();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_interval(AsyncTask *task) {

    CIntervalManager* mgr = CIntervalManager::get_global_ptr();
    mgr->step();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_render(AsyncTask *task) {

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    if (engine != nullptr) {
        engine->render_frame();

        return AsyncTask::DS_cont;
    }
    return AsyncTask::DS_done;
}


AsyncTask::DoneStatus Game::process_data(AsyncTask *task) {

    DataGraphTraverser traverser;
    traverser.traverse(_data_root);

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_input(AsyncTask *task) {

    InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();

    mgr->update(); 

    return AsyncTask::DS_cont;
}





BulletWorld* get_physic_world() {

    Map* map = get_current_map();
    return map->get_physic_world();
}

AIWorld* get_ai_world() {

    Map* map = get_current_map();
    return map->get_ai_world();
}

Map* get_current_map() {
    
    Game* game = Game::get_global_ptr();
    return game->get_current_map();
}

SoundManager* get_sound_manager() {
    
    Map* map = get_current_map();
    return map->get_sound_manager();
}

ParticleSystemManager* get_particle_manager() {
    
    Map* map = get_current_map();
    return map->get_particle_manager();
}

PhysicsManager* get_physics_manager() {
    
    Map* map = get_current_map();
    return map->get_physics_manager();
}