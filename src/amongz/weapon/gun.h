// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __GUN_H__
#define __GUN_H__

#include <panda3d/animBundle.h>

#include "weapon.h"
#include "attachmentAnchor.h"
#include "attachment.h"
#include "muzzle.h"
#include "localRef.h"
#include "utils.h"


class Mag;
class AmmoData;
class Muzzle;
class AudioSound;


class Gun: public Weapon, public LocalRef<AttachmentBundle> {// private TaskPool {

    REGISTER_TYPE("Gun", Weapon)

public:
    using AttachmentBundle::AttachFlag;

    enum FireMode {
        FM_bolt_action  = 0x0, // special case, cannot be combined with other modes
        FM_semi_auto,
        FM_burst,
        FM_full_auto,
    };

    enum Class {
        C_unknown = 0,
        C_handgun,
        C_assault,
        C_submachine_gun,
        C_lightmachine_gun,
        C_sniper,
        C_launcher,
        C_shotgun
    };

    Gun(const Gun& copy);
    virtual ~Gun() = default;

    Class get_class() const;
    std::string get_class_name() const;

    bool pull_trigger();
    void release_trigger();

    bool is_firing() const;

    bool set_fire_mode(FireMode mode);
    FireMode get_fire_mode() const;

    FireMode toggle_fire_mode();
    
    void set_event_queue(EventQueue* queue);

    size_t get_fire_count() const;
    size_t get_num_rounds() const;
    void set_num_rounds(size_t rounds);

    size_t get_max_rounds() const;
    float get_mass() const;
    float get_muzzle_velocity() const;
    float get_fire_rate() const;
    float get_recoil() const;

    bool has_mode(FireMode mode) const;
    AmmoData* get_ammo() const;

    AnimBundle* get_anim(size_t id) const;
    size_t get_num_anim() const;

    virtual PandaNode* make_copy() const override;

    bool require_fully_complete() const;

    static void register_with_read_factory();

    using AttachmentBundle::attach;

    int get_num_fire_sound() const;
    Filename get_fire_sound(int id) const;

    void set_suppressed(bool suppressed);

protected:
    Gun(const std::string& name="");


    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;

        typedef pvector<PT(AnimBundle)> Anims;
        typedef pvector<Filename> Sounds;

        Sounds _fire_sounds;
        Sounds _suppressed_sounds;

        PT(AmmoData) _ammo_data;
        uint8_t _fire_modes;
        Anims _anims;
        Class _class;
        float _muzzle_velocity;
        float _mass;
        float _recoil;
        float _fire_rate;
    };

    const PT(Data) _data;

    virtual int complete_pointers(TypedWritable** p_list, BamReader* reader) override;
    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);

    bool do_fire();

    class FireTask final: public AsyncTask {
    public:
        FireTask(Gun& gun);
        ~FireTask();

        void start_fire();
        void stop_fire(bool force=false);
        size_t get_fire_count() const;

    private:
        AsyncTask::DoneStatus do_task() override;
        bool fire_one();

    private:
        Gun& _gun;
        AsyncTaskManager* _mgr;
        uint32_t _fire_count;
    };

    FireMode _current_mode;
    EventQueue* _queue;

    size_t _rounds;
    bool _is_fire;
    bool _has_suppressor;

    FireTask _fire_task;

};


// template<class T>
// T* find_attachment(Gun* gun, Gun::AttachFlag flag=Gun::MT_first) {
//     return DCAST(T, gun->get_attachment(T::get_class_type(), flag));
// }

inline Gun::FireMode operator|(Gun::FireMode a, Gun::FireMode b) {

    int res = static_cast<int>(a) | static_cast<int>(b);
    return static_cast<Gun::FireMode>(res);
}

#endif // __GUN_H__