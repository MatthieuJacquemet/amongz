// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ATTACHMENT_H__
#define __ATTACHMENT_H__


#include "lootableItem.h"
#include <panda3d/nodePath.h>
#include "attachmentAnchor.h"
#include "gdk/utils.h"


class Attachment: public LootableItem {

    REGISTER_TYPE("Attachment", LootableItem)

public:
    Attachment(const std::string& name="");
    virtual ~Attachment() = default;

    virtual bool attach(AttachmentBundle* bundle, AttachmentAnchor* anchor);
    virtual bool detach(AttachmentBundle* bundle, AttachmentAnchor* anchor);

    virtual void attached(AttachmentAnchor* anchor);
    virtual void detached(AttachmentAnchor* anchor);
};


#endif // __ATTACHMENT_H__