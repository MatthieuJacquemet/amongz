// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/texturePool.h>
#include <panda3d/shaderPool.h>
#include <panda3d/cullBin.h>

#include "defines.h"
#include "render_utils.h"
#include "assetManager.h"
#include "sight.h"


using namespace std;

DEFINE_TYPEHANDLE(Sight)


void Sight::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* Sight::make_copy() const {
    return new Sight(*this);
}

float Sight::get_foreground_fov() const {
    return _data->_fg_fov;    
}

float Sight::get_background_fov() const {
    return _data->_bg_fov;
}

float Sight::get_height() const {
    return _data->_height;
}


void Sight::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_float32(_data->_fg_fov);
    me.add_float32(_data->_bg_fov);
    me.add_float32(_data->_height);
}


void Sight::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_fg_fov = scan.get_float32();
    _data->_bg_fov = scan.get_float32();
    _data->_height = scan.get_float32();
}

void Sight::finalize(BamReader* reader) {
    
    NodePath np(this);
    NodePath reticle_np = np.find("**/reticle");
    if (!reticle_np.is_empty()) {
        reticle_np.set_bin("transparent", 0);
        reticle_np.set_transparency(TransparencyAttrib::M_alpha);
        reticle_np.set_shader(Shader::load(SHADER_LANG,
            GDK_SHADER_PATH VERT_SHADER("basic"),
            GDK_SHADER_PATH FRAG_SHADER("transparent")), 500);
    }
}


TypedWritable* Sight::make_from_bam(const FactoryParams& params) {

    Sight* me = new Sight;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);
    manager->register_finalize(me);

    return me;
}


Sight::Sight(const string& name):
    Attachment(name),
    _data(new Data)
{
    
}


Sight::Sight(const Sight& copy): Attachment(copy), _data(copy._data) {

}
