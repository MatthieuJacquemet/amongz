// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/texturePool.h>
#include <panda3d/shaderPool.h>

#include "assetManager.h"
#include "stock.h"


using namespace std;

DEFINE_TYPEHANDLE(Stock)


void Stock::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* Stock::make_copy() const {
    return new Stock(*this);
}


float Stock::get_recoil_factor() const {
    return _data->_recoil_factor;
}


void Stock::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_float32(_data->_recoil_factor);
}


void Stock::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_recoil_factor = scan.get_float32();
}


TypedWritable* Stock::make_from_bam(const FactoryParams& params) {

    Stock* me = new Stock;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Stock::Stock(const string& name):
    Attachment(name),
    _data(new Data)
{
    
}


Stock::Stock(const Stock& copy): Attachment(copy), _data(copy._data) {

}