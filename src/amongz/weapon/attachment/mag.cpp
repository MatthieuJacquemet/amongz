// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include "ammoData.h"
#include "defines.h"
#include "gun.h"
#include "mag.h"


using namespace std;

DEFINE_TYPEHANDLE(Mag)


string Mag::get_ammo() const {
    return _data->_ammo;
}


size_t Mag::get_capacity() const {
    return _data->_capacity;
}


void Mag::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* Mag::make_copy() const {
    return new Mag(*this);
}


void Mag::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_uint16(_data->_capacity);
    me.add_string(_data->_ammo);
}


void Mag::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_capacity = scan.get_uint16();
    _data->_ammo = scan.get_string();
}


TypedWritable* Mag::make_from_bam(const FactoryParams& params) {

    Mag* me = new Mag;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Mag::Mag(const string& name): 
    Attachment(name),
    _data(new Data)
{
    
}


bool Mag::attach(AttachmentBundle* bundle, AttachmentAnchor*) {

    PandaNode* node = bundle->get_node_path().node();

    if (node->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, node);

        AmmoData* ammo_data = gun->get_ammo();
        
        if (ammo_data->get_name() == _data->_ammo)
            return true;
    }

    return false;
}

Mag::Mag(const Mag& copy): 
    Attachment(copy), 
    _data(copy._data)
{
    
}
