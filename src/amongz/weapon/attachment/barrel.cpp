// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/texturePool.h>
#include <panda3d/shaderPool.h>

#include "gun.h"
#include "assetManager.h"
#include "barrel.h"


using namespace std;

DEFINE_TYPEHANDLE(Barrel)


void Barrel::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* Barrel::make_copy() const {
    return new Barrel(*this);
}


string Barrel::get_gun() const {
    return _data->_gun;
}


float Barrel::get_velocity_factor() const {
    return _data->_velocity_factor;
}

void Barrel::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_float32(_data->_velocity_factor);
    me.add_string(_data->_gun);
}


void Barrel::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_velocity_factor = scan.get_float32();
    _data->_gun = scan.get_string();
}


TypedWritable* Barrel::make_from_bam(const FactoryParams& params) {

    Barrel* me = new Barrel;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Barrel::Barrel(const string& name):
    Attachment(name),
    _data(new Data)
{
    
}


Barrel::Barrel(const Barrel& copy): 
    Attachment(copy),
    _data(copy._data) {

}


bool Barrel::attach(AttachmentBundle* bundle, AttachmentAnchor* anchor) {
    
    PandaNode* node = bundle->get_node_path().node();

    if (node->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, node);

        // check if the gun we are attaching to is compatible with this barrel
        if (gun->get_name() == _data->_gun)
            return true;
    }

    return false;
}
