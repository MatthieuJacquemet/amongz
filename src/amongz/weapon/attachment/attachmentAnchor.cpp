// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/nodePath.h>

#include "config_game.h"
#include "attachment.h"
#include "attachmentAnchor.h"

using namespace std;

DEFINE_TYPEHANDLE(AttachmentAnchor)


AttachmentAnchor::AttachmentAnchor(TypeHandle type): 
    PandaNode("attachment-anchor"),
    _type(type)
{

}


AttachmentAnchor::AttachmentAnchor(const AttachmentAnchor& copy):
    PandaNode(copy),
    _type(copy._type),
    _attachment(nullptr)
{

}


bool AttachmentAnchor::attach(AttachmentBundle* bundle, Attachment* attachment) 
{    
    if (attachment->is_of_type(_type) && detach(bundle)) {

        if (attachment->attach(bundle, this)) {
            NodePath np(this);
            _attachment_np = np.attach_new_node(attachment);
            _attachment = attachment;
            attachment->attached(this);

            return true;
        }
    }
    return false;
}


bool AttachmentAnchor::detach(AttachmentBundle* bundle) {
    
    if (_attachment == nullptr)
        return true;

    if (!_attachment->detach(bundle, this))
        return false;

    nassertr(!_attachment_np.is_empty(), false)

    _attachment->detached(this);
    _attachment_np.remove_node();
    _attachment.clear();

    return true;
}


TypeHandle AttachmentAnchor::get_attachment_type() {

    return _type;    
}


Attachment* AttachmentAnchor::get_attachment() const {
    
    return _attachment;
}


void AttachmentAnchor::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* AttachmentAnchor::make_copy() const {
    return new AttachmentAnchor(*this);
}


AttachmentAnchor::AttachmentAnchor():
    PandaNode("attachment-anchor"),
    _type(TypeHandle::none()),
    _attachment(nullptr) 
{
    
}



int AttachmentAnchor::complete_pointers(TypedWritable** p_list, BamReader* reader) {
    
    int n = PandaNode::complete_pointers(p_list, reader);
    _attachment = DCAST(Attachment, p_list[n++]);

    return n;
}


void AttachmentAnchor::write_datagram(BamWriter* manager, Datagram& me) {
    
    PandaNode::write_datagram(manager, me);
    me.add_string(_type.get_name());
    manager->write_pointer(me, _attachment);
}


void AttachmentAnchor::fillin(DatagramIterator& scan, BamReader *manager) {
    
    PandaNode::fillin(scan, manager);

    TypeRegistry* registry = TypeRegistry::ptr();
    string type_name = scan.get_string();
    _type = registry->find_type(type_name);
    manager->read_pointer(scan);
}


TypedWritable* AttachmentAnchor::make_from_bam(const FactoryParams& params) {

    AttachmentAnchor* me = new AttachmentAnchor(TypeHandle::none());
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


AttachmentBundle::AttachmentBundle(NodePath path): _np(path) {
    
}



Attachment* AttachmentBundle::get_attachment(TypeHandle type,
                                            AttachFlag flag) const
{
    AttachmentAnchor* anchor = get_anchor(type, flag);

    if (anchor != nullptr)
        return anchor->get_attachment();
    
    return nullptr;
}


bool AttachmentBundle::attach(Attachment* attachment, AttachFlag flag) {
    
    AttachmentAnchor* anchor = get_anchor(attachment->get_type(), flag);
    
    if (anchor != nullptr && anchor->attach(this, attachment))
        return true;

    return false;
}


bool AttachmentBundle::detach(TypeHandle type, AttachFlag flag) {

    AttachmentAnchor* anchor = get_anchor(type, flag);

    if (anchor != nullptr) {
        _cache[flag].erase(type);
        return anchor->detach(this);
    }
    return false;
}


bool AttachmentBundle::detach_all() {

    NodePathCollection anchors = _np.find_all_matches("**/-AttachmentAnchor");

    bool all_detached = true;

    for (int i=0; i<anchors.size(); ++i) {
        NodePath path = anchors.get_path(i);

        AttachmentAnchor* anchor = DCAST(AttachmentAnchor, path.node());
        all_detached &= anchor->detach(this);
    }
    // invalidate cache entries
    _cache.fill(CacheType());

    return all_detached;
}


size_t AttachmentBundle::preload_cache() {
    
    NodePathCollection anchors = _np.find_all_matches("**/-AttachmentAnchor");

    for (int i=0; i<anchors.size(); ++i) {
        NodePath path = anchors.get_path(i);

        AttachmentAnchor* anchor = DCAST(AttachmentAnchor, path.node());
        _cache[MT_exact].emplace(make_pair(anchor->_type, anchor));
    }

    _cache[MT_first] = _cache[MT_exact];
    _cache[MT_best]  = _cache[MT_exact];

    return anchors.size();
}


NodePath AttachmentBundle::get_node_path() const {

    return _np;
}


AttachmentAnchor* AttachmentBundle::find_anchor(TypeHandle type, 
                                                AttachFlag flag) const
{
    nassertr(!_np.is_empty(), nullptr);
    PandaNode* node = _np.node();

    switch (flag) {
    case MT_first:
        return r_find_anchor(node, type, true);
    case MT_exact:
        return r_find_anchor(node, type, false);
    case MT_best:
        CacheType result;
        r_find_all_anchors(node, result);
        return find_best(result, type);
    }
    return nullptr;
}


AttachmentAnchor* AttachmentBundle::r_find_anchor(PandaNode* node, 
                        TypeHandle type, bool match_derived) const
{

    if (node->is_of_type(AttachmentAnchor::get_class_type())) {
        AttachmentAnchor* anchor = DCAST(AttachmentAnchor, node);
        TypeHandle anchor_type = anchor->_type;

        if (anchor_type == TypeHandle::none())
            game_warning("encounter unknown attachment anchor");
        else if (anchor_type == type)
            return anchor;
        else if (match_derived && type.is_derived_from(anchor_type))
            return anchor;
    }

    PandaNode::Children children = node->get_children();

    for (size_t i=0; i<children.size(); ++i) {
        PandaNode* child = children.get_child(i);
        AttachmentAnchor* anchor = r_find_anchor(child, type, match_derived);

        if (anchor != nullptr)
            return anchor;
    }
    return nullptr;
}


void AttachmentBundle::r_find_all_anchors(PandaNode* node,
                                        CacheType& result) const
{

    if (node->is_of_type(AttachmentAnchor::get_class_type())) {
        AttachmentAnchor* anchor = DCAST(AttachmentAnchor, node);
        TypeHandle type = anchor->_type;

        if (type != TypeHandle::none())
            result.emplace(make_pair(type, anchor));
    }

    PandaNode::Children children = node->get_children();

    for (size_t i=0; i<children.size(); ++i)
        r_find_all_anchors(children.get_child(i), result);
}


AttachmentAnchor* AttachmentBundle::get_anchor(TypeHandle type, 
                                            AttachFlag flag) const 
{   
    // first look up the cache entries
    AttachmentAnchor* anchor = find_exact(_cache[flag], type);;

    if (anchor != nullptr)
        return anchor;

    // if no anchor was found with exact type, try to find an anchor node
    anchor = find_anchor(type, flag);

    if (anchor != nullptr)
        _cache[flag].emplace(make_pair(type, anchor));

    return anchor;
}









// void AttachmentBundle::add_anchor(AttachmentAnchor* anchor) {

//     _cache[anchor->_type] = anchor;
//     _np.attach_new_node(anchor);
// }


// void AttachmentBundle::register_with_read_factory() {

//     BamReader::get_factory()->register_factory(get_class_type(), make);
// }



// int AttachmentBundle::complete_pointers(TypedWritable** p_list, BamReader* reader) {

//     for (size_t i=0; i<_temp_num_anchors; ++i) {
//         AttachmentAnchor* anchor = DCAST(AttachmentAnchor, p_list[i]);
//         _anchors[anchor->_type] = anchor;
//     }

//     return _temp_num_anchors;
// }


// void AttachmentBundle::write_datagram(BamWriter* manager, Datagram& me) {
    
//     me.add_uint8(_anchors.size());

//     for (auto it=_anchors.begin(); it != _anchors.end(); ++it)
//         manager->write_pointer(me, it->second);
// }


// void AttachmentBundle::fillin(DatagramIterator& scan, BamReader *manager) {
    
//   _temp_num_anchors = scan.get_uint8();

//   for (size_t i=0; i < _temp_num_anchors; i++)
//     manager->read_pointer(scan);
// }


// TypedWritable* AttachmentBundle::make(const FactoryParams& params) {

//     AttachmentBundle* me = new AttachmentBundle;
//     DatagramIterator scan;
//     BamReader* manager;

//     parse_params(params, scan, manager);
//     me->fillin(scan, manager);

//     return me;
// }
