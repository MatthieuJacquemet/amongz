// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/texturePool.h>
#include <panda3d/shaderPool.h>
#include <panda3d/bulletRigidBodyNode.h>
#include <panda3d/bulletSphereShape.h>
#include <panda3d/bulletWorld.h>
#include <panda3d/particleSystem.h>
#include <panda3d/pointEmitter.h>
#include <panda3d/spriteParticleRenderer.h>
#include <panda3d/pointParticleRenderer.h>
#include <panda3d/zSpinParticleFactory.h>
#include <panda3d/particleSystemManager.h>
#include <panda3d/physicalNode.h>
#include <panda3d/cardMaker.h>
#include <panda3d/sequenceNode.h>
#include <panda3d/physicsManager.h>
#include <panda3d/stencilAttrib.h>

#include "shaderState.h"
#include "render_utils.h"
#include "shaderLoader.h"
#include "game.h"
#include "map.h"
#include "gun.h"
#include "ammoData.h"
#include "assetManager.h"
#include "viewRoot.h"
#include "muzzle.h"


using namespace std;

DEFINE_TYPEHANDLE(Muzzle)


void Muzzle::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* Muzzle::make_copy() const {
    return new Muzzle(*this);
}


float Muzzle::get_recoil_factor() const {
    return _data->_recoil_factor;
}

float Muzzle::get_length() const {
    return _data->_length;
}


void Muzzle::fire(Gun* gun) {

    Map* map = get_current_map();
    BulletWorld* world = get_physic_world();


    Bullet* bullet = new Bullet(world, gun);
    NodePath bullet_np = map->attach_new_node(bullet);

    bullet->set_into_collide_mask(CollideMask::all_off());

    NodePath np(this);

    CPT(TransformState) transform = querry_net_transform(np);
    LMatrix4 mat = transform->get_mat();
    
    LVector4 dir = mat.get_row(1);
    LVector4 offset = dir * _data->_length;

    mat.set_row(3, mat.get_row(3) + offset);

    bullet->set_linear_velocity(dir.get_xyz() *  gun->get_muzzle_velocity());
    bullet_np.set_mat(mat);
    bullet_np.reparent_to(*map); 

    bullet->fired();

    LPoint3 pos = np.get_pos(_particle_sys->get_physical_node_path());

    _emitter->set_location(transform->get_pos() + offset.get_xyz());
    _emitter->set_explicit_launch_vector(dir.get_xyz());

    _particle_sys->birth_litter();
}


void Muzzle::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_float32(_data->_recoil_factor);
    me.add_float32(_data->_length);
}


void Muzzle::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_recoil_factor = scan.get_float32();
    _data->_length = scan.get_float32();
}


NodePath create_sprite(Texture* tex, int nx, int ny) {

    // static CardMaker cm("sprite maker");
    
    // NodePath np(new SequenceNode("sprite-" + tex->get_name()));

    // LVector2 delta(1.0f / nx, 1.0f / ny);
    
    // for (int y=0; y<ny; ++y) {
    //     for (int x=0; x<nx; ++x) {
    //         LTexCoord ll(delta.get_x() * x, delta.get_y() * y);

    //         cm.set_uv_range(ll, ll + delta);
    //         NodePath path = np.attach_new_node(cm.generate());
    //         path.set_texture(tex);
    //     }
    // }
    NodePath np(Loader::get_global_ptr()->load_sync("sprite.bam"));
    return np;
}


void Muzzle::setup_particles() {
    
    Filename smoke_path(TEXTURE_PATH, "smoke.png");

    ParticleSystemManager* mgr = get_particle_manager();
    Texture* tex = TexturePool::load_texture(smoke_path);
    NodePath sp = create_sprite(tex, 8, 4);








    ZSpinParticleFactory* factory = new ZSpinParticleFactory();
    SpriteParticleRenderer* renderer = new SpriteParticleRenderer();

    _particle_sys->set_emitter(_emitter);
    _particle_sys->set_factory(factory);
    _particle_sys->set_renderer(renderer);

    _particle_sys->set_pool_size(32);
    _particle_sys->set_birth_rate(HUGE_VALF);
    _particle_sys->set_litter_size(1);
    _particle_sys->set_litter_spread(0);
    _particle_sys->set_system_lifespan(0.0000);
    _particle_sys->set_local_velocity_flag(1);
    _particle_sys->set_system_grows_older_flag(0);
    // Factory parameters
    factory->set_lifespan_base(0.7000);
    factory->set_lifespan_spread(0.1000);
    factory->set_mass_base(1.0000);
    factory->set_mass_spread(0.0000);
    factory->set_terminal_velocity_base(400.0000);
    factory->set_terminal_velocity_spread(0.0000);
    // Z Spin factory parameters
    factory->set_initial_angle(0.0000);
    factory->set_initial_angle_spread(360.0);
    factory->enable_angular_velocity(1);
    factory->set_angular_velocity(0.0000);
    factory->set_angular_velocity_spread(0.0000);
    // Renderer parameters
    renderer->set_alpha_mode(BaseParticleRenderer::PR_ALPHA_OUT);
    renderer->set_user_alpha(0.050);
    // Sprite parameters
    renderer->set_from_node(sp);//, "models/sprite.bam","**/*");
    renderer->set_color(LColor(1.00, 1.00, 1.00, 1.00));
    renderer->set_x_scale_flag(1);
    renderer->set_y_scale_flag(1);
    renderer->set_anim_angle_flag(1);
    renderer->set_initial_x_scale(0.0000);
    renderer->set_final_x_scale(1.5000);
    renderer->set_initial_y_scale(0.0000);
    renderer->set_final_y_scale(1.5000);
    renderer->set_nonanimated_theta(0.0000);
    renderer->set_animate_frames_rate(60.0);
    renderer->set_animate_frames_enable(true);
    renderer->set_alpha_blend_method(BaseParticleRenderer::PP_BLEND_CUBIC);
    renderer->set_alpha_disable(0);
    renderer->set_color_blend_mode( ColorBlendAttrib::M_add,
                                    ColorBlendAttrib::O_incoming_alpha,
                                    ColorBlendAttrib::O_one_minus_incoming_alpha);
    // Emitter parameters
    _emitter->set_emission_type(BaseParticleEmitter::ET_EXPLICIT);
    _emitter->set_amplitude(5.0000);
    _emitter->set_amplitude_spread(0.0000);
    _emitter->set_offset_force(LVector3(0.0000, 0.0000, 0.0000));
    _emitter->set_explicit_launch_vector(LVector3(0.0000, 1.0000, 0.0000));
    _emitter->set_radiate_origin(LPoint3(0.0000, 0.0000, 0.0000));



    mgr->attach_particlesystem(_particle_sys);

    PhysicalNode* phn = new PhysicalNode("muzzle_smoke");
    phn->add_physical(_particle_sys);

    get_physics_manager()->attach_physical_node(phn);




    LocalRef<NodePath> np(this, this);
    NodePath render_np = renderer->get_render_node_path();
    NodePath physic_np = _particle_sys->get_physical_node_path();

    ShaderState state;
    state.set_shader(ShaderLoader::load(
        GDK_SHADER_PATH "softParticle.vert",
        GDK_SHADER_PATH "softParticle.frag"));

    state.set_shader_input("u_transitionSize", PTA_float(1, 2.0f));
    state.set_shader_input("u_texture", tex);
    state.set_depth_write(false, RenderState::get_max_priority());
    state.set_flag(ShaderAttrib::F_shader_point_size, true);
    
    render_np.set_attrib(StencilAttrib::make_off(), RenderState::get_max_priority());
    render_np.set_tag("main", "post-opaque");
    render_np.set_attrib(state);
    
    DrawMask mask = DrawMask::all_on();
    mask.clear_bit(Map::RL_transparent);

    render_np.hide(mask);

    render_np.reparent_to(*get_current_map());
    physic_np.reparent_to(*get_current_map());

}


TypedWritable* Muzzle::make_from_bam(const FactoryParams& params) {

    Muzzle* me = new Muzzle;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Muzzle::Muzzle(const string& name):
    Attachment(name),
    _particle_sys(nullptr),
    _emitter(nullptr),
    _data(new Data)
{

}


Muzzle::Muzzle(const Muzzle& copy): 
    Attachment(copy),
    _data(copy._data),
    _particle_sys(new ParticleSystem),
    _emitter(new PointEmitter)
{
    setup_particles();
}







    // ZSpinParticleFactory* factory = new ZSpinParticleFactory();
    // factory->set_lifespan_base(0.7f);
    // factory->set_lifespan_spread(0.1f);
    // factory->set_terminal_velocity_base(400.0f);
    // factory->set_initial_angle_spread(180.0f);

    // factory->enable_angular_velocity(true);

    // SpriteParticleRenderer* renderer = new SpriteParticleRenderer();
    // renderer->set_alpha_mode(BaseParticleRenderer::PR_ALPHA_OUT);
    // renderer->set_alpha_blend_method(BaseParticleRenderer::PP_BLEND_LINEAR);

    // renderer->set_color_blend_mode( ColorBlendAttrib::M_add,
    //                                 ColorBlendAttrib::O_incoming_alpha,
    //                                 ColorBlendAttrib::O_one_minus_incoming_alpha);

    // renderer->set_final_x_scale(1.5f);
    // renderer->set_final_y_scale(1.5f);
    // renderer->set_initial_x_scale(0.0f);
    // renderer->set_initial_y_scale(0.0f);
    // renderer->set_x_scale_flag(true);
    // renderer->set_y_scale_flag(true);
    // renderer->set_anim_angle_flag(true);
    // renderer->set_user_alpha(0.05f);
    // renderer->set_animate_frames_rate(60.0);
    // renderer->set_animate_frames_enable(true);
    // renderer->set_from_node(sp);


    // _emitter->set_emission_type(BaseParticleEmitter::ET_EXPLICIT);
    // _emitter->set_amplitude(5.0f);

    // _particle_sys->set_emitter(_emitter);
    // _particle_sys->set_factory(factory);
    // _particle_sys->set_renderer(renderer);
    // _particle_sys->set_birth_rate(HUGE_VALF);
    // _particle_sys->set_pool_size(1024);
    // _particle_sys->set_litter_size(10);
    // _particle_sys->set_litter_spread(0);
    // _particle_sys->set_local_velocity_flag(false);
