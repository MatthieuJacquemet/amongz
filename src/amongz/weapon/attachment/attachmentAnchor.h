// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ATTACHMENTANCHOR_H__
#define __ATTACHMENTANCHOR_H__


#include <panda3d/pandaNode.h>
#include <panda3d/nodePath.h>
#include "utils.h"
#include "typeUtils.h"

class Attachment;
class AttachmentBundle;


class AttachmentAnchor: public PandaNode {

    REGISTER_TYPE("AttachmentAnchor", PandaNode)

public:
    AttachmentAnchor(TypeHandle type);
    virtual ~AttachmentAnchor() = default;

    bool attach(AttachmentBundle* bundle, Attachment* attachment);
    bool detach(AttachmentBundle* bundle);

    TypeHandle get_attachment_type();
    Attachment* get_attachment() const;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    AttachmentAnchor();
    AttachmentAnchor(const AttachmentAnchor& copy);

    virtual int complete_pointers(TypedWritable** p_list, BamReader* reader);
    virtual void write_datagram(BamWriter* manager, Datagram& me);
    virtual void fillin(DatagramIterator& scan, BamReader *manager);

    TypeHandle _type;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);

    NodePath _attachment_np;

    PT(Attachment) _attachment;
    
    friend class AttachmentBundle;
};



class AttachmentBundle: private TypeUtils<PT(AttachmentAnchor)> {

public:
    using AttachFlag = TypeUtils::MatchType;

    AttachmentBundle(NodePath path);
    ~AttachmentBundle() = default;

    AttachmentAnchor* get_anchor(TypeHandle type, 
            AttachFlag flag=MT_first) const;

    Attachment* get_attachment(TypeHandle type, 
            AttachFlag flag=MT_first) const;
    
    bool attach(Attachment* attachment, AttachFlag flag=MT_first);
    bool detach(TypeHandle type, AttachFlag flag=MT_first);
    bool detach_all();

    size_t preload_cache();

    NodePath get_node_path() const;

private:
    typedef pmap<TypeHandle, PT(AttachmentAnchor)> CacheType;
    typedef std::array<CacheType, 3> CacheEntries;

    AttachmentAnchor* find_anchor(TypeHandle type, AttachFlag flag) const;

    AttachmentAnchor* r_find_anchor(PandaNode* node, 
            TypeHandle type, bool match_derived) const;
    
    void r_find_all_anchors(PandaNode* node, CacheType& result) const;

    void add_anchor(AttachmentAnchor* anchor, TypeHandle type);

    NodePath _np;

    mutable CacheEntries _cache;
};

#endif // __ATTACHMENTANCHOR_H__