// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/asyncTaskManager.h>
#include <panda3d/throw_event.h>
#include <panda3d/modelPool.h>
#include <panda3d/animBundle.h>
#include <panda3d/filename.h>
#include <panda3d/animBundle.h>
#include <panda3d/audioManager.h>
#include <panda3d/audioSound.h>
#include <panda3d/virtualFileSystem.h>

#include "config_game.h"
#include "game.h"
#include "muzzle.h"
#include "defines.h"
#include "callbacks.h"
#include "ammoData.h"
#include "mag.h"
#include "sight.h"
#include "gun.h"

using namespace std;


DEFINE_TYPEHANDLE(Gun);



Gun::Gun(const string& name): Weapon(name),     
    LocalRef<AttachmentBundle>(this, NodePath::any_path(this)),
    _data(new Data),
    _rounds(0),
    _queue(nullptr),
    _fire_task(*this),
    _is_fire(false),
    _has_suppressor(false)
{

}


bool Gun::pull_trigger() {

    if (_rounds == 0)
        return false;

    _fire_task.start_fire();
    return true;
}


void Gun::release_trigger() {

    _fire_task.stop_fire();
}


bool Gun::is_firing() const {

    return _fire_task.is_alive(); 
}


bool Gun::set_fire_mode(FireMode mode) {

    if (has_mode(mode)) {
        _fire_task.stop_fire(true);
        _current_mode = mode;    
        return true;
    }
    return false;
}


Gun::FireMode Gun::get_fire_mode() const {
    
    return _current_mode;
}


Gun::FireMode Gun::toggle_fire_mode() {
    
    do {
        int mode = (_current_mode + 1) & 3u;
        _current_mode = static_cast<FireMode>(mode);
    } 
    while (!has_mode(_current_mode));

    return _current_mode;
}


void Gun::set_event_queue(EventQueue* queue) {
    _queue = queue;    
}


size_t Gun::get_fire_count() const {
    return _fire_task.get_fire_count();
}


size_t Gun::get_num_rounds() const {
    return _rounds;
}


void Gun::set_num_rounds(size_t rounds) {
    _rounds = rounds;
}


size_t Gun::get_max_rounds() const {
    
    Mag* mag = DCAST(Mag, get_attachment(Mag::get_class_type()));

    if (mag != nullptr) {
        return mag->get_capacity();
    }

    return 0;
}


int Gun::get_num_fire_sound() const {

    if (_has_suppressor)
        return _data->_suppressed_sounds.size();

    return _data->_fire_sounds.size();
}


Filename Gun::get_fire_sound(int id) const {

    const Data::Sounds* sounds = &_data->_fire_sounds;

    if (_has_suppressor)
        sounds = &_data->_suppressed_sounds;

    nassertr(id < sounds->size(), Filename())
    return sounds->at(id);
}


void Gun::set_suppressed(bool suppressed) {
    _has_suppressor = suppressed;
}

float Gun::get_mass() const {
    return _data->_mass;
}

float Gun::get_muzzle_velocity() const {
    return _data->_muzzle_velocity;
}

float Gun::get_fire_rate() const {
    return _data->_fire_rate;
}

float Gun::get_recoil() const {
    return _data->_recoil;
}

bool Gun::has_mode(FireMode mode) const {
    return _data->_fire_modes & (0x1 << mode);
}

AmmoData* Gun::get_ammo() const {
    return _data->_ammo_data;
}

size_t Gun::get_num_anim() const {
    return _data->_anims.size();    
}

AnimBundle* Gun::get_anim(size_t id) const {
    
    nassertr(id < _data->_anims.size(), nullptr)
    return _data->_anims[id];
}

PandaNode* Gun::make_copy() const {
    return new Gun(*this);    
}


bool Gun::do_fire() {

    if (_rounds > 0) {
        --_rounds;

        Muzzle* muzzle = DCAST(Muzzle, get_attachment(Muzzle::get_class_type()));
        
        if (muzzle == nullptr)
            return false;
        
        muzzle->fire(this);

        if (_queue != nullptr) {
            Event* event = new Event("fire");
            event->add_parameter(this);
            _queue->queue_event(event);
        }

        return true;
    }
    return false;
}


Gun::FireTask::FireTask(Gun& gun): AsyncTask("fire"),
    _mgr(AsyncTaskManager::get_global_ptr()),
    _fire_count(0),
    _gun(gun)
{
    local_object();
}


Gun::FireTask::~FireTask() {
    remove();
}


void Gun::FireTask::start_fire() {
    
    if (_mgr->has_task(this))
        return;

    set_delay(_gun._data->_fire_rate);
    _fire_count = 0;

    // we add the task event in semi auto to add delay between each shot
    if (fire_one());
        _mgr->add(this); 
}


void Gun::FireTask::stop_fire(bool force) {

    if (_gun._current_mode != FM_burst || force)
        remove();
}

size_t Gun::FireTask::get_fire_count() const {
    return _fire_count;
}


AsyncTask::DoneStatus Gun::FireTask::do_task() {

    switch (_gun._current_mode) {
        case Gun::FM_burst:
            if (_fire_count == 3) break;
        case Gun::FM_full_auto:
            if (fire_one())
                return AsyncTask::DS_again;
        default: break;
    }
    return AsyncTask::DS_done;
}

bool Gun::FireTask::fire_one() {
    
    if (_gun.do_fire()) {
        ++_fire_count;
        return true;
    }
    return false;
}


Gun::Gun(const Gun& copy):
    Weapon(copy),
    LocalRef<AttachmentBundle>(this, NodePath::any_path(this)),
    _data(copy._data),
    _current_mode(copy._current_mode),
    _has_suppressor(copy._has_suppressor),
    _rounds(0),
    _queue(copy._queue),
    _fire_task(*this),
    _is_fire(false)
{

}


Gun::Class Gun::get_class() const {
    return _data->_class;
}


string Gun::get_class_name() const {
    
    switch (_data->_class) {
    case C_handgun:         return "handgun";
    case C_assault:         return "assault";
    case C_submachine_gun:  return "submachine";
    case C_lightmachine_gun:return "lightmachine";
    case C_sniper:          return "sniper";
    case C_launcher:        return "launcher";
    case C_shotgun:         return "shotgun";
    default: break;
    }
    return "unknown";
}


bool Gun::require_fully_complete() const {
    return true;
}


int Gun::complete_pointers(TypedWritable** p_list, BamReader* reader) {

    int num = Weapon::complete_pointers(p_list, reader);
    
    Data::Anims::iterator it;
    for (it = _data->_anims.begin(); it != _data->_anims.end(); ++it)
        (*it) = DCAST(AnimBundle, p_list[num++]);

    return num;
}


void Gun::write_datagram(BamWriter* manager, Datagram& me) {
    
    Weapon::write_datagram(manager, me);
    
    me.add_uint8(_data->_class);
    me.add_float32(_data->_muzzle_velocity);
    me.add_float32(_data->_mass);
    me.add_float32(_data->_recoil);
    me.add_float32(_data->_fire_rate);
    me.add_uint8(_data->_fire_modes);
    me.add_string(_data->_ammo_data->get_name());
    

    me.add_uint16(_data->_anims.size());

    for (size_t i=0; i<_data->_anims.size(); ++i)
        manager->write_pointer(me, _data->_anims[i]);
}


void Gun::fillin(DatagramIterator& scan, BamReader *manager) {
    
    Weapon::fillin(scan, manager);

    _data->_class = static_cast<Class>(scan.get_uint8());
    _data->_muzzle_velocity = scan.get_float32();
    _data->_mass = scan.get_float32();
    _data->_recoil = scan.get_float32();
    _data->_fire_rate = scan.get_float32();
    _data->_fire_modes = scan.get_uint8();

    _data->_ammo_data = AmmoData::get_ammo_data(scan.get_string());

    // take the first fire mode available
    int mode = get_lowest_on_bit((unsigned short)_data->_fire_modes);

    if (mode == -1) {
        game_warning("warning : no fire mode set for " << get_name());
        _current_mode = FM_semi_auto;
    } else
        _current_mode = static_cast<FireMode>(mode);

    size_t num_anims = scan.get_uint16();
    _data->_anims.reserve(num_anims);

    for (size_t i=0; i<num_anims; ++i) {
        manager->read_pointer(scan);
        _data->_anims.push_back(nullptr);
    }

    // load sounds

    Filename sound_path(AssetManager::get_location(Weapon::get_class_type()));
    sound_path = sound_path / get_name() / "sound";

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFileList) files = vfs->scan_directory(sound_path);

    if (files != nullptr) {

        for (size_t i=0; i<files->get_num_files(); ++i) {
            VirtualFile* file = files->get_file(i);

            Filename filename = file->get_filename();
            string name = filename.get_basename_wo_extension();

            if (name.rfind("fire_", 0) == 0)
                _data->_fire_sounds.push_back(filename);
            else if (name.rfind("suppressed_fire_", 0) == 0)
                _data->_suppressed_sounds.push_back(filename);
        }
    }
}


void Gun::register_with_read_factory() {
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}



TypedWritable* Gun::make_from_bam(const FactoryParams& params) {
    
    Gun* me = new Gun;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}



// Attachment* Gun::get_attachment(TypeHandle type) const {    
//     return _attachments.get(type);
// }


// bool Gun::add_attachment(Attachment* attachment) {
//     return _attachments.attach(attachment);    
// }


// bool Gun::remove_attachment(TypeHandle type) {
//     return _attachments.detach(type);
// }
