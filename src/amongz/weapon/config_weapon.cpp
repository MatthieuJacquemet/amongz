// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/dconfig.h>

#include "tracerBullet.h"
#include "ammoPack.h"
#include "ammoData.h"
#include "gun.h"
#include "mag.h"
#include "melee.h"
#include "sight.h"
#include "redDotSight.h"
#include "barrel.h"
#include "stock.h"
#include "grip.h"
#include "bullet.h"
#include "suppressor.h"
#include "defines.h"
#include "assetManager.h"
#include "chargingHandle.h"
#include "bulletHole.h"
#include "config_weapon.h"


using namespace std;


ConfigureDef(config_weapon);

ConfigureFn(config_weapon) {
    init_weapon();
}


void init_weapon() {

    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;
    
    AmmoData::init_type();
    AmmoData::Helper::init_type();
    AmmoPack::init_type();
    Gun::init_type();
    Melee::init_type();
    Mag::init_type();
    Attachment::init_type();
    Barrel::init_type();
    ChargingHandle::init_type();
    Stock::init_type();
    Grip::init_type();
    Muzzle::init_type();
    Suppressor::init_type();
    Sight::init_type();
    RedDotSight::init_type();
    AttachmentAnchor::init_type();
    Bullet::init_type();
    BulletHole::init_type();
    BulletHolePool::init_type();
    MaterialSlot::init_type();


    Gun::register_with_read_factory();
    Muzzle::register_with_read_factory();
    Suppressor::register_with_read_factory();
    Mag::register_with_read_factory();
    AmmoData::register_with_read_factory();
    AttachmentAnchor::register_with_read_factory();
    Grip::register_with_read_factory();
    ChargingHandle::register_with_read_factory();
    Barrel::register_with_read_factory();
    Stock::register_with_read_factory();
    Sight::register_with_read_factory();
    RedDotSight::register_with_read_factory();

    MaterialDef<"bullet hole"_hash>::init_type();
    MaterialDef<"bullet hole"_hash>::register_with_read_factory();

    AssetManager::set_location(Attachment::get_class_type(), ATTACHMENT_PATH);
    AssetManager::set_location(Weapon::get_class_type(), WEAPON_PATH);

}