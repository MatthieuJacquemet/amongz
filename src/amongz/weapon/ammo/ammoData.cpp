// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bamFile.h>

#include "defines.h"
#include "ammoData.h"

using namespace std;


DEFINE_TYPEHANDLE(AmmoData)
DEFINE_TYPEHANDLE(AmmoData::Helper)

AmmoData::AmmoDatas AmmoData::_ammo_datas;


float AmmoData::get_mass() const {
    return _mass;
}

float AmmoData::get_radius() const {
    return _radius;    
}

float AmmoData::get_damage_factor() const {
    return _damage_factor;
}

float AmmoData::get_penetration() const {
    return _penetration;
}

float AmmoData::get_drag_factor() const {
    return _drag_factor;
}

Bullet::Type AmmoData::get_bullet_types() const {
    return _bullet_types;    
}


AmmoData* AmmoData::get_ammo_data(const string& name) {
    
    if (_ammo_datas.empty())
        Helper::load_data();
    
    AmmoData::AmmoDatas::iterator it = _ammo_datas.find(name);

    if (it != _ammo_datas.end())
        return it->second;

    return load_data(name);
}


size_t AmmoData::get_num_ammo_datas() {
    
    return _ammo_datas.size();
}



void AmmoData::write_datagram(BamWriter* manager, Datagram& me) {

    me.add_string(get_name());
    me.add_float32(_radius);
    me.add_float32(_mass);
    me.add_float32(_damage_factor);
    me.add_float32(_penetration);
    me.add_float32(_drag_factor);
    me.add_uint8(_bullet_types);
}


void AmmoData::fillin(DatagramIterator& scan, BamReader* manager) {

    set_name(scan.get_string());

    _radius = scan.get_float32();
    _mass = scan.get_float32();
    _damage_factor = scan.get_float32();
    _penetration = scan.get_float32();
    _drag_factor = scan.get_float32();
    _bullet_types = static_cast<Bullet::Type>(scan.get_uint8());
}


TypedWritable* AmmoData::make_from_bam(const FactoryParams& params) {

    AmmoData* me = new AmmoData;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


void AmmoData::register_with_read_factory() {

    WritableFactory* factory = BamReader::get_factory();

    factory->register_factory(get_class_type(), make_from_bam);

    factory->register_factory(Helper::get_class_type(), 
                                Helper::make_from_bam);
}


AmmoData* AmmoData::load_data(const string& name) {
    
    BamFile bam_file;

    Filename path(AMMO_PATH);

    path.set_basename_wo_extension(name);
    path.set_extension("bam");

    if (!bam_file.open_read(path))
        return nullptr;
    
    TypedWritable* object = bam_file.read_object();
    AmmoData* data = nullptr;

    if (object->is_of_type(AmmoData::get_class_type())) {
        data = DCAST(AmmoData, object);
        _ammo_datas[name] = data;
    }
    bam_file.close();

    return data;
}


void AmmoData::Helper::write_datagram(BamWriter* manager, Datagram& me) {
    
    AmmoDatas& datas = AmmoData::_ammo_datas;

    me.add_uint16(datas.size());

    AmmoDatas::const_iterator it;

    for (it = datas.begin(); it != datas.end(); ++it) {
        AmmoData* data = it->second;
        data->write_datagram(manager, me);
    }
}


void AmmoData::Helper::fillin(DatagramIterator& scan, BamReader *manager) {

    AmmoDatas& datas = AmmoData::_ammo_datas;

    size_t num_data = scan.get_uint16();

    for (size_t i=0; i<num_data; ++i) {
        PT(AmmoData) data = new AmmoData;
        data->fillin(scan, manager);
        datas[data->get_name()] = data;
    }
}


TypedWritable* AmmoData::Helper::make_from_bam(const FactoryParams& params) {
    
    Helper* me = new Helper;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


void AmmoData::Helper::load_data() {
    
    BamFile bam_file;

    Filename path(AMMO_PATH);

    path.set_basename_wo_extension("ammo");
    path.set_extension("bam");

    if (bam_file.open_read(path)) {
        delete bam_file.read_object();

        bam_file.close();
    }
}