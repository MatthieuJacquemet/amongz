// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/boundingBox.h>
#include <panda3d/omniBoundingVolume.h>
#include <panda3d/transparencyAttrib.h>
#include <panda3d/depthWriteAttrib.h>
#include <panda3d/depthTestAttrib.h>
#include <panda3d/cullFaceAttrib.h>
#include <panda3d/alphaTestAttrib.h>
#include <panda3d/cullBinAttrib.h>
#include <panda3d/cullBinEnums.h>
#include <panda3d/cullBinManager.h>
#include <panda3d/cullHandler.h>
#include <panda3d/instanceList.h>
#include <panda3d/virtualFileSystem.h>

#include "renderPipeline.h"
#include "materialsPool.h"
#include "decalVolume.h"
#include "instanceManager.h"
#include "config_game.h"
#include "bulletHole.h"

using namespace std;


CPT(DecalVolume) BulletHolePool::_volume = new DecalVolume;

DEFINE_TYPEHANDLE(BulletHole)
DEFINE_TYPEHANDLE(BulletHolePool)
DEFINE_TYPEHANDLE(MaterialSlot)


MaterialSlot::MaterialSlot(const Filename& filename):
    _data(get_ceil_size(max_bullet_holes << 1, SST_32), 0)
{

    _instances = new InstanceList;
    

    ShaderMaterial* material = MaterialsPool::load_material(filename);
    nassertv(material != nullptr)

    set_name(material->get_name());

    _state.set_attrib(material->get_attrib());
    _state.set_shader_input(ShaderInput("u_offsets", _data));
}


InstanceList* MaterialSlot::get_instances() const {
    return _instances;
}

const RenderState* MaterialSlot::get_state() const {
    return _state;
}

size_t MaterialSlot::get_num_instances() const {
    return _instances->size();    
}

void MaterialSlot::reset() {
    _instances->clear();
}


void MaterialSlot::add_instance(const TransformState* transform, uchar id) {

    size_t num_instances = _instances->size();

    int index = num_instances >> 4;
    uint8_t offset = (num_instances & 15u) << 1;

    _data[index] &= ~(3u << offset); // first clear the two bits
    _data[index] |= (id & 3u) << offset; // the set those two bits

    _instances->append(transform);
}




BulletHole::BulletHole(const string& material):
    PandaNode(material)
{    
    ClockObject* clock = ClockObject::get_global_clock();
    _timestamp = clock->get_long_time();

    set_internal_bounds(new BoundingBox(LPoint3(-1.0f,-1.0f, 0.0f), 
                                        LPoint3( 1.0f, 1.0f, 1.0f)));
}


bool BulletHole::is_renderable() const {
    return true;
}

bool BulletHole::safe_to_combine() const {
    return false;
}

bool BulletHole::safe_to_flatten() const {
    return false;
}


void BulletHole::add_for_draw(CullTraverser* trav, CullTraverserData& data) {
    
    if (trav->is_of_type(PipelineCullTraverser::get_class_type())) {
        PipelineCullTraverser* traverser = (PipelineCullTraverser*)trav;

        traverser->record_node(data);
    }
}




BulletHolePool::BulletHolePool() {

    Filename path(MATERIAL_PATH "bullet_hole");
    
    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();

    PT(VirtualFile) folder = vfs->get_file(path);
    nassertv(folder->is_directory())

    PT(VirtualFileList) subfiles = folder->scan_directory();

    for (int i=0; i<subfiles->get_num_files(); ++i) {

        VirtualFile* file = subfiles->get_file(i);
        Filename filename = file->get_filename();

        if (filename.get_extension() != "bam")
            continue;

        MaterialSlot* slot = new MaterialSlot(filename);
        _slots[slot->get_name()] = slot;
    }
}


void BulletHolePool::record_node(CullTraverserData& data,
                                const PipelineCullTraverser* trav) {

    BulletHole* bullet_hole = DCAST(BulletHole, data.node());

    if (UNLIKELY(_slots.empty()))
        return;

    PT(MaterialSlot) slot = bullet_hole->_slot.lock(*this);

    if (UNLIKELY(slot == nullptr)) {
        MaterialSlots::iterator it = _slots.find(bullet_hole->get_name());
        
        if (it == _slots.end())
            it = _slots.begin();
        
        slot = it->second;
        bullet_hole->_slot.set_handler(*this, slot);
    }

    uchar id = *reinterpret_cast<uchar*>(&bullet_hole->_timestamp);

    CPT(TransformState) transform = data.get_internal_transform(trav);
    slot->add_instance(transform, id);
}



void BulletHolePool::post_cull(const PipelineCullTraverser* trav) {

    for (MaterialSlots::value_type it : _slots) {
        MaterialSlot* slot = it.second;

        if (slot->get_num_instances() == 0)
            continue;

        const RenderState* state = slot->get_state();

        CullableObject* object = new CullableObject(_volume,
                                        state->compose(_state),
                                        TransformState::make_identity());

        object->_instances = slot->get_instances();
        trav->get_cull_handler()->record_object(object, trav);

        slot->reset();
    }
}


TypeHandle BulletHolePool::get_node_type() const {
    return BulletHole::get_class_type();
}


void BulletHolePool::setup(RenderPipeline* pipeline) {

    const SceneData* data = pipeline->get_scene_data();

    _state.set_shader_input("u_resolution", data->_resolution);
    _state.set_transparency(TransparencyAttrib::M_alpha, 1000);
    _state.set_bin("decal-opaque", 0, 1000);
    _state.set_depth_test(true, 1000);
    _state.set_depth_write(false, 1000);


    _state.set_shader_input("u_color",
        pipeline->get_render_target("albedo"));

    _state.set_shader_input("u_normal",
        pipeline->get_render_target("normal"));

    _state.set_shader_input("u_depthStencil",
        pipeline->get_render_target("depth-stencil"));
}