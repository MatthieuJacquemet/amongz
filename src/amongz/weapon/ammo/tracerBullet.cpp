// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/audioSound.h>
  
#include "ammoData.h"
#include "gun.h"
#include "tracerBullet.h"

using namespace std;


TracerBullet::TracerBullet(BulletWorld* world, Gun* gun, Color color): 
    Bullet(world, gun)
{    
    set_color(color);
}


Bullet::Type TracerBullet::get_bullet_type() const {
    
    return BT_tracer;
}

void TracerBullet::set_color(const Color& color) {

    NodePath np(this);
    np.set_color(color);
}
