// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __BULLETHOLE_H__
#define __BULLETHOLE_H__

#include <panda3d/pandaNode.h>
#include <panda3d/cullTraverserData.h>

#include "instanceManager.h"
#include "shaderMaterial.h"
#include "shaderState.h"
#include "defines.h"
#include "cullHandlerHook.h"


class InstanceManager;
class InstancePool;
class DecalVolume;
class BulletHolePool;
class MaterialSlot;


class BulletHole: public PandaNode {

    REGISTER_TYPE("BulletHole", PandaNode)

public:
    BulletHole(const std::string& material);
    virtual ~BulletHole() = default;

    virtual bool is_renderable() const override;
    virtual bool safe_to_combine() const override;
    virtual bool safe_to_flatten() const override;

protected:
    virtual void add_for_draw(CullTraverser* trav, CullTraverserData& data) override;

private:
    HandlerCache<MaterialSlot> _slot;
    double _timestamp;

    friend class BulletHolePool;
};


class MaterialSlot: public Namable, public ReferenceCount {

    REGISTER_TYPE("MaterialSlot", Namable, ReferenceCount)

public:
    MaterialSlot() = default;
    MaterialSlot(const Filename& filename);
    ~MaterialSlot() = default;

    InstanceList* get_instances() const;
    const RenderState* get_state() const;

    size_t get_num_instances() const;
    void reset();

    void add_instance(const TransformState* transform, uchar id);

private:
    PTA_int _data;
    ShaderState _state;
    PT(InstanceList) _instances;
};


class BulletHolePool: public NodeHandler, public InstanceIndex<BulletHolePool> {

    REGISTER_TYPE("BulletHolePool", NodeHandler)

public:
    typedef std::map<std::string, PT(MaterialSlot)> MaterialSlots;

    BulletHolePool();
    virtual ~BulletHolePool() = default;

    virtual void record_node(CullTraverserData& data,
                            const PipelineCullTraverser* trav) override;

    virtual void post_cull(const PipelineCullTraverser* trav) override;

    virtual TypeHandle get_node_type() const override;
    virtual void setup(RenderPipeline* pipeline) override;

private:
    static CPT(DecalVolume) _volume;
    ShaderState _state;

    MaterialSlots _slots;
};


MATERIAL("bullet hole") {

    SHADING_LANGUAGE(GLSL)

    SHADER_FLAG(F_hardware_instancing)

    VERTEX_SHADER(SHADER_PATH "bulletHole.vert")
    FRAGMENT_SHADER(SHADER_PATH "bulletHole.frag")

    SHADER_PROP(Texture, "u_overrideAlbedo")
    SHADER_PROP(Texture, "u_overrideNormal")
};

#endif // __BULLETHOLE_H__