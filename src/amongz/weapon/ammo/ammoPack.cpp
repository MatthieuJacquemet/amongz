// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include "slotPool.h"
#include <panda3d/namable.h>
#include "ammoData.h"
#include "ammoPack.h"

using namespace std;

DEFINE_TYPEHANDLE(AmmoPack)


AmmoPack::AmmoPack(const string& name, size_t quantity): 
    LootableItem(name),
    _data(AmmoData::get_ammo_data(name)),
    _quantity(quantity)

{

}


size_t AmmoPack::loot() {
    
    size_t quantity = _quantity;
    _quantity = 0;

    return quantity;
}


AmmoData* AmmoPack::get_data() const {

    return _data;    
}