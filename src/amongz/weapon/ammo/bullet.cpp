// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennaoura, Nicolas Leray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/bulletSphereShape.h>    // for BulletSphereShape
#include <panda3d/bulletPersistentManifold.h>
#include <panda3d/texturePool.h>
#include <panda3d/bulletWorld.h>          // for BulletWorld
#include <panda3d/nodePath.h>             // for NodePath
#include <panda3d/cardMaker.h>
#include <panda3d/cullFaceAttrib.h>

#include "config_game.h"
#include "effects.h"
#include "defines.h"              // for PLAYER_COLLISION_MASK
#include "game.h"                 // for Game
#include "utils.h"
#include "map.h"
#include "ammoData.h"
#include "gun.h"
#include "bulletHole.h"
#include "decalVolume.h"
#include "bullet.h"

using namespace std;


DEFINE_TYPEHANDLE(Bullet)

CallbackTask* Bullet::_update_bullets;
Bullet::Bullets Bullet::_bullets;
Bullet::BulletHoles Bullet::_bullet_holes;


Bullet::Bullet(BulletWorld* world, Gun* gun): 
    BulletRigidBodyNode("bullet"),
    _gun(gun),
    _prev_pos(0.0f),
    _world(world)
{

    ClockObject* clock = ClockObject::get_global_clock();
    _timeout = clock->get_long_time() + bullet_lifespan;

    if (_update_bullets == nullptr)
        setup_update_task();

    AmmoData* ammo = gun->get_ammo();

    notify_collisions(true);
    set_mass(ammo->get_mass());

    world->attach_rigid_body(this);

    float radius = ammo->get_radius();

    set_collision_response(false);
    // set_ccd_motion_threshold(1e-7);
    // set_ccd_swept_sphere_radius(radius);

    _bullets.push_back(this);
    _it = prev(_bullets.end());
}


Bullet::Type Bullet::get_bullet_type() const {
    return BT_standard;
}


void Bullet::fired() {
    _prev_pos = get_transform()->get_pos();    
}





float Bullet::get_damage() const {

    AmmoData* ammo = _gun->get_ammo();
    return ammo->get_mass() * ammo->get_damage_factor();
}


Gun* Bullet::get_gun() const {
    
    return _gun;
}


bool Bullet::update() {

    ClockObject* clock = ClockObject::get_global_clock();

    if (clock->get_long_time() >= _timeout)
        return false;
    
    LVector3 to = get_transform()->get_pos();
    LVector3 from = _prev_pos;

    _prev_pos = to;

    CollideMask mask;
    mask.set_bit(Map::CM_bullet);
    mask.set_bit(Map::CM_mesh);

    auto result = _world->ray_test_closest(from, to, mask);

    
    if (!result.has_hit())
        return true;
    
    PandaNode* node = result.get_node();
    LPoint3 pos = result.get_hit_pos();
    LVector3 nor = result.get_hit_normal();


    add_bullet_hole(node, pos, nor);
    
    node = ::find_parent(node, BulletRigidBodyNode::get_class_type());

    if (node != nullptr) {
        BulletRigidBodyNode* body = DCAST(BulletRigidBodyNode, node);
        
        if (body->get_mass() > 0.0f) {

            LVector3 force = (to - from) * get_mass() * 10.0f;
            body->apply_impulse(force, pos - body->get_transform()->get_pos());
            body->force_active(true);

            body->set_friction(3.0f);
        }
    }

    return false;
}


string find_material(NodePath np) {

    while (np.has_parent()) {

        if (np.has_tag("material"))
            return np.get_tag("material");

        np = np.get_parent();
    }
    return "";
}


void Bullet::add_bullet_hole(PandaNode* node, LPoint3 pos, LVector3 nor) const {
    

    Map* map = get_current_map();
    NodePath np(node);

    string material = find_material(np);

    NodePath bullet_hole(new BulletHole(material));


    pos += nor * bullet_hole_offset;
    pos = np.get_relative_point(bullet_hole, pos);
    nor = np.get_relative_vector(bullet_hole, nor);

    bullet_hole.reparent_to(np);

    float scale = randf(0.07f, 0.1f);
    float theta = randf(0.0f, 360.0f);

    LQuaternion quat;
    LQuaternion quat2;
    quat2.set_from_axis_angle(theta, LVector3::up());

    look_at(quat, nor.normalized(), LVector3::up(), 
            CoordinateSystem::CS_yup_left);

    bullet_hole.set_pos(pos);
    bullet_hole.set_quat(quat2 * quat);
    
    bullet_hole.set_scale(*map, LVector3(scale, scale, bullet_hole_depth));


    if (_bullet_holes.size() == max_bullet_holes) {
        NodePath& oldest = _bullet_holes.front();
        oldest.remove_node();
        _bullet_holes.pop();
    }
    _bullet_holes.push(bullet_hole);
}


Bullet::Bullets::iterator Bullet::remove() {

    if (get_num_parents())
        get_parent(0)->remove_child(this);

    if (PT(BulletWorld) world = _world.lock())
        world->remove_rigid_body(this);

    Bullets::iterator it = _it;
    _it = _bullets.end();

    return _bullets.erase(it);
}


void Bullet::setup_update_task() {

    nassertv(_update_bullets == nullptr)

    AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

    _update_bullets = new CallbackTask(update_bullets, "clear_bullets");
    _update_bullets->set_sort(41);

    if (threaded_physic)
        _update_bullets->set_task_chain("physic");

    mgr->add(_update_bullets);
}


AsyncTask::DoneStatus Bullet::update_bullets(AsyncTask* task) {
    
    Bullets::iterator it = _bullets.begin();

    while (it != _bullets.end()) {
        Bullet* bullet = *it;
        
        if (!bullet->update())
            it = bullet->remove();
        else
            ++it;
    }
    return AsyncTask::DS_cont;
}

















// void Bullet::do_callback(BulletPersistentManifold* manifold, 
//                             PandaNode* other) {

    
    // if (_it == _bullets.end())
    //     return;

    // nout << "intersect with " << other->get_name() << endl;

    // if (!manifold->get_num_manifold_points())
    //     return;

    // BulletManifoldPoint* point = manifold->get_manifold_point(0);
    // nout << "collide with " << other->get_name() << endl;
    
    // CardMaker cm("bullet_hole");
    // cm.set_frame(-0.05f,0.05f,-0.05f,0.05f);

    // NodePath np(other);
    // NodePath root(get_current_map());

    // NodePath bullet_hole(cm.generate());

    // LPoint3 pos = point->get_position_world_on_b();
    // LVector3 dir = point->get_normal_world_on_b();

    // pos = np.get_relative_point(bullet_hole, pos);
    // dir = np.get_relative_vector(bullet_hole, -dir);
    
    // bullet_hole.reparent_to(np);
    // // bullet_hole.set_effect(KeepScale::make());

    // LQuaternion quat;
    // look_at(quat, dir.normalized(), LVector3::up());
    // bullet_hole.set_quat(quat);
    
    // bullet_hole.set_pos(pos);
    // bullet_hole.set_scale(root, LVector3(1.0f));


    // Texture* tex = TexturePool::load_texture(TEXTURE_PATH "bullet_hole_0.png");

    // if (tex != nullptr) {
    //     bullet_hole.set_bin("opaque", CullBin::BT_back_to_front);
    //     bullet_hole.set_transparency(TransparencyAttrib::M_alpha);
    //     bullet_hole.set_depth_write(false, 100);
    //     bullet_hole.set_texture(tex);
    // }


    // if (_bullet_holes.size() == MAX_BULLET_HOLES) {
    //     NodePath oldest = _bullet_holes.front();
    //     oldest.remove_node();
    //     _bullet_holes.pop();
    // }
    // _bullet_holes.push(bullet_hole);

    // delete point;
// }