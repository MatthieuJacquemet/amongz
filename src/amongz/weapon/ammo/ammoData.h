

// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __AMMODATA_H__
#define __AMMODATA_H__

#include <panda3d/typedWritableReferenceCount.h>
#include <panda3d/namable.h>

#include "bullet.h"
#include "utils.h"

class AmmoData;
class NodePath;


class AmmoData: public TypedWritableReferenceCount, public Namable {

    REGISTER_TYPE("AmmoData", TypedWritableReferenceCount, Namable)

public:

    float get_mass() const;
    float get_radius() const;
    float get_damage_factor() const;
    float get_penetration() const;
    float get_drag_factor() const;
    Bullet::Type get_bullet_types() const;

    static AmmoData* get_ammo_data(const std::string& name);
    static size_t get_num_ammo_datas();

    ~AmmoData() = default;

    static void register_with_read_factory();

    class Helper: public TypedWritable {

        REGISTER_TYPE("AmmoData::Helper", TypedWritable)
    public:
        void write_datagram(BamWriter* manager, Datagram& me);
        void fillin(DatagramIterator& scan, BamReader *manager);
        static TypedWritable* make_from_bam(const FactoryParams& params);
        static void load_data();
    };

protected:
    AmmoData() = default;

    using Namable::set_name;

    float _radius;
    float _mass;
    float _damage_factor;
    float _penetration;
    float _drag_factor;
    Bullet::Type _bullet_types;

    typedef pmap<std::string, PT(AmmoData)> AmmoDatas;
    static AmmoDatas _ammo_datas;

    void write_datagram(BamWriter* manager, Datagram& me);
    void fillin(DatagramIterator& scan, BamReader *manager);

private:

    static AmmoData* load_data(const std::string& name);

    static TypedWritable* make_from_bam(const FactoryParams& params);

    friend class Helper;
};

#endif // __AMMODATA_H__