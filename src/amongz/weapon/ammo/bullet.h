// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BULLET_H__
#define __BULLET_H__

#include <queue>
#include <panda3d/bulletRigidBodyNode.h>

#include "callbacks.h"

class BulletWorld;
class Gun;


class Bullet: public BulletRigidBodyNode  {

    REGISTER_TYPE("Bullet", BulletRigidBodyNode)

public:
    enum Type {
        BT_standard         = 0x0,
        BT_armor_piercing   = 0x1<<0,
        BT_tracer           = 0x1<<1,
        BT_incendiary       = 0x1<<2,
        BT_explosive        = 0x1<<3,
        BT_hollow_point     = 0x1<<4
    };

    Bullet(BulletWorld* world, Gun* gun);
    virtual ~Bullet() = default;

    virtual Type get_bullet_type() const;

    void fired();

    virtual float get_damage() const;

    Gun* get_gun() const;

private:
    PT(Gun) _gun;
    WPT(BulletWorld) _world;
    double _timeout;
    LVector3 _prev_pos;

    bool update();
    void add_bullet_hole(PandaNode* node, LPoint3 pos, LVector3 nor) const;

    static void setup_update_task();
    static AsyncTask::DoneStatus update_bullets(AsyncTask* task);
    static CallbackTask* _update_bullets;

    typedef plist<PT(Bullet)> Bullets;
    typedef std::queue<NodePath> BulletHoles;

    static BulletHoles _bullet_holes;
    static Bullets _bullets;

    Bullets::iterator remove();
    Bullets::iterator _it;
};


inline Bullet::Type operator|(Bullet::Type a, Bullet::Type b) {

    int res = static_cast<int>(a) | static_cast<int>(b);
    return static_cast<Bullet::Type>(res);
}

#endif // __BULLET_H__