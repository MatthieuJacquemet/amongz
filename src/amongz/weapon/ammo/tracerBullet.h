// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.



#ifndef __TRACERBULLET_H__
#define __TRACERBULLET_H__

#include "bullet.h"
#include "color.h"


class TracerBullet: public Bullet {

public:
    TracerBullet(BulletWorld* world, Gun* gun, 
                Color color=Color::green());
    ~TracerBullet() = default;

    virtual Type get_bullet_type() const;

    void set_color(const Color& color);
};

#endif // __TRACERBULLET_H__