
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAP_H__
#define __MAP_H__

#include <panda3d/typedWritableReferenceCount.h>
#include <panda3d/asyncTask.h>
#include <panda3d/nodePath.h>

#include "callbacks.h"
#include "scene.h"

class BulletWorld;
class Player;
class AIWorld;
class Player;
class BulletBodyNode;
class Spawn;
class SoundManager;
class InstanceManager;
class DecalVolume;
class ParticleSystemManager;
class PhysicsManager;

class Map: public Scene, public TaskPool {

    REGISTER_TYPE("Map", Scene)

public:

    enum CollisionMask {
        CM_mesh = 1,
        CM_dynamic,
        CM_player,
        CM_hitbox,
        CM_bullet
    };

    enum RenderLayer {
        RL_static = 1,
        RL_dynamic,
        RL_transparent
    };

    Map(const std::string& name="");
    Map(const Map& map) = delete;
    virtual ~Map();

    virtual void init();

    virtual void add_player(Player* player);
    
    virtual void remove_player(Player* player);
    virtual void remove_player(size_t id);

    virtual bool has_player(Player* player) const;

    virtual Player* make_player(const std::string& name);

    void add_bullet_for_removal(BulletBodyNode* node);

    size_t get_num_player() const;
    Player* get_player(size_t id) const;

    BulletWorld* get_physic_world() const;
    AIWorld* get_ai_world() const;
    InstanceManager* get_instance_manager() const;

    static Map* load(const std::string& name);

    static void register_with_read_factory();

    virtual void play_sound(Filename& filename, NodePath path);

    NodePath get_root() const;
    SoundManager* get_sound_manager();
    ParticleSystemManager* get_particle_manager();
    PhysicsManager* get_physics_manager();

protected:

    virtual void fillin(DatagramIterator& scan, BamReader* manager);
    virtual void write_datagram(BamWriter* manager, Datagram& me);
    virtual int complete_pointers(TypedWritable** p_list, BamReader* reader) override;
    virtual void finalize(BamReader* reader) override;

    AIWorld* _ai_world;
    PhysicsManager* _physics_mgr;
    ParticleSystemManager* _particle_mgr;

    PT(BulletWorld) _physic_world;
    PT(SoundManager) _sound_mgr;
    PT(InstanceManager) _instance_mgr;

    typedef pvector<PT(Spawn)> ZombieSpawns;
    typedef pvector<std::string> ZombieNames;

    ZombieSpawns _spawns;
    ZombieNames _zombies;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
    
    void load_zombies();
    void load_collide_mask();

    typedef pvector<PT(Player)> Players;
    typedef plist<PT(BulletBodyNode)> NodesToRemove;

    Players _players;
    size_t _num_bodies; // only used for reading bam file

    AsyncTask::DoneStatus update_physic(AsyncTask *task);
    AsyncTask::DoneStatus update_ai(AsyncTask *task);
    AsyncTask::DoneStatus process_audio(AsyncTask *task);
    AsyncTask::DoneStatus update_particles(AsyncTask* task);

    AsyncTask::DoneStatus spawn_zombies(AsyncTask *task);

};

#endif // __MAP_H__