
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#ifndef __SPAWN_H__
#define __SPAWN_H__


#include <panda3d/pandaNode.h>
#include "gdk/utils.h"


class Spawn: public PandaNode {

    REGISTER_TYPE("Spawn", PandaNode)

public:
    typedef Factory<PandaNode> ObjectFactory;

    Spawn(TypeHandle type, const std::string& name="");
    Spawn(const Spawn& copy);
    ~Spawn() = default;

    virtual PandaNode* spawn(FactoryParams params=FactoryParams());

    void set_area(float width, float height);
    LVector2 get_area() const;

    static ObjectFactory* get_factory();

    virtual PandaNode* make_copy() const override;
    static void register_with_read_factory();

    virtual void set_spawn_type(TypeHandle type);
    virtual TypeHandle get_spawn_type() const;

protected:
    Spawn();

    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;
    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    static TypedWritable* make_from_bam(const FactoryParams& params);

    TypeHandle _type;

private:
    static Singleton<ObjectFactory> _factory;
};

#endif // __SPAWN_H__