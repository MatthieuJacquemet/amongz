// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/nodePath.h>
#include <panda3d/transformState.h>
#include <panda3d/cullTraverserData.h>

#include "effects.h"

using namespace std;

DEFINE_TYPEHANDLE(ComposeTransform)
DEFINE_TYPEHANDLE(KeepScale)


void ComposeTransform::adjust_transform(CPT(TransformState)& net_transform,
                                        CPT(TransformState)& node_transform,
                                        const PandaNode* node) const
{

    nassertv(node != _node_0.p() && node != _node_1.p())

    if (PT(PandaNode) node_0 = _node_0.lock()) {
        if (PT(PandaNode) node_1 = _node_1.lock()) {

            NodePath np_0(node_0);
            NodePath np_1(node_1);

            CPT(TransformState) t0 = np_0.get_net_transform();
            CPT(TransformState) t1 = np_1.get_net_transform();

            net_transform = t0->compose(t1);
        }
    }
}


bool ComposeTransform::has_adjust_transform() const {
    return true;
}



CPT(RenderEffect) ComposeTransform::make(PandaNode* node_0, PandaNode* node_1) {

    ComposeTransform* effect = new ComposeTransform;
    effect->_node_0 = node_0;
    effect->_node_1 = node_1;

    return effect;
}




void KeepScale::cull_callback(CullTraverser* trav, CullTraverserData& data,
                            CPT(TransformState)& node_transform,
                            CPT(RenderState)& node_state) const
{

    LVector3 scale = data.get_net_transform(trav)->get_scale();

    scale.set(  1.0f / scale.get_x(),
                1.0f / scale.get_y(),
                1.0f / scale.get_z());

    node_transform = node_transform->compose(TransformState::make_scale(scale));
}


CPT(RenderEffect) KeepScale::make() {
    return return_new(new KeepScale);
}


bool KeepScale::has_cull_callback() const {
    return true;
}