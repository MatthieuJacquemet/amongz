// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/loader.h>
#include <panda3d/bulletWorld.h>

#include <panda3d/rescaleNormalAttrib.h>
#include <panda3d/shadeModelAttrib.h>
#include <panda3d/bulletPlaneShape.h>
#include <panda3d/bulletPersistentManifold.h>
#include <panda3d/bulletTickCallbackData.h>
#include <panda3d/virtualFileSystem.h>
#include <panda3d/aiWorld.h>
#include <panda3d/cardMaker.h>
#include <panda3d/animBundle.h>
#include <panda3d/audioSound.h>
#include <panda3d/particleSystemManager.h>
#include <panda3d/physicsManager.h>
#include <panda3d/linearEulerIntegrator.h>
#include <panda3d/angularEulerIntegrator.h>

#include "pointLightNode.h"
#include "directionalLightNode.h"
#include "debugNode.h"
#include "config_game.h"
#include "fpsController.h"
#include "collisionHandler.h"
#include "player.h"
#include "gun.h"
#include "barrel.h"
#include "mag.h"
#include "melee.h"
#include "stock.h"
#include "grip.h"
#include "ammoData.h"
#include "chargingHandle.h"
#include "suppressor.h"
#include "redDotSight.h"
#include "ammoPack.h"
#include "defines.h"
#include "callbacks.h"
#include "spawn.h"
#include "zombieAIController.h"
#include "render_utils.h"
#include "zombieView.h"
#include "soundManager.h"
#include "instanceManager.h"
#include "bulletHole.h"
#include "map.h"

using namespace std;

DEFINE_TYPEHANDLE(Map)



Map::Map(const string& name): 
    Scene(name),
    _physic_world(new BulletWorld()),
    _ai_world(new AIWorld(*this)),
    _sound_mgr(new SoundManager(this)),
    _physics_mgr(new PhysicsManager),
    _particle_mgr(new ParticleSystemManager)
{
    load_collide_mask();
    load_zombies();
}


Map::~Map() {

    delete _particle_mgr;
    delete _physics_mgr;
    delete _ai_world;
}


void Map::init() {

    _physic_world->set_gravity(0,0,-GRAVITY);
    _physic_world->set_tick_callback(new CollisionHandler(_physic_world));
    
    AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

    AsyncTask* physic = do_add_task(named_method(update_physic), 40);
    AsyncTask* audio  = do_add_task(named_method(process_audio), 50);
    AsyncTask* ai     = do_add_task(named_method(update_ai), 30);
    AsyncTask* parts  = do_add_task(named_method(update_particles), 20);

    _physics_mgr->attach_linear_integrator(new LinearEulerIntegrator);
    _physics_mgr->attach_angular_integrator(new AngularEulerIntegrator);

    // AsyncTask* spawn  = do_add_task(named_method(spawn_zombies), 0);


    // PointLightNode* light = new PointLightNode;
    // light->set_radius(10.0);
    // light->set_energy(1.3);
    // light->set_color(Color(1.0f,1.0f,1.0f));
    // light->set_shadow_caster(true);

    // NodePath liht_np = attach_new_node(light);
    // liht_np.set_pos(0.0, 0.0, 2.0);


    for (int i=0; i<200; ++i) {

        PointLightNode* light = new PointLightNode;
        float energy = randf(1.0, 2.0);
        light->set_radius(energy * 5.0);
        light->set_energy(energy * 10.0);
        light->set_color(Color(randf(0,1),randf(0,1),randf(0,1)).normalized());
        // light->set_shadow_caster(true);
        NodePath light_np = attach_new_node(light);
        light_np.set_pos(randf(-7, 40), randf(30, -20), randf(-10, 10));
    }

    DirectionalLightNode* sun = new DirectionalLightNode;
    sun->set_energy(0.5);
    NodePath sun_np = attach_new_node(sun);
    sun_np.set_pos(20,20,20);
    sun_np.look_at(LPoint3(0,0,0), LVector3::right());


    // DebugNode* debug = new DebugNode;
    // debug->show_wireframe(false);
    // debug->show_bounding_boxes(true);
    // debug->show_normals(true);

    // NodePath debug_np = attach_new_node(debug);

    // debug_np.set_bin("overlay", 0, 100);
    // debug_np.show();

    // _physic_world->set_debug_node(debug);

    if (threaded_physic) {
        AsyncTaskChain* physic_chain = mgr->make_task_chain("physic");
        physic_chain->set_num_threads(1);
        physic_chain->set_frame_sync(true);
        physic->set_task_chain("physic");
    }


    if (threaded_ai) {
        AsyncTaskChain* ai_chain = mgr->make_task_chain("ai");
        ai_chain->set_num_threads(1);
        ai_chain->set_frame_sync(true);
        ai->set_task_chain("ai");
    }
}


void Map::add_player(Player* player) {
    
    nassertv(!has_player(player))

    _players.push_back(player);

    _physic_world->attach_character(player);
    player->set_pos(0,0,1);
    
    player->reparent_to(*this);
    player->set_map(this);
}


void Map::remove_player(Player* player) {

    auto found = std::find(_players.begin(), _players.end(), player);

    if (found != _players.end())
        _players.erase(found);    
}


void Map::remove_player(size_t id) {
    
    nassertv(id < _players.size())
    _players.erase(_players.begin() + id);
}


bool Map::has_player(Player* player) const {
    
    auto it = std::find(_players.begin(), _players.end(), player);
    return it != _players.end();
}


Player* Map::make_player(const string& name) {

    FpsController* controller = new FpsController(name);

    // give the player a default gun
    PT(Gun) gun = make_asset<Gun>("m4a1");

    gun->set_fire_mode(Gun::FM_full_auto);

    gun->attach(make_asset<Barrel>("m4a1_barrel_long"));
    gun->attach(make_asset<Mag>("mag_stanag"));
    gun->attach(make_asset<Stock>("m4a1_stock"));
    gun->attach(make_asset<Grip>("vertical_grip"));
    gun->attach(make_asset<ChargingHandle>("ar15_std"));
    // gun->attach(make_asset<Suppressor>("socom_monster"));
    gun->attach(make_asset<Muzzle>("m4a1_muzzle"));
    gun->attach(make_asset<RedDotSight>("aimpoint_micro_t1"));
    // gun->attach(make_asset<RedDotSight>("reflex"));
    // gun->attach(make_asset<Sight>("acog"));


    attach_new_node(gun);

    // give the player some ammos
    controller->loot(new AmmoPack("5.56", 500));
    gun->set_num_rounds(gun->get_max_rounds());

    do_task_later([controller, gun](AsyncTask*) {
        controller->loot(gun);
        return AsyncTask::DS_done;

    }, "loot", 0.0);

    return controller;
}


size_t Map::get_num_player() const {
    return _players.size();
}


Player* Map::get_player(size_t id) const {

    nassertr(id < _players.size(), nullptr)
    return _players[id];
}


BulletWorld* Map::get_physic_world() const {
    return _physic_world;
}


AIWorld* Map::get_ai_world() const {
    return _ai_world;
}

InstanceManager* Map::get_instance_manager() const {
    return _instance_mgr;
}

ParticleSystemManager* Map::get_particle_manager() {
    return _particle_mgr;
}

PhysicsManager* Map::get_physics_manager() {
    return _physics_mgr;
}


Map* Map::load(const std::string& name) {

    PandaNode* root = AssetManager::get_asset(Map::get_class_type(), name);
    Map* map = DCAST(Map, root);

    if (map != nullptr)
        map->init();

    return map;
}


void Map::load_collide_mask() {

    _physic_world->set_group_collision_flag(CM_mesh,   CM_mesh,     false);
    _physic_world->set_group_collision_flag(CM_mesh,   CM_dynamic,  true);
    _physic_world->set_group_collision_flag(CM_player, CM_dynamic,  true);
    _physic_world->set_group_collision_flag(CM_player, CM_mesh,     true);
    _physic_world->set_group_collision_flag(CM_hitbox, CM_hitbox,   false);
    _physic_world->set_group_collision_flag(CM_bullet, CM_mesh,     true);
    _physic_world->set_group_collision_flag(CM_bullet, CM_player,   false);
    _physic_world->set_group_collision_flag(CM_bullet, CM_dynamic,  false);


    // // bullets can collide with anything
    // for (int i=0; i<32; ++i) {
    //     _physic_world->set_group_collision_flag(CM_, i, true);
    //     _physic_world->set_group_collision_flag(DYNAMIC_COLLISION_MASK, i, true);
    // }

    // except player's collision capsule and other bullets
    // _physic_world->set_group_collision_flag(BULLET_COLLISION_MASK,
    //                                         PLAYER_COLLISION_MASK, false);

    // _physic_world->set_group_collision_flag(BULLET_COLLISION_MASK,
    //                                         BULLET_COLLISION_MASK, false);

    // _physic_world->set_group_collision_flag(BULLET_COLLISION_MASK,
    //                                         DYNAMIC_COLLISION_MASK, false);
}


AsyncTask::DoneStatus Map::update_particles(AsyncTask* task) {

    ClockObject* clock = ClockObject::get_global_clock();

    _particle_mgr->do_particles(clock->get_dt());
    _physics_mgr->do_physics(clock->get_dt());

    return AsyncTask::DS_cont;

}


AsyncTask::DoneStatus Map::update_physic(AsyncTask *task) {

    ClockObject* clock = ClockObject::get_global_clock();
    _physic_world->do_physics(clock->get_dt(), 10);

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Map::update_ai(AsyncTask *task) {

    _ai_world->update();
    
    return AsyncTask::DS_cont;    
}


AsyncTask::DoneStatus Map::process_audio(AsyncTask *task) {

    _sound_mgr->update();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Map::spawn_zombies(AsyncTask *task) {

    task->set_delay(randf(15.0f, 25.0f));

    if (_spawns.size() == 0)
        return AsyncTask::DS_again;
    
    int n = rand() % _spawns.size();

    for (int i=0; i<=n; ++i) {
        Spawn* spawn_point = _spawns[rand() % _spawns.size()];
        string name = _zombies[rand() % _zombies.size()];

        FactoryParams params;
        params.add_param(new ZombieAIController::ZombieParam(name));
        
        PandaNode* node = spawn_point->spawn(params);
        
        if (node != nullptr)
            attach_new_node(node);
    }

    return AsyncTask::DS_again;    
}


void Map::load_zombies() {

    Filename path(ZOMBIE_PATH);

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFileList) files = vfs->scan_directory(path);

    if (files == nullptr)
        return;

    for (size_t i=0; i<files->get_num_files(); ++i) {
        
        VirtualFile* file = files->get_file(i);
        Filename filename = file->get_filename();
        _zombies.push_back(filename.get_basename_wo_extension());
    }
}


void Map::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

void Map::play_sound(Filename& filename, NodePath path) {
    
}

NodePath Map::get_root() const {
    return *this;
}

SoundManager* Map::get_sound_manager() {
    return _sound_mgr;
}


void Map::fillin(DatagramIterator& scan, BamReader* manager) {
    
    PandaNode::fillin(scan, manager);

    // _num_bodies = scan.get_uint32();
    // manager->read_pointers(scan, _num_bodies);

    int num_spawn = scan.get_uint32();

    for (int i=0; i<num_spawn; ++i) {
        manager->read_pointer(scan);
        _spawns.push_back(nullptr);
    }
}


void Map::write_datagram(BamWriter* manager, Datagram& me) {
    
    PandaNode::write_datagram(manager, me);
    
    // int num_char = _physic_world->get_num_characters();
    // int num_rigi = _physic_world->get_num_rigid_bodies();
    // int num_soft = _physic_world->get_num_soft_bodies();
    // int num_ghos = _physic_world->get_num_ghosts();

    // me.add_uint32(num_char + num_rigi + num_soft + num_ghos);

    // for (int i=0; i<num_char; ++i)
    //     manager->write_pointer(me, _physic_world->get_character(i));

    // for (int i=0; i<num_rigi; ++i)
    //     manager->write_pointer(me, _physic_world->get_rigid_body(i));

    // for (int i=0; i<num_soft; ++i)
    //     manager->write_pointer(me, _physic_world->get_soft_body(i));

    // for (int i=0; i<num_ghos; ++i)
    //     manager->write_pointer(me, _physic_world->get_ghost(i));


    me.add_uint32(_spawns.size());

    for (int i=0; i<_spawns.size(); ++i)
        manager->write_pointer(me, _spawns[i]);
}


int Map::complete_pointers(TypedWritable** p_list, BamReader* reader) {
    
    int n = PandaNode::complete_pointers(p_list, reader);

    // for (int i=0; i<_num_bodies; ++i) {
    //     BulletBodyNode* body = DCAST(BulletBodyNode, p_list[n++]);
    //     _physic_world->attach(body);
    // }

    ZombieSpawns::iterator it;
    for (it = _spawns.begin(); it != _spawns.end(); ++it)
        (*it) = DCAST(Spawn, p_list[n++]);
    
    return n;
}


void Map::finalize(BamReader* reader) {
    
    PandaNode::finalize(reader);

    NodePathCollection objects = find_all_matches("**/+BulletBodyNode");

    for (int i=0; i<objects.get_num_paths(); ++i) {
        NodePath np = objects.get_path(i);
        _physic_world->attach(DCAST(BulletBodyNode, np.node()));
    }
}


TypedWritable* Map::make_from_bam(const FactoryParams& params) {

    Map* me = new Map;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);
    manager->register_finalize(me);

    return me;
}
