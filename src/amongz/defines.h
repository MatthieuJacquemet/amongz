// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __DEFINES_H__
#define __DEFINES_H__

#include "config.h"

#define MAP_PATH        DATA_PATH "map/"
#define WEAPON_PATH     DATA_PATH "weapon/"
#define ZOMBIE_PATH     DATA_PATH "zombie/"
#define AMMO_PATH       DATA_PATH "ammo/"
#define MODEL_PATH      DATA_PATH "model/"
#define VIEWARM_PATH    DATA_PATH "viewarm/"
#define ATTACHMENT_PATH DATA_PATH "attachment/"
#define CROSSHAIR_PATH  DATA_PATH "crosshair/"
#define SHADER_PATH     DATA_PATH "shader/"
#define FONT_PATH       DATA_PATH "font/"
#define TEXTURE_PATH    DATA_PATH "texture/"
#define SOUND_PATH      DATA_PATH "sound/"
#define MATERIAL_PATH   DATA_PATH "material/"
#define PIPELINE_PATH   DATA_PATH "pipeline/"

#define GRAVITY 9.81


#endif // __DEFINES_H__