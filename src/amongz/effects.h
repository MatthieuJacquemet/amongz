// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __EFFECTS_H__
#define __EFFECTS_H__


#include <panda3d/renderEffect.h>
#include "gdk/utils.h"


class ComposeTransform final: public RenderEffect {

    REGISTER_TYPE("ComposeTransform", RenderEffect)

public:
    static CPT(RenderEffect) make(PandaNode* node_0, PandaNode* node_1);
    ~ComposeTransform() = default;

    void adjust_transform(CPT(TransformState)& net_transform,
                        CPT(TransformState)& node_transform,
                        const PandaNode* node) const override;
    
    bool has_adjust_transform() const override;

private:
    ComposeTransform() = default;
    WPT(PandaNode) _node_0;
    WPT(PandaNode) _node_1;
};



class KeepScale final: public RenderEffect {

    REGISTER_TYPE("KeepScale", RenderEffect)

public:
    static CPT(RenderEffect) make();
    ~KeepScale() = default;
    
    void cull_callback(CullTraverser* trav, CullTraverserData& data,
                        CPT(TransformState)& node_transform,
                        CPT(RenderState)& node_state) const override;

    bool has_cull_callback() const override;

private:
    KeepScale() = default;
};

#endif // __EFFECTS_H__