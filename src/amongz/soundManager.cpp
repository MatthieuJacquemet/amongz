// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/audioSound.h>

#include "soundManager.h"
#include "player.h"
#include "map.h"

using namespace std;

DEFINE_TYPEHANDLE(SoundManager)


SoundManager::SoundManager(Map* map): 
    _map(map)
{
    
}


void SoundManager::update() {

    ClockObject* clock = ClockObject::get_global_clock();
    Emitters::iterator it = _emitters.begin();

    while (it != _emitters.begin()) {
        
        WeakNodePath wpath = it->first;

        if (wpath.was_deleted()) {
            it = _emitters.erase(it);
            continue;
        }
        
        NodePath path = wpath.get_node_path();
        EmitterData& data = it->second;

        LPoint3 pos = path.get_pos(*_map);

        LPoint3 prev = data._prev_pos;
        data._prev_pos = pos;

        LVector3 vel = (pos - prev) / clock->get_dt();

        Sounds::iterator sid = data._sounds.begin();

        while (sid != data._sounds.end()) {

            if (PT(AudioSound) sound = sid->lock()) {
                sound->set_3d_attributes(pos.get_x(), pos.get_y(), pos.get_z(),
                                         vel.get_x(), vel.get_y(), vel.get_z());
                ++sid;
            } else
                sid = data._sounds.erase(sid);
        }
        ++it;
    }
}


void SoundManager::play_sound(const NodePath& path, Filename filename) {
    
    size_t num_players = _map->get_num_player();

    for (size_t i=0; i<num_players; ++i) {
        Player* player = _map->get_player(i);

        AudioSound* sound = player->play_sound(filename);

        if (sound != nullptr)
            _emitters[path]._sounds.insert(sound);
    }
}
