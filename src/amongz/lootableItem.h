// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LOOTABLEITEM_H__
#define __LOOTABLEITEM_H__

#include <panda3d/modelNode.h>
#include "utils.h"


class LootableItem: public ModelNode {

    REGISTER_TYPE("LootableItem", ModelNode)

public:
    LootableItem(const std::string& name);
    ~LootableItem() = default;

    float get_wear() const;

protected:
    float _wear;
};

#endif // __LOOTABLEITEM_H__