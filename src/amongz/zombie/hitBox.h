// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#ifndef __HITBOX_H__
#define __HITBOX_H__

#include "physic_utils.h"
#include "gdk/utils.h"
#include "callbacks.h"

class Zombie;


class HitBox: public GhostCallback, public TaskPool {

    REGISTER_TYPE("HitBox", GhostCallback);

public:
    HitBox(const HitBox& copy);
    ~HitBox() = default;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

    virtual void do_callback(BulletPersistentManifold* manifold, 
                            PandaNode* other) override;

    Zombie* get_zombie(bool force=false);

    virtual void parents_changed() override;

protected:
    HitBox();

    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;
    
    Zombie* _zombie;
    float _damage_factor;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __HITBOX_H__