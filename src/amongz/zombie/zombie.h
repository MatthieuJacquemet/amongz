// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#ifndef __ZOMBIE_H__
#define __ZOMBIE_H__

#include <panda3d/nameUniquifier.h>
#include <panda3d/character.h>

#include "customEventHandler.h"

#include "gdk/utils.h"


class ZombieData;
class ZombieView;
class ZombieController;


class Zombie: public LocalNodePath<PandaNode>, public EventHandlerBase {

    REGISTER_TYPE("Zombie", LocalNodePath<PandaNode>)

public:
    ~Zombie() = default;
    Zombie(const Zombie& copy);

    float get_max_speed() const;
    float get_damage() const;
    float get_max_health() const;
    float get_mass() const;
    float get_health() const;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

    void  wound(float damage);

    bool is_alive() const;
    
    virtual void kill();

    virtual AnimBundle* get_anim(const std::string& name) const;
    virtual AnimBundle* get_anim(size_t id) const;
    virtual size_t get_num_anims() const;


protected:
    Zombie(const std::string& name="");

    class ZombieData: public ReferenceCount {
    public:
        ZombieData() = default;
        ~ZombieData() = default;

        float _max_speed;
        float _damage;
        float _max_health;
        float _mass;
    };

    PT(ZombieData) _data;

    float _health;

    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;

    using Namable::set_name;
    static NameUniquifier _names;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);

    typedef pmap<std::string, PT(AnimBundle)> Anims;
    static Anims _anims;
};









#endif // __ZOMBIE_H__