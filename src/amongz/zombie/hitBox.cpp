// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bulletWorld.h>
#include <panda3d/bulletCapsuleShape.h>
#include <panda3d/bulletPersistentManifold.h>

#include "bullet.h"
#include "game.h"
#include "map.h"
#include "zombie.h"
#include "hitBox.h"

using namespace std;

DEFINE_TYPEHANDLE(HitBox)


HitBox::HitBox(): 
    GhostCallback("hitbox"), 
    _zombie(nullptr),
    _damage_factor(1.0)
{

    notify_collisions(true);
    set_collision_response(true);
    set_into_collide_mask(CollideMask::bit(Map::CM_hitbox));
}


HitBox::HitBox(const HitBox& copy):
    GhostCallback(copy),
    _zombie(nullptr),
    _damage_factor(copy._damage_factor)
{


    notify_collisions(true);
    set_collision_response(true);
    set_into_collide_mask(CollideMask::bit(Map::CM_hitbox));
    
    BulletWorld* world = get_physic_world();
    world->attach_ghost(this);
}


void HitBox::register_with_read_factory() {
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* HitBox::make_copy() const {
    return new HitBox(*this);
}


void HitBox::do_callback(BulletPersistentManifold* manifold, PandaNode* other) {


    if (get_zombie() == nullptr)
        return;

    if (other->is_of_type(Bullet::get_class_type())) {
        Bullet* bullet = DCAST(Bullet, other);
        
        BulletManifoldPoint* point = manifold->get_manifold_point(0);
        float impulse = point->get_applied_impulse();
        float damage = impulse * bullet->get_damage() * 10.0f;

        _zombie->wound(damage);
    }
}


Zombie* HitBox::get_zombie(bool force) {

    if (_zombie != nullptr && !force)
        return _zombie;

    PandaNode* node = ::find_parent(this, Zombie::get_class_type());
    _zombie = DCAST(Zombie, node);

    return _zombie;
}


void HitBox::parents_changed() {

    BulletWorld* world = get_physic_world();

    if (get_num_parents() == 0)
        world->remove_ghost(this);
}


void HitBox::write_datagram(BamWriter* manager, Datagram& me) {
    
    GhostCallback::write_datagram(manager, me);
    me.add_float32(_damage_factor);
}


void HitBox::fillin(DatagramIterator& scan, BamReader *manager) {
    
    GhostCallback::fillin(scan, manager);
    _damage_factor = scan.get_float32();
}


TypedWritable* HitBox::make_from_bam(const FactoryParams& params) {
    
    HitBox* me = new HitBox;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}
