// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bamFile.h>
#include <panda3d/animBundleNode.h>
#include <panda3d/eventQueue.h>

#include "game.h"
#include "map.h"
#include "zombieView.h"
#include "zombieAIController.h"
#include "zombie.h"

using namespace std;

DEFINE_TYPEHANDLE(Zombie)
NameUniquifier Zombie::_names;
Zombie::Anims Zombie::_anims;


Zombie::Zombie(const string& name):
    LocalNodePath(_names.add_name(name)),
    _data(new ZombieData),
    _health(1.0)
{
    ref();
}


float Zombie::get_max_speed() const {
    return _data->_max_speed;
}

float Zombie::get_damage() const {
    return _data->_damage;
}

float Zombie::get_max_health() const {
    return _data->_max_health;
}

float Zombie::get_mass() const {
    return _data->_mass;
}

float Zombie::get_health() const {
    return _health;
}

void Zombie::register_with_read_factory() {
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* Zombie::make_copy() const {
    return new Zombie(*this);
}

void Zombie::wound(float damage) {

    _health = max(_health - damage, 0.0f);
    
    // die
    if (_health == 0.0)
        kill();
}


bool Zombie::is_alive() const {
    return _health > 0.0f;
}


void Zombie::kill() {

    remove_node();
    unref_delete(this);
}


AnimBundle* Zombie::get_anim(const std::string& name) const {

    Anims::const_iterator it = _anims.find(name);

    if (it != _anims.end())
        return it->second;

    AnimBundle* anim = nullptr;


    Filename path(AssetManager::get_default_location());
    path.set_basename_wo_extension(name);
    path.set_extension("bam");

    AnimBundleNode* node = DCAST(AnimBundleNode, read_object(path));

    if (node != nullptr) {
        anim = node->get_bundle();
        _anims[name] = anim;
    }

    return anim;
}


AnimBundle* Zombie::get_anim(size_t id) const {
    
    nassertr(id < _anims.size(), nullptr)
    return next(_anims.begin(), id)->second;
}


size_t Zombie::get_num_anims() const {    
    return _anims.size();
}


Zombie::Zombie(const Zombie& copy):
    LocalNodePath(copy),
    _health(copy._health),
    _data(copy._data)
{
    ref();
    set_name(_names.add_name(get_name()));
}


void Zombie::write_datagram(BamWriter* manager, Datagram& me) {
    
    PandaNode::write_datagram(manager, me);

    me.add_float32(_data->_max_health);
    me.add_float32(_data->_max_speed);
    me.add_float32(_data->_damage);
    me.add_float32(_data->_mass);
}


void Zombie::fillin(DatagramIterator& scan, BamReader *manager) {
    
    PandaNode::fillin(scan, manager);

    _data->_max_health = scan.get_float32();
    _data->_max_speed = scan.get_float32();
    _data->_damage = scan.get_float32();
    _data->_mass = scan.get_float32();
}


TypedWritable* Zombie::make_from_bam(const FactoryParams& params) {
    
    Zombie* me = new Zombie;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}