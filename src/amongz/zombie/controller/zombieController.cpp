// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/modelPool.h>

#include "zombie.h"
#include "zombieView.h"
#include "zombieController.h"


using namespace std;

DEFINE_TYPEHANDLE(ZombieController)
DEFINE_TYPEHANDLE(ZombieController::ZombieParam)


Singleton<ZombieController::ControllerFactory> ZombieController::_factory;



ZombieController::ZombieController(const string& name): 
    Zombie(name)
{
   
}

ZombieController::ZombieController(const Zombie& zombie):
    Zombie(zombie)
{
    
}


PT(ZombieController) ZombieController::make(TypeHandle type, const string& name) {
    
    Zombie* zombie = get_asset<Zombie>(name);

    if (zombie == nullptr)
        return nullptr;


    FactoryParams params;
    params.add_param(new ZombieParam(zombie));

    ControllerFactory* factory = get_factory();

    PT(ZombieController) controller = factory->make_instance(type, params);

    PandaNode::InstanceMap map;
    controller->r_copy_children(zombie, map, Thread::get_current_thread());

    return controller;
}


Zombie* ZombieController::parse_params(const FactoryParams& params) {
    
    nassertr(params.get_num_params() > 0, nullptr)
    ZombieParam* param = DCAST(ZombieParam, params.get_param(0));
    
    return param->get_zombie();
}


ZombieController::ZombieParam::ZombieParam(Zombie* zombie): 
    _zombie(zombie)
{
    
}


Zombie* ZombieController::ZombieParam::get_zombie() const {
    return _zombie;
}


void ZombieController::follow(NodePath target) {
    
}


ZombieController::ControllerFactory* ZombieController::get_factory() {
    
    if (UNLIKELY(!_factory))
        _factory = new ControllerFactory;
    
    return _factory;
}
