// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __ZOMBIECONTROLLER_H__
#define __ZOMBIECONTROLLER_H__

#include <panda3d/pointerTo.h>
#include <panda3d/aa_luse.h>
#include "utils.h"
#include "zombie.h"


class Player;
class NodePath;


class ZombieController: public Zombie {

    REGISTER_TYPE("ZombieController", Zombie)

public:
    static PT(ZombieController) make(TypeHandle type, const std::string& name);

    typedef Factory<ZombieController> ControllerFactory;

    virtual ~ZombieController() = default;

    virtual void follow(NodePath target);

    static ControllerFactory* get_factory();


protected:
    ZombieController(const std::string& name);
    ZombieController(const Zombie& zombie);

    static Singleton<ControllerFactory> _factory;

    static Zombie* parse_params(const FactoryParams& params);

private:
    class ZombieParam: public FactoryParam {

        REGISTER_TYPE("ZombieController::ZombieParam", FactoryParam)    
    public:
        ZombieParam(Zombie* zombie);
        Zombie* get_zombie() const;
    private:
        PT(Zombie) _zombie;
    };
};



#endif // __ZOMBIECONTROLLER_H__