// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/aiWorld.h>
#include "zombie.h"
#include "zombieIdleState.h"
#include "zombieFollowState.h"
#include "player.h"
#include "game.h"
#include "map.h"
#include "callbacks.h"
#include "spawn.h"
#include "zombieView.h"
#include "zombieAIController.h"


using namespace std;


DEFINE_TYPEHANDLE(ZombieAIController)
DEFINE_TYPEHANDLE(ZombieAIController::ZombieParam)


ZombieAIController::ZombieAIController(const string& name):
    ZombieController(name),
    _view(new ZombieView(this))
{

}


ZombieAIController::ZombieAIController(const ZombieAIController& copy):
    ZombieController(copy),
    _view(new ZombieView(this))
{

}


ZombieAIController::ZombieAIController(const Zombie& zombie): 
    ZombieController(zombie),
    _view(new ZombieView(this))
{

}


void ZombieAIController::follow(NodePath target) {
    
    _target = target;
    AIBehaviors* behavior = _ai->get_ai_behaviors();

    behavior->pursue(target);
}


void ZombieAIController::register_factory() {

    get_factory()->register_factory(get_class_type(), make);
    Spawn::get_factory()->register_factory(get_class_type(), spawn);
}


void ZombieAIController::kill() {
    
    AIWorld* world = get_ai_world();
    world->remove_ai_char(get_name());
    _fsm.clear_states();
    
    ZombieController::kill();    
}


void ZombieAIController::r_copy_children(const PandaNode* from, 
            InstanceMap& inst_map, Thread *current_thread)
{
    ZombieController::r_copy_children(from, inst_map, current_thread);

    setup();
}


void ZombieAIController::setup() {

    do_add_task(named_method(update));

    _ai = new AICharacter(get_name(), *this, get_mass(), 0.3, get_max_speed());

    AIWorld* world = get_ai_world();
    world->add_ai_char(_ai);
    
    _fsm.add_state(new ZombieIdleState(this));
    _fsm.add_state(new ZombieFollowState(this));

    _fsm.push_state("idle");

    _view->setup();
}


AsyncTask::DoneStatus ZombieAIController::update(AsyncTask* task) {

    _fsm.update();
    _view->update();

    return AsyncTask::DS_cont;
}


std::string ZombieAIController::parse_params(const FactoryParams& params) {
    
    nassertr(params.get_num_params() > 0, nullptr)
    ZombieParam* param = DCAST(ZombieParam, params.get_param(0));
    
    return param->get_name();
}


ZombieAIController::ZombieParam::ZombieParam(const string& name):
    _name(name)
{
    
}


string ZombieAIController::ZombieParam::get_name() const {

    return _name;
}


ZombieController* ZombieAIController::make(const FactoryParams& params) {
    
    Zombie* zombie = ZombieController::parse_params(params);

    if (zombie != nullptr)
        return new ZombieAIController(*zombie);

    return new ZombieAIController;
}


PandaNode* ZombieAIController::spawn(const FactoryParams& params) {
    
    string name = parse_params(params);
    
    if (name.empty())
        return nullptr;

    Zombie* zombie = get_asset<Zombie>(name);

    if (zombie == nullptr)
        return nullptr;

    PT(ZombieAIController) controller = new ZombieAIController(*zombie);

    PandaNode::InstanceMap map;
    controller->r_copy_children(zombie, map, Thread::get_current_thread());

    return controller;
}