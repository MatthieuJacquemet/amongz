// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ZOMBIEAICONTROLLER_H__
#define __ZOMBIEAICONTROLLER_H__

#include <panda3d/aiCharacter.h>
#include "zombieController.h"
#include "fsm.h"
#include "callbacks.h"


class Zombie;
class ZombieView;

class ZombieAIController: public ZombieController, public TaskPool {

    REGISTER_TYPE("ZombieAIController", ZombieController)

public:
    virtual ~ZombieAIController() = default;

    virtual void follow(NodePath target);

    static void register_factory();

    virtual void kill() override;

    virtual void r_copy_children(const PandaNode* from, InstanceMap& inst_map,
                                Thread *current_thread) override;

    static std::string parse_params(const FactoryParams& params);

    class ZombieParam: public FactoryParam {

        REGISTER_TYPE("ZombieAIController::ZombieParam", FactoryParam)    
    public:
        ZombieParam(const std::string& name);
        std::string get_name() const;
    private:
        std::string _name;
    };

protected:
    ZombieAIController(const std::string& name="");
    ZombieAIController(const ZombieAIController& copy);

    void setup();

    AsyncTask::DoneStatus update(AsyncTask* task);

    PT(ZombieView) _view;
    PT(AICharacter) _ai;

    NodePath _target;

    FSM _fsm;

private:
    

    ZombieAIController(const Zombie& zombie);

    static ZombieController* make(const FactoryParams& params);
    static PandaNode* spawn(const FactoryParams& params);
    
};




#endif // __ZOMBIEAICONTROLLER_H__