// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __ZOMBIEIDLESTATE_H__
#define __ZOMBIEIDLESTATE_H__


#include "map.h"
#include "game.h"
#include "fsm.h"
#include "zombieController.h"
#include "zombieIdleState.h"


using namespace std;


void ZombieIdleState::enter() {
    
    Game* game = Game::get_global_ptr();
    Map* map = game->get_current_map();

    int num_players = map->get_num_player();

    if (num_players == 0)
        return;

    NodePath zombie_np(_controller);
    NodePath target(map->get_player(0));

    float min_dist = zombie_np.get_distance(target);

    for (int i=0; i<num_players; ++i) {
        NodePath player(map->get_player(i));

        float dist = zombie_np.get_distance(player);

        if (dist < min_dist) {
            min_dist = dist;
            target = player;
        }
    }

    _controller->follow(target);
    get_fsm()->push_state("follow");
}


void ZombieIdleState::update() {

}


void ZombieIdleState::exit() {
    
}


void ZombieIdleState::move() {
    
}


ZombieIdleState::ZombieIdleState(ZombieController* controller): 
    ZombieState("idle", controller) 
{
    // _flags = F_always_update;
}

#endif // __ZOMBIEIDLESTATE_H__