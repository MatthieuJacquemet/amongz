// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/animControlCollection.h>  // for AnimControlCollection
#include <panda3d/loader.h>                 // for Loader
#include <panda3d/bulletCapsuleShape.h>
#include <panda3d/bulletRigidBodyNode.h>
#include <panda3d/bulletWorld.h>
#include <panda3d/cIntervalManager.h>

#include "animSequenceControl.h"
#include "game.h"
#include "map.h"
#include "zombie.h"
#include "render_utils.h"
#include "config_game.h"
#include "zombieView.h"


using namespace std;

DEFINE_TYPEHANDLE(ZombieView)


ZombieView::ZombieView(Zombie* zombie):
    _zombie(zombie)

{    

}


void ZombieView::setup() {
    
    // enable GPU skinning
    if (hardware_skinning) {
        NodePathCollection paths = _zombie->find_all_matches("**/-GeomNode");

        for (int i=0; i<paths.get_num_paths(); ++i) {
            NodePath path = paths.get_path(i);

            path.set_shader(Shader::load(SHADER_LANG,
                    GDK_SHADER_PATH VERT_SHADER("skinning"),
                    GDK_SHADER_PATH FRAG_SHADER("opaque")), 20);
            
            const RenderState* state = path.get_state();
            const ShaderAttrib* sattr;

            static const int flag = ShaderAttrib::F_hardware_skinning;

            if (state->get_attrib(sattr))
                path.set_attrib(sattr->set_flag(flag, true));
        }
    }


    int flags = PartGroup::HMF_ok_wrong_root_name | 
                PartGroup::HMF_ok_part_extra | 
                PartGroup::HMF_ok_anim_extra; 

    static vector<string> anims = {"zombie_0_zombie_walk"};

    NodePath rig_np = _zombie->find("**/+Character");

    if (!rig_np.is_empty()) {

        _character = DCAST(Character, rig_np.node());

        PartBundle* bundle = _character->get_bundle(0);
        
        for (const string& anim_name: anims) {
            AnimBundle* anim = _zombie->get_anim(anim_name);

            PT(AnimSequenceControl) control = 
                new AnimSequenceControl(anim_name, bundle);

            control->set_event_name("anim-sequence");
            control->set_event_queue(_zombie->get_event_queue());
            
            PartSubset subset;

            if (bundle->do_bind_anim(control, anim, flags, subset))
                _controls.store_anim(control, anim_name);
        }
    }

    // _controls.loop("zombie_0_zombie_walk", true);
}


void ZombieView::update() {

}