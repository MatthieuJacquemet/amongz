// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/camera.h>
#include <panda3d/orthographicLens.h>
#include <panda3d/displayRegion.h>
#include <panda3d/cardMaker.h>
#include <panda3d/textNode.h>
#include <panda3d/fontPool.h>
#include <panda3d/texturePool.h>
#include <panda3d/samplerState.h>

#include "viewport.h"
#include "render_utils.h"
#include "defines.h"
#include "gun.h"
#include "fpsController.h"
#include "color.h"
#include "playerHUD.h"

using namespace std;


DEFINE_TYPEHANDLE(PlayerHUD)



PlayerHUD::PlayerHUD(Player* player, DisplayRegion* region):
    _region(region),
    _player(player),
    _crosshair_value(0.0f),
    _crosshair_scale(0.0f),
    _viewport_offset(1, LVector2i(0)),
    _root("hud")
{
    create_2d_scene(_root, _camera_np);

    region->set_camera(_camera_np);
    init();
}


PlayerHUD::~PlayerHUD() {

    if (_region != nullptr && _region->get_camera() == _camera_np)
        _region->set_camera(NodePath());
}


void PlayerHUD::set_view_texture(Texture* texture) {

    if (texture == nullptr)
        return;

    if (_view_quad.is_empty()) {
        create_fullscreen_quad(_root, _view_quad);
        _view_quad.set_shader_input("u_viewport_offset", _viewport_offset);
    }

    _view_texture = texture;
    _view_quad.set_shader_input("u_render", texture);;
}


void PlayerHUD::update_region() {

    int l, r, b, t;
    _region->get_pixels(l, r, b, t);
    _viewport_offset.set_element(0, LVector2i(l, b));

    Camera* camera = DCAST(Camera, _camera_np.node());
    Lens* lens = camera->get_lens();

    lens->set_aspect_ratio(float(r - l) / float(t - b));
}


void PlayerHUD::update() {

    CPT(TransformState) transform = _crosshair->get_transform();
    ClockObject* clock = ClockObject::get_global_clock();

    LVector3 pos(transform->get_pos());

    float value = -clock->get_dt();
    float motion = _player->get_motion().length();

    if (motion > 0.001f)
        value = motion;
    

    float crouch = lerp(1.0, 0.5, _player->get_crouch());

    _crosshair_value = saturate(_crosshair_value + value * 3.0f / crouch);

    float min_v = _crosshair_scale * crouch;
    float max_v = _crosshair_scale * crouch * 4.0f;

    value = lerp(min_v, max_v, _crosshair_value);

    float ads = 1.0f - _player->get_ads();
    float alpha = lerp(1.0f, 0.5f, _crosshair_value) * ads;

    _crosshair_np.set_color(Color(1.0f,1.0f,1.0f, alpha));
    _crosshair_np.set_z(value * ads);
}


void PlayerHUD::init() {

    _root.set_depth_write(false);
    _root.set_depth_test(false);
    _root.set_material_off(true);
    _root.set_two_sided(true);

    // ammo counter

    _player->add_hook("take-gun", callback(take_gun));
    _player->add_hook("clip_in", callback(reload));
    _player->add_hook("fire", callback(fire));

    TextFont* font = FontPool::load_font(FONT_PATH "roboto.ttf");
    nassertv(font != nullptr)

    _ammo_counter = new TextNode("ammo-counter");
    _ammo_counter->set_align(TextNode::A_right);
    _ammo_counter->set_font(font);
    _ammo_counter->set_text("0/0");
    _ammo_counter->set_text_scale(0.05);
    _ammo_counter->set_shadow(0.0f, 0.002f);
    _ammo_counter->set_shadow_color(Color::black());

    NodePath counter_np = _root.attach_new_node(_ammo_counter, 10);
    counter_np.set_pos(0.9, 0.0, -0.5);

    // crosshair

    CardMaker cm("crosshair");
    cm.set_frame(-0.01f,0.01f,-0.01f,0.01f);

    _crosshair = cm.generate();
    _crosshair_np = NodePath(_crosshair);

    Texture* tex = TexturePool::load_texture(TEXTURE_PATH "crosshair.png");
    tex->set_wrap_u(SamplerState::WM_border_color);
    tex->set_wrap_v(SamplerState::WM_border_color);
    tex->set_border_color(Color::transparent());

    for (int i=0; i<4; ++i) {

        NodePath path = _root.attach_new_node("rotation");
        NodePath crosshair_np = path.attach_new_node(_crosshair);

        crosshair_np.set_bin("transparent", 0);
        crosshair_np.set_transparency(TransparencyAttrib::M_alpha);
        crosshair_np.set_texture(tex);
        path.set_r(i * 90.0f);
    }
}


void PlayerHUD::fire(const Event* event) {
    
    nassertv(event->get_num_parameters() > 0)
    EventParameter param = event->get_parameter(0);

    Gun* gun = DCAST(Gun, param.get_ptr());
    _crosshair_value += 0.8f;

    set_ammo(gun->get_num_rounds());
}


void PlayerHUD::set_region(DisplayRegion* region) {

    if (_region != nullptr)
        _region->set_camera(NodePath());

    _region = region;
    _region->set_camera(_camera_np);    
}


void PlayerHUD::reload(const Event* event) {

    Weapon* weapon = _player->get_weapon();

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);

        _total_rounds = _player->get_ammo(gun->get_ammo());
        set_ammo(gun->get_num_rounds());
    }
}


void PlayerHUD::take_gun(const Event* event) {

    nassertv(event->get_num_parameters() > 0)
    EventParameter param = event->get_parameter(0);

    Gun* gun = DCAST(Gun, param.get_ptr());

    _total_rounds = _player->get_ammo(gun->get_ammo());
    _crosshair_scale = gun->get_recoil() * 0.15;

    set_ammo(gun->get_num_rounds());
}


void PlayerHUD::set_ammo(size_t ammo) {
    
    _ammo_counter->set_text(to_string(ammo) + '/' + to_string(_total_rounds));
}