// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FPSCONTROLLER_H__
#define __FPSCONTROLLER_H__


#include <panda3d/nodePath.h>
#include <panda3d/displayRegion.h>
#include <panda3d/animControlCollection.h>

#include "fsm.h"
#include "player.h"


class PlayerInput;
class AmmoPack;
class Weapon;
class ViewRoot;
class Gun;
class AudioManager;
class AudioSound;


class FpsController final : public Player, private TaskPool {

    REGISTER_TYPE("FpsController", Player)

public:
    FpsController(const std::string& name);
    ~FpsController();

    void take_weapon(WeaponSlot slot);
    void loot(LootableItem* item) override;
    void switch_weapon();

private:
    PT(ViewRoot) _view_root;

    AsyncTask::DoneStatus update(AsyncTask* task);
    AsyncTask::DoneStatus fire_seq(AsyncTask* task);

    PT(AudioManager) _sfx_mgr;

    bool has_input() const;
    void pick_input();

    void move(LVector2 dir);
    void rotate_view(LVector2 dir);

    void advance_crosshair(int n);

    void next_crosshair(bool enable);
    void prev_crosshair(bool enable);
    void reload(bool activate);
    void jump(bool activate);
    void fire(bool activate);
    void pullout(bool activate);
    void putaway(bool activate);
    void ads(bool activate);
    void crouch(bool activate);
    void toggle_fire_mode(bool activate);

    void set_map(Map* map) override;

    void do_fire(const Event* event);
    void do_action(const Event* event);
    void clip_in(const Event* event);
    void anim_sequence(const Event* event);

    void update_sfx();

    void do_play_sound(const std::string& name);

    AudioSound* play_gun_sound(const std::string& name);
    virtual AudioSound* play_sound(Filename filename) override;

    void loot_weapon(Weapon* weapon);
    void loot_ammo(AmmoPack* ammo);

    bool _auto_reload;
    bool _force_feedback;
    bool _is_crouch;

    LVector2 _move_vel;
    PT(PlayerInput) _input;

    PT(AsyncTask) _update;
    PT(AsyncTask) _fire_seq;

    struct ActionHandler {
        typedef void (FpsController::*Handler)(bool);
        Handler _handler = nullptr;
        bool _enabled = true;
    };

    std::array<ActionHandler, A_none> _action_handlers;
};


#endif // __FPSCONTROLLER_H__