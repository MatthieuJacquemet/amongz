// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include "anims.h"

using namespace std;


AdsControl::AdsControl():
    AnimPostProcess("ads"),
    _height_factor(1.0f),
    _final_height(0.0f)
{
    
}


bool AdsControl::set_control(AnimControl* control) {
    
    if (AnimPostProcess::set_control(control)) {

        int last_frame = control->get_num_frames() - 1;
        CPT(TransformState) transform = get_value(last_frame);

        _final_height = transform->get_pos().get_z();

        return true;
    }
    return false;
}


CPT(TransformState) AdsControl::update() {
    
    CPT(TransformState) value = get_blend_value();

    LVecBase3 pos = value->get_pos();
    pos.set_z(pos.get_z() * _height_factor);

    return value->set_pos(pos);
}


void AdsControl::set_ads_height(float height) {

    if (_final_height != 0.0)
        _height_factor = (_final_height - height) / _final_height;
    else
        _height_factor = 1.0f;
}



WeaponSway::WeaponSway():
    AnimLayer("weapon_sway"),
    _sway(0.0f),
    _value(0.0f),
    _stiffness(10.0f),
    _factor(1.0f),
    _easing(2.0)
{       

}


CPT(TransformState) WeaponSway::update() {
    
    ClockObject* clock = ClockObject::get_global_clock();

    LVector3 value(atan(_sway.get_x()), atan(_sway.get_y()), 0.0f);

    _value /= clock->get_dt() * _stiffness + 1.0f;
    _value += value;

    LVector3 pos(_value.get_x(), 0.0f, -_value.get_y());
    LVector3 rot(_value.get_x(), _value.get_y(), -_value.get_x());

    float tfact = lerp(0.002f, 0.001f, _factor);
    float rfact = (1.0f - _factor) * 0.3f;

    pos.set_z(pos.get_z() * lerp(0.6, 1.0, _factor));
    rot.set_y(rot.get_y() * 2.0f);

    return TransformState::make_pos_hpr(pos * tfact, rot * rfact);

    // CPT(TransformState) offset = TransformState::make_pos(-_offset);
    // CPT(TransformState) transform = TransformState::make_hpr(value);

    // return offset->invert_compose(transform)->compose(offset);
}


void WeaponSway::set_factor(float factor) {
    _factor = factor;
}

void WeaponSway::set_sway(float x, float y) {
    _sway.set(x, y);
}


float WeaponSway::ease(float x) {

    float s = copysignf(1.0f, x);
    return s - pow(1.0f - abs(x), _easing) * s;
}


void WeaponSway::set_stiffness(float stiffness) {

    _stiffness = max(0.0f, stiffness);
}


IdleSway::IdleSway():
    AnimLayer("idle_sway"),
    _factor(1.0f),
    _speed(1.3f),
    _time(0.0)
{

}


CPT(TransformState) IdleSway::update() {
    
    ClockObject* clock = ClockObject::get_global_clock();

    _time += clock->get_dt() * _speed;

    float ty = _time;
    float tx = ty * 0.73f + 21.84;// - 0.25f;

    float x = noise(tx) * _factor * 0.01f;
    float y = noise_sin(ty, 1.0f) * _factor * 0.002f;

    LVector3 pos(x, 0.0f, y);
    LVector3 hpr(rad_2_deg(-x), rad_2_deg(y), 0.0f);

    return TransformState::make_pos_hpr(pos*0.3f, hpr);
}


void IdleSway::set_factor(float factor) {
    _factor = factor;
}

void IdleSway::set_speed(float speed) {
    _speed = speed;
}



CamRecoil::CamRecoil(): 
    AnimLayer("cam_recoil"),
    _stiffness(20.0f),
    _value(0.0f),
    _torque(0.0f)
{
    
}


CPT(TransformState) CamRecoil::update() {

    ClockObject* clock = ClockObject::get_global_clock();

    _value += _torque;

    _torque /= (60.f * clock->get_dt() + 1.0f);
    _value /= (_stiffness * clock->get_dt() + 1.0f);

    return TransformState::make_hpr(_value);    
}


void CamRecoil::add_recoil(float factor) {

    _torque.set(randf(-factor*0.5, factor) * 1.0f,
                randf(-factor, factor) * 0.7f,
                randf(-factor, factor) * 0.4f);
}


void CamRecoil::set_stiffness(float stiffness) {
    _stiffness = stiffness;
}



CamBobbing::CamBobbing(): 
    AnimLayer("cam_bobbing"),
    _factor(1.0f),
    _velocity(0.0f),
    _time(0.0),
    _move(0.0f)
{
    
}


CPT(TransformState) CamBobbing::update() {

    LVector3 value(0.0f);
    ClockObject* clock = ClockObject::get_global_clock();

    _velocity += _factor * _move.length() * 5.0f;
    _velocity /= 1.0f + 5.0f * clock->get_dt();

    _time += _velocity * clock->get_dt();

    float ty = _time * 7.0;
    float tx = ty * 0.5 + 0.25; 

    value.set_x(noise_sin(tx, 0.4f));
    value.set_y(noise_sin(ty, 0.4f) * 0.5f);

    return TransformState::make_hpr(value * _velocity * 0.0);
}


void CamBobbing::set_factor(float factor) {
    _factor = factor;
}

void CamBobbing::set_move(LVector2 move) {
    _move = move;
}



WeaponBobbing::WeaponBobbing(): 
    AnimPostProcess("weapon_bobbing"),
    _direction(0.0f),
    _factor(1.0f),
    _move(0.0f),
    _time(0.0)
{
    
}


CPT(TransformState) WeaponBobbing::update() {


    ClockObject* clock = ClockObject::get_global_clock();
    CPT(TransformState) bobbing = get_blend_value();

    float attenuation = 1.0f + clock->get_dt() * 4.0f;

    _direction += _move * 4.0f;
    _direction /= attenuation;

    float vel = min(_direction.length(), 1.0f);
    float velocity = vel * _factor;

    if (velocity > 0.001f) {
        if (_control && !_control->is_playing())
            _control->loop(true);
    }
    else if (_control && _control->is_playing())
        _control->stop();

    _control->set_play_rate(velocity * 1.2f);

    _time += vel * clock->get_dt();
    float ty = _time * 7.0;
    float tx = ty * 0.5 + 0.25;

    LPoint3 ads_pos;
    LVecBase3 ads_hpr;

    const float ads_fact = 0.0015;

    ads_pos.set_x(noise_sin(tx, 0.4f) * ads_fact);
    ads_pos.set_y(0.0f);
    ads_pos.set_z(noise_sin(ty, 0.4f) * ads_fact * 0.5f);

    ads_hpr.set_x(ads_pos.get_x() * 100.0f);
    ads_hpr.set_y(ads_pos.get_z() * -100.0f);
    ads_hpr.set_z(0.0f);


    LVector3 pos = lerp(ads_pos, bobbing->get_pos(), _factor) * vel;
    LVector3 hpr = lerp(ads_hpr, bobbing->get_hpr(), _factor) * vel;


    LVector3 base_hpr;
    LVector3 base_pos(0.0f, 0.0f, -velocity * 0.01);

    float backward = min(_direction.get_y(), 0.0f) * 5.0f;

    base_hpr.set_y(abs(_direction.get_x()) * 2.0f - backward);
    base_hpr.set_x(_direction.get_x() * -5.0f);
    base_hpr.set_z(_direction.get_x() * 2.0f);

    base_hpr *= _factor;

    CPT(TransformState) base = TransformState::make_pos_hpr(base_pos, base_hpr);

    return base->compose(TransformState::make_pos_hpr(pos * 0.7, hpr * 0.7));
}


void WeaponBobbing::set_move(LVector2 move) {
    _move = move;
}


void WeaponBobbing::set_factor(float factor) {
    _factor = factor;
}