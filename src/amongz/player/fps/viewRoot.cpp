// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/camera.h>
#include <panda3d/auto_bind.h>
#include <panda3d/cLerpAnimEffectInterval.h>
#include <panda3d/characterJointEffect.h>
#include <panda3d/animControlCollection.h>  // for AnimControlCollection
#include <panda3d/loader.h>                 // for Loader
#include <panda3d/graphicsOutput.h>         // for GraphicsOutput
#include <panda3d/stencilAttrib.h>          // for StencilAttrib
#include <panda3d/renderAttrib.h>           // for RenderAttrib, RenderAttrib::M_always
#include <panda3d/renderState.h>            // for RenderState
#include <panda3d/shaderAttrib.h>
#include <panda3d/cIntervalManager.h>
#include <panda3d/animChannelMatrixDynamic.h>
#include <panda3d/eventQueue.h>
#include <panda3d/animBundleNode.h>

#include "config_game.h"
#include "effects.h"
#include "game.h"
#include "gun.h"
#include "redDotSight.h"
#include "fpsController.h"
#include "animSequenceControl.h"
#include "animStack.h"
#include "anims.h"
#include "viewport.h"
#include "viewportManager.h"
#include "renderStage.h"
#include "renderPipeline.h"
#include "render_utils.h"
#include "playerHUD.h"
#include "partFilterAnim.h"
#include "bulletHole.h"
#include "viewRoot.h"

using namespace std;

DEFINE_TYPEHANDLE(ViewRoot)
DEFINE_TYPEHANDLE(ViewRoot::ViewLayer)



ViewRoot::ViewRoot(FpsController* controller):
    LocalNodePath<PandaNode>("view-root"),
    _controller(controller),
    _weapon_bobbing(new WeaponBobbing),
    _ads_control(new AdsControl),
    _weapon_sway(new WeaponSway),
    _idle_sway(new IdleSway),
    _cam_recoil(new CamRecoil),
    _cam_bobbing(new CamBobbing),
    _anim_stack(new AnimStack("tag_anim")),
    _ads_stack(new AnimStack("tag_ads")),
    _cam_stack(new AnimStack("tag_view")),
    _pipeline(nullptr),
    _view_cap(170.0f),
    _sway_offset(0.0f)
{
    
    EventQueue* queue = controller->get_event_queue();

    controller->add_hook("dimension-changed", callback(update_viewport));

    _viewport = make_viewport();
    _viewport->set_event_queue(queue);
    _viewport->set_event_name("dimension-changed");

    _hud = new PlayerHUD(_controller, _viewport);
    _pipeline = make_pipeline(_viewport);
    
 
    if (_pipeline != nullptr) {
        _pipeline->set_tag_name("main");
        _buffer = _pipeline->get_buffer();
        _hud->set_view_texture(_pipeline->get_output());
    }
    else {
        _buffer = _viewport->get_window();

        _buffer->set_clear_color_active(true);
        _buffer->set_clear_depth_active(true);
        _buffer->set_clear_stencil_active(true);

        _buffer->set_clear_color(LColor(0.2,0.2,0.2,1.0));
    }

    _foreground_layer = new ViewLayer(this);
    _background_layer = new ViewLayer(this);


    _anim_stack->add_layer(_weapon_sway);
    _anim_stack->add_layer(_weapon_bobbing);
    _anim_stack->add_layer(_idle_sway);
    _ads_stack->add_layer(_ads_control);
    _cam_stack->add_layer(_cam_recoil);
    _cam_stack->add_layer(_cam_bobbing);

    _fg_fov = 80.0, _fg_fov_ads = _fg_fov;
    _bg_fov = 60.0, _bg_fov_ads = _bg_fov;

    init_viewarms();
    init_layers();
}


ViewRoot::~ViewRoot() {

    if (_viewport != nullptr)
        _viewport->stash();
}


FpsController* ViewRoot::get_controller() const {
    
    return _controller;
}


float ViewRoot::get_ads_value() const {

    AnimControl* control = _ads_control->get_control();    

    if (control != nullptr && control->get_num_frames())
        return control->get_full_fframe() / control->get_num_frames();

    return 0.0f;
}


void ViewRoot::update() {

    float value = _controller->get_ads();

    _foreground_layer->set_fov(lerp(_fg_fov, _fg_fov_ads, value));
    _background_layer->set_fov(lerp(_bg_fov, _bg_fov_ads, value));

    _weapon_sway->set_factor(value);

    _idle_sway->set_factor(lerp(1.0f, 0.5f, value));
    _idle_sway->set_speed(lerp(1.0f, 0.5f, value));

    LVector3 vel = _controller->get_motion();
    float fact = _controller->is_on_ground();
    vel *= fact * lerp(1.0f, 1.5f, _controller->get_crouch());

    _weapon_bobbing->set_move(vel.get_xy());
    _cam_bobbing->set_move(vel.get_xy());

    _weapon_bobbing->set_factor(1.0f - value);
    _cam_bobbing->set_factor(value);

    Weapon* weapon = _controller->get_weapon();

    if (weapon != nullptr && weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);
        if (gun->is_firing())
            _cam_recoil->set_stiffness(7.0f);
        else
            _cam_recoil->set_stiffness(20.0f);
    }

    static PStatCollector anim_pcollector("ViewRoot-anims");
    {
        PStatTimer timer(anim_pcollector);
        _anim_stack->update();
        _ads_stack->update();
        _cam_stack->update(); 
    }
    _hud->update();
}


void ViewRoot::rotate_view(LVector2 dir) {

    float pitch = get_p();
    float cap = _view_cap * 0.5f;
    float clamped = clamp(pitch + dir.get_y(), -cap, cap);

    set_p(clamped);

    _weapon_sway->set_sway(-dir.get_x(), clamped - pitch);    
}


float ViewRoot::get_background_fov() const {

    Lens* lens = _background_layer->get_lens();
    if (lens != nullptr)
        return lens->get_hfov();

    return 1.0f;
}

float ViewRoot::get_foreground_fov() const {

    Lens* lens = _foreground_layer->get_lens();
    if (lens != nullptr)
        return lens->get_hfov();

    return 1.0f;
}


void ViewRoot::reload(bool empty) {

    if (empty) {
        if (!_controls.play("reload_empty"))
            _controls.play("reload");
    } else
        _controls.play("reload");
}


void ViewRoot::fire(bool ads) {

    Gun* gun = DCAST(Gun, _controller->get_weapon());
    nassertv(gun != nullptr)

    float fact = lerp(1.0, 0.5, ads);
    _cam_recoil->add_recoil(gun->get_recoil() * fact);

    if (ads) {
        if (!_controls.play("fire_ads"))
            _controls.play("fire");
    } else
        _controls.play("fire");
}


void ViewRoot::ads(bool activate) {

    AnimControl* control = _controls.find_anim("ads");

    if (control != nullptr) {
        
        int current = control->get_frame();
        int end = control->get_num_frames() - 1;

        if (activate) {
            control->set_play_rate(1.0);
            control->play(current, end);
        } else {
            control->set_play_rate(-1.0);
            control->play(0, current);
        }
    }
}


void ViewRoot::pullout(bool first_time) {

    _controls.stop_all();

    if (first_time) {
        if (!_controls.play("first_time_pullout"))
            _controls.play("pullout");
    } else
        _controls.play("pullout");
}


void ViewRoot::putaway(bool quick) {
    
    _controls.stop_all();

    if (quick) {
        if (!_controls.play("putaway_quick"))
            _controls.play("putaway");
    } else
        _controls.play("putaway");
}


void ViewRoot::set_scene(NodePath scene) {
    
    _background_layer->set_scene(scene);
}


CPT(TransformState) ViewRoot::get_fg_2_bg() const {
    
    Lens* fg_lens = _foreground_layer->get_lens();
    Lens* bg_lens = _background_layer->get_lens();

    LMatrix4 fg_proj = fg_lens->get_projection_mat();
    LMatrix4 bg_proj = bg_lens->get_projection_mat_inv();

    return TransformState::make_mat(fg_proj * bg_proj);
}


NodePath ViewRoot::get_camera_np() const {

    return _background_layer->get_camera();
}



Viewport* ViewRoot::make_viewport() {

    Game* game = Game::get_global_ptr();
    ViewportManager* mgr = game->get_viewport_manager();
    
    return mgr->acquire_viewport();
}


PT(RenderPipeline) ViewRoot::make_pipeline(Viewport* viewport) {
    
    if (render_pipeline.empty())
        return nullptr;

    GraphicsOutput* host = viewport->get_window();

    Filename filename(PIPELINE_PATH);
    filename.set_basename_wo_extension(render_pipeline);
    filename.set_extension("bam");

    TypedWritable* object = read_object(filename);
    PT(RenderPipeline) pipeline = (RenderPipeline*)object;

    if (pipeline != nullptr) {
        pipeline->setup(host, viewport->get_pixel_size());
        pipeline->register_node_handler(new BulletHolePool);
    }

    return pipeline;
}


void ViewRoot::update_viewport(const Event* event) {

    GraphicsOutput* window = _viewport->get_window();
    int width = 0, height = 0;

    if (window->has_size()) {
        height = _viewport->get_pixel_height();
        width = _viewport->get_pixel_width();
    }

    if (height == 0)
        return;
    
    float aspect_ratio = (float)width / (float)height;

    _hud->update_region();
    
    LVector4 dimensions = _viewport->get_dimensions();

    if (!_pipeline.is_null())
        _pipeline->set_size(LVector2i(width, height));
    else {
        _foreground_layer->set_dimensions(dimensions);
        _background_layer->set_dimensions(dimensions);
    }
    
    Lens* fg_lens = _foreground_layer->get_lens();
    Lens* bg_lens = _background_layer->get_lens();

    fg_lens->set_aspect_ratio(aspect_ratio);
    bg_lens->set_aspect_ratio(aspect_ratio);
}


void ViewRoot::init_viewarms() {

    Loader* loader = Loader::get_global_ptr();

    Filename path(MODEL_PATH, "us_military_viewarms.bam");;
    PT(PandaNode) model = loader->load_sync(path);

    // PandaNode* node = model->get_child(0);

    _character = DCAST(Character, model);
    _char_np = attach_new_node(_character);

    // enable GPU skinning
    if (hardware_skinning) {
        NodePath viewarms_np = _char_np.find("**/-GeomNode");

        viewarms_np.set_shader(Shader::load(SHADER_LANG,
                GDK_SHADER_PATH VERT_SHADER("skinning"),
                GDK_SHADER_PATH FRAG_SHADER("opaque")), 20);
        
        const RenderState* state = viewarms_np.get_state();
        const ShaderAttrib* sattr;

        static const int flag = ShaderAttrib::F_hardware_skinning;

        if (state->get_attrib(sattr))
            viewarms_np.set_attrib(sattr->set_flag(flag, true));
    }
}


void ViewRoot::init_layers() {

    // using two differents lenses for each layer allows more control on the
    // the field of view of each layer separately

    PerspectiveLens* bg_lens = new PerspectiveLens();

    bg_lens->set_fov(_bg_fov);
    bg_lens->set_film_size(1.0,1.0);
    bg_lens->set_near(0.01);
    bg_lens->set_far(500.0);

    PerspectiveLens* fg_lens = new PerspectiveLens(*bg_lens);

    fg_lens->set_fov(_fg_fov);


    _foreground_layer->set_lens(fg_lens);
    _background_layer->set_lens(bg_lens);

    Camera* fg_cam = _foreground_layer->get_camera_node();
    Camera* bg_cam = _background_layer->get_camera_node();

    bg_cam->set_effect(ComposeTransform::make(_controller, fg_cam));

    bg_cam->set_tag_state_key("main");
    fg_cam->set_tag_state_key("view");

    if (_pipeline != nullptr) {
        const SceneData* data = _pipeline->get_scene_data();
        ShaderState state;

        state.set_shader_input("u_linearDepth",
            _pipeline->get_render_target("linear-depth"));
        state.set_shader_input("u_nearFar", data->_near_far);

        bg_cam->set_tag_state("post-opaque", state);
        fg_cam->set_tag_state("post-opaque", state);
    }

    // foreground layer should be rendered before background layer writes to 
    // the depth buffer, aside frome preventing view model to clip through,
    // it optimizes depth occlusion since model covers a good part of the screen
    _foreground_layer->set_sort(-2);
    _background_layer->set_sort(-1);

    _background_layer->disable_clears();
    _foreground_layer->disable_clears();


    CPT(RenderAttrib) fg_attr = 
        StencilAttrib::make(true,
            RenderAttrib::M_always,
            StencilAttrib::SO_zero,
            StencilAttrib::SO_replace,
            StencilAttrib::SO_replace,
            1, 0, 1);

    CPT(RenderAttrib) bg_attr =
        StencilAttrib::make(true,
            RenderAttrib::M_equal,
            StencilAttrib::SO_keep,
            StencilAttrib::SO_keep,
            StencilAttrib::SO_keep,
            0, 1, 0);


    CPT(RenderState) fg_state = RenderState::make(fg_attr);
    CPT(RenderState) bg_state = RenderState::make(bg_attr);


    // foreground layer sets the stencil mask so that view models dont go
    // through scene's geometry, for instance when you stand close to a wall 
    _foreground_layer->set_initial_state(fg_state);
    _background_layer->set_initial_state(bg_state);


    // foreground layer must only render the view models, so starting traversal
    // from this node prevents from uselessly traversing the whole scene
    _foreground_layer->set_scene(*this);

    CharacterJoint* tag_cam = _character->find_joint("tag_camera");
    nassertv(tag_cam != nullptr)

    tag_cam->add_net_transform(_foreground_layer->get_camera_node());

    _foreground_layer->set_scissor_enabled(true);
    _background_layer->set_scissor_enabled(true);
}


void ViewRoot::take_gun(Gun* gun, bool first_time) {

    // replace current weapon
    if (!_weapon_np.is_empty()) {
        Gun* old_gun = DCAST(Gun, _weapon_np.node());
        old_gun->set_event_queue(nullptr);
        _weapon_np.reparent_to(_controller->get_top());
    }

    _weapon_np = NodePath(gun);
    _weapon_np.reparent_to(*this);

    NodePath path = _weapon_np.find("**/-Character");

    if (path.is_empty())
        return;

    Character* rig = DCAST(Character, path.node());
    _part_handle = rig->get_bundle_handle(0);

    _character->merge_bundles(_character->get_bundle_handle(0), _part_handle);


    _controls.clear_anims();

    size_t num_anims = gun->get_num_anim();


    CharacterJointBundle* part = _character->get_bundle(0);
    
    for (size_t i=0; i<num_anims; ++i)
        bind_anim(gun->get_anim(i), part);


    _ads_stack->set_bundle(part);
    _anim_stack->set_bundle(part);
    _cam_stack->set_bundle(part);


    AnimControl* control = _controls.find_anim("ads");
    if (control != nullptr)
        _ads_control->set_control(control);


    AnimBundle* bobbing = get_anim("bobbing");

    if (bobbing != nullptr)
        _weapon_bobbing->set_control(bind_anim(bobbing, part));


    AttachmentAnchor* anchor = gun->get_anchor(Sight::get_class_type());

    float height = 0.0f;
    float offset = 0.0f;

    _fg_fov_ads = _fg_fov;
    _bg_fov_ads = _bg_fov;

    if (anchor != nullptr) {
        Sight* sight = DCAST(Sight, anchor->get_attachment());
        NodePath anchor_np(anchor);

        if (sight != nullptr) {
            height = sight->get_height();
            _fg_fov_ads = sight->get_foreground_fov();
            _bg_fov_ads = sight->get_background_fov();

            offset = anchor_np.get_z() + height;
        }
    }

    _sway_offset = offset;
    _ads_control->set_ads_height(height);

    pullout(first_time);

    Event* event = new Event("take-gun");
    event->add_parameter(gun);
    _controller->dispatch_event(event);
}



CPT(TransformState) querry_net_transform(NodePath from) {
    
    NodePath path = from;
    FpsController* player_node = nullptr;

    NodePath root_path = from.get_top();
    PandaNode* root = root_path.node();
    
    if (!root->is_of_type(ViewRoot::get_class_type()))
        return from.get_net_transform();

    ViewRoot* view = DCAST(ViewRoot, root);
    
    FpsController* controller = view->get_controller();

    auto world_to_cont = controller->get_net_transform();
    auto view_trans = view->get_transform();

    auto child_to_obj = view_trans->invert_compose(from.get_net_transform());
    view_trans = view_trans->compose(view->get_fg_2_bg());

    return world_to_cont->compose(view_trans)->compose(child_to_obj);
}



void ViewRoot::ViewLayer::set_fov(float fov) {

    Lens* lens = get_lens();

    if (lens != nullptr)
        lens->set_fov(fov);
}


ViewRoot::ViewLayer::ViewLayer(ViewRoot* view_root): 
    DisplayRegion(view_root->_buffer, LVector4(0,1,0,1)), 
    _camera(new Camera("view-layer"))
{
    set_camera(view_root->attach_new_node(_camera));
}


ViewRoot::ViewLayer::~ViewLayer() {
    
    NodePath cam(_camera);
    cam.remove_node();
}


Camera* ViewRoot::ViewLayer::get_camera_node() const {
    return _camera;    
}

Lens* ViewRoot::ViewLayer::get_lens() const {
    return _camera->get_lens(get_lens_index());
}

void ViewRoot::ViewLayer::set_lens(Lens* lens) {
    _camera->set_lens(lens);
}

void ViewRoot::ViewLayer::set_lens(int index, Lens* lens) {
    _camera->set_lens(index, lens);
}

void ViewRoot::ViewLayer::set_initial_state(const RenderState* state) {
    _camera->set_initial_state(state);
}

void ViewRoot::ViewLayer::set_camera_mask(DrawMask mask) {
    _camera->set_camera_mask(mask);
}

void ViewRoot::ViewLayer::set_scene(NodePath scene) {
    _camera->set_scene(scene);
}


Viewport* ViewRoot::get_viewport() const {

    return _viewport;    
}


AnimSequenceControl* ViewRoot::bind_anim(AnimBundle* anim, PartBundle* part) {

    const string& name = anim->get_name();

    static const int flags =    PartGroup::HMF_ok_wrong_root_name | 
                                PartGroup::HMF_ok_part_extra | 
                                PartGroup::HMF_ok_anim_extra; 

    PartFilterAnim subsets(part);
    subsets.add_filter("tag_ads");
    subsets.add_filter("tag_view");
    subsets.add_filter("tag_anim");

    static map<string, string> mapping = {
        {"ads", "tag_ads"}, {"bobbing", "tag_anim"}
    };

    auto control = new AnimSequenceControl(name, part);
    control->set_event_queue(_controller->get_event_queue());
    control->set_event_name("anim-sequence");

    const string& filter = mapping[name];

    const PartSubset& subset = subsets.get_filter(filter);

    if (part->do_bind_anim(control, anim, flags, subset))
        _controls.store_anim(control, name);

    return control; 
}


AnimBundle* ViewRoot::get_anim(const std::string name) const {
    
    if (_char_np.is_empty())
        return nullptr;

    NodePathCollection paths = _char_np.find_all_matches("**/+AnimBundleNode");

    for (int i=0; i<paths.get_num_paths(); ++i) {
        NodePath path = paths.get_path(i);

        AnimBundleNode* node = DCAST(AnimBundleNode, path.node());
        AnimBundle* bundle = node->get_bundle();

        if (bundle->get_name() == name)
            return bundle;
    }
    return nullptr;
}