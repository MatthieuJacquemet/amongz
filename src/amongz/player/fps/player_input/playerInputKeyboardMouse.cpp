// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/inputDevice.h>
#include <panda3d/mouseButton.h>
#include <panda3d/mouseAndKeyboard.h>
#include <panda3d/mouseWatcher.h>
#include <panda3d/mouseWatcherParameter.h>
#include <panda3d/executionEnvironment.h>
#include <panda3d/lvector2.h>

#include "game.h"
#include "utils.h"
#include "playerInputKeyboardMouse.h"

using namespace std;
using DeviceClass = InputDevice::DeviceClass;
using Action = InputMapping::Action;

DEFINE_TYPEHANDLE(PlayerInputKeyboardMouse)

bool PlayerInputKeyboardMouse::_used = false;



PlayerInputKeyboardMouse::PlayerInputKeyboardMouse(int device):
    _dev_id(device),
    _view_sensitivity(1.0), 
    _accel(1.5),
    _prev_mouse_pos(0.0),
    _is_on_focus(false)
{
    nassertv(!_used)
    _used = true;
    
    string grab = ExecutionEnvironment::get_environment_variable("GRAB_MOUSE");

    if (grab.empty() || grab == "1" || downcase(grab) == "true")
        register_hook(callback(window_events), "window-event");
    else if (grab == "0" || downcase(grab) == "false")
        _is_on_focus = true;


    Game* game = Game::get_global_ptr();
    DataNode* root = game->get_data_root();
    _window = game->get_window();
    
    _device = new MouseAndKeyboard(_window, device, "km-dev");
    _watcher = new MouseWatcher("km-watcher");
    _watcher->add_region(new MKEventHandler(*this));

    root->add_child(_device);
    _device->add_child(_watcher);


    load_default_mapping();
}



void PlayerInputKeyboardMouse::window_events(const Event* event) {
    
    if (event->get_num_parameters() == 1) {

        EventParameter param = event->get_parameter(0);
        const GraphicsWindow* window;
        DCAST_INTO_V(window, param.get_ptr());

        if (window != _window)
            return;

        WindowProperties props = window->get_properties();

        bool is_foreground = props.get_foreground();
        
        if (_is_on_focus) {

            if (!is_foreground) {
                props.set_cursor_hidden(false);
                props.set_mouse_mode(WindowProperties::M_absolute);
                _window->request_properties(props);
            }
        } else if (is_foreground) {

            props.set_cursor_hidden(true);
            props.set_mouse_mode(WindowProperties::M_relative);
            _prev_mouse_pos.fill(make_nan(0.f));
            _window->request_properties(props);
        }

        _is_on_focus = is_foreground;
    }
}



LVector4 make_infinite_rectangle() {

    static const float inf = make_inf(0.f);
    return LVector4(-inf, inf, -inf, inf);
}


PlayerInputKeyboardMouse::
MKEventHandler::MKEventHandler(PlayerInputKeyboardMouse& host): 
    MouseWatcherRegion("km-handler", make_infinite_rectangle()),
    _host(host)
{
    
}


void PlayerInputKeyboardMouse::
MKEventHandler::press(const MouseWatcherParameter& param) {

    ButtonEvent event(param.get_button(), ButtonEvent::T_down);
    _host.dispatch(event);
}


void PlayerInputKeyboardMouse::
MKEventHandler::release(const MouseWatcherParameter& param) {

    ButtonEvent event(param.get_button(), ButtonEvent::T_up);
    _host.dispatch(event);
}



void PlayerInputKeyboardMouse::load_default_mapping() {
    
    _mapping.map(KeyboardButton::up(), Action::A_walk_forward);
    _mapping.map(KeyboardButton::down(), Action::A_walk_backward);
    _mapping.map(KeyboardButton::left(), Action::A_strafe_left);
    _mapping.map(KeyboardButton::right(), Action::A_strafe_right);

    _mapping.map(KeyboardButton::ascii_key('z'), Action::A_walk_forward);
    _mapping.map(KeyboardButton::ascii_key('s'), Action::A_walk_backward);
    _mapping.map(KeyboardButton::ascii_key('q'), Action::A_strafe_left);
    _mapping.map(KeyboardButton::ascii_key('d'), Action::A_strafe_right);

    _mapping.map(MouseButton::wheel_down(), Action::A_previous_crosshair);
    _mapping.map(MouseButton::wheel_up(), Action::A_next_crosshair);

    _mapping.map(KeyboardButton::space(), Action::A_jump);
    _mapping.map(MouseButton::three(), Action::A_ads);
    _mapping.map(MouseButton::one(), Action::A_fire);
    _mapping.map(KeyboardButton::control(), Action::A_crouch);
    _mapping.map(KeyboardButton::ascii_key('r'), Action::A_reload);
}



PlayerInputKeyboardMouse::~PlayerInputKeyboardMouse() {
    
    EventHandler* handler = EventHandler::get_global_event_handler();
    handler->remove_hooks_with(this);

    NodePath _device_node(_device);
    _device_node.remove_node();

    _used = false;
}


LVector2 PlayerInputKeyboardMouse::get_move() const {

    LVector2 value(0.0);

    value.set_y(is_any_button_down(Action::A_walk_forward) - 
                is_any_button_down(Action::A_walk_backward));
    
    value.set_x(is_any_button_down(Action::A_strafe_right) - 
                is_any_button_down(Action::A_strafe_left));

    // normalize diagonal moves
    if (value.dot(value) == 2.0f)
        value *= M_SQRT1_2;

    return value;
}


LVector2 PlayerInputKeyboardMouse::get_view() const {

    LVector2 value(0.0);

    if (_is_on_focus && _watcher->has_mouse()) {

        value = _watcher->get_mouse();

        if (_prev_mouse_pos.is_nan())
            _prev_mouse_pos = value;

        LVector2 delta = value - _prev_mouse_pos;
        _prev_mouse_pos = value;

        float acc = pow(delta.length(), _accel - 1.0f);
        value = delta * _view_sensitivity * acc * 100.0f;
    }

    return value;
}


bool PlayerInputKeyboardMouse::is_valid() const {
    
    return _window->has_keyboard(_dev_id) && _window->has_pointer(_dev_id);
}


bool PlayerInputKeyboardMouse::is_button_down(ButtonHandle button) const {
    
    return _watcher->is_button_down(button);
}



PlayerInputKeyboardMouse* PlayerInputKeyboardMouse::make() {

    // only one instance at a time can use the keyboard and mouse
    if (!_used) {

        Game* game = Game::get_global_ptr();
        GraphicsWindow* window = game->get_window();

        for (int i=0; i<window->get_num_input_devices(); ++i) {
            InputDevice* dev = window->get_input_device(i);

            if (!dev->has_pointer() || !dev->has_keyboard())
                continue;

            return new PlayerInputKeyboardMouse(i);
        }
    }
    return nullptr;
}





















    // _watcher->button
    // _watcher->set_extra_handler(_handler);
    // _watcher->set_button_down_pattern("%b-down");
    // _watcher->set_button_up_pattern("%b-up");

    // _handler->add_hook("mouse3-down", callback(ads_in));
    // _handler->add_hook("mouse3-up", callback(ads_out));
    // _handler->add_hook("mouse1-down", callback(pull_trigger));
    // _handler->add_hook("mouse1-up", callback(release_trigger));
    // _handler->add_hook("r-down", callback(reload));

// void PlayerInputKeyboardMouse::ads_in(const Event* event) {
    
//     _controller->do_action(PlayerController::A_ads_in);
// }

// void PlayerInputKeyboardMouse::ads_out(const Event* event) {
    
//     _controller->do_action(PlayerController::A_ads_out);
// }

// void PlayerInputKeyboardMouse::pull_trigger(const Event* event) {

//     _controller->do_action(PlayerController::A_pull_trigger);    
// }

// void PlayerInputKeyboardMouse::release_trigger(const Event* event) {

//     _controller->do_action(PlayerController::A_release_trigger);  
// }

// void PlayerInputKeyboardMouse::reload(const Event* event) {
    
//     EventParameter param = event->get_parameter(1);
//     nout << param.get_string_value() << endl;
//     nout << "reload " << endl;
//     _controller->do_action(PlayerController::A_reload);
// }

