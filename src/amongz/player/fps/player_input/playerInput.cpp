// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/eventQueue.h>

#include "inputInterfaceManager.h"
#include "game.h"
#include "utils.h"

#include "playerInput.h"

DEFINE_TYPEHANDLE(PlayerInput)


void InputMapping::map(ButtonHandle button, Player::Action action) {
    
    // the implementation provides two-way mapping on two distinct container 
    // for faster retrieval on both way
    _button_map[button].insert(action);
    _action_map[action].insert(button);
}



void InputMapping::unmap(ButtonHandle button, Action action) {
    
    // first remove the reference of the action on the buttons container
    ButtonMap::iterator actions_it = _button_map.find(button);

    if (actions_it != _button_map.end()) {
        Actions& actions = actions_it->second;

        Actions::iterator it = actions.find(action);
        if (it != actions.end())
            actions.erase(it);
    }

    // then remove the reference of the button on the actions container
    Buttons& buttons = _action_map[action];

    Buttons::iterator it = buttons.find(button);
    if (it != buttons.end())
        buttons.erase(it);
}


void InputMapping::unmap_all() {

    _button_map.clear();

    for (Buttons& buttons: _action_map)
        buttons.clear();    
}


InputMapping::Buttons InputMapping::get_buttons(Action action) const {
    
    return _action_map[action];
}


InputMapping::Actions InputMapping::get_actions(ButtonHandle button) const {
    
    ButtonMap::const_iterator it = _button_map.find(button);

    if (it != _button_map.end())
        return it->second;

    return Actions();
}


bool InputMapping::is_mapped(ButtonHandle button, Action action) const {

    Buttons buttons = _action_map[action];
    return buttons.find(button) != buttons.end();
}


bool PlayerInput::is_any_button_down(Player::Action action) const {

    InputMapping::Buttons buttons = _mapping.get_buttons(action);

    for (ButtonHandle button: buttons) {
        if (is_button_down(button))
            return true;
    }
    return false;
}


bool PlayerInput::are_all_buttons_down(Player::Action action) const {

    InputMapping::Buttons buttons = _mapping.get_buttons(action);

    for (ButtonHandle button: buttons) {
        if (!is_button_down(button))
            return false;
    }
    return true;
}


void PlayerInput::dispatch(const ButtonEvent& event) {
    
    bool activate;

    switch (event._type) {
        case ButtonEvent::T_down: activate = true; break;
        case ButtonEvent::T_up: activate = false; break;
        default: return;
    }
    InputMapping::Actions actions = _mapping.get_actions(event._button);

    for (Player::Action action: actions)
        queue_action(action, activate);
}


void PlayerInput::set_force_feedback(float low, float high) {
    
}


void PlayerInput::set_event_queue(EventQueue* queue) {

    _queue = queue;    
}


void PlayerInput::set_event_name(const std::string& name) {
    
    _action_name = name;
}


void PlayerInput::queue_action(Player::Action action, bool activate) {
    
    if (_queue != nullptr && !_action_name.empty()) {

        Event* event = new Event(_action_name);
        event->add_parameter(action);
        event->add_parameter(activate);

        _queue->queue_event(event);
    }
}
