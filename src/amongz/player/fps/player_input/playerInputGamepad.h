// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERINPUTGAMEPAD_H__
#define __PLAYERINPUTGAMEPAD_H__

#include <panda3d/inputDevice.h>
#include <panda3d/dataNode.h>

#include "playerInput.h"

class ButtonThrower;
class InputDevice;
class InputDeviceNode;
class PlayerControllerInput;


class PlayerInputGamepad final: public PlayerInput {

    REGISTER_TYPE("PlayerInputGamepad", PlayerInput)

public:
    static PlayerInputGamepad* make();
    ~PlayerInputGamepad();

    LVector2 get_move() const override;
    LVector2 get_view() const override;
    bool is_valid() const override;

    float _view_sensitivity;
    float _deadzone;
    float _accel;

    void dispatch(const ButtonEvent& event) override;
    void set_force_feedback(float low, float high) override;

    class ButtonHandler: public DataNode {

        REGISTER_TYPE("PlayerInputGamepad::ButtonHandler", DataNode)
    public:
        ButtonHandler(PlayerInputGamepad& host);

        virtual void do_transmit_data(DataGraphTraverser* trav,
                                        const DataNodeTransmit& input,
                                        DataNodeTransmit& output);
    private:
        PlayerInputGamepad& _host;
        int _input;
    };

private:
    PlayerInputGamepad(InputDevice* device);
    bool is_button_down(ButtonHandle button) const override;

    void load_default_mapping();

    LVector2 filter_joystick(LVector2 value) const;

    class AxisManager {
    public:
        AxisManager() = default;
        ~AxisManager() = default;

        void set_device(InputDevice* device);
        double get_axis_value(InputDevice::Axis axis) const;
    private:
        PT(InputDevice) _device;

        typedef std::array<int, 19> CacheEntries;
        mutable CacheEntries _index_cache;
    };

    PT(InputDevice) _device;
    PT(InputDeviceNode) _device_node;
    PT(ButtonHandler) _handler;
    
    BitArray _current_buttons_down;
    AxisManager _axes;
};

#endif // __PLAYERINPUTGAMEPAD_H__    fri