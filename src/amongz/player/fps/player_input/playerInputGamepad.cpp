// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/buttonThrower.h>
#include <panda3d/inputDeviceManager.h>
#include <panda3d/inputDeviceNode.h>
#include <panda3d/gamepadButton.h>
#include <panda3d/lvector2.h>
#include <panda3d/dataNodeTransmit.h>

#include "game.h"
#include "utils.h"
#include "playerInputGamepad.h"

using namespace std;
using DeviceClass = InputDevice::DeviceClass;
using Action = InputMapping::Action;
using Axis = InputDevice::Axis;

DEFINE_TYPEHANDLE(PlayerInputGamepad)
DEFINE_TYPEHANDLE(PlayerInputGamepad::ButtonHandler)


PlayerInputGamepad::PlayerInputGamepad(InputDevice* device):
    _device(device),
    _view_sensitivity(2.0),
    _deadzone(0.2),
    _accel(2.0)
{

    // InputInterfaceManager* mgr = InputInterfaceManager::get_global_ptr();
    // _interface = mgr->acquire_interface(DeviceClass::gamepad);

    Game* game = Game::get_global_ptr();
    DataNode* root = game->get_data_root();

    _device_node = new InputDeviceNode(device, "gamepad");
    _handler = new ButtonHandler(*this);

    _device_node->add_child(_handler);
    root->add_child(_device_node);

    _axes.set_device(device);

    load_default_mapping();
}


bool PlayerInputGamepad::is_button_down(ButtonHandle button) const {

    return _current_buttons_down.get_bit(button.get_index());
}


void PlayerInputGamepad::load_default_mapping() {

    _mapping.map(GamepadButton::face_a(), Action::A_jump);
    _mapping.map(GamepadButton::face_b(), Action::A_crouch);
    _mapping.map(GamepadButton::ltrigger(), Action::A_ads);
    _mapping.map(GamepadButton::rtrigger(), Action::A_fire);
    _mapping.map(GamepadButton::face_y(), Action::A_reload);
    _mapping.map(GamepadButton::dpad_down(), Action::A_toggle_fire_mode);

    _mapping.map(GamepadButton::lshoulder(), Action::A_previous_crosshair);
    _mapping.map(GamepadButton::rshoulder(), Action::A_next_crosshair);
}


PlayerInputGamepad::~PlayerInputGamepad() {

    NodePath device_node(_device_node);
    device_node.remove_node();

    if (_device->is_connected()) {
        InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();
        mgr->add_device(_device);
    }
}


LVector2 PlayerInputGamepad::get_move() const {

    LVector2 value(0.0f);

    nassertr(_device->is_connected(), value)
    
    value.set(  _axes.get_axis_value(Axis::left_x),
                _axes.get_axis_value(Axis::left_y));

    return filter_joystick(value);
}


LVector2 PlayerInputGamepad::get_view() const {
    
    LVector2 value(0.0f);

    nassertr(_device->is_connected(), value);

    value.set(  _axes.get_axis_value(Axis::right_x),
                _axes.get_axis_value(Axis::right_y));
    
    return filter_joystick(value) * _view_sensitivity;
}


bool PlayerInputGamepad::is_valid() const {

    return _device->is_connected(); 
}


void PlayerInputGamepad::dispatch(const ButtonEvent& event) {

    int index = event._button.get_index();
    
    switch (event._type) {
        case ButtonEvent::T_down:
            _current_buttons_down.set_bit(index);
            break;
        case ButtonEvent::T_up:
            _current_buttons_down.clear_bit(index);
            break;
        default: return;
    }

    PlayerInput::dispatch(event);
}


void PlayerInputGamepad::set_force_feedback(float low, float high) {
    _device->set_vibration(high, low);
}


LVector2 PlayerInputGamepad::filter_joystick(LVector2 value) const {
    
    float d = value.length();

    if (d == 0.0f)
        return 0.0f;

    float factor = saturate((_deadzone - d)/(_deadzone - 1.0f));

    return value / d * pow(factor, _accel);
}


PlayerInputGamepad::ButtonHandler::ButtonHandler(PlayerInputGamepad& host):
    DataNode("gamepad-handler"),
    _host(host)
{
    _input = define_input("button_events", ButtonEventList::get_class_type());
}


void PlayerInputGamepad::
ButtonHandler::do_transmit_data(DataGraphTraverser* trav,
                                const DataNodeTransmit& input,
                                DataNodeTransmit& output) 
{
    if (!input.has_data(_input))
        return;

    const EventParameter& param = input.get_data(_input);
    const ButtonEventList* events = DCAST(ButtonEventList, param.get_ptr());

    int num_events = events->get_num_events();

    for (int i = 0; i < num_events; i++)
        _host.dispatch(events->get_event(i));
}



void PlayerInputGamepad::AxisManager::set_device(InputDevice* device) {
    
    if (UNLIKELY(_device != device)) {

        _device = device;
        _index_cache.fill(-1); // invalidate cache
    }
}


double PlayerInputGamepad::AxisManager::get_axis_value(Axis axis) const {

    nassertr(_device != nullptr, 0.0f)

    size_t axis_id = size_t(axis);
    int index = _index_cache[axis_id];

    if (UNLIKELY(index == -1)) {
        for (int i=0; i<_device->get_num_axes(); ++i) {
            // update the cache entry
            if (_device->get_axis(i).axis == axis) {
                _index_cache[axis_id] = i;
                return _device->get_axis_value(i);
            }
        }
        return 0.0f;
    }
    return _device->get_axis_value(index);
}



PlayerInputGamepad* PlayerInputGamepad::make() {
        
    InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();
    
    InputDeviceSet devices = mgr->get_devices(DeviceClass::gamepad);

    if (devices.size() > 0) {
        InputDevice* device = devices[0];
        device->ref();
        // release the device so that it won't be reused by another player
        mgr->remove_device(device); 
        return new PlayerInputGamepad(device);
    }

    return nullptr;
}
