// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYERINPUT_H__
#define __PLAYERINPUT_H__

#include <panda3d/pointerTo.h>
#include <panda3d/genericAsyncTask.h>
#include <panda3d/buttonHandle.h>

#include "player.h"


class InputDevice;
class InputInterfaceManager;
class PlayerControllerInput;

/**
 * @brief classe permettant des lier un bouton à un ou pliseurs action
 **/
class InputMapping {

public:
    InputMapping() = default;
    ~InputMapping() = default;
    
    typedef Player::Action Action;

    typedef std::set<ButtonHandle> Buttons;
    typedef std::set<Action> Actions;

    typedef std::array<Buttons, Player::A_none> ActionMap;
    typedef std::map<ButtonHandle, Actions> ButtonMap;

    /**
     * @brief map un bouton a une action
     */
    void map(ButtonHandle button, Action action);

    /**
     * @brief supprime le mapping d'un bouton a une action
     */
    void unmap(ButtonHandle button, Action action);

    /**
     * @brief reinitialise tous les mapping
     */
    void unmap_all();
    
    /**
     * @brief vérifie si un bouton est lié a une action
     */
    bool is_mapped(ButtonHandle button, Action action) const;
    
    /**
     * @brief renvoie tous les bouttons liés à une action
     */
    Buttons get_buttons(Action action) const;

    /**
     * @brief renvoie tous les actions liés à un boutons
     */
    Actions get_actions(ButtonHandle button) const;

private:
    ButtonMap _button_map;
    ActionMap _action_map;
};


/**
 * @interface PlayerInput
 * @brief classe abstraite représentant un périphérique controlant un joueur
 **/
class PlayerInput: public TypedReferenceCount {

    REGISTER_TYPE("PlayerInput", TypedReferenceCount)

public:

    PlayerInput() = default;
    virtual ~PlayerInput() = default;

    /**
     * @brief récupere le déplacement relatif d'un joueur
     **/
    virtual LVector2 get_move() const = 0;

    /**
     * @brief récupere le rotation e la vue relatif d'un joueur
     **/
    virtual LVector2 get_view() const = 0;

    /**
     * @brief vérifie si le périphérique est valide.
     * @details si cette metode retourne faux, alors les valeurs retourné par 
     * les méthodes virtuelles ne doivent pas être prises en compte
     **/
    virtual bool is_valid() const = 0;

    virtual bool is_any_button_down(Player::Action action) const;

    virtual bool are_all_buttons_down(Player::Action action) const;

    virtual void dispatch(const ButtonEvent& event);

    virtual void set_force_feedback(float low, float high);
    
    void queue_action(Player::Action action, bool activate);

    void set_event_queue(EventQueue* queue);

    void set_event_name(const std::string& name);


protected:
    virtual bool is_button_down(ButtonHandle button) const = 0;

    EventQueue* _queue;
    std::string _action_name;
    InputMapping _mapping;
};


#endif // __PLAYERINPUT_H__