// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/asyncTaskManager.h>
#include <panda3d/eventQueue.h>
#include <panda3d/eventHandler.h>
#include <panda3d/pStatTimer.h>

#include "animSequenceControl.h"
#include "playerInputGamepad.h"
#include "playerInputKeyboardMouse.h"
#include "callbacks.h"
#include "gun.h"
#include "redDotSight.h"
#include "attachment.h"
#include "ammoPack.h"
#include "ammoData.h"
#include "melee.h"
#include "game.h"
#include "map.h"
#include "viewRoot.h"
#include "sight.h"
#include "soundManager.h"
#include "fpsController.h"



using namespace std;


DEFINE_TYPEHANDLE(FpsController)

#define MAP_ACTION(action, handler) \
    _action_handlers[action]._handler = &FpsController::handler;

FpsController::FpsController(const string& name):
    Player(name),
    _view_root(new ViewRoot(this)),
    _move_vel(0.0),
    _auto_reload(true),
    _is_crouch(false),
    _force_feedback(true)
{

    _view_root->set_pos(0.0f ,0.0f , _height);


    add_hook("fire", callback(do_fire));
    add_hook("clip_in", callback(clip_in));
    add_hook("do-action", callback(do_action));
    add_hook("anim-sequence", callback(anim_sequence));

    _update     = do_add_task(method(update), "player-update");
    _fire_seq   = do_add_task(method(fire_seq), "fire-sequence");


    MAP_ACTION(A_reload, reload)
    MAP_ACTION(A_fire, fire)
    MAP_ACTION(A_jump, jump)
    MAP_ACTION(A_ads, ads)
    MAP_ACTION(A_crouch, crouch)
    MAP_ACTION(A_toggle_fire_mode, toggle_fire_mode)
    MAP_ACTION(A_next_crosshair, next_crosshair)
    MAP_ACTION(A_previous_crosshair, prev_crosshair)

    _sfx_mgr = AudioManager::create_AudioManager();
    _sfx_mgr->set_volume(0.4f);

    FilterProperties* filters = new FilterProperties;
    // filters->add_echo(1.0f, 1.0f, 100000, 0.3f);
    // filters->add_sfxreverb(1.0f, -5000.0f, -5000.0f, 0.2f);

    _sfx_mgr->configure_filters(filters);
    pick_input();
}


void FpsController::do_action(const Event* event) {

    nassertv(event->get_num_parameters() == 2)

    EventParameter param0 = event->get_parameter(0);
    EventParameter param1 = event->get_parameter(1);

    Action action = static_cast<Action>(param0.get_int_value());
    bool activate = param1.get_int_value();

    ActionHandler handler = _action_handlers[action];

    if (handler._enabled && handler._handler)
        (this->*handler._handler)(activate);
}


bool FpsController::has_input() const {

    return _input != nullptr && _input->is_valid();    
}


AsyncTask::DoneStatus FpsController::update(AsyncTask* task) {

    Player::update();

    if (UNLIKELY(!has_input()))
        pick_input();
    else {
        rotate_view(_input->get_view());
        move(_input->get_move());
    }

    _ads_value = _view_root->get_ads_value();

    ClockObject* clock = ClockObject::get_global_clock();

    float crouch = get_shape()->get_local_scale().get_z();

    float t = clock->get_dt() * 5.0f;
    _crouch = saturate(_crouch + (_is_crouch ? t : -t));

    float z = lerp(1.0, 0.5, _crouch);
    get_shape()->set_local_scale(LVector3(1.0f, 1.0f, z));
    _view_root->set_pos(0.0f, 0.0f, z * _height);

    _sfx_mgr->update();
    _view_root->update();
    _handler.process_events();

    update_sfx();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus FpsController::fire_seq(AsyncTask* task) {
    
    if (_force_feedback && _input != nullptr)
        _input->set_force_feedback(0.0, 0.0);

    return AsyncTask::DS_done;
}


void FpsController::move(LVector2 dir) {
    
    ClockObject* clock = ClockObject::get_global_clock();

    dir *= lerp(1.0f, 0.5f, _crouch);
    dir *= lerp(1.0f, 0.5f, _ads_value);

    _req_motion = LVector3(dir * clock->get_dt(), 0.0f);

    // simulate inertia
    _move_vel /= clock->get_dt() * 15.0 + 1.0;
    _move_vel += dir;

    LVector3 motion(_move_vel,0.0);

    set_linear_movement(motion * 1.5f, true);

}


void FpsController::rotate_view(LVector2 dir) {


    ClockObject* clock = ClockObject::get_global_clock();

    dir *= _view_root->get_background_fov() * lerp(0.02f, 0.015f, _ads_value);

    dir.set(rad_2_deg(dir.get_x()), rad_2_deg(dir.get_y()));

    set_angular_movement(-dir.get_x());
    _view_root->rotate_view(dir * clock->get_dt());
}


void FpsController::fire(bool activate) {

    Weapon* weapon = get_weapon();

    if (weapon == nullptr)
        return;

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);

        if (activate) {
            if (!gun->pull_trigger())
                do_play_sound("dryfire_rifle");
        } else
            gun->release_trigger();
    }
}


void FpsController::reload(bool activate) {

    Weapon* weapon = get_weapon();

    if (!activate || weapon == nullptr)
        return;

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);
        size_t rounds = gun->get_num_rounds();

        if (get_ammo(gun->get_ammo()) == 0 || rounds == gun->get_max_rounds())
            return;

        ads(false);
        fire(false);
        _action_handlers[A_ads]._enabled = false;

        bool empty = rounds == 0;
        _view_root->reload(empty);

        if (empty)
            _action_handlers[A_fire]._enabled = false;
    }
}


void FpsController::jump(bool activate) {

    if (activate) {
        if (_is_crouch)
            _is_crouch = false;

        else //if (can_jump())
            do_jump();
    }
}


void FpsController::set_map(Map* map) {

    _view_root->set_scene(*map);
}


void FpsController::loot(LootableItem* item) {

    if (item->is_of_type(Weapon::get_class_type()))
        loot_weapon(DCAST(Weapon, item));
    
    else if (item->is_of_type(AmmoPack::get_class_type()))
        loot_ammo(DCAST(AmmoPack, item));

    if (item->get_ref_count() == 0)
        delete item;
}


void FpsController::switch_weapon() {

    if (_primary_weapon.is_null() || _secondary_weapon.is_null())
        return;

    Weapon* weapon = get_weapon();

    if (weapon->is_of_type(Gun::get_class_type())) {

        Gun* gun = DCAST(Gun, weapon);
        gun->release_trigger();

        if (_current_weapon_slot == WS_primary)
            _current_weapon_slot = WS_secondary;
        else
            _current_weapon_slot = WS_primary;
        
        _view_root->take_gun(gun, false);
    }
}


void FpsController::advance_crosshair(int n) {
    
    Weapon* weapon = get_weapon();

    if (weapon && weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);
        
        RedDotSight* sight = DCAST(RedDotSight, 
            gun->get_attachment(RedDotSight::get_class_type()));

        if (sight != nullptr) {
            
            size_t id = sight->get_crosshair();
            id = (id + n) % sight->get_num_crosshair_texture();
            
            sight->set_crosshair(id);
        }
    }
}



void FpsController::next_crosshair(bool activate) {

    if (activate)
        advance_crosshair(+1);
}


void FpsController::prev_crosshair(bool activate) {

    if (activate)
        advance_crosshair(-1);
}


void FpsController::ads(bool activate) {

    _view_root->ads(activate);
}


void FpsController::crouch(bool activate) {
    
    if (activate)
        _is_crouch = !_is_crouch;
}


void FpsController::toggle_fire_mode(bool activate) {

    if (activate) {
        Weapon* weapon = get_weapon();
        if (weapon->is_of_type(Gun::get_class_type())) {
            Gun* gun = DCAST(Gun, weapon);
            gun->toggle_fire_mode();
        }
    }    
}


void FpsController::loot_weapon(Weapon* weapon) {
        
    if (weapon->is_of_type(Melee::get_class_type())) {

        _current_weapon_slot = WS_melee;
        _melee_weapon = DCAST(Melee, weapon);
    }
    
    else if (weapon->is_of_type(Gun::get_class_type())) {

        Gun* gun = DCAST(Gun, weapon);
        gun->set_event_queue(get_event_queue());

        if (!_primary_weapon) {
            _primary_weapon = gun;
            _current_weapon_slot = WS_primary;
        }
        else if (!_secondary_weapon) {
            _secondary_weapon = gun;
            _current_weapon_slot = WS_secondary;
        }

        else if (_current_weapon_slot == WS_primary)
            _primary_weapon = gun;

        else if (_current_weapon_slot == WS_secondary)
            _secondary_weapon = gun;

        _view_root->take_gun(gun, true);
    }
}


void FpsController::loot_ammo(AmmoPack* ammo) {
    
    AmmoData* data = ammo->get_data();
    _ammos[data->get_name()] += ammo->loot();
}


void FpsController::do_fire(const Event* event) {

    AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

    nassertv(event->get_num_parameters() == 1)

    EventParameter param = event->get_parameter(0);
    Gun* gun = DCAST(Gun, param.get_ptr());

    if (get_weapon() == gun) {

        _view_root->fire(_ads_value > 0.5f);
        int num_sounds = gun->get_num_fire_sound();

        if (num_sounds > 0) {
            Filename sound_name = gun->get_fire_sound(rand(num_sounds));
            
            PT(AudioSound) sound = _sfx_mgr->get_sound(sound_name);

            sound->set_play_rate(randf(1.0f,1.2f));
            sound->set_volume(randf(1.5f,1.7f));
            sound->play();
        }

        if (gun->get_num_rounds() == 0 && _auto_reload) {

            do_task_later([gun, this](AsyncTask*) {
                if (gun == get_weapon()) 
                    reload(true);
                return AsyncTask::DS_done;
            }, "reload", 0.2);
        }

        if (_fire_seq->is_alive())
            _fire_seq->remove();
        
        _fire_seq->set_delay(0.07);
        mgr->add(_fire_seq);

        if (_force_feedback)
            _input->set_force_feedback(0.0, 0.4);
    }
}


void FpsController::clip_in(const Event* event) {

    Weapon* weapon = get_weapon();
    nassertv(weapon != nullptr);

    _action_handlers[A_ads]._enabled = true;
    _action_handlers[A_fire]._enabled = true;

    if (_input && _input->is_valid() && _input->is_any_button_down(A_ads))
        _view_root->ads(true);

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);

        AmmoData* ammo = gun->get_ammo();

        Ammos::iterator it = _ammos.find(ammo->get_name());

        if (it == _ammos.end())
            return;

        size_t rounds = gun->get_num_rounds();
        size_t refill = min(gun->get_max_rounds() - rounds, it->second);

        gun->set_num_rounds(rounds + refill);
        it->second -= refill;
    }
}

void FpsController::anim_sequence(const Event* event) {
    
    nassertv(event->get_num_parameters() == 1)
    EventParameter param = event->get_parameter(0);

    SequenceInterval* interval = 
        DCAST(SequenceInterval, param.get_typed_ref_count_value());

    const string& name = interval->get_name();

    switch (interval->get_tag_type()) {
    
        case SequenceInterval::Tag::T_sound:
            do_play_sound(name); break;
        
        case SequenceInterval::Tag::T_event:
            _handler.dispatch_event(new Event(name));
    }
}


void FpsController::update_sfx() {

    ClockObject* clock = ClockObject::get_global_clock();
    Map* map = get_current_map();

    LVector3 vel = get_velocity() / clock->get_dt();

    NodePath listener = _view_root->get_camera_np();
    LMatrix4 mat = listener.get_net_transform()->get_mat();
    
    LVector3 fw = mat.get_row3(1);
    LVector3 up = mat.get_row3(2);
    LPoint3 pos = mat.get_row3(3);

    _sfx_mgr->audio_3d_set_listener_attributes(
            pos.get_x(), pos.get_y(), pos.get_z(), 
            vel.get_x(), vel.get_y(), vel.get_z(), 
            fw.get_x(),  fw.get_y(),  fw.get_z(),
            up.get_x(),  up.get_y(),  up.get_z());
}


void FpsController::do_play_sound(const string& name) {
    
    Filename filename(SOUND_PATH);
    filename.set_basename_wo_extension(name);
    filename.set_extension("wav");

    SoundManager* mgr = get_sound_manager();
    mgr->play_sound(*this, filename);
}


AudioSound* FpsController::play_gun_sound(const string& name) {

    Gun* gun = DCAST(Gun, get_weapon());
    nassertr(gun != nullptr, nullptr)

    Filename filename(AssetManager::get_location(Gun::get_class_type()));
    filename = filename / gun->get_name() / "sound" / name;
    filename.set_extension("wav");

    return play_sound(filename);
}


AudioSound* FpsController::play_sound(Filename filename) {
    
    PT(AudioSound) sound = _sfx_mgr->get_sound(filename);

    if (sound != nullptr)
        sound->play();

    return sound;
}


void FpsController::pick_input() {

    _input = PlayerInputGamepad::make();

    if (_input == nullptr)
        _input = PlayerInputKeyboardMouse::make();

    if (_input != nullptr) {
        _input->set_event_queue(get_event_queue());
        _input->set_event_name("do-action");
    }
}



FpsController::~FpsController() {

    if (!_primary_weapon.is_null())
        _primary_weapon->set_event_queue(nullptr);

    if (!_secondary_weapon.is_null())
        _secondary_weapon->set_event_queue(nullptr);
}


void FpsController::take_weapon(WeaponSlot slot) {

    if (slot == _current_weapon_slot)
        return;

    if (slot == WS_melee)
        return; //todo
    else {
        Gun* gun = DCAST(Gun, get_weapon(slot));
        if (gun != nullptr)
            _view_root->take_gun(gun, false);
    }
}



void FpsController::pullout(bool activate) {

    if (activate) {
        _view_root->pullout(false);
        do_play_sound("draw_weapon_" + rand() % 3);
    }
}


void FpsController::putaway(bool activate) {
    
    if (activate)
        _view_root->putaway(true);
}



