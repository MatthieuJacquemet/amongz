
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __VIEWROOT_H__
#define __VIEWROOT_H__

#include <panda3d/animControlCollection.h>
#include <panda3d/pandaNode.h>
#include "gdk/utils.h"


class FpsController;
class RenderPipeline;
class GraphicsBuffer;
class PartBundleHandle;
class Character;
class WeaponSway;
class IdleSway;
class CIntervalManager;
class AnimStack;
class AdsControl;
class CamBobbing;
class PlayerHUD;
class Viewport;
class CamRecoil;
class WeaponBobbing;
class AnimSequenceControl;
class Gun;


class ViewRoot: public LocalNodePath<PandaNode> {

    REGISTER_TYPE("ViewRoot", LocalNodePath<PandaNode>)

public:
    ViewRoot(FpsController* controller);
    ~ViewRoot();

    FpsController* get_controller() const;

    float get_ads_value() const;
    Viewport* get_viewport() const;

    void update();

    void reload(bool empty);
    void fire(bool ads);
    void ads(bool activate);
    void pullout(bool first_time);
    void putaway(bool quick);

    void set_scene(NodePath scene);
    void take_gun(Gun* gun, bool first_time);
    void rotate_view(LVector2 dir);

    float get_background_fov() const;
    float get_foreground_fov() const;

    CPT(TransformState) get_fg_2_bg() const;

    NodePath get_camera_np() const;

    enum DrawLayer {
        DL_dynamic = 0,
        DL_player,
        DL_shadow
    };

private:

    void init_viewarms();
    void init_layers();

    void update_viewport(const Event* event);

    PartBundleHandle* _part_handle;

    AnimControlCollection _controls;

    float _view_cap;

    float _fg_fov, _fg_fov_ads;
    float _bg_fov, _bg_fov_ads;

    PT(RenderPipeline) _pipeline;
    PT(GraphicsOutput) _buffer;
    PT(Viewport) _viewport;

    FpsController* _controller;


    class ViewLayer final: public DisplayRegion {

        REGISTER_TYPE("PlayerView::ViewLayer", DisplayRegion)
    
    public:
        ViewLayer(ViewRoot* view_root);
        ~ViewLayer();

        Camera* get_camera_node() const;
        Lens* get_lens() const;
        void set_lens(Lens* lens);
        void set_lens(int index, Lens* lens);
        void set_fov(float fov);
        void set_initial_state(const RenderState* state);
        void set_camera_mask(DrawMask mask);
        void set_scene(NodePath scene);

    private:
        PT(Camera) _camera;
    };

    AnimSequenceControl* bind_anim(AnimBundle* anim, PartBundle* part);

    AnimBundle* get_anim(const std::string name) const;

    PT(ViewLayer) _foreground_layer;
    PT(ViewLayer) _background_layer;
    PT(Character) _character;
    PT(PlayerHUD) _hud;

    PT(AnimStack) _anim_stack;
    PT(AnimStack) _ads_stack;
    PT(AnimStack) _cam_stack;

    PT(WeaponBobbing) _weapon_bobbing;
    PT(AdsControl) _ads_control;
    PT(WeaponSway) _weapon_sway;
    PT(CamRecoil) _cam_recoil;
    PT(CamBobbing) _cam_bobbing;

    PT(IdleSway) _idle_sway;

    NodePath _weapon_np;
    NodePath _char_np;

    float _sway_offset;

    static PT(RenderPipeline) make_pipeline(Viewport* viewport);
    static Viewport* make_viewport();
};


CPT(TransformState) querry_net_transform(NodePath from);

#endif // __VIEWROOT_H__