// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYERHUD_H__
#define __PLAYERHUD_H__

#include <panda3d/nodePath.h>                     // for NodePath

#include "utils.h"

class Camera;
class DisplayRegion;
class Texture;
class TextNode;
class Player;


class PlayerHUD: public TypedWritableReferenceCount {

    REGISTER_TYPE("PlayerHUD", TypedWritableReferenceCount)

public:
    PlayerHUD(Player* player, DisplayRegion* region);
    ~PlayerHUD();

    void set_view_texture(Texture* texture);

    void update_region();
    void update();

    void set_region(DisplayRegion* region);

private:

    void init();

    void fire(const Event* event);
    void reload(const Event* event);
    void take_gun(const Event* event);

    void set_ammo(size_t ammo);

    size_t _total_rounds;
    float _crosshair_scale;
    float _crosshair_value;

    PT(TextNode) _ammo_counter;
    PT(PandaNode) _crosshair;
    PT(Texture) _view_texture;
    PT(DisplayRegion) _region;

    // to pointer to prevent circular reference
    Player* _player;

    NodePath _root;
    NodePath _camera_np;
    NodePath _view_quad;
    NodePath _crosshair_np;

    PTA_LVecBase2i _viewport_offset;
};

#endif // __PLAYERHUD_H__