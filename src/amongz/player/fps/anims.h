// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __ANIMS_H__
#define __ANIMS_H__

#include "animPostProcess.h"


class AdsControl final : public AnimPostProcess {
public:
    AdsControl();
    ~AdsControl() = default;

    bool set_control(AnimControl* control);
        
    CPT(TransformState) update() override;
    void set_ads_height(float height);

private:
    float _height_factor;
    float _final_height;
};


class WeaponSway final : public AnimLayer {
public:
    WeaponSway();
    ~WeaponSway() = default;

    CPT(TransformState) update() override;

    void set_factor(float factor);
    void set_stiffness(float stiffness);

    void set_sway(float x, float y);

private:
    float ease(float x);

    LVector3 _value;
    LVector2 _sway;
    float _factor;
    float _stiffness;
    float _easing;
};


class IdleSway final : public AnimLayer {
public:
    IdleSway();
    ~IdleSway() = default;

    CPT(TransformState) update() override;

    void set_factor(float factor);
    void set_speed(float speed);

private:
    float _factor;
    float _speed;
    double _time;
};


class CamRecoil final : public AnimLayer {
public:
    CamRecoil();
    ~CamRecoil() = default;

    CPT(TransformState) update() override;

    void add_recoil(float factor);
    void set_stiffness(float stiffness);

private:
    LVector3 _torque;
    LVector3 _value;
    float _stiffness;
};


class CamBobbing final : public AnimLayer {
public:
    CamBobbing();
    ~CamBobbing() = default;

    CPT(TransformState) update() override;

    void set_factor(float factor);
    void set_move(LVector2 move);

private:
    LVector2 _move;
    float _factor;
    float _velocity;
    double _time;
};


class WeaponBobbing final : public AnimPostProcess {
public:
    WeaponBobbing();
    ~WeaponBobbing() = default;

    CPT(TransformState) update() override;

    void set_move(LVector2 move);
    void set_factor(float factor);

private:
    float _factor;
    double _time;
    LVector2 _move;
    LVector2 _direction;
};


#endif // __ANIMS_H__