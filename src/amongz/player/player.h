
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <panda3d/nameUniquifier.h>
#include <panda3d/bulletCharacterControllerNode.h>

#include "callbacks.h"
#include "customEventHandler.h"
#include "utils.h"


class BulletCapsuleShape;
class Gun;
class Melee;
class Weapon;
class EventHandler;
class EventQueue;
class AmmoData;
class LootableItem;
class Map;
class AudioSound;


class Player:   public LocalNodePath<BulletCharacterControllerNode>,  
                public EventHandlerBase
{

    REGISTER_TYPE("Player", LocalNodePath<BulletCharacterControllerNode>,
                            EventHandler)

public:

    enum WeaponSlot {
        WS_none = 0,
        WS_primary,
        WS_secondary,
        WS_melee,
    };

    enum Action {
        A_walk_forward,
        A_walk_backward,
        A_strafe_left,
        A_strafe_right,
        A_ads,
        A_reload,
        A_interact,
        A_jump,
        A_fire,
        A_toggle_fire_mode,
        A_throw_lethal,
        A_throw_tactical,
        A_crouch,
        A_prone,
        A_switch_weapon,
        A_primary_weapon,
        A_secondary_weapon,
        A_melee,
        A_next_crosshair,
        A_previous_crosshair,
        A_none
    };

    Player(const std::string& name);
    virtual ~Player() = default;

    void set_name(const std::string& name);

    Weapon* get_weapon(WeaponSlot slot=WS_none) const;

    size_t get_ammo(const std::string& name) const;
    size_t get_ammo(AmmoData* ammo) const;

    WeaponSlot get_current_weapon() const;
    Gun* get_primary_weapon() const;
    Gun* get_secondary_weapon() const;
    Melee* get_melee_weapon() const;

    virtual void set_map(Map* map);

    virtual void update();
    virtual void loot(LootableItem* item);
    virtual AudioSound* play_sound(Filename filename);

    float get_crouch() const;
    float get_ads() const;

    LVector3 get_velocity() const;
    LVector3 get_local_velocity() const;
    LVector3 get_requested_motion() const;
    LVector3 get_motion() const;

protected:
    float _health;
    float _stamina;
    float _ads_value;
    float _height;
    float _crouch;


    WeaponSlot _current_weapon_slot;
    PT(Gun) _primary_weapon;
    PT(Gun) _secondary_weapon;
    PT(Melee) _melee_weapon;

    typedef std::map<std::string, size_t> Ammos;
    Ammos _ammos;

    LVector3 _prev_pos;
    LVector3 _curr_pos;
    LVector3 _req_motion;

private:
    BulletShape* make_shape();

    static NameUniquifier _names;
    static std::string add_name(const std::string& name);
};


#endif // __PLAYER_H__








    // void set_ammo(const std::string& name, size_t n);
    // void set_ammo(AmmoData* ammo, size_t n);
    // void set_current_weapon(WeaponSlot slot);
    // void set_primary_weapon(Gun* gun);
    // void set_secondary_weapon(Gun* gun);
    // void set_melee_weapon(Melee* melee);
    // void set_ads(bool activate);
    // void set_fire(bool activate);