// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bulletMultiSphereShape.h>
#include <panda3d/bulletCapsuleShape.h>
#include <panda3d/bulletRigidBodyNode.h>
#include <panda3d/eventHandler.h>
#include <panda3d/eventQueue.h>
#include <panda3d/lvector2.h>

#include "game.h"
#include "map.h"
#include "utils.h"
#include "fsm.h"
#include "gun.h"
#include "melee.h"
#include "config_weapon.h"
#include "ammoData.h"
#include "player.h"


using namespace std;

DEFINE_TYPEHANDLE(Player)

#define PLAYER_RADIUS 0.2f
#define PLAYER_HEIGHT 1.8f
#define PLAYER_MAX_JUMP_HEIGHT 2.0
#define PLAYER_JUMP_SPEED 3.0


NameUniquifier Player::_names;
using Controller = LocalNodePath<BulletCharacterControllerNode>;


Player::Player(const string& name): 
    Controller(make_shape(), 0.4f, add_name(name).c_str()),
    _ads_value(0.0f),
    _crouch(0.0f),
    _curr_pos(get_pos()),
    _prev_pos(_curr_pos)
{   

    set_use_ghost_sweep_test(true);
    set_max_jump_height(PLAYER_MAX_JUMP_HEIGHT);
    set_jump_speed(PLAYER_JUMP_SPEED);
    set_max_slope(M_PI_2 * 0.25f);

    set_collide_mask(CollideMask::bit(Map::CM_player));
}


void Player::set_name(const string& name) {
    
    BulletCharacterControllerNode::set_name(add_name(name));
}

float Player::get_ads() const {
    return _ads_value;
}

float Player::get_crouch() const {
    return _crouch;
}

LVector3 Player::get_velocity() const {
    return get_pos() - _prev_pos;
}

LVector3 Player::get_local_velocity() const {

    CPT(TransformState) transform = TransformState::make_hpr(get_hpr());
    CPT(TransformState) pos = TransformState::make_pos(get_velocity());
    
    return transform->invert_compose(pos)->get_pos();
}


LVector3 Player::get_requested_motion() const {
    return _req_motion;
}

LVector3 Player::get_motion() const {

    LVector3 vel = get_local_velocity();
    LVector3 dir = _req_motion.normalized();
    
    float orig_mag = _req_motion.length();
    float move_mag = abs(vel.dot(dir));
    
    return dir * min(orig_mag, move_mag);
}


Weapon* Player::get_weapon(WeaponSlot slot) const {
    
    if (slot == WS_none)
        slot = _current_weapon_slot;

    switch (slot) {
        case WS_primary:
            return _primary_weapon;
        case WS_secondary:
            return _secondary_weapon;
        case WS_melee:
            return _melee_weapon;
        case WS_none:
            break;
    };
    return nullptr;
}


size_t Player::get_ammo(const string& name) const {
    
    Ammos::const_iterator found = _ammos.find(name);

    if (found != _ammos.end())
        return found->second;
    
    return 0;
}


size_t Player::get_ammo(AmmoData* ammo) const {
    return get_ammo(ammo->get_name());
}

Player::WeaponSlot Player::get_current_weapon() const {
    return _current_weapon_slot;
}

Gun* Player::get_primary_weapon() const {
    return _primary_weapon;
}

Gun* Player::get_secondary_weapon() const {
    return _secondary_weapon;
}

Melee* Player::get_melee_weapon() const {
    return _melee_weapon;
}

void Player::set_map(Map* map) {
    
}

string Player::add_name(const string& name) {
    return _names.add_name(name);
}


BulletShape* Player::make_shape() {

    _height = PLAYER_HEIGHT * 0.5f - PLAYER_RADIUS;

    return new BulletCapsuleShape(PLAYER_RADIUS, 
                    PLAYER_HEIGHT - PLAYER_RADIUS * 2.0f);
    // PTA_LVecBase3 points;
    // PTA_float radius(2, PLAYER_RADIUS);


    // points.push_back(LVector3(0.0f , 0.0f, PLAYER_RADIUS));
    // points.push_back(LVector3(0.0f , 0.0f, _height));

    // return new BulletMultiSphereShape(points, radius);
}


void Player::update() {

    _prev_pos = _curr_pos;
    _curr_pos = get_pos();
}


void Player::loot(LootableItem* item) {
    
}


AudioSound* Player::play_sound(Filename filename) {
    
    return nullptr;
}
