
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __GAME_H__
#define __GAME_H__


#include <panda3d/audioManager.h>

#include "applicationFramework.h"
#include "player.h"
#include "defines.h"


class ViewportManager;
class EventHandler;
class DataNode;
class InputInterfaceManager;
class AudioManager;
class GraphicsWindow;
class InputDevice;
class Map;
class GamePlaceHolder;
class AsyncTask;
class AIWorld;
class BulletWorld;
class SoundManager;
class ParticleSystemManager;
class PhysicsManager;


class Game final: public ApplicationFramework {

    REGISTER_TYPE("Game", ApplicationFramework)

public:
    ~Game() override;

    static Game* get_global_ptr();

    
    GraphicsWindow* get_window() const;
    
    void close_window();
    
    AudioManager*  get_audio_manager(const std::string& name) const;
    ViewportManager*  get_viewport_manager() const;
    DataNode* get_data_root() const;

    Map* get_current_map();

    void load_map(const std::string& name);

private:
    Game();
    Game(const Game&) = delete;
    Game& operator =(const Game&) = delete;
    
    void start() override;
    void cleanup() override;

    AsyncTask::DoneStatus process_input(AsyncTask *task);
    AsyncTask::DoneStatus process_data(AsyncTask *task);
    AsyncTask::DoneStatus process_event(AsyncTask *task);
    AsyncTask::DoneStatus process_interval(AsyncTask *task);
    AsyncTask::DoneStatus process_render(AsyncTask *task);
    AsyncTask::DoneStatus process_garbage_collect(AsyncTask *task);

    void process_window_event(const Event* event);
    
    
    PT(DataNode) _data_root;
    PT(GraphicsWindow) _window;
    PT(Map) _current_map;

    typedef pmap<std::string, PT(AudioManager)> AudioManagers;
    mutable AudioManagers _audio_mgrs;

    ViewportManager* _viewport_mgr;
    EventHandler* _event_handler;

    static Singleton<Game> _global_ptr;

    friend class GamePlaceHolder;
};


BulletWorld* get_physic_world();
SoundManager* get_sound_manager();
AIWorld* get_ai_world();
Map* get_current_map();
ParticleSystemManager* get_particle_manager();
PhysicsManager* get_physics_manager();


#endif // __GAME_H__