// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __CONFIG_GAME_H__
#define __CONFIG_GAME_H__

#include <panda3d/configVariableBool.h>
#include <panda3d/configVariableDouble.h>
#include <panda3d/configVariableInt.h>
#include <panda3d/configVariableDouble.h>
#include <panda3d/configVariableString.h>
#include <panda3d/notifyCategoryProxy.h>


NotifyCategoryDeclNoExport(game);

#define GDK_SHADER_PATH "/gdk/shader/"


#ifdef NOTIFY_DEBUG
    // Non-release build:
    #define game_debug(msg) \
        if (game_cat.is_debug()) \
            game_cat->debug() << msg << std::endl; \
        else {}
#else
    // Release build:
    #define game_debug(msg) ((void)0);
#endif

#define game_info(msg) \
  game_cat->info() << msg << std::endl

#define game_warning(msg) \
  game_cat->warning() << msg << std::endl

#define game_error(msg) \
  game_cat->error() << msg << std::endl


extern ConfigVariableDouble bullet_lifespan;
extern ConfigVariableString render_pipeline;
extern ConfigVariableBool threaded_physic;
extern ConfigVariableBool threaded_ai;
extern ConfigVariableInt max_bullet_holes;
extern ConfigVariableInt max_bullet_textures;
extern ConfigVariableDouble bullet_hole_offset;
extern ConfigVariableDouble bullet_hole_depth;
extern ConfigVariableBool hardware_skinning;

void init_game();

#endif // __CONFIG_GAME_H__