// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/dconfig.h>
#include <panda3d/virtualFileSystem.h>
#include <panda3d/panda.h>
#include <panda3d/config_bullet.h>
#include <panda3d/load_prc_file.h>  // for load_prc_file

#include "asyncTextureLoader.h"
#include "defines.h"
#include "assetManager.h"
#include "config_weapon.h"
#include "config_gdk.h"
#include "fpsController.h"
#include "hitBox.h"
#include "zombieView.h"
#include "zombieController.h"
#include "zombie.h"
#include "map.h"
#include "physic_utils.h"
#include "spawn.h"
#include "zombieAIController.h"
#include "playerInputKeyboardMouse.h"
#include "playerInputGamepad.h"
#include "config_game.h"

using namespace std;

NotifyCategoryDef(game, "");

ConfigureDef(config_game);

ConfigureFn(config_game) {
    init_game();
}



void init_asset_paths() {

    Filename exec_path = ExecutionEnvironment::get_binary_name();
    exec_path.make_absolute();

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    vfs->chdir(exec_path.get_dirname());
    
    AssetManager::set_default_location(MODEL_PATH);
    AssetManager::set_location(Zombie::get_class_type(), ZOMBIE_PATH);
    AssetManager::set_location(Map::get_class_type(), MAP_PATH);
}


void init_game() {

    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;


    init_libpanda();
    init_libbullet();
    init_gdk();
    init_weapon();

    FpsController::init_type();
    HitBox::init_type();
    ZombieView::init_type();
    ZombieController::init_type();
    Zombie::init_type();
    ZombieAIController::init_type();
    Map::init_type();
    Spawn::init_type();
    PlayerInputGamepad::init_type();
    PlayerInputGamepad::ButtonHandler::init_type();
    PlayerInputKeyboardMouse::init_type();

    AsyncTextureLoader::register_with_read_factory();
    Zombie::register_with_read_factory();
    HitBox::register_with_read_factory();
    Spawn::register_with_read_factory();
    Map::register_with_read_factory();
    ZombieAIController::register_factory();

    init_asset_paths();
}




ConfigVariableBool threaded_physic
("threaded-physic", true,
    PRC_DESC("Enabling this will make physic simulation run on its own thread."));

ConfigVariableBool threaded_ai
("threaded-ai", true,
    PRC_DESC("Enabling this will make ai logics run on its own thread."));

ConfigVariableString render_pipeline
("render-pipeline", "",
    PRC_DESC("Name of the render pipeline, is empty, simple rendering is used."));

ConfigVariableDouble bullet_lifespan
("bullet-lifespan", 3.0,
    PRC_DESC("This defines how long can bullets exist before they hit an object,"
    "if this timeout is reached, the bullet is automatically destroyed."));

ConfigVariableInt max_bullet_holes
("max-bullet-holes", 512,
    PRC_DESC("The maximum number of bullet holes that can be rendered."
    "If a new bullet hole is spawned, then the oldest bullet hole is destroyed"));

ConfigVariableInt max_bullet_textures
("max-bullet-textures", 32,
    PRC_DESC("The maximum number of bullet impact texture that can be stored in"
    "th atlas"));

ConfigVariableDouble bullet_hole_offset
("bullet-hole-offset", 0.05,
    PRC_DESC("This defines how much the bullet hole bounding volume is offseted"
    "from the impact point"));

ConfigVariableDouble bullet_hole_depth
("bullet-hole-depth", 0.1,
    PRC_DESC("This defines how much the bullet hole bounding volume penetrate"
    "the surface"));

ConfigVariableBool hardware_skinning
("hardware-skinning", true,
    PRC_DESC("Whene enabled, the animated mesh deformation will be computed in"
    "the fragment shader. It is recommended let it enabled as GPU are more"
    "performant for skinning than traditional CPU skinning."));