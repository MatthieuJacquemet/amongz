// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/config_dxml.h>
#include <panda3d/tinyxml.h>
#include "materialReader.h"
#include "shaderMaterial.h"


using namespace std;



MaterialReader::PropertyHandlers MaterialReader::_handler;


ShaderMaterial* MaterialReader::read(istream& stream) {

    TiXmlDocument* doc = read_xml_stream(stream);
    nassertr(doc != nullptr, nullptr)

    if (doc->Error()) {
        mat_error("failled to parse material file : line " << doc->ErrorRow()
                << " col " << doc->ErrorCol() << " : " << doc->ErrorDesc());
        return nullptr;
    }

    TiXmlElement* root = doc->RootElement();
    TiXmlElement* name_elem = root->FirstChildElement("name");
    nassertr(name_elem != nullptr, nullptr)

    const char* class_name = root->Attribute("class");
    
    if (class_name == nullptr) {
        mat_error("no class specified in root tag");
        return nullptr;
    }
    
    if (root->ValueStr() != "material")
        mat_warning("root tag should be named 'material'");

    string type_name = template_name("Material", class_name);

    WritableFactory* factory = BamReader::get_factory();
    TypedWritable* object = factory->make_instance(type_name);

    ShaderMaterial* material = DCAST(ShaderMaterial, object);
    material->set_name(name_elem->FirstChild()->ValueStr());

    read_properties(root, material);
    read_flags(root, material);
    
    delete doc;

    return material;
}


void MaterialReader::read_properties(TiXmlElement* root, ShaderMaterial* material) {
    
    using Properties = ShaderMaterial::Properties;
    const Properties& props = material->get_properties();

    TiXmlElement* props_elem = root->FirstChildElement("properties");
    const TiXmlNode* prop_node = nullptr;

    if (props_elem == nullptr)
        return;

    while (prop_node = props_elem->IterateChildren(prop_node)) {

        const TiXmlElement* element = prop_node->ToElement();
        const char* id = element->Attribute("id");

        Properties::const_iterator it = props.find(id);
        if (it == props.end())
            mat_error("invalid property : " + string(id));
        else
            set_property(it->second, element);
    }
}


void MaterialReader::read_flags(TiXmlElement* root, ShaderMaterial* material) {

    static map<string, int> flags = {
        {"disable_alpha_write", ShaderAttrib::F_disable_alpha_write},
        {"subsume_alpha_test", ShaderAttrib::F_subsume_alpha_test},
        {"hardware_skinning", ShaderAttrib::F_hardware_skinning},
        {"shader_point_size", ShaderAttrib::F_shader_point_size},
        {"hardware_instancing", ShaderAttrib::F_hardware_instancing}
    };

    TiXmlElement* flags_elem = root->FirstChildElement("flags");
    const TiXmlNode* flag_node = nullptr;
    
    if (flags_elem == nullptr)
        return;


    while (flag_node = flags_elem->IterateChildren(flag_node)) {

        const TiXmlElement* element = flag_node->ToElement();
        string flag_name = element->ValueStr();

        map<string, int>::iterator it = flags.find(flag_name);
    
        if (it != flags.end())
            material->set_flag(it->second, true);
        else
            mat_error("unknown flag : " << flag_name);
    }
}


void parse_value(string& dest, const TiXmlElement* element, const string& name) {
    int res = element->QueryStringAttribute(name.c_str(), &dest);
    nassertv(res == TIXML_SUCCESS)
}

void parse_value(float& dest, const TiXmlElement* element, const string& name) {
    int res = element->QueryFloatAttribute(name.c_str(), &dest);
    nassertv(res == TIXML_SUCCESS)
}

void parse_value(double& dest, const TiXmlElement* element, const string& name) {
    int res = element->QueryDoubleAttribute(name, &dest);
    nassertv(res == TIXML_SUCCESS)
}

void parse_value(int& dest, const TiXmlElement* element, const string& name) {
    int res = element->QueryIntAttribute(name, &dest);
    nassertv(res == TIXML_SUCCESS)
}


void parse_value(LVecBase2f& dest, const TiXmlElement* element, const string&) {
    parse_value(dest[0], element, "x");
    parse_value(dest[1], element, "y");
}

void parse_value(LVecBase3f& dest, const TiXmlElement* element, const string&) {
    parse_value(dest[0], element, "x");
    parse_value(dest[1], element, "y");
    parse_value(dest[2], element, "z");
}

void parse_value(LVecBase4f& dest, const TiXmlElement* element, const string&) {
    parse_value(dest[0], element, "x");
    parse_value(dest[1], element, "y");
    parse_value(dest[2], element, "z");
    parse_value(dest[3], element, "w");
}


template<class T>
void set_prop_value(PropertyBase* prop, const TiXmlElement* element) {

    T value;
    parse_value(value, element, "value");

    MaterialProperty<T>* property = DCAST(MaterialProperty<T>, prop);
    property->set_value(value);
}


template<>
void set_prop_value<Texture>(PropertyBase* prop, const TiXmlElement* element) {

    string path;
    parse_value(path, element, "path");

    MaterialProperty<Texture>* property = DCAST(MaterialProperty<Texture>, prop);
    property->set_value(path);
}


void MaterialReader::set_property(PropertyBase* prop, const TiXmlElement* element) {
    
    const string& class_name = element->ValueStr();

    PropertyHandlers::iterator it = _handler.find(class_name);
    if (it != _handler.end())
        it->second(prop, element);
    else
        mat_error("unsupported property type : " + class_name);
}


#define REGISTER_HANDLER(class) _handler[#class] = &set_prop_value<class>;

void MaterialReader::init() {
    
    REGISTER_HANDLER(float)
    REGISTER_HANDLER(double)
    REGISTER_HANDLER(int)

    REGISTER_HANDLER(LVecBase2f)
    REGISTER_HANDLER(LVecBase3f)
    REGISTER_HANDLER(LVecBase4f)

    /// @todo
    // REGISTER_HANDLER(LMatrix3f)
    // REGISTER_HANDLER(LMatrix4f)

    REGISTER_HANDLER(Texture)
}
