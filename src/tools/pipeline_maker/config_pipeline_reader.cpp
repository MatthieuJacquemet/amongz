// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/load_prc_file.h>
#include <panda3d/dconfig.h>

#include "config_game.h"
#include "graphicsBuffer.h"
#include "renderScene.h"
#include "downscaleDepth.h"
#include "lighting.h"
#include "renderPipelineReader.h"
#include "lightPool.h"
#include "computeStage.h"
#include "config_pipeline_reader.h"

using namespace std;


ConfigureDef(config_pipeline_reader);

ConfigureFn(config_pipeline_reader) {
    init_pipeline_reader();
}


void init_pipeline_reader() {

    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;

    init_game();

    XmlElementParam::init_type();

    XmlElementReader<RenderScene>::init_type();
    XmlElementReader<DownscaleDepth>::init_type();
    XmlElementReader<Lighting>::init_type();
    XmlElementReader<ComputeStage>::init_type();

    XmlElementReader<RenderScene>::register_with_read_factory();
    XmlElementReader<DownscaleDepth>::register_with_read_factory();
    XmlElementReader<Lighting>::register_with_read_factory();
    XmlElementReader<ComputeStage>::register_with_read_factory();
}
