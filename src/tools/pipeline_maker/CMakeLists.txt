cmake_minimum_required(VERSION 3.1)

get_filename_component(PROJECTID ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${PROJECTID} LANGUAGES CXX)


file(GLOB_RECURSE SRCS ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)

add_executable(${PROJECT_NAME} ${SRCS})
include_directories(${INCLUDE_DIRS} ${BULLET_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC ${PANDA_LIBS} gdk ${AMONGZ_COMMON})


