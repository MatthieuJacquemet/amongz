// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/config_dxml.h>
#include <panda3d/tinyxml.h>

#include "config_render_pipeline.h"
#include "renderPipelineReader.h"

using namespace std;

DEFINE_TYPEHANDLE(XmlElementParam)


#define REGISTER_HANDLER(name) \
    _handlers[#name] = handle_ ## name;


RenderPipeline* RenderPipelineReader::read(std::istream& stream) {
    
    const TiXmlDocument* doc = read_xml_stream(stream);
    nassertr(doc != nullptr, nullptr)

    if (doc->Error()) {
        rp_error("failled to parse pipeline file : line " << doc->ErrorRow()
                << " col " << doc->ErrorCol() << " : " << doc->ErrorDesc());
        return nullptr;
    }

    const TiXmlElement* root = doc->RootElement();

    XmlRenderPipelineReader* pipeline = new XmlRenderPipelineReader(root);


    const TiXmlNode* child_node = nullptr;



    while (child_node = root->IterateChildren(child_node)) {

        const TiXmlElement* element = child_node->ToElement();
        
        if (element == nullptr)
            continue;
        
        const string& tag_name = element->ValueStr();
        Handlers::iterator it = _handlers.find(tag_name);

        if (it != _handlers.end())
            it->second(element, pipeline);
        else
            rp_warning("unknown tag name " << tag_name);
    }
    pipeline->finalize();

    delete doc;

    return pipeline;
}


StageFactory* RenderPipelineReader::get_factory() {

    static Singleton<StageFactory> singleton(new StageFactory);
    return singleton;
}



void RenderPipelineReader::handle_stage(const TiXmlElement* element,
                                    XmlRenderPipelineReader* pipeline)
{
    
    string type;

    if (element->QueryStringAttribute("type", &type) == TIXML_SUCCESS) {

        StageFactory* factory = get_factory();

        FactoryParams params;
        params.add_param(new XmlElementParam(element));

        RenderStage* stage = factory->make_instance(type, params);

        if (stage == nullptr)
            rp_error("stage type not found : " << type);
        else {
            pipeline->add_stage(stage);
            stage->set_pipeline(pipeline);
            stage->setup();
        }

    } else
        rp_error("stage type not specified line " << element->Row());
}


void RenderPipelineReader::handle_target(const TiXmlElement* element,
                                    XmlRenderPipelineReader* pipeline)
{
    
    string id;
    if (element->QueryStringAttribute("id", &id) != TIXML_SUCCESS) {
        rp_error("target id not specified" << element->Row());
        return;
    }
    

    string type_name = "2d_texture";
    string cmp_name = "unsigned_byte";
    string fmt_name = "rgba8";
    string scale_name = "1";
    string size_name = "1x1x1";
    string min_filter_name = "nearest";
    string mag_filter_name = "nearest";

    element->QueryStringAttribute("type", &type_name);
    element->QueryStringAttribute("component", &cmp_name);
    element->QueryStringAttribute("format", &fmt_name);
    element->QueryStringAttribute("scale", &scale_name);
    element->QueryStringAttribute("size", &size_name);
    element->QueryStringAttribute("min_filter", &min_filter_name);
    element->QueryStringAttribute("mag_filter", &mag_filter_name);

    Texture::TextureType type = Texture::string_texture_type(type_name);
    Texture::ComponentType component = Texture::string_component_type(cmp_name);
    Texture::Format format = Texture::string_format(fmt_name);
    SamplerState::FilterType min_filter = SamplerState::string_filter_type(min_filter_name);
    SamplerState::FilterType mag_filter = SamplerState::string_filter_type(mag_filter_name);

    float numerator = 0.0f;
    float denumerator = 1.0f;
    int size_x = 1, size_y = 1, size_z = 1;

    sscanf(scale_name.c_str(), "%f/%f", &numerator, &denumerator);
    sscanf(size_name.c_str(), "%dx%dx%d", &size_x, &size_y, &size_z);

    if (denumerator == 0.0f) {
        rp_error("division by zero line : " + element->Row());
        return;
    }

    RenderTarget* target = new RenderTarget(id);
    target->setup_texture(type, size_x, size_y, size_z, component, format);
    target->set_scale(numerator / denumerator);
    target->set_minfilter(min_filter);
    target->set_magfilter(mag_filter);

    PTA_uchar image = target->make_ram_image();
    memset(image.p(), 0, image.size());

    pipeline->add_target(target);
}



XmlElementParam::XmlElementParam(const TiXmlElement* element):
    _element(element)
{
    
}

const TiXmlElement* XmlElementParam::get_element() const {
    return _element;
}


XmlRenderPipelineReader::XmlRenderPipelineReader(const TiXmlElement* element):
    _element(element)
{

    element->QueryStringAttribute("id", &_name);
    
    string state_name;
    if (element->QueryStringAttribute("initial_state", &state_name) == TIXML_SUCCESS) {
        TypedWritable* object = read_object(state_name);

        if (object != nullptr)
            _initial_state = DCAST(RenderState, object);
    }
}


void XmlRenderPipelineReader::add_target(RenderTarget* target) {

    _targets[target->get_name()] = target;
}


void XmlRenderPipelineReader::finalize() {
    
    string output_name;
    if (_element->QueryStringAttribute("output", &output_name) == TIXML_SUCCESS)
        _output = get_render_target(output_name);
    else {
        _output = new RenderTarget("output");
        _output->setup_2d_texture(1,1, Texture::T_unsigned_byte, Texture::F_rgb8);
        add_target(_output);
    }
}


RenderPipelineReader::RenderPipelineReader() {

    REGISTER_HANDLER(stage)
    REGISTER_HANDLER(target)
}



    // size_t div_pos = scale_name.find("/");
    // if (div_pos != string::npos) {
    //     string numer_str = scale_name.substr(0, div_pos);
    //     string denom_str = scale_name.substr(div_pos + 1);

    //     try {
    //         scale = stof(numer_str) / stof(denom_str);
    //     } catch (...) {
    //         rp_error("failed to parse : " << scale_name << " line " << element->Row());
    //     }
    // }