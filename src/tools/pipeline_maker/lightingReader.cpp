// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/tinyxml.h>

#include "lighting.h"
#include "renderPipelineReader.h"
#include "config_render_pipeline.h"

using namespace std;


template<> void XmlElementReader<Lighting>::setup() {

    read_id();

    define_target("geom_normal", _geom_normal);
    define_target("light_volume", _light_volume);
    define_target("linear_depth", _linear_depth);
    define_target("diffuse", _diffuse);
    define_target("accumulator", _accumulator);
    define_target("normal", _normal);
    define_target("specular", _specular);
    define_target("depth", _depth);
}
