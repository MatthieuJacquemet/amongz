// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/tinyxml.h>

#include "computeStage.h"
#include "renderPipelineReader.h"
#include "config_render_pipeline.h"


using namespace std;



template<> void XmlElementReader<ComputeStage>::setup() {

    int line = _element->Row();

    string result = "compute-pass";
    _element->QueryStringAttribute("id", &result);

    set_name(result);

    int group_size;

    if (_element->QueryIntAttribute("group_size", &group_size) == TIXML_SUCCESS)
        _group_size.fill(group_size);

    else if (_element->QueryStringAttribute("group_size", &result) == TIXML_SUCCESS)
        sscanf(result.c_str(), "%dx%d", &_group_size[0], &_group_size[1]);
    else
        _group_size.fill(16);


    typedef map<string, Shader::ShaderLanguage> Languages;
    Languages languages = {
        {"glsl", Shader::SL_GLSL},
        {"spirv", Shader::SL_SPIR_V},
        {"hlsl", Shader::SL_HLSL},
        {"cg", Shader::SL_Cg},
    };

    Shader::ShaderLanguage lang = Shader::SL_none;

    if (_element->QueryStringAttribute("language", &result) == TIXML_SUCCESS) {
        Languages::iterator it = languages.find(result);
        if (it != languages.end())
            lang = it->second;
        else
            rp_error("unknown shading language : " << result << " line " << line);
    } else
        lang = Shader::SL_GLSL;


    if (_element->QueryStringAttribute("shader", &result) == TIXML_SUCCESS)
        _state.set_shader(Shader::load_compute(lang, result));
    else
        rp_error("shader not specified line : " << line);


    const TiXmlNode* child = nullptr;

    while (child = _element->IterateChildren(child)) {
        const TiXmlElement* child_elem = child->ToElement();
        line = child_elem->Row();

        if (child_elem->ValueStr() == "input") {
            string id, target;
            if (!child_elem->QueryStringAttribute("id", &id) == TIXML_SUCCESS) {
                rp_error("id is not specified in ComputeStage input" << line);
                continue;
            }
            if (!child_elem->QueryStringAttribute("target", &target) == TIXML_SUCCESS) {
                rp_error("target is not specified in ComputeStage input" << line);
                continue;
            }
            _inputs[id] = _pipeline->get_render_target(target);
        } else
            rp_warning("unknown tag name in ComputeStage line : " << line);
    }
}


        // int delim_pos = result.find('x');
        
        // if (delim_pos != string::npos) {
        //     string left = result.substr(0, delim_pos);
        //     string right = result.substr(delim_pos + 1);
    
        //     try {
        //         _group_size.set(stoi(left), stoi(right));
        //     } catch (...) {
        //         rp_error("failed to parse group_size");
        //     }
        // }