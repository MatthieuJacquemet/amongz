// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/tinyxml.h>

#include "renderSceneReader.h"
#include "config_render_pipeline.h"

using namespace std;

using RenderTexturePlane = DrawableRegion::RenderTexturePlane;
using BinType = CullBinEnums::BinType;


template<> void XmlElementReader<RenderScene>::setup() {

    read_id();

    typedef map<string, RenderTexturePlane> Bindings;
    typedef map<string, BinType> BinTypes;

    static const Bindings bindings = {
        {"color", RenderTexturePlane::RTP_color},
        {"rgba_0", RenderTexturePlane::RTP_aux_rgba_0},
        {"rgba_1", RenderTexturePlane::RTP_aux_rgba_1},
        {"rgba_2", RenderTexturePlane::RTP_aux_rgba_2},
        {"rgba_3", RenderTexturePlane::RTP_aux_rgba_3},
        {"hrgba_0", RenderTexturePlane::RTP_aux_hrgba_0},
        {"hrgba_1", RenderTexturePlane::RTP_aux_hrgba_1},
        {"hrgba_2", RenderTexturePlane::RTP_aux_hrgba_2},
        {"hrgba_3", RenderTexturePlane::RTP_aux_hrgba_3},
        {"float_0", RenderTexturePlane::RTP_aux_float_0},
        {"float_1", RenderTexturePlane::RTP_aux_float_1},
        {"float_2", RenderTexturePlane::RTP_aux_float_2},
        {"float_3", RenderTexturePlane::RTP_aux_float_3},
        {"depth", RenderTexturePlane::RTP_depth},
        {"stencil", RenderTexturePlane::RTP_stencil},
        {"depthstencil", RenderTexturePlane::RTP_depth_stencil}
    };

    static const BinTypes bin_types = {
        {"unsorted", BinType::BT_unsorted},
        {"state_sorted", BinType::BT_state_sorted},
        {"back_to_front", BinType::BT_back_to_front},
        {"front_to_back", BinType::BT_front_to_back},
        {"fixed", BinType::BT_fixed}
    };


    for (const Bindings::value_type& it : bindings) {
        const char* id = it.first.c_str();

        string result;
        if (_element->QueryStringAttribute(id, &result) == TIXML_SUCCESS) {
            BindingSlot& slot = _bindings[it.second];
            slot._target = _pipeline->get_render_target(result);
            slot._clear = false;
        }
    }

    const TiXmlNode* child = nullptr;

    while (child = _element->IterateChildren(child)) {
        const TiXmlElement* child_elem = child->ToElement();
        int line = child_elem->Row();

        if (child_elem->ValueStr() == "cullbin") {
            string bin_name;

            if (child_elem->QueryStringAttribute("id", &bin_name) != TIXML_SUCCESS) {
                rp_error("cullbin id not specified line : " << line);
                continue;
            }

            string result;
            int sort = 0;
            BinType bin_type = BinType::BT_unsorted;

            if (child_elem->QueryStringAttribute("type", &result) == TIXML_SUCCESS) {
                BinTypes::const_iterator it = bin_types.find(result.c_str());

                if (it != bin_types.end())
                    bin_type = it->second;
            }

            child_elem->QueryIntAttribute("sort", &sort);
            add_bin(bin_name, bin_type, sort);
        }
        else if (child_elem->ValueStr() == "clear") {

            string target_name;
            if (child_elem->QueryStringAttribute("target", &target_name) != TIXML_SUCCESS) {
                rp_error("no clear target specified line : " << line);
                continue;
            }

            Bindings::const_iterator it = bindings.find(target_name);

            if (it == bindings.end()) {
                rp_error("unknown target : " << target_name << " line : " << line);
                continue;
            }
            BindingSlot& slot = _bindings[it->second];
            if (slot._target != nullptr)
                slot._clear = true;
            else
                rp_error("trying to clear unbound render target : "
                        << target_name << " line : " << line);
        }
        else
            rp_warning("unknown tag name in RenderScene line : " << line);
    }
}
