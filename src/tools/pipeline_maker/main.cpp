// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/virtualFileSystem.h>
#include <panda3d/bamFile.h>

#include "renderPipeline.h"
#include "config_pipeline_reader.h"
#include "renderPipelineReader.h"
#include "defines.h"

using namespace std;



int main(int argc, char* argv[]) {

    if (argc == 1) {
        cerr << "invalid number of argument" << endl;
        return EXIT_FAILURE;
    }

    Filename src_path(argv[1]);
    src_path.set_text();

    init_pipeline_reader();

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    
    PT(VirtualFile) file = vfs->get_file(src_path);
    nassertr(file->is_directory(), EXIT_FAILURE)


    PT(VirtualFileList) sub = file->scan_directory();

    if (sub == nullptr)
        return EXIT_FAILURE;


    for (int i=0; i<sub->get_num_files(); ++i) {
        Filename filename = sub->get_file(i)->get_filename();

        if (filename.get_extension() != "xml")
            continue;

        filename.set_text();

        PT(VirtualFile) subfile = vfs->get_file(filename);
        std::istream* stream = subfile->open_read_file(false);

        if (stream == nullptr) {
            nout << "could not open file " << filename << endl;
            continue;
        }

        RenderPipelineReader reader;
        PT(RenderPipeline) pipeline = reader.read(*stream);

        subfile->close_read_file(stream);

        if (pipeline == nullptr) {
            cerr << "could not read render pipeline file : " << filename << endl;
            continue;
        }


        Filename path(PIPELINE_PATH);
        path.set_basename_wo_extension(filename.get_basename_wo_extension());
        path.set_extension("bam");

        write_object(path, pipeline);
    }

    return EXIT_SUCCESS;
}