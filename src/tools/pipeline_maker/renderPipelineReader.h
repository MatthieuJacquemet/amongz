// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDERPIPELINEREADER_H__
#define __RENDERPIPELINEREADER_H__

#include <panda3d/factoryParam.h>
#include <istream>
#include <map>

#include "renderPipeline.h"
#include "renderStage.h"
#include "utils.h"


class TiXmlElement;
class RenderPipeline;




class XmlElementParam: public FactoryParam {

    REGISTER_TYPE("XmlElementParam", FactoryParam)

public:
    XmlElementParam(const TiXmlElement* element);
    const TiXmlElement* get_element() const;

private:
    const TiXmlElement* _element;
};



template<class T>
class XmlElementReader: public T {

public:
    static void register_with_read_factory();

protected:
    XmlElementReader(const TiXmlElement* element);
    void setup() override;

    const TiXmlElement* _element;

    void read_id();
    void define_target(const std::string& name, PT(Texture)& target) const;

private:
    static RenderStage* make(const FactoryParams& params);
};



class XmlRenderPipelineReader: public RenderPipeline {

public:
    XmlRenderPipelineReader(const TiXmlElement* element);
    virtual ~XmlRenderPipelineReader() = default;

    void add_target(RenderTarget* target);

    virtual void finalize();

protected:
    const TiXmlElement* _element;
};



class RenderPipelineReader {

public:
    typedef Factory<RenderStage> StageFactory;

    RenderPipelineReader();
    ~RenderPipelineReader() = default;

    RenderPipeline* read(std::istream& stream);

    static StageFactory* get_factory();


private:
    static void handle_stage(const TiXmlElement* element,
                            XmlRenderPipelineReader* pipeline);

    static void handle_target(const TiXmlElement* element,
                            XmlRenderPipelineReader* pipeline);

    typedef void ChildHandler(const TiXmlElement*, XmlRenderPipelineReader*);
    typedef std::map<std::string, ChildHandler*> Handlers;

    Handlers _handlers;
};


typedef RenderPipelineReader::StageFactory StageFactory;

#include "renderPipelineReader.T"


#endif // __RENDERPIPELINEREADER_H__