// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/virtualFileSystem.h>
#include <panda3d/loader.h>
#include <panda3d/bamFile.h>

#include "textureConverter.h"
#include "config_game.h"
#include "utils.h"


using namespace std;


void r_convert(TextureConverter& converter, PT(VirtualFile) file) {


    if (file->is_regular_file()) {
        Filename name = file->get_filename();
        
        if (name.get_extension() != "bam")
            return;

        Loader* loader = Loader::get_global_ptr();
        PT(PandaNode) node = loader->load_sync(name);

        if (node != nullptr) {
            converter.convert(node);
            write_object(name, node);
        }
    }
    
    else if (file->is_directory()) {

        PT(VirtualFileList) sub = file->scan_directory();

        for (int i=0; i<sub->get_num_files(); ++i)
            r_convert(converter, sub->get_file(i));
    }
}


int main(int argc, char* argv[]) {

    if (argc == 1) {
        cerr << "invalid number of argument" << endl;
        return EXIT_FAILURE;
    }

    Filename path(argv[1]);

    if (!path.exists()) {
        cerr << "path parameter do not exists" << endl;
        return EXIT_FAILURE;
    }
    
    init_game();

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();

    
    TextureConverter converter;
    converter.set_compression_mode(Texture::CM_dxt5);

    r_convert(converter, vfs->get_file(path));

    return EXIT_SUCCESS;
}