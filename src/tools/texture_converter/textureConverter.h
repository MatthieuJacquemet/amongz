

// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TEXTURE_CONVERTER_H__
#define __TEXTURE_CONVERTER_H__

#include <panda3d/texture.h>


class PandaNode;


class TextureConverter {

public:
    TextureConverter() = default;
    ~TextureConverter() = default;

    void set_quality(Texture::QualityLevel quality);
    void set_compression_mode(Texture::CompressionMode mode);

    void set_pz_flag(bool flag);
    void set_mipmap_flag(bool flag);

    void convert(PandaNode* node);

private:

    void convert_txo(Texture *tex);

    typedef pset<Texture*> Textures;
    Textures _textures;

    bool _tex_txopz = false;
    bool _tex_mipmap = true;

    void collect_textures(PandaNode* node);
    void collect_textures(const RenderState *state);

    Texture::QualityLevel _quality = Texture::QL_default;
    Texture::CompressionMode _mode = Texture::CM_on;
};


#endif // __TEXTURE_CONVERTER_H__