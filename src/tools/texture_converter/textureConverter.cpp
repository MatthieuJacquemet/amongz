// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/load_prc_file.h>
#include <panda3d/textureAttrib.h>
#include <panda3d/geomNode.h>
#include <panda3d/bamFile.h>

#include "textureConverter.h"

using namespace std;


string quality_to_string(Texture::QualityLevel level) {

    switch (level) {
        case Texture::QL_fastest: return "fastest";
        case Texture::QL_normal: return "normal";
        case Texture::QL_best: return "best";
    }
    return "normal";
};


void TextureConverter::convert(PandaNode* node) {


    string level = quality_to_string(_quality);
    string prc = "texture-quality-level " + level;
    load_prc_file_data("prc", prc);


    if (_mode != Texture::CM_off)
        bam_texture_mode = BamFile::BTM_rawdata;

    collect_textures(node);


    Textures::iterator ti;

    for (Texture* tex : _textures) {

        tex->get_ram_image();

        if (_tex_mipmap || tex->uses_mipmaps())
            tex->generate_ram_mipmap_images();

        if (_mode != Texture::CM_off) {
            if (!tex->compress_ram_image())
                nout << "  couldn't compress " << tex->get_name() << "\n";

            tex->set_compression(_mode);
        }

        convert_txo(tex);
    }
}


/**
 * Recursively walks the scene graph, looking for Texture references.
 */
void TextureConverter::collect_textures(PandaNode *node) {

    collect_textures(node->get_state());

    if (node->is_geom_node()) {
        GeomNode* geom_node = DCAST(GeomNode, node);
        int num_geoms = geom_node->get_num_geoms();

        for (int i = 0; i < num_geoms; ++i)
            collect_textures(geom_node->get_geom_state(i));
    }

    PandaNode::Children children = node->get_children();
    int num_children = children.get_num_children();
    
    for (int i = 0; i < num_children; ++i)
        collect_textures(children.get_child(i));
}


/**
 * Recursively walks the scene graph, looking for Texture references.
 */
void TextureConverter::collect_textures(const RenderState *state) {

    const TextureAttrib *tex_attrib = 
        DCAST(TextureAttrib, state->get_attrib(TextureAttrib::get_class_type()));

    if (tex_attrib != nullptr) {
        int num_on_stages = tex_attrib->get_num_on_stages();

        for (int i = 0; i < num_on_stages; ++i) {
            TextureStage* stage = tex_attrib->get_on_stage(i);
            _textures.insert(tex_attrib->get_on_texture(stage));
        }
    }
}


/**
 * If the indicated Texture was not already loaded from a txo file, writes it
 * to a txo file and updates the Texture object to reference the new file.
 */
void TextureConverter::convert_txo(Texture *tex) {
    
    if (tex->get_loaded_from_txo())
        return;

    Filename filename = tex->get_filename().get_filename_index(0);

    if (_tex_txopz)
        filename.set_extension("txo.pz");
    else
        filename.set_extension("txo");


    if (!tex->write(filename)) {
        nout << "failed to write texture in " << filename << endl;
        return;
    }

    nout << "  Writing " << filename;
    
    if (tex->get_ram_image_compression() != Texture::CM_off)
        nout << " (compressed " << tex->get_ram_image_compression() << ")";

    nout << "\n";

    tex->set_loaded_from_txo();
    tex->set_filename(filename);
    tex->clear_alpha_filename();
}


void TextureConverter::set_quality(Texture::QualityLevel quality) {
    _quality = quality;
}

void TextureConverter::set_compression_mode(Texture::CompressionMode mode) {
    _mode = mode;
}

void TextureConverter::set_pz_flag(bool flag) {
    _tex_txopz = flag;
}

void TextureConverter::set_mipmap_flag(bool flag) {
    _tex_mipmap = flag;
}