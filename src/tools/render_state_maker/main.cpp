// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/bamFile.h>
#include <panda3d/rescaleNormalAttrib.h>
#include <panda3d/shadeModelAttrib.h>

#include "utils.h"
#include "config_game.h"
#include "render_utils.h"
#include "shaderState.h"


using namespace std;



int main(int argc, char* argv[]) {

    if (argc == 1) {
        cerr << "invalid number of argument" << endl;
        return EXIT_FAILURE;
    }

    init_game();

    Filename filename(argv[1]);
    filename.set_text();



    ShaderState state;

    state.set_shader(Shader::load(SHADER_LANG, 
                GDK_SHADER_PATH VERT_SHADER("basic"),
                GDK_SHADER_PATH FRAG_SHADER("opaque")));
    
    state.set_two_sided(false, 100);
    state.set_material_off(100);
    state.set_light_off(100);

    state.set_attrib(RescaleNormalAttrib::make_default());
    state.set_attrib(ShadeModelAttrib::make(ShadeModelAttrib::M_smooth));

    if (!write_object(filename, state.get_state()))
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}