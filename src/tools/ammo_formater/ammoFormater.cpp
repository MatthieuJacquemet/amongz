// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>                 // for EXIT_SUCCESS
#include <loader.h>
#include <nodePath.h>
#include <animBundleNode.h>
#include <bamFile.h>

#include "ammoFormater.h"


using namespace std;



Bullet::Type get_bullet_type(const string& name) {

    string delim = "/";

    size_t start = 0;
    size_t end;

    Bullet::Type types = Bullet::BT_standard;

    do {
        end = name.find(delim, start);
        string type = name.substr(start, end - start);
        start = end + 1;
        
        if (type == "tracer")
            types = types | Bullet::BT_tracer;
        else if (type == "armor_piercing")
            types = types | Bullet::BT_armor_piercing;
        else if (type == "incendiary")
            types = types | Bullet::BT_incendiary;
        else if (type == "explosive")
            types = types | Bullet::BT_explosive;
        else if (type == "hollow_point")
            types = types | Bullet::BT_hollow_point;
        
    } while (end != string::npos);

    return types;
}


AmmoFormater::AmmoWriter::AmmoWriter(const CSVRow& row) {

    set_name(row.get_string("name"));
    
    _mass           = row.get_float("mass");
    _radius         = row.get_float("radius");
    _penetration    = row.get_float("penetration");
    _drag_factor    = row.get_float("drag_factor");
    _damage_factor  = row.get_float("damage_factor");
    _bullet_types   = get_bullet_type(row.get_string("bullet_types"));
}


bool AmmoFormater::format(const string& path) {

    CSVParser parser(path);

    if (parser.get_num_rows() <= 1)
        return false;


    for (int i=1; i<parser.get_num_rows(); ++i) {
        AmmoWriter* writer = new AmmoWriter(parser.get_row(i));
        AmmoData::_ammo_datas[writer->get_name()] = writer;
    }

    AmmoData::Helper helper;

    return write_object("/ammo/ammo.bam", &helper);
}

AmmoFormater::AmmoFormater() {
    local_object();
}
