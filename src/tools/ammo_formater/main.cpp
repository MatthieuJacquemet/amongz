#include "ammoFormater.h"
#include "config_game.h"

using namespace std;


int main(int argc, char* argv[]) {

    if (argc == 1) {
        cerr << "invalid number of argument" << endl;
        return EXIT_FAILURE;
    }

    Filename path(argv[1]);
    if (!path.is_regular_file()) {
        cerr << "path parameter is not a file" << endl;
        return EXIT_FAILURE;
    }

    init_game();

    AmmoFormater formater;

    if (!formater.format(path))
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

