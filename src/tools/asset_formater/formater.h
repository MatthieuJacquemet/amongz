// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FORMATER_H__
#define __FORMATER_H__


#include <panda3d/pandaNode.h>
#include <panda3d/loader.h>
#include "color.h"
#include "utils.h"



class AssetData {

public:
    AssetData(PandaNode* node);
    
    /** @brief recupere une chaine de caratère dans propriété dont le nom est passé en paramettre
     *  @param name nom de la propriété
     *  @param default_value valeur retourné si aucune propriété n'a été trouvé
     */
    std::string get_string(const std::string& name, 
                            const std::string& default_value="") const;


    /** @brief recupere un entier dans propriété dont le nom est passé en paramettre
     *  @param name nom de la propriété
     *  @param default_value valeur retourné si aucune propriété n'a été trouvé
     */
    int get_int(const std::string& name, int default_value=0) const;


    /** @brief recupere un réel dans une propriété dont le nom est passé en paramettre
     *  @param name nom de la propriété
     *  @param default_value valeur retourné si aucune propriété n'a été trouvé
     */
    float get_float(const std::string& name, float default_value=0.0f) const;


    /** @brief recupere une couleur dans une propriété dont le nom est passé en paramettre
     *  @param name nom de la propriété
     *  @param default_value valeur retourné si aucune propriété n'a été trouvé
     */
    Color get_color(const std::string& name, 
                Color default_value=Color::black()) const;

    void clear_tags();

private:
    PandaNode* _node;
    mutable std::set<std::string> _to_remove;
};


class FormaterParam: public FactoryParam {
    
    REGISTER_TYPE("FormaterParam", FactoryParam)  
  
public:
    FormaterParam(PandaNode* node);
    PandaNode* get_node() const;

private:
    PT(PandaNode) _node;
};



template<class T>
class Formater: public T {

public:

protected:

    static PandaNode* parse_params(const FactoryParams& params) {
    
        nassertr(params.get_num_params() > 0, nullptr)
        FormaterParam* param = DCAST(FormaterParam, params.get_param(0));
        
        return param->get_node();
    }

    AssetData get_data() const {
        return _node_data;
    }

    void write_datagram(BamWriter* manager, Datagram& me) final {

        fillin(_node_data);
        _node_data.clear_tags();
         
        T::write_datagram(manager, me);
    }

    virtual void fillin(const AssetData& data) {};

    template<class... Args>
    Formater(Args... args): T(args...), _node_data(this) {};
    virtual ~Formater() = default;

private:
    AssetData _node_data;
};


class FormaterManager {

public:
    typedef Factory<PandaNode> FormaterFactory;

    FormaterManager() = default;
    ~FormaterManager() = default;

    bool format_asset(const std::string& source_file);

    static void register_location(TypeHandle type, const std::string& location);
    static std::string get_location(TypeHandle type);

    static FormaterFactory* get_factory();
private:
   
private:
    bool write(PandaNode* asset, const std::string& source_path);

    static Singleton<FormaterFactory> _formaters;
};

typedef FormaterManager::FormaterFactory FormaterFactory;

#endif // __FORMATER_H__