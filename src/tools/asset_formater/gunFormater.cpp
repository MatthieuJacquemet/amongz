// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/animBundleNode.h>

#include "animSequence.h"
#include "ammoData.h"
#include "gunFormater.h"


using namespace std;


uint8_t get_fire_mode(string name) {

    string delim = "/";
    size_t start = 0;
    size_t end;

    uint8_t fire_mode = 0;

    do {
        end = name.find(delim, start);
        string mode = name.substr(start, end - start);
        start = end + 1;
    
        if (mode == "bolt_action") fire_mode |= (0x1 << Gun::FM_bolt_action);
        else if (mode == "semi") fire_mode |= (0x1 << Gun::FM_semi_auto);
        else if (mode == "full") fire_mode |= (0x1 << Gun::FM_full_auto);
        else if (mode == "burst") fire_mode |= (0x1 << Gun::FM_burst);
                    
    } while (end != string::npos);

    return fire_mode;
}


Gun::Class get_gun_class(string name) {

    if      (name == "handgun")         return Gun::C_handgun;
    else if (name == "assault")         return Gun::C_assault;
    else if (name == "submachine_gun")  return Gun::C_submachine_gun;
    else if (name == "lightmachine_gun")return Gun::C_lightmachine_gun;
    else if (name == "sniper")          return Gun::C_sniper;
    else if (name == "launcher")        return Gun::C_launcher;
    else if (name == "shotgun")         return Gun::C_shotgun;

    return Gun::C_unknown;
}


class AnimWriter: private AnimSequence {

    AnimWriter(AnimBundle* bundle, const Tags& tags): 
            AnimSequence(bundle, "events")
            
    {
        _tags = tags;
    }
    
    friend class GunFormater;
};


void GunFormater::fillin(const AssetData& data) {

    NodePath viewarms = NodePath(this).find("**/viewarms");
    viewarms.remove_node();

    _data->_class           = get_gun_class(data.get_string("gun_class"));
    _data->_mass            = data.get_float("mass");
    _data->_recoil          = data.get_float("recoil");
    _data->_muzzle_velocity = data.get_float("muzzle_velocity");
    _data->_fire_rate       = data.get_float("fire_rate");
    _data->_fire_modes      = ::get_fire_mode(data.get_string("fire_modes"));
    _data->_ammo_data       = AmmoData::get_ammo_data(data.get_string("ammo"));

    format_anims(data);
}


void GunFormater::format_anims(const AssetData& data) {
    
    NodePath np(this);
    NodePathCollection anims = np.find_all_matches("**/-AnimBundleNode");

    
    map<string, pvector<AnimWriter::Tag>> tags;

    vector_string keys;
    get_tag_keys(keys);

    for (const string& key: keys) {

        size_t pos = key.find_first_of('@');

        if (pos != string::npos) {
            string anim_name = key.substr(pos + 1);
            
            AnimWriter::Tag tag;
            tag._timestamp = data.get_float(key);

            if (key[0] == '#') {
                tag._name = key.substr(1, pos - 1);
                tag._type = AnimWriter::Tag::T_sound;
            } else {
                tag._name = key.substr(0, pos);
                tag._type = AnimWriter::Tag::T_event;
            }
            tags[anim_name].push_back(tag);
        }
    }

    for (size_t i=0; i<anims.size(); ++i) {

        NodePath path = anims.get_path(i);
        AnimBundleNode* node = DCAST(AnimBundleNode, path.node());
        
        string anim_name = node->get_bundle()->get_name();

        pvector<AnimWriter::Tag> tag_datas;

        for (auto& binding: tags) {
            pvector<AnimWriter::Tag>& datas = binding.second;

            if (GlobPattern(binding.first).matches(anim_name))
                tag_datas.insert(tag_datas.end(), datas.begin(), datas.end());
        }
        if (!tag_datas.empty())
            AnimWriter* writer = new AnimWriter(node->get_bundle(), tag_datas);

        _data->_anims.push_back(node->get_bundle());
        path.detach_node();
    }
}


void GunFormater::register_formater() {
    
    TypeHandle type = Gun::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* GunFormater::make(const FactoryParams& params) {
    
    return new GunFormater;
}
