// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/animBundleNode.h>

#include "animSequence.h"
#include "ammoData.h"
#include "gripFormater.h"


using namespace std;


void GripFormater::fillin(const AssetData& data) {

    _data->_recoil_factor = data.get_float("recoil_factor");

    
}

void GripFormater::register_formater() {
    
    TypeHandle type = Grip::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* GripFormater::make(const FactoryParams& params) {
    
    return new GripFormater;
}
