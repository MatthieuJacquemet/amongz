// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/character.h>

#include "attachment.h"
#include "anchorFormater.h"

using namespace std;


void AnchorFormater::register_formater() {
    
    TypeHandle type = AttachmentAnchor::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* AnchorFormater::make(const FactoryParams& params) {
    return new AnchorFormater;
}


void AnchorFormater::fillin(const AssetData& data) {

    if (!has_tag("attachment_type"))
        return;

    TypeRegistry* registry = TypeRegistry::ptr();
    _type = registry->find_type(get_tag("attachment_type"));
    
    string parent_name = data.get_string("parent");
    
    if (parent_name.empty())
        return;

    if (get_num_parents() == 1) {
        PandaNode* parent = get_parent(0);
        
        if (parent->is_of_type(Character::get_class_type())) {
            Character* ch = DCAST(Character, parent);
            CharacterJoint* joint = ch->find_joint(parent_name);

            if (joint != nullptr)
                joint->add_net_transform(this);
            else
                nout << "warning parent was not found" << endl;
        }
    } else if (get_num_parents() > 1)
        nout << "warning : ambiguous parent" << endl;
    else
        nout << "warning : no parent found" << endl;
}