// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/character.h>
#include "hitBoxFormater.h"


using namespace std;


CharacterJoint* find_joint(PandaNode* node, PartGroup* part) {

    if (part->is_of_type(CharacterJoint::get_class_type())) {
        CharacterJoint* joint = DCAST(CharacterJoint, part);
        
        if (joint->has_net_transform(node))
            return joint;
    }

    size_t num_children = part->get_num_children();

    for (size_t i=0; i<num_children; ++i) {
        PartGroup* child = part->get_child(i);

        CharacterJoint* joint = find_joint(node, child);
        if (joint != nullptr)
            return joint;
    }
    return nullptr;
}


void HitBoxFormater::register_formater() {
    
    TypeHandle type = HitBox::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* HitBoxFormater::make(const FactoryParams& params) {

    return new HitBoxFormater;
}


void HitBoxFormater::fillin(const AssetData& data) {

    _damage_factor = data.get_float("damage_factor");

    NodePath np(this);
    NodePath body_np = np.find("**/-BulletRigidBodyNode");

    if (!body_np.is_empty()) {
        BulletRigidBodyNode* body = DCAST(BulletRigidBodyNode, body_np.node());
        nassertv(body->get_num_shapes() > 0)
        add_shape(body->get_shape(0));
    }
    remove_all_children();
}


void HitBoxFormater::parents_changed() {
    // bypass HitBox because it loads the game
    GhostCallback::parents_changed();
}
