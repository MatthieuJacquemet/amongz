// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/bulletWorld.h>
#include <panda3d/bulletRigidBodyNode.h>
#include <panda3d/bulletTriangleMesh.h>
#include <panda3d/bulletTriangleMeshShape.h>
#include <panda3d/bulletGenericConstraint.h>
#include <panda3d/bulletSphereShape.h>

#include "defines.h"
#include "zombie.h"
#include "spawn.h"
#include "mapFormater.h"


using namespace std;


void MapFormater::fillin(const AssetData& data) {


    NodePathCollection objects = find_all_matches("**/+BulletBodyNode");

    for (int i=0; i<objects.get_num_paths(); ++i) {

        NodePath np = objects.get_path(i);
        BulletBodyNode* node = DCAST(BulletBodyNode, np.node());

        if (!node->is_of_type(BulletRigidBodyNode::get_class_type()))
            continue;

        PT(BulletRigidBodyNode) body = DCAST(BulletRigidBodyNode, node);
    

        if (body->get_mass() > 0.0f && body->get_num_parents()) {
            
            
            PandaNode* parent = body->get_parent(0);
            np.detach_node();
            body->replace_node(parent);
            
            BulletShape* shape = body->get_shape(0);
            
            if (!shape->is_of_type(BulletTriangleMeshShape::get_class_type())) {

                BulletTriangleMesh* mesh = new BulletTriangleMesh;

                NodePathCollection geoms_np = np.find_all_matches("**/+GeomNode");

                for (int j=0; j<geoms_np.get_num_paths(); ++j) {
                    NodePath geom_np = geoms_np.get_path(j);

                    GeomNode* geom_node = DCAST(GeomNode, geom_np.node());
                    GeomNode::Geoms geoms = geom_node->get_geoms();
                    
                    for (int k=0; k<geoms.get_num_geoms(); ++k)
                        mesh->add_geom(geoms.get_geom(k));

                }
                BulletTriangleMeshShape* mesh_shape = 
                    new BulletTriangleMeshShape(mesh, true);

                PT(BulletGhostNode) mesh_body = new BulletGhostNode;

                mesh_body->add_shape(mesh_shape);
                mesh_body->set_transform(TransformState::make_identity());
                
                NodePath body_np = np.attach_new_node("body_node");
                body_np.attach_new_node(mesh_body);

                mesh_body->set_into_collide_mask(CollideMask::bit(CM_bullet));
                body->set_into_collide_mask(CollideMask::bit(CM_dynamic));
                continue;
            }
        }

        body->set_into_collide_mask(CollideMask::bit(CM_mesh));
    }
    NodePath::ls();

    NodePathCollection spawns = find_all_matches("**/+Spawn");
    
    for (int i=0; i<spawns.get_num_paths(); ++i) {
        NodePath np = spawns.get_path(i);
        Spawn* spawn = DCAST(Spawn, np.node());

        TypeHandle spawn_type = spawn->get_spawn_type();
        TypeHandle zombie_type = Zombie::get_class_type();
        
        if (spawn_type == zombie_type || spawn_type.is_derived_from(zombie_type))
            _spawns.push_back(spawn);
    }
}


void MapFormater::register_formater() {
    
    TypeHandle type = Map::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}



PandaNode* MapFormater::make(const FactoryParams& params) {
    
    return new MapFormater;
}
