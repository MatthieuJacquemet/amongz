// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/animBundleNode.h>
#include <panda3d/bamFile.h>

#include "zombieFormater.h"

using namespace std;


void ZombieFormater::fillin(const AssetData& data) {

    _data->_mass = data.get_float("mass");
    _data->_damage = data.get_float("damage");
    _data->_max_health = data.get_float("max_health");
    _data->_max_speed = data.get_float("max_speed");

    NodePath np(this);
    NodePathCollection anims = np.find_all_matches("**/-AnimBundleNode");

    string location = AssetManager::get_location(get_class_type());
    Filename file_path(location);
    
    // for (size_t i=0; i<anims.size(); ++i) {
    //     NodePath path = anims.get_path(i);
    //     AnimBundleNode* bundle_node = DCAST(AnimBundleNode, path.node());
    //     string name = bundle_node->get_bundle()->get_name();

    //     file_path.set_basename_wo_extension(name);
    //     file_path.set_extension("bam");

    //     BamFile file;

    //     if (!file.open_write(file_path))
    //         cerr << "could not open file " << file_path << endl;
    //     else {
    //         if (!file.write_object(bundle_node))
    //             cerr << "could not write zombie anim" << endl;

    //         file.close();
    //     }
    //     path.detach_node();
    // }
}


void ZombieFormater::register_formater() {
    
    TypeHandle type = Zombie::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* ZombieFormater::make(const FactoryParams& params) {
    
    return new ZombieFormater;
}
