// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SPAWNFORMATER_H__
#define __SPAWNFORMATER_H__

#include "formater.h"
#include "spawn.h"


class SpawnFormater: private Formater<Spawn> {

public:
    ~SpawnFormater() = default;

    virtual void fillin(const AssetData& data) override;

    static void register_formater();

    virtual TypeHandle get_spawn_type() const override;

private:
    static PandaNode* make(const FactoryParams& params);
    SpawnFormater() = default;
};

#endif // __SPAWNFORMATER_H__