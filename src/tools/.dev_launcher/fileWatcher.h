// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FILEWATCHER_H__
#define __FILEWATCHER_H__

#include <QCoreApplication>
#include <QFileSystemWatcher>
#include "callbacks.h"

#include "main.moc"


class FileWatcher final: public QCoreApplication, private TaskPool {
    
    Q_OBJECT

public:
    FileWatcher(int argc, char* argv[]);
    ~FileWatcher() = default;

    static int exec();

public slots:

    void shaderChanged(QString file);

private:
    AsyncTask::DoneStatus processEvent(AsyncTask* task);
    void addDirectory(const QString& directory);

    QFileSystemWatcher m_watcher;
};


#endif // __FILEWATCHER_H__