// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/virtualFileSystem.h>
#include <panda3d/virtualFileSimple.h>
#include <panda3d/virtualFileMountSystem.h>
#include <panda3d/virtualFileComposite.h>
#include <panda3d/bamFile.h>

#include <QDebug>

#include "fileWatcher.h"
#include "shaderLoader.h"
#include "game.h"

using namespace std;



Filename get_physical_filename(VirtualFile* vfile) {

    Filename filename = vfile->get_filename();

    if (!vfile->is_of_type(VirtualFileSimple::get_class_type())) {
        qDebug() << filename.c_str() << " is not mounted on local filesystem";
        return filename;
    }
    VirtualFileSimple* file = DCAST(VirtualFileSimple, vfile);

    VirtualFileMountSystem* mount = 
        DCAST(VirtualFileMountSystem, file->get_mount());

    const Filename& mount_path = mount->get_physical_filename();
    Filename mount_point = mount->get_mount_point();
    Filename resolved(filename.substr(mount_point.length() + 1));
    
    Filename physical_filename(mount_path, resolved);
    physical_filename.standardize();

    return physical_filename;
}


Filename get_virtual_filename(const Filename& filename) {

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFile) vfile = vfs->get_file(filename);

    int num_mounts = vfs->get_num_mounts();

    for (int i=0; i<num_mounts; ++i) {
        PT(VirtualFileMount) mount = vfs->get_mount(i);

        if (!mount->is_of_type(VirtualFileMountSystem::get_class_type()))
            continue;

        VirtualFileMountSystem* mnt = DCAST(VirtualFileMountSystem, mount);
        Filename physic_filename(mnt->get_physical_filename());
        Filename virtual_filename(filename);
        const Filename& mount_point = mnt->get_mount_point();

        if (virtual_filename.make_relative_to(physic_filename))
            return Filename(mount_point, virtual_filename);;
    }
    return filename;
}


void FileWatcher::addDirectory(const QString& directory) {
    
    Filename filename(directory.toStdString());

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFile) vfile = vfs->get_file(filename);

    PT(VirtualFileList) files = vfile->scan_directory();

    int num_files = files->get_num_files();

    for (int i=0; i<num_files; ++i) {
        VirtualFile* entity = files->get_file(i);

        if (!entity->is_regular_file())
            continue;

        Filename fullpath = get_physical_filename(entity);

        if (!fullpath.exists())
            continue;

        string os_specific = fullpath.to_os_specific();

        if (!m_watcher.addPath(QString::fromStdString(os_specific)))
            nout << os_specific << " not watched" << endl;
    }
}


int FileWatcher::exec() {

    ApplicationRunner runner(Game::get_global_ptr());
    return runner.exec();
}



void FileWatcher::shaderChanged(QString file) {

    string path = file.toStdString();
    Filename filename = Filename::from_os_specific(path);
    filename = get_virtual_filename(filename);

    ShaderLoader::update(filename);
}


AsyncTask::DoneStatus FileWatcher::processEvent(AsyncTask* task) {
    QCoreApplication::processEvents();

    return AsyncTask::DS_cont;
}


 FileWatcher::FileWatcher(int argc, char* argv[]):
        QCoreApplication(argc, argv)
{
    do_add_task(named_method(processEvent));

    
    // addDirectory(SHADER_PATH);
    // addDirectory(GDK_SHADER_PATH);

    QObject::connect(&m_watcher, &QFileSystemWatcher::fileChanged, 
                    this, &FileWatcher::shaderChanged);
}

